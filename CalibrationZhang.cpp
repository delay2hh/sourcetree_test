// CalibrationZhang.cpp : 定义控制台应用程序的入口点。
//gittest_edit

#include "stdafx.h"
//#define EIGEN_USE_MKL_ALL
//#define EIGEN_VECTORIZE_SSE4_2

#include <iostream>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/SVD>
#include <Eigen/LU>
#include <time.h>
#include <math.h>
//#include "mkl.h"

using namespace std;
using namespace Eigen;

typedef VectorXd(*Errfunc)(VectorXd, MatrixXd, MatrixXd);

VectorXd findH(MatrixXd pntI, MatrixXd pntW);
VectorXd findB(MatrixXd H);
Matrix3d findA(VectorXd B);
MatrixXd findRt(VectorXd H, Matrix3d A);
Vector2d findk(Matrix3d A, MatrixXd Rt_ALL, MatrixXd pntI_ALL, MatrixXd pntW);
MatrixXd euler2matRt(VectorXd q);
VectorXd matRt2euler(MatrixXd q);
VectorXd ErrOfCalibration(VectorXd coes, MatrixXd pntI_ALL, MatrixXd pntW_ALL);
MatrixXd JacMatDisc(Errfunc FuncsMuilt, VectorXd x, MatrixXd outData, MatrixXd inData);
VectorXd LevenbergMarquardtMethod(Errfunc FuncsMuilt, VectorXd x0, MatrixXd otData, MatrixXd inData, int MaxIt, double Tol);
VectorXd ZhangsCalibration(MatrixXd pntI_ALL, MatrixXd pntW_ALL);



int main(int argc, _TCHAR* argv[])
{
	MatrixXd pntI_ALL = MatrixXd::Zero(48, 3), pntW_ALL = MatrixXd::Zero(48, 3);
	pntI_ALL <<
		2.009279174804687500e+02, 1.295617675781250000e+02, 0,
		2.015600891113281250e+02, 1.037897186279296875e+02, 0,
		2.011521453857421875e+02, 7.767218017578125000e+01, 0,
		1.997951049804687500e+02, 5.270306777954101562e+01, 0,
		2.240385894775390625e+02, 1.303019256591796875e+02, 0,
		2.252841644287109375e+02, 1.044969100952148438e+02, 0,
		2.250080261230468750e+02, 7.839810180664062500e+01, 0,
		2.233220977783203125e+02, 5.329862213134765625e+01, 0,
		2.457568359375000000e+02, 1.306747436523437500e+02, 0,
		2.474377441406250000e+02, 1.054234695434570312e+02, 0,
		2.473694915771484375e+02, 7.951490020751953125e+01, 0,
		2.454009552001953125e+02, 5.461135101318359375e+01, 0,
		2.655725097656250000e+02, 1.315156402587890625e+02, 0,
		2.676844482421875000e+02, 1.064538879394531250e+02, 0,
		2.677202148437500000e+02, 8.121423339843750000e+01, 0,
		2.654496765136718750e+02, 5.679212188720703125e+01, 0,
		2.832312927246093750e+02, 1.317946472167968750e+02, 0,
		2.853179321289062500e+02, 1.077239685058593750e+02, 0,
		2.852906188964843750e+02, 8.345761108398437500e+01, 0,
		2.832466735839843750e+02, 5.962955474853515625e+01, 0,
		2.984426879882812500e+02, 1.323807678222656250e+02, 0,
		3.004562072753906250e+02, 1.092427062988281250e+02, 0,
		3.004790649414062500e+02, 8.582289123535156250e+01, 0,
		2.984232788085937500e+02, 6.296356201171875000e+01, 0,
		1.528467102050781250e+02, 2.081896057128906250e+02, 1,
		1.544591217041015625e+02, 1.861292266845703125e+02, 1,
		1.563500518798828125e+02, 1.597178649902343750e+02, 1,
		1.585348968505859375e+02, 1.303403167724609375e+02, 1,
		1.772991790771484375e+02, 2.122018432617187500e+02, 1,
		1.805383758544921875e+02, 1.898407135009765625e+02, 1,
		1.841269226074218750e+02, 1.627920227050781250e+02, 1,
		1.869928283691406250e+02, 1.324864654541015625e+02, 1,
		2.018745880126953125e+02, 2.146640777587890625e+02, 1,
		2.072704010009765625e+02, 1.925870056152343750e+02, 1,
		2.119842376708984375e+02, 1.654556732177734375e+02, 1,
		2.157182006835937500e+02, 1.346971130371093750e+02, 1,
		2.256804199218750000e+02, 2.160828704833984375e+02, 1,
		2.326327209472656250e+02, 1.936821289062500000e+02, 1,
		2.386364898681640625e+02, 1.667357330322265625e+02, 1,
		2.428024139404296875e+02, 1.364599151611328125e+02, 1,
		2.477094726562500000e+02, 2.155753784179687500e+02, 1,
		2.558023071289062500e+02, 1.935392150878906250e+02, 1,
		2.624679565429687500e+02, 1.674854125976562500e+02, 1,
		2.671234130859375000e+02, 1.382072143554687500e+02, 1,
		2.671699218750000000e+02, 2.141099700927734375e+02, 1,
		2.756994934082031250e+02, 1.926130523681640625e+02, 1,
		2.827395324707031250e+02, 1.674270172119140625e+02, 1,
		2.875419616699218750e+02, 1.393380584716796875e+02, 1;
	pntW_ALL <<
		0.000000000000000000e+00, 0.000000000000000000e+00, 0,
		1.000000000000000000e+00, 0.000000000000000000e+00, 0,
		2.000000000000000000e+00, 0.000000000000000000e+00, 0,
		3.000000000000000000e+00, 0.000000000000000000e+00, 0,
		0.000000000000000000e+00, 1.000000000000000000e+00, 0,
		1.000000000000000000e+00, 1.000000000000000000e+00, 0,
		2.000000000000000000e+00, 1.000000000000000000e+00, 0,
		3.000000000000000000e+00, 1.000000000000000000e+00, 0,
		0.000000000000000000e+00, 2.000000000000000000e+00, 0,
		1.000000000000000000e+00, 2.000000000000000000e+00, 0,
		2.000000000000000000e+00, 2.000000000000000000e+00, 0,
		3.000000000000000000e+00, 2.000000000000000000e+00, 0,
		0.000000000000000000e+00, 3.000000000000000000e+00, 0,
		1.000000000000000000e+00, 3.000000000000000000e+00, 0,
		2.000000000000000000e+00, 3.000000000000000000e+00, 0,
		3.000000000000000000e+00, 3.000000000000000000e+00, 0,
		0.000000000000000000e+00, 4.000000000000000000e+00, 0,
		1.000000000000000000e+00, 4.000000000000000000e+00, 0,
		2.000000000000000000e+00, 4.000000000000000000e+00, 0,
		3.000000000000000000e+00, 4.000000000000000000e+00, 0,
		0.000000000000000000e+00, 5.000000000000000000e+00, 0,
		1.000000000000000000e+00, 5.000000000000000000e+00, 0,
		2.000000000000000000e+00, 5.000000000000000000e+00, 0,
		3.000000000000000000e+00, 5.000000000000000000e+00, 0,
		0.000000000000000000e+00, 0.000000000000000000e+00, 1,
		1.000000000000000000e+00, 0.000000000000000000e+00, 1,
		2.000000000000000000e+00, 0.000000000000000000e+00, 1,
		3.000000000000000000e+00, 0.000000000000000000e+00, 1,
		0.000000000000000000e+00, 1.000000000000000000e+00, 1,
		1.000000000000000000e+00, 1.000000000000000000e+00, 1,
		2.000000000000000000e+00, 1.000000000000000000e+00, 1,
		3.000000000000000000e+00, 1.000000000000000000e+00, 1,
		0.000000000000000000e+00, 2.000000000000000000e+00, 1,
		1.000000000000000000e+00, 2.000000000000000000e+00, 1,
		2.000000000000000000e+00, 2.000000000000000000e+00, 1,
		3.000000000000000000e+00, 2.000000000000000000e+00, 1,
		0.000000000000000000e+00, 3.000000000000000000e+00, 1,
		1.000000000000000000e+00, 3.000000000000000000e+00, 1,
		2.000000000000000000e+00, 3.000000000000000000e+00, 1,
		3.000000000000000000e+00, 3.000000000000000000e+00, 1,
		0.000000000000000000e+00, 4.000000000000000000e+00, 1,
		1.000000000000000000e+00, 4.000000000000000000e+00, 1,
		2.000000000000000000e+00, 4.000000000000000000e+00, 1,
		3.000000000000000000e+00, 4.000000000000000000e+00, 1,
		0.000000000000000000e+00, 5.000000000000000000e+00, 1,
		1.000000000000000000e+00, 5.000000000000000000e+00, 1,
		2.000000000000000000e+00, 5.000000000000000000e+00, 1,
		3.000000000000000000e+00, 5.000000000000000000e+00, 1;

	VectorXd c;
	c = ZhangsCalibration( pntI_ALL, pntW_ALL);
	//****************************************************************************
	// Display the solution: 
	Matrix3d A = Matrix3d::Zero(3, 3);
	A(0, 0) = c(0); A(1, 1) = c(1); A(0, 2) = c(2); A(1, 2) = c(3); A(2, 2) = 1;
	double k1 = c(4), k2 = c(5);
	printf("==================================================================\n");
	printf("The internal reference matrix A is: \n");
	cout << A << endl;
	printf("\n The distortion coefficients : \n k1 = %f \n k2 = %f \n",k1,k2);
	int numFrame = ((int)c.size() - 6) / 6;
	VectorXd q;
	MatrixXd Rt;
	for (int i = 0; i < numFrame; ++i)
	{
		q = c.segment(i * 6 + 6, 6);
		Rt = euler2matRt(q);
		printf("\n The %i -th picture's outer reference matrix Rt is: \n",i+1);
		cout << Rt << endl;
	}
	printf("==================================================================\n");
	//****************************************************************************
	system("pause");
	return 1;
}

// Get the matrix H in Zhang's paper
VectorXd findH(MatrixXd pntI, MatrixXd pntW)
{
	int numPoints = (int)pntW.rows();
	MatrixXd matrixForH = MatrixXd::Zero(2 * numPoints, 8);
	VectorXd vectorForH = VectorXd::Zero(2 * numPoints);
	double xw, yw, u, v;
	VectorXd Hvec;
	for (int i = 0; i < numPoints; ++i)
	{
		xw = pntW(i, 0);
		yw = pntW(i, 1);
		u = pntI(i, 0);
		v = pntI(i, 1);
		matrixForH.row(2 * i) << xw, yw, 1.0, 0.0, 0.0, 0.0, -u*xw, -u*yw;
		matrixForH.row(2 * i + 1) << 0.0, 0.0, 0.0, xw, yw, 1.0, -v*xw, -v*yw;
		vectorForH(2 * i) = u;
		vectorForH(2 * i + 1) = v;
	}
	// form the LA equation
	MatrixXd A = matrixForH.transpose()*matrixForH;
	VectorXd b = matrixForH.transpose()*vectorForH;
	Hvec = A.lu().solve(b);
	return Hvec;
}

// Get the matrix B in Zhang's paper
VectorXd findB(MatrixXd H)
{
	int numFrame = (int)H.rows();
	MatrixXd matrixForB = MatrixXd::Zero(3 * numFrame, 6);
	VectorXd B;
	double h11, h12, h13, h21, h22, h23, h31, h32;
	for (int i = 0; i < numFrame; ++i)
	{
		h11 = H(i, 0); h12 = H(i, 1); h13 = H(i, 2); h21 = H(i, 3);
		h22 = H(i, 4); h23 = H(i, 5); h31 = H(i, 6); h32 = H(i, 7);
		matrixForB.row(3 * i) << h11*h12, h11*h22 + h21*h12, h11*h32 + h31*h12,
			h21*h22, h21*h32 + h31*h22, h31*h32;
		matrixForB.row(3 * i + 1) << h11*h11 - h12*h12, 2 * h11*h21 - 2 * h12*h22,
			2 * h11*h31 - 2 * h12*h32, h21*h21 - h22*h22,
			2 * h21*h31 - 2 * h22*h32, h31*h31 - h32*h32;
		matrixForB.row(3 * i + 2) << 0.0, 10000.0, 0.0, 0.0, 0.0, 0.0;
	}
	MatrixXd A = matrixForB.transpose()*matrixForB;
	JacobiSVD<Eigen::MatrixXd> svd(A, ComputeThinU | ComputeThinV);
	MatrixXd U = svd.matrixU();
	B = U.col(5);
	return B;
}

// Get the matrix A in Zhang's paper
Matrix3d findA(VectorXd B)
{
	double B11, B12, B13, B22, B23, B33;
	B11 = B(0); B12 = B(1); B13 = B(2);
	B22 = B(3); B23 = B(4); B33 = B(5);
	double v0 = (B12*B13 - B11*B23) / (B11*B22 - B12*B12);
	double c0 = B33 - (B13*B13 + v0*(B12*B13 - B11*B23)) / B11;
	double alpha = sqrt(c0 / B11);
	double beta = sqrt(c0*B11 / (B11*B22 - B12*B12));
	double gamma = -B12*alpha*alpha*beta / c0;
	double u0 = gamma*v0 / beta - B13*alpha*alpha / c0;
	Matrix3d A;
	A << alpha, gamma, u0, 0.0, beta, v0, 0.0, 0.0, 1.0;
	return A;
}

// Get the matrix Rt for each Frame
MatrixXd findRt(VectorXd H, Matrix3d A)
{
	MatrixXd Rt = MatrixXd::Zero(3, 4);
	Vector3d h1, h2, h3;
	h1 << H(0), H(3), H(6);
	h2 << H(1), H(4), H(7);
	h3 << H(2), H(5), 1.0;
	Matrix3d Ainv = A.inverse();

	Vector3d r1C = Ainv*h1;
	r1C = r1C / r1C.norm();
	Vector3d r2C = Ainv*h2;
	r2C = r2C / r2C.norm();
	Vector3d r3C = r1C.cross(r2C);
	r3C = r3C / r3C.norm();

	Matrix3d Rc;
	Rc.col(0) = r1C;
	Rc.col(1) = r2C;
	Rc.col(2) = r3C;

	JacobiSVD<Eigen::MatrixXd> svd(Rc, ComputeThinU | ComputeThinV);
	Matrix3d U = svd.matrixU(), V = svd.matrixV();

	Matrix3d R = U*V.transpose();

	Vector3d temp1, temp2;
	temp1 = Ainv*h1;
	temp2 = Ainv*h2;
	double lamda1 = 1.0 / temp1.norm();
	double lamda2 = 1.0 / temp2.norm();
	printf("....The Error between two lamda is %f....\n", lamda1 - lamda2);
	double lamda = (lamda1 + lamda2) / 2.0;
	Vector3d t = lamda*Ainv*h3;

	Rt.leftCols<3>() = R;
	Rt.col(3) = t;
	return Rt;
}

// find the coes k1,k2,k3
Vector2d findk(Matrix3d A, MatrixXd Rt_ALL, MatrixXd pntI_ALL, MatrixXd pntW_ALL)
{
	int numFrame = (int)Rt_ALL.rows() / 3;
	double u0 = A(0, 2), v0 = A(1, 2);
	//..........................................................................
	// get the matching nodes's number of each Picture
	int numNode = (int)pntI_ALL.rows();
	VectorXi numNodeVec = VectorXi::Zero(numFrame);
	for (int i = 0; i < numNode; ++i)
	{
		numNodeVec((int)pntI_ALL(i, 2)) += 1;
	}
	//..........................................................................
	MatrixXd K1 = MatrixXd::Zero(numNode, 2),
		K2 = MatrixXd::Zero(numNode, 2);
	VectorXd f1 = VectorXd::Zero(numNode),
		f2 = VectorXd::Zero(numNode);
	MatrixXd Rt = MatrixXd::Zero(3, 4);
	int nodeId = 0;

	MatrixXd pntW_col, pntI, pntC_col, pntC;
	VectorXd x, y, u, v, ur, vr, u_u0, v_v0, rSqure, rForth;
	for (int i = 0; i < numFrame; ++i)
	{
		Rt = Rt_ALL.middleRows<3>(3 * i);
		// get pnt_I pnt_W pnt_C-------------------
		pntW_col = MatrixXd::Ones(4, numNodeVec(i));
		pntI = MatrixXd::Ones(numNodeVec(i), 2);
		pntC_col = MatrixXd::Ones(3, numNodeVec(i));
		pntC = MatrixXd::Ones(numNodeVec(i), 3);
		pntW_col.topRows(2) = pntW_ALL.block(nodeId, 0, numNodeVec(i), 2).transpose();
		pntW_col.row(2) *= 0;

		pntI = pntI_ALL.block(nodeId, 0, numNodeVec(i), 2);

		pntC_col = Rt*pntW_col;
		pntC = pntC_col.transpose();

		x = pntC.col(0).cwiseQuotient(pntC.col(2));
		y = pntC.col(1).cwiseQuotient(pntC.col(2));

		pntC_col = A*pntC_col;
		pntC = pntC_col.transpose();

		u = pntC.col(0).cwiseQuotient(pntC.col(2));
		v = pntC.col(1).cwiseQuotient(pntC.col(2));
		ur = pntI.col(0); vr = pntI.col(1);

		u_u0 = u.array() - u0;
		v_v0 = v.array() - v0;

		rSqure = x.array().pow(2) + y.array().pow(2);
		K1.block(nodeId, 0, numNodeVec(i), 1) = u_u0.cwiseProduct(rSqure);
		K2.block(nodeId, 0, numNodeVec(i), 1) = v_v0.cwiseProduct(rSqure);
		f1.segment(nodeId, numNodeVec(i)) = ur - u;
		f2.segment(nodeId, numNodeVec(i)) = vr - v;

		rForth = rSqure.array().pow(2);
		K1.block(nodeId, 1, numNodeVec(i), 1) = u_u0.cwiseProduct(rForth);
		K2.block(nodeId, 1, numNodeVec(i), 1) = v_v0.cwiseProduct(rForth);
		
		nodeId += (int)numNodeVec(i);
	}
	MatrixXd K = MatrixXd::Zero(2 * numNode, 2);
	K.topRows(numNode) = K1;
	K.bottomRows(numNode) = K2;
	VectorXd f = VectorXd::Zero(2 * numNode);
	f.head(numNode) = f1;
	f.tail(numNode) = f2;
	MatrixXd A0 = K.transpose()*K;
	VectorXd b0 = K.transpose()*f;
	VectorXd ks = VectorXd::Zero(2);
	ks = A0.lu().solve(b0);
	Vector2d k;
	k.head(2) = ks;
	return k;
}

// euler angle and displacement(1*6) to 3x4 Rotation matrix
MatrixXd euler2matRt(VectorXd q)
{
	Matrix3d R3x3;
	MatrixXd Rt = MatrixXd::Zero(3, 4);
	Vector3d  t3x1;
	double thetaX = q(0), thetaY = q(1), thetaZ = q(2);
	double sx = sin(thetaX), cx = cos(thetaX),
		sy = sin(thetaY), cy = cos(thetaY),
		sz = sin(thetaZ), cz = cos(thetaZ);
	R3x3 << cy*cz, cz*sx*sy - cx*sz, sx*sz + cx*cz*sy,
		cy*sz, cx*cz + sx*sy*sz, cx*sy*sz - cz*sx,
		-sy, cy*sx, cx*cy;
	t3x1 = q.tail(3);
	Rt.leftCols(3) = R3x3;
	Rt.rightCols(1) = t3x1;
	return Rt;
}

// 3x4 Rotation matrix to euler angle and displacement(1*6)
VectorXd matRt2euler(MatrixXd Rt)
{
	VectorXd q = VectorXd::Zero(6);
	q.tail(3) = Rt.rightCols(1);
	Matrix3d R3x3 = Rt.leftCols(3);
	q(0) = atan2(R3x3(2, 1), R3x3(2, 2));
	q(1) = atan2(-R3x3(2, 0), sqrt(R3x3(2, 1)*R3x3(2, 1) + R3x3(2, 2)*R3x3(2, 2)));
	q(2) = atan2(R3x3(1, 0), R3x3(0, 0));
	return q;
}

//Caculate the calibration error
VectorXd ErrOfCalibration(VectorXd coes, MatrixXd pntI_ALL, MatrixXd pntW_ALL)
{
	Matrix3d A;
	A << coes(0), 0.0, coes(2), 0.0, coes(1), coes(3), 0.0, 0.0, 1.0;
	double k1 = coes(4), k2 = coes(5);
	double u0 = A(0, 2), v0 = A(1, 2);
	int numFrame = ((int)coes.size() - 6) / 6;
	//..........................................................................
	// get the matching nodes's number of each Picture
	int numNode = (int)pntI_ALL.rows();
	VectorXi numNodeVec = VectorXi::Zero(numFrame);
	VectorXd frameMark = pntI_ALL.col(2);
	for (int i = 0; i < numNode; ++i)
	{
		numNodeVec((int)frameMark(i)) += 1;
	}
	//..........................................................................
	VectorXd errsU = VectorXd::Zero(numNode),
		errsV = VectorXd::Zero(numNode);
	int nodeId = 0;
	VectorXd q = VectorXd::Zero(6);
	MatrixXd Rt = MatrixXd::Zero(3, 4);
	// Definition
	MatrixXd pntW_col, pntI, pntC_col, pntC;
	VectorXd x, y, u, v, ur, vr, u_u0, v_v0, rSqure, rForth, rSixth, change, u_cal, v_cal;
	for (int i = 0; i < numFrame; ++i)
	{
		q = coes.segment(i * 6 + 6, 6);
		Rt = euler2matRt(q);
		// get pnt_I pnt_W pnt_C-------------------
		pntW_col = MatrixXd::Ones(4, numNodeVec(i));
		pntI = MatrixXd::Ones(numNodeVec(i), 2);
		pntC_col = MatrixXd::Ones(3, numNodeVec(i));
		pntC = MatrixXd::Ones(numNodeVec(i), 3);
		pntW_col.topRows(2) = pntW_ALL.block(nodeId, 0, numNodeVec(i), 2).transpose();
		pntW_col.row(2) *= 0;
		pntI = pntI_ALL.block(nodeId, 0, numNodeVec(i), 2);

		pntC_col = Rt*pntW_col;
		pntC = pntC_col.transpose();

		x = pntC.col(0).cwiseQuotient(pntC.col(2));
		y = pntC.col(1).cwiseQuotient(pntC.col(2));

		pntC_col = A*pntC_col;
		pntC = pntC_col.transpose();

		u = pntC.col(0).cwiseQuotient(pntC.col(2));
		v = pntC.col(1).cwiseQuotient(pntC.col(2));
		ur = pntI.col(0); vr = pntI.col(1);

		u_u0 = u.array() - u0;
		v_v0 = v.array() - v0;

		rSqure = x.array().pow(2) + y.array().pow(2);
		rForth = rSqure.array().pow(2);
		//rSixth = rSqure.array().pow(3);

		change = k1*rSqure + k2*rForth;// +k3*rSixth;
		u_cal = u + u_u0.cwiseProduct(change);
		v_cal = v + v_v0.cwiseProduct(change);
		errsU.segment(nodeId, numNodeVec(i)) = u_cal - ur;
		errsV.segment(nodeId, numNodeVec(i)) = v_cal - vr;

		nodeId += (int)numNodeVec(i);
	}
	VectorXd errs = VectorXd::Zero(2 * numNode);
	errs.head(numNode) = errsU;
	errs.tail(numNode) = errsV;

	return errs;
}

// COMPUTE THE DISCRETE DERIVATIVE OF A "BLACK-BOX" FUNCTION FuncsMuilt.m
// INPUT:
//       FuncsMuilt : @FuncsMuilt the black - box function of error of fitting
//       x         : independent variable of FuncsMuilt, fitting parameters
//       outData : for non - linear data fitting
//       inData : for non - linear data fitting
//                   Example : FuncsMuilt = outData - exp(x*inData)
// OUTPUT :
//        J : the Jacobi matrix J_ij = \partial{ f_i } / \partial{ x_i }
// Edit by Huashan Sheng, UEG MEDICAL, Nov. 11 2016

MatrixXd JacMatDisc(Errfunc FuncsMuilt, VectorXd x, MatrixXd outData, MatrixXd inData)
{
	VectorXd f = FuncsMuilt(x, outData, inData);
	VectorXd x_pls, x_mns;
	int rows = (int)f.size();
	int cols = (int)x.size();
	MatrixXd J(rows, cols);
	J.setZero(rows, cols);
	for (int i = 0; i < cols; ++i)
	{
		x_pls = x;
		x_mns = x;
		x_pls(i) = x_pls(i) + 1e-3*max(abs(x(i)), 1.0);
		x_mns(i) = x_mns(i) - 1e-3*max(abs(x(i)), 1.0);
		J.col(i) = (FuncsMuilt(x_pls, outData, inData) - FuncsMuilt(x_mns, outData, inData)) / (x_pls(i) - x_mns(i));
	}
	return J;
}

// LEVENBERG MARQUARDT METHOD fOR NON - LINEAR DATA FITTING
// INPUT:
//       FuncsMuilt : @FuncsMuilt the black - box function of error of fitting
//       x0        : initil guess of FuncsMuilt's variable, fitting parameters
//       otData : for data fitting, output var.of the fitting func.
//       inData : for data fitting, input var.of the fitting func.
//                   Example : FuncsMuilt = otData - exp(lbd*inData)
// fitting func. : b = exp(lbd*a)
// OUTPUT :
//        x_tml : the best parameters for minization || FuncsMuilt || _2
// Edit by Huashan Sheng, UEG MEDICAL, Nov. 11 2016

VectorXd LevenbergMarquardtMethod(Errfunc FuncsMuilt, VectorXd x0, MatrixXd otData, MatrixXd inData, int MaxIt, double Tol)
{
	bool FINDIT = false;            // flag for stop the loop
	int  k = 0;                     // iteration step number
	double nu = 2.0;                // parameter for adjust the stepsize
	double tau = 1.0;               // parameter set by user (myself?)

	VectorXd x_k = x0;              // initilize
	VectorXd f_k = FuncsMuilt(x_k, otData, inData);                 // get the fitting errs.
	MatrixXd J_k = JacMatDisc(FuncsMuilt, x_k, otData, inData);     // get the Jacobi Matrix
	MatrixXd A = J_k.transpose()*J_k;
	VectorXd g = J_k.transpose()*f_k;
	double mu = tau*A.diagonal().maxCoeff();

	MatrixXd Ident;
	MatrixXd A_mu;
	VectorXd hlm, x_new, f_old, f_new, x_tml;
	double rho;
	Ident.setIdentity(A.rows(), A.cols());

	if (g.cwiseAbs().maxCoeff() < Tol)
	{
		FINDIT = true;                                              // initial guess good enough
	}

	while (k < MaxIt && !FINDIT)
	{
		k = k + 1;
		//......................................................................
		printf(".......The %d -th iteration...... \n", k);
		//......................................................................
		A_mu = A + mu*Ident;  hlm = A_mu.lu().solve(-g);                        // test a direction
		//......................................................................
		printf(".............NORM OF ||hlm|| = %f......\n ", hlm.norm());
		if (hlm.norm() <= Tol*(x_k.norm() + Tol))
		{
			FINDIT = true;                                                     // no way to go
		}
		else
		{
			x_new = x_k + hlm;                                                 // try a new position
			f_old = FuncsMuilt(x_k, otData, inData);
			f_new = FuncsMuilt(x_new, otData, inData);
			rho = (f_old.dot(f_old) - f_new.dot(f_new)) / (hlm.dot(mu*hlm - g));     // evaluate
			if (rho>0)                                                         // accept
			{
				x_k = x_new;                                                   // overwrite the point
				f_k = FuncsMuilt(x_k, otData, inData);
				J_k = JacMatDisc(FuncsMuilt, x_k, otData, inData);
				A = J_k.transpose()*J_k;
				g = J_k.transpose()*f_k;
				if (g.cwiseAbs().maxCoeff() < Tol)
				{
					FINDIT = true;
				}
				mu = mu*max(1 / 3.0, 1 - pow(2 * rho - 1, 3));
				nu = 2;
			}
			else
			{
				mu = mu*nu;
				nu = 2 * nu;
			}
		}
		//......................................................................
		printf(".............NORM OF ||g||_infty = %f......\n", g.cwiseAbs().maxCoeff());
		//......................................................................
	}
	x_tml = x_k;
	return x_tml;
}


//****************************************************************
// Total part of Zhang's Calibration
//****************************************************************
VectorXd ZhangsCalibration(MatrixXd pntI_ALL, MatrixXd pntW_ALL)
{
    // get the Frame(Picture) number
	VectorXd frameMark = pntI_ALL.col(2);

	int numFrame = (int)frameMark.maxCoeff() + 1;
	//..........................................................................
	// get the matching nodes's number of each Picture

	int numNode = (int)pntI_ALL.rows();
	VectorXi numNodeVec = VectorXi::Zero(numFrame);

	for (int i = 0; i < numNode; ++i)
	{
		numNodeVec((int)frameMark(i)) += 1;
	}
	//..........................................................................
	int nodeId = 0;
	VectorXd Hvec = VectorXd::Zero(8);
	MatrixXd H = MatrixXd::Zero(numFrame, 8);
	MatrixXd pntW, pntI;
	for (int i = 0; i < numFrame; ++i)
	{
		// get pnt_I pnt_W pnt_C-------------------
		pntW = MatrixXd::Ones(numNodeVec(i), 2);
		pntI = MatrixXd::Ones(numNodeVec(i), 2);
		pntW = pntW_ALL.block(nodeId, 0, numNodeVec(i), 2);
		pntI = pntI_ALL.block(nodeId, 0, numNodeVec(i), 2);
		Hvec = findH(pntI, pntW);
		H.row(i) = Hvec;

		nodeId += numNodeVec(i);
	}
	VectorXd B = VectorXd::Zero(6);
	B = findB(H);
	Matrix3d A;
	A = findA(B);

	MatrixXd Rt = MatrixXd::Zero(3, 4);
	MatrixXd Rt_ALL = MatrixXd::Zero(3 * numFrame, 4);
	for (int i = 0; i < numFrame; ++i)
	{
		Hvec = H.row(i);
		Rt = findRt(Hvec, A);
		Rt_ALL.middleRows(3 * i, 3) = Rt;
	}
	Vector2d k;
	k = findk(A, Rt_ALL, pntI_ALL, pntW_ALL);
	double k1 = k(0), k2 = k(1);


	VectorXd coeInitial = VectorXd::Zero(numFrame * 6 + 6);
	coeInitial.head(6) << A(0, 0), A(1, 1), A(0, 2), A(1, 2), k1, k2;
	for (int i = 0; i < numFrame; ++i)
	{
		Rt = Rt_ALL.middleRows(3 * i, 3);
		coeInitial.segment(6 + 6 * i, 6) = matRt2euler(Rt);
	}
	VectorXd errInitial = ErrOfCalibration(coeInitial, pntI_ALL, pntW_ALL);
	printf("The errors of the initial state is : %f \n", errInitial.norm());

	VectorXd coeTerminal = VectorXd::Zero(numFrame * 6 + 6);
	coeTerminal = LevenbergMarquardtMethod(ErrOfCalibration, coeInitial, pntI_ALL, pntW_ALL, 1000, 1e-14);

	VectorXd errTerminal = ErrOfCalibration(coeTerminal, pntI_ALL, pntW_ALL);
	printf("The errors of the terminal state is : %f \n", errTerminal.norm());

	return coeTerminal;
}
