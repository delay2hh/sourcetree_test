#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "RS232Ndi.h"

class QAction;
class QMenu;
class QPushButton;
class QTreeWidget;
class QLineEdit;
class QSettings;
class QTimer;

class KneeMotionObserver;
class EndoGadget;

#include "auxdatastructure.h"

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow();

	void selectAVerDevice(unsigned int dev); // 0 - SD; 1 - HD

protected:
	QTimer *refreshTimer;
	
	bool  m_bIsTracking;
	RS232Ndi m_ndiSystem;
	bool  m_bResetHardware;		// reset hardware variable 
	bool  m_bWireless;			// uses the wireless compatible settings
	bool  m_bSystemInitialized;	// is the system initialized
	bool  m_bPortsActivated;	// are ports activated
	int   m_nCOMPort;			// the current com port number

private slots:
	// menu slots
	void comPortSettings();
	void sromImageFileSettings();
	void about();
	// button slots
	void resetSystem();
	void initializeSystem();
	void activateHandles();
	void tracking();
	void closeSystem();
	// timer slots
	void on_timer_refreshFrame();

	// for extended functions
	//void observeKneeMotion();
	void updateTestPointList();
	void clearTestPointList();

private:
	void createINI();
	void createActions();
	void createMenus();
	void createUserInterface();
	void updateTrackingInfo();
	void updateKneeMotionObserver();

	void startTracking();
    void stopTracking();

	// menus
	QMenu *fileMenu;
	QMenu *settingsMenu;
	QMenu *systemMenu;
	QMenu *viewMenu;
	QMenu *optionMenu;
	QMenu *helpMenu;

	// file menu item(s)
	QAction *exitAct;
	// settings menu item(s)
	QAction *comPortSettingsAct;
	QAction *sromImageFileSettingsAct;
	// system menu items(s)
	QAction *initializeSystemAct;
	QAction *activateHandlesAct;
	QAction *startTrackingAct;
	QAction *stopTrackingAct;
	// view menu item(s) 
	QAction *systemPropertiesAct;
	QAction *diagnosticAlertsAct;
	// option menu item(s)
	QAction *illuminatorActivationRateAct;
	QAction *programOptionsAct;
	// help menu item(s)
	QAction *aboutAct;

	// buttons
	QPushButton *resetSystemButton;
	QPushButton *initializeSystemButton;
	QPushButton *activateHandlesButton;
	QPushButton *trackingButton;
	QPushButton *closeSystemButton;

	// lists
	QTreeWidget *transformInQuaternionList;
	QTreeWidget *transformInEulerAngleList;

	// edits
	QLineEdit *frameEdit;
	QLineEdit *modeEdit;

	// for ini file
	QSettings *settingsINI; 

	// for extended functions
	//QPushButton* observeKneeMotionButton;
	KneeMotionObserver *kneeMotionObserver;
	bool kneeMotionObserverVisible;
	//HandleData handleDataForKnee[4];  // to observe the knee motion, 4 handles(tools) are needed 
	HandleData handleDataForKnee[NUM_ENDO_TOOL];  // to implement the endoscopy augment reality, NUM_ENDO_TOOL(3) handles(tools) are needed 

	// for aver
	EndoGadget *m_endoGadget;
};

#endif