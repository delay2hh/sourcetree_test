//@Yeming,Usage:
//probe method can be used in combination with Ascension device for automatic device 
//installation purpose, m_serialPort could be designated or probed on your will.
//loadVirtualSROM--tell the NDI system where to find the SROM FILE 
//                 for specific Port;
// m_baudRate for new system could be rasied to 115200;
//qDebug is for debugging purpose;
//If wireless tool is used, port number begins from 4.


#include "RS232Ndi.h"
// #include "../ndi/include/ndicapi_math.h"
#include "ndicapi/ndicapi_math.h"
#include <QDebug>
#include <QStringList>
#include <iostream>

RS232Ndi::RS232Ndi()
{
	m_pDevice = 0;
	m_commandReply[0] = '\0';
	m_serialPort = -1; // default is to probe
	m_baudRate = 115200;
	m_isDeviceTracking = false;

	for (int i = 0; i < NDI_NTOOLS; i++)
	{
		m_portHandle[i] = -1;
		m_portEnabled[i] = -1;
		m_pVirtualSROM[i] = 0;
		m_handleToPort[i] = -1;
	}

	// banbanfish
	m_activePortNum = 0;
    m_passivePortNum = 0;

}

RS232Ndi::~RS232Ndi()
{

	if(m_isDeviceTracking)
	{
		stopTracking();
	}
	for (int i = 0; i < NDI_NTOOLS; i++)
	{
		if (m_pVirtualSROM[i] != 0)
		{
			delete [] m_pVirtualSROM[i];
		}
	}

}

bool RS232Ndi::probe()
{

	int errnum = NDI_OPEN_ERROR;
	if (m_isDeviceTracking)
	{
		return 1;
	}

	if (m_serialPort < 0)
	{
		for (int i = 0; i < 4; i++)
		{
			char *devicename = ndiDeviceName(i);

			if (devicename)
			{
				errnum = ndiProbe(devicename);

				if (errnum == NDI_OKAY)
				{
					m_serialPort = i+1;
					break;
				}
			}
		}
	}
	else // otherwise probe the specified serial port only
	{

		char *devicename = ndiDeviceName(m_serialPort-1);
		
		if (devicename)
		{
			errnum = ndiProbe(devicename);			
		}
	}

	// if probe was okay, then send VER:0 to identify device
	if (errnum == NDI_OKAY)
	{
		char *devicename = ndiDeviceName(m_serialPort-1);
		m_pDevice = ndiOpen(devicename);
		if (m_pDevice)
		{
			ndiClose(m_pDevice);
			m_pDevice = 0;
		}

		return 1;
	}

	return 0;
}

bool RS232Ndi::startTracking()
{
	int errnum, tool;
	int baud;

	if (m_isDeviceTracking)
	{
		return 1;
	}

	switch (m_baudRate)
	{
	case 9600: baud = NDI_9600; break; 
	case 14400: baud = NDI_14400; break; 
	case 19200: baud = NDI_19200; break; 
	case 38400: baud = NDI_38400; break; 
	case 57600: baud = NDI_57600; break; 
	case 115200: baud = NDI_115200; break;
	default:
		qDebug("Wrong baudrate");
		return 0;
	}

	char*	devicename = ndiDeviceName(m_serialPort-1);
	qDebug(devicename);

	m_pDevice = ndiOpen(devicename);
	if (m_pDevice == 0) 
	{
		qDebug(ndiErrorString(NDI_OPEN_ERROR));
		return 0;
	}
	// initialize Device
	ndiCommand(m_pDevice,"INIT:");
	if (ndiGetError(m_pDevice))
	{
		ndiRESET(m_pDevice);
		errnum = ndiGetError(m_pDevice);
		if (errnum) 
		{
			qDebug(ndiErrorString(errnum));
			ndiClose(m_pDevice);
			m_pDevice = 0;
			return 0;
		}
		ndiCommand(m_pDevice,"INIT:");
		if (errnum) 
		{
			qDebug(ndiErrorString(errnum));
			ndiClose(m_pDevice);
			m_pDevice = 0;
			return 0;	
		}
	}

	// set the baud rate
	// also: NOHANDSHAKE cuts down on CRC errs and timeouts
	ndiCommand(m_pDevice,"COMM:%d%03d%d",baud,NDI_8N1,NDI_NOHANDSHAKE);
	errnum = ndiGetError(m_pDevice);
	if (errnum) 
	{
		qDebug(ndiErrorString(errnum));
		ndiClose(m_pDevice);
		m_pDevice = 0;
		return 0;
	}


	for (tool = 0; tool < NDI_NTOOLS; tool++)
	{
		if (m_pVirtualSROM[tool])
		{
	//		std::cout << "this tool is reading rom file  " << tool << std::endl; 
			readSROM(tool,m_pVirtualSROM[tool]);
		}
	}

	enableToolPorts();

	//ndiCommand(m_pDevice,"VSEL:2");
	//errnum = ndiGetError(m_pDevice);
	/*if (errnum) 
	{
		qDebug(ndiErrorString(errnum));
		ndiClose(m_pDevice);
		m_pDevice = 0;
		return 0;
	}*/

	 ndiCommand(m_pDevice,"TSTART:");

	errnum = ndiGetError(m_pDevice);
	if (errnum) 
	{
		qDebug(ndiErrorString(errnum));
		ndiClose(m_pDevice);
		m_pDevice = 0;
		return 0;
	}

	m_isDeviceTracking = 1;
	int ph[NDI_NTOOLS];
	for(int h=0; h<NDI_NTOOLS; h++ )
		ph[h] = m_portHandle[h];
	m_trackingThread.setNdiSystem(m_pDevice,ph);
	m_trackingThread.setTrackingStatus(true);
	if(!m_trackingThread.isRunning())
		m_trackingThread.start();

    //qDebug("successfully started");
	return 1;

}

void RS232Ndi::loadVirtualSROM(int tool, char *filename)
{
	FILE *file = fopen(filename,"rb");
	if (file == NULL)
	{
		qDebug("couldn't find srom file %s" , filename);
		return;
	}

	if (m_pVirtualSROM[tool] == 0)
	{
		m_pVirtualSROM[tool] = new unsigned char[1024];
	}

	memset(m_pVirtualSROM[tool],0,1024);
	fread(m_pVirtualSROM[tool],1,1024,file);

	fclose(file);

	if (m_isDeviceTracking)
	{
		m_trackingThread.setTrackingStatus(false);
		ndiCommand(m_pDevice,"TSTOP:");

		readSROM(tool,m_pVirtualSROM[tool]);

		ndiCommand(m_pDevice,"TSTART:");
		m_trackingThread.setTrackingStatus(true);

	}
}

void RS232Ndi::readSROM(int tool,const unsigned char data[1024])
{
	if (data == NULL)
	{
		return;
	}

	int errnum;
	int ph;
	int n, i;
	char hexbuffer[128];
	char location[14];

	//if(tool < 3) // wired tools
	if(tool < m_activePortNum) // wired tools - modified by banbanfish polaris vicra has no active ports!
	{
		ndiCommand(m_pDevice, "PHSR:00");
		n = ndiGetPHSRNumberOfHandles(m_pDevice);
		for (i = 0; i < n; i++)
		{
			if (ndiGetPHSRInformation(m_pDevice,i) & NDI_TOOL_IN_PORT)
			{
				ph = ndiGetPHSRHandle(m_pDevice,i);
				ndiCommand(m_pDevice,"PHINF:%02X0021",ph);
				ndiGetPHINFPortLocation(m_pDevice,location);
				if (tool == (location[10]-'0')*10 + (location[11]-'0') - 1)
				{
					break;
				}
			}
		}
		if (i == n)
		{
			qDebug("can't load SROM: no tool found in port %d ", tool);
			return;
		}

	}
	else // wireless tools
	{
		//ndiCommand(m_pDevice, "PHRQ:**********0%c**", tool-3+'A');
		//ndiCommand(m_pDevice, "PHRQ:**********0%c**", tool - m_activePortNum + 'A');
		// PHRQ *********1****
		ndiCommand(m_pDevice, "PHRQ:*********1****");
		
		ph = ndiGetPHRQHandle(m_pDevice);
		std::cout <<"tool ph is " << ph <<std::endl;
	}
	errnum = ndiGetError(m_pDevice);
	if (errnum)
	{
		qDebug(ndiErrorString(errnum));
		return;
	}
	for ( i = 0; i < 1024; i += 64)
	{
		ndiCommand(m_pDevice, "PVWR:%02X%04X%.128s",
			ph, i, ndiHexEncode(hexbuffer, &data[i], 64));
	}  
}

void RS232Ndi::enableToolPorts()
{
	int errnum = 0;
	int tool;
	int ph;
	int port;
	int mode;
	int ntools;
	int status;
	char identity[34];
	char location[14];

	// reset our information about the tool ports
	for (tool = 0; tool < NDI_NTOOLS; tool++)
	{
		m_portHandle[tool] = 0;
		m_portEnabled[tool] = 0;
	}

	// stop tracking
	if (m_isDeviceTracking)
	{
		m_trackingThread.setTrackingStatus(false);
		ndiCommand(m_pDevice,"TSTOP:");
		errnum = ndiGetError(m_pDevice);
		if (errnum)
		{ 
			qDebug(ndiErrorString(errnum));
		}    
	}

	ndiCommand(m_pDevice,"PHSR:01");
	ntools = ndiGetPHSRNumberOfHandles(m_pDevice);
	//std::cout << "The number of tools is " << ntools << std::endl;
	for (tool = 0; tool < ntools; tool++)
	{
		ph = ndiGetPHSRHandle(m_pDevice,tool);
		port = getPortFromHandle(ph);
		ndiCommand(m_pDevice,"PHF:%02X",ph);
		//fprintf(stderr,"PHF:%02X\n",ph);
		errnum = ndiGetError(m_pDevice);
		if (errnum)
		{ 
			qDebug(ndiErrorString(errnum));
		}
	}

	do 
	{
		ndiCommand(m_pDevice,"PHSR:02");
		ntools = ndiGetPHSRNumberOfHandles(m_pDevice);
		for (tool = 0; tool < ntools; tool++)
		{
			ph = ndiGetPHSRHandle(m_pDevice,tool);
			ndiCommand(m_pDevice,"PINIT:%02X",ph);
			errnum = ndiGetError(m_pDevice);
			if (errnum)
			{ 
				qDebug(ndiErrorString(errnum));
			}
		}
	}
	while (ntools > 0 && errnum == 0);

	ndiCommand(m_pDevice,"PHSR:03");
	ntools = ndiGetPHSRNumberOfHandles(m_pDevice);
	for (tool = 0; tool < ntools; tool++)
	{
		ph = ndiGetPHSRHandle(m_pDevice,tool);
		ndiCommand(m_pDevice,"PHINF:%02X0001",ph);
		ndiGetPHINFToolInfo(m_pDevice,identity);
		if (identity[1] == 0x03) 
		{
			mode = 'B';
		}
		else if (identity[1] == 0x01) 
		{
			mode = 'S';
		}
		else // anything else
		{
			mode = 'D';
		}

		// enable the tool
		ndiCommand(m_pDevice,"PENA:%02X%c",ph,mode);
		//fprintf(stderr,"PENA:%02X%c\n",ph,mode);
		errnum = ndiGetError(m_pDevice);
		if (errnum)
		{
			qDebug(ndiErrorString(errnum));
		}
	}

	// get information for all tools
	ndiCommand(m_pDevice,"PHSR:00");
	ntools = ndiGetPHSRNumberOfHandles(m_pDevice);
	std::cout << "the number of tools is " << ntools << std::endl;
	for (tool = 0; tool < ntools; tool++)
	{
		ph = ndiGetPHSRHandle(m_pDevice,tool);
		std::cout << "Port Handle for this tool is " << ph << std::endl;
		ndiCommand(m_pDevice,"PHINF:%02X0025",ph);
		errnum = ndiGetError(m_pDevice);
		if (errnum)
		{ 
			qDebug(ndiErrorString(errnum));
			continue;
		}    
		// get the physical port identifier
		ndiGetPHINFPortLocation(m_pDevice,location);
		//std::cout << "The physical location is " << location[11] << std::endl;
		if (location[11] >= 'A')
		{
			port = location[11] - 'A' + 3;
			std::cout << " Port is " << port << std::endl;
		}
		else
		{
			port = (location[10]-'0')*10 + (location[11]-'0') - 1;
		}
		if (port < NDI_NTOOLS)
		{
			std::cout << "now the tool is 0? " << tool << std::endl;
			m_portHandle[tool] = ph;
			m_handleToPort[tool] = port;
		}

		status = ndiGetPHINFPortStatus(m_pDevice);
		m_portEnabled[tool] = ((status & NDI_ENABLED) != 0);

	}

	// re-start the tracking
	if (m_isDeviceTracking)
	{
		ndiCommand(m_pDevice,"TSTART:");
		m_trackingThread.setTrackingStatus(true);
		if(!m_trackingThread.isRunning())
			m_trackingThread.start();
		errnum = ndiGetError(m_pDevice);
		if (errnum)
		{ 
			qDebug(ndiErrorString(errnum));
		}
	}
}


int RS232Ndi::getPortFromHandle(int handle)
{
	int tool;

	for (tool = 0; tool < NDI_NTOOLS; tool++)
	{
		if (m_portHandle[tool] == handle)
		{
			return tool;
		}
	}

	return -1;

}

void RS232Ndi::disableToolPorts()
{
	int errnum = 0;
	int ph;
	int tool;
	int ntools;

	// stop tracking
	if (m_isDeviceTracking)
	{
		m_trackingThread.setTrackingStatus(false);
		ndiCommand(m_pDevice,"TSTOP:");
		errnum = ndiGetError(m_pDevice);
		if (errnum)
		{ 
			qDebug(ndiErrorString(errnum));
		}    
	}

	// disable all enabled tools
	ndiCommand(m_pDevice,"PHSR:04");
	ntools = ndiGetPHSRNumberOfHandles(m_pDevice);
	for (tool = 0; tool < ntools; tool++)
	{
		ph = ndiGetPHSRHandle(m_pDevice,tool);
		ndiCommand(m_pDevice,"PDIS:%02X",ph);
		//fprintf(stderr,"PDIS:%02X\n",ph);
		errnum = ndiGetError(m_pDevice);
		if (errnum)
		{ 
			qDebug(ndiErrorString(errnum));
		}    
	}

	// disable the enabled ports
	for (tool = 0; tool < NDI_NTOOLS; tool++)
	{
		m_portEnabled[tool] = 0;
	}

	// re-start the tracking
	if (m_isDeviceTracking)
	{
		ndiCommand(m_pDevice,"TSTART:");
		m_trackingThread.setTrackingStatus(true);
		if(!m_trackingThread.isRunning())
			m_trackingThread.start();
		errnum = ndiGetError(m_pDevice);
		if (errnum)
		{ 
			qDebug(ndiErrorString(errnum));
		}
	}
}

void RS232Ndi::clearVirtualSROM(int tool)
{
	int ph = m_portHandle[tool];
	ndiCommand(m_pDevice, "PHF:%02X", ph);
	m_portEnabled[tool] = 0;
	m_portHandle[tool] = 0;
}

bool RS232Ndi::stopTracking()
{
	if (m_pDevice == 0)
	{
		return 0;
	}

	int errnum, tool;
	m_trackingThread.setTrackingStatus(false);
	m_trackingThread.wait();

	ndiCommand(m_pDevice,"TSTOP:");
	errnum = ndiGetError(m_pDevice);
	if (errnum) 
	{
		qDebug(ndiErrorString(errnum));
	}
	m_isDeviceTracking = 0;

	for (tool = 0; tool < NDI_NTOOLS; tool++)
	{
		if (m_pVirtualSROM[tool])
		{
			clearVirtualSROM(tool);
		}
	}

	disableToolPorts();

	// return to default comm settings
	ndiCommand(m_pDevice,"COMM:00000");
	errnum = ndiGetError(m_pDevice);
	if (errnum) 
	{
		qDebug(ndiErrorString(errnum));
	}
	ndiClose(m_pDevice);
	m_pDevice = 0;

	return 1;
}

void RS232Ndi::readOffsetFromSROM(double offset[3])
{
	if(!m_isDeviceTracking)
		return;
	int errnum;

	if (m_isDeviceTracking)
	{
		m_trackingThread.setTrackingStatus(false);
		ndiCommand(m_pDevice,"TSTOP:");
		errnum = ndiGetError(m_pDevice);
		if (errnum)
		{ 
			qDebug(ndiErrorString(errnum));
		}    
	}


	/*char buf[128];

	QString chunk;
	chunk.sprintf("%7.3f,%7.3f,%7.3f",0.0,0.0,10.0);
	int strSize = chunk.size();
	chunk += QString(64-strSize+1,'0');
	QByteArray ba = chunk.toLatin1();
	const char *c_str2 = ba.data();

	for(int i=128; i <192; i+=64)
	{
	ndiCommand(m_pDevice, "PUWR:%02X%04X%.128s",
	m_portHandle[1], i, ndiHexEncode(buf, &c_str2[0], 64));
	errnum = ndiGetError(m_pDevice);
	if (errnum)
	{ 
	qDebug(ndiErrorString(errnum));
	}  
	}*/

	char *rp;

	rp=ndiCommand(m_pDevice,"PURD:%02X%04X",m_portHandle[1],128);
	errnum = ndiGetError(m_pDevice);
	if (errnum)
	{ 
		qDebug(ndiErrorString(errnum));
	}  
	char data[64];
	ndiHexDecode(data,rp,64);

	QString readStr;
    readStr=QString::fromLatin1(data,23);

	QStringList offset_str = readStr.split(',');
	offset[0] = offset_str[0].toDouble();
	offset[1] = offset_str[1].toDouble();
	offset[2] = offset_str[2].toDouble();

	if (m_isDeviceTracking)
	{
		ndiCommand(m_pDevice,"TSTART:");
		m_trackingThread.setTrackingStatus(true);
		if(!m_trackingThread.isRunning())
			m_trackingThread.start();
		errnum = ndiGetError(m_pDevice);
		if (errnum)
		{ 
			qDebug(ndiErrorString(errnum));
		}
	}



}

int RS232Ndi::numberOfSensors()
{
	int numSensors = 0;
	for(; numSensors<8; numSensors++)
	{
		if(m_portHandle[numSensors]==-1)
			break;
	}
	return numSensors-1;
}

bool
RS232Ndi::getToolTransform(int tool, SensorData& s)
{
	SensorData temp = m_trackingThread.getToolTransform(tool);

		/*if(((abs(temp.x) + abs(temp.y)+abs(temp.z)))>0)
		{
			s = temp;
			return true;
		}
		else
		{
			return false;
		}*/
	s = temp;
	return true;
}

//////////////////////////////////////////////////////////
// functions implemented by banbanfish
//////////////////////////////////////////////////////////
bool RS232Ndi::resetHareware()
{
	if(m_pDevice == 0)
	{
		char *devicename = ndiDeviceName(m_serialPort-1);

		m_pDevice = ndiOpen(devicename);
		if (m_pDevice == 0) 
		{
			qDebug(ndiErrorString(NDI_OPEN_ERROR));
			return 0;
		}		
	}
	
	ndiRESET(m_pDevice);
	int errnum = ndiGetError(m_pDevice);
	if(errnum)
	{
		qDebug(ndiErrorString(errnum));
		ndiClose(m_pDevice);
		m_pDevice = 0;
		return 0;
	}

	return 1;
}

bool RS232Ndi::setCOMParams(int baudRate, int dataBits, int parity, int stopBits, int hardware)
{
	if(m_pDevice == 0)
	{
		char *devicename = ndiDeviceName(m_serialPort-1);

		m_pDevice = ndiOpen(devicename);
		if (m_pDevice == 0) 
		{
			qDebug(ndiErrorString(NDI_OPEN_ERROR));
			return 0;
		}		
	}

	ndiCommand(m_pDevice,"COMM:%d%d%d%d%d", baudRate, dataBits, parity, stopBits, hardware);
	int errnum = ndiGetError(m_pDevice);
	if (errnum) 
	{
		qDebug(ndiErrorString(errnum));
		ndiClose(m_pDevice);
		m_pDevice = 0;
		return 0;
	}

	return 1;
}

bool RS232Ndi::initializeSystem()
{
	if(m_pDevice == 0)
	{
		char *devicename = ndiDeviceName(m_serialPort-1);

		m_pDevice = ndiOpen(devicename);
		if (m_pDevice == 0) 
		{
			qDebug(ndiErrorString(NDI_OPEN_ERROR));
			return 0;
		}
	}

	for (int i = 0; i < NDI_NTOOLS; i++)
	{
		m_portHandle[i] = -1;
		m_portEnabled[i] = -1;
		m_pVirtualSROM[i] = 0;
		m_handleToPort[i] = -1;
	}
	// initialize Device
	ndiCommand(m_pDevice,"INIT:");
	if (ndiGetError(m_pDevice))
	{
		ndiRESET(m_pDevice);
		int errnum = ndiGetError(m_pDevice);
		if (errnum) 
		{
			qDebug(ndiErrorString(errnum));
			ndiClose(m_pDevice);
			m_pDevice = 0;
			return 0;
		}
		ndiCommand(m_pDevice,"INIT:");
		errnum = ndiGetError(m_pDevice);
		if (errnum) 
		{
			qDebug(ndiErrorString(errnum));
			ndiClose(m_pDevice);
			m_pDevice = 0;
			return 0;	
		}
	}
	return 1;
}

bool RS232Ndi::getSystemInfo()
{
	// just get some handle no.
	if(m_pDevice == 0)
	{
		return 0;
	}

	char *reply;
	int numActivePorts, numPassivePorts;
	reply = ndiSFLIST(m_pDevice, 01);  // active tools - 1 hexadecimal character
	numActivePorts = ndiHexToUnsignedLong(reply, 1);
	reply = ndiSFLIST(m_pDevice, 02);  // passive tools - 1 hexadecimal character
	numPassivePorts = ndiHexToUnsignedLong(reply, 1);

	std::cout << "active ports " << numActivePorts << std::endl;
	std::cout << "passive ports " << numPassivePorts << std::endl;

	m_activePortNum = numActivePorts;
	m_passivePortNum = numPassivePorts;

	return 1;
}

bool RS232Ndi::activeAllPorts()
{
	// before call this function, 
	// the virtual SROM file should be loaded to the respective element of the array in the RS232Ndi class
	int errnum = 0;
	int tool;
	int ntools;
	int ph;
	int port;
	int status;
	int mode;
	char identity[34];
	char location[14];

	if(m_pDevice == 0)
	{
		return 0;
	}

	// read the rom files
	for (tool = 0; tool < NDI_NTOOLS; tool++)
	{
		if (m_pVirtualSROM[tool])
		{
			readSROM(tool,m_pVirtualSROM[tool]);
		}
	}

	// reset our information about the tool ports
	for (tool = 0; tool < NDI_NTOOLS; tool++)
	{
		m_portHandle[tool] = 0;
		m_portEnabled[tool] = 0;
	}

	// free port handles
	ndiCommand(m_pDevice,"PHSR:01");  // port handles that need to be released
	ntools = ndiGetPHSRNumberOfHandles(m_pDevice);
	for (tool = 0; tool < ntools; tool++)
	{
		ph = ndiGetPHSRHandle(m_pDevice,tool);
		port = getPortFromHandle(ph);
		ndiCommand(m_pDevice,"PHF:%02X",ph);
		errnum = ndiGetError(m_pDevice);
		if (errnum)
		{ 
			qDebug(ndiErrorString(errnum));
		}
	}

	// initialize all ports
	do 
	{
		ndiCommand(m_pDevice,"PHSR:02");  // port handles that are occupied, but are not initialized or embed
		ntools = ndiGetPHSRNumberOfHandles(m_pDevice);
		for (tool = 0; tool < ntools; tool++)
		{
			ph = ndiGetPHSRHandle(m_pDevice,tool);
			ndiCommand(m_pDevice,"PINIT:%02X",ph);
			errnum = ndiGetError(m_pDevice);
			if (errnum)
			{ 
				qDebug(ndiErrorString(errnum));
			}
		}
	}
	while (ntools > 0 && errnum == 0);

	// enable all ports
	ndiCommand(m_pDevice,"PHSR:03");
	ntools = ndiGetPHSRNumberOfHandles(m_pDevice);
	for (tool = 0; tool < ntools; tool++)
	{
		ph = ndiGetPHSRHandle(m_pDevice,tool);
		ndiCommand(m_pDevice,"PHINF:%02X0001",ph);
		ndiGetPHINFToolInfo(m_pDevice,identity);
		if (identity[1] == 0x03) 
		{
			mode = 'B';
		}
		else if (identity[1] == 0x01) 
		{
			mode = 'S';
		}
		else // anything else
		{
			mode = 'D';
		}

		// enable the tool
		ndiCommand(m_pDevice,"PENA:%02X%c",ph,mode);
		//fprintf(stderr,"PENA:%02X%c\n",ph,mode);
		errnum = ndiGetError(m_pDevice);
		if (errnum)
		{
			qDebug(ndiErrorString(errnum));
		}
	}

	// get all information of the port
    ndiCommand(m_pDevice,"PHSR:00");
	ntools = ndiGetPHSRNumberOfHandles(m_pDevice);
	std::cout << "the number of tools is " << ntools << std::endl;
	for (tool = 0; tool < ntools; tool++)
	{
		ph = ndiGetPHSRHandle(m_pDevice,tool);
		std::cout << "Port Handle for this tool is " << ph << std::endl;
		ndiCommand(m_pDevice,"PHINF:%02X0025",ph);
		errnum = ndiGetError(m_pDevice);
		if (errnum)
		{ 
			qDebug(ndiErrorString(errnum));
			continue;
		}    
		// get the physical port identifier
		ndiGetPHINFPortLocation(m_pDevice,location);
		//std::cout << "The physical location is " << location[11] << std::endl;
		if (location[11] >= 'A')
		{
			port = location[11] - 'A' + 3;
			std::cout << " Port is " << port << std::endl;
		}
		else
		{
			port = (location[10]-'0')*10 + (location[11]-'0') - 1;
		}
		if (port < NDI_NTOOLS)
		{
			std::cout << "now the tool is 0? " << tool << std::endl;
			m_portHandle[tool] = ph;
			m_handleToPort[tool] = port;
		}

		status = ndiGetPHINFPortStatus(m_pDevice);
		m_portEnabled[tool] = ((status & NDI_ENABLED) != 0);

	}

	return 1;
}

bool RS232Ndi::startTracking_banban()
{
	if(m_pDevice == 0)
	{
		return 0;
	}

	ndiCommand(m_pDevice,"TSTART:");

	int errnum = ndiGetError(m_pDevice);
	if (errnum) 
	{
		qDebug(ndiErrorString(errnum));
		ndiClose(m_pDevice);
		m_pDevice = 0;
		return 0;
	}

	m_isDeviceTracking = 1;
	int ph[NDI_NTOOLS];
	for(int h = 0; h < NDI_NTOOLS; h++)
		ph[h] = m_portHandle[h];
	m_trackingThread.setNdiSystem(m_pDevice,ph);
	m_trackingThread.setTrackingStatus(true);
	if(!m_trackingThread.isRunning())
		m_trackingThread.start();

	return 1;
}

bool RS232Ndi::stopTracking_banban()
{
	if(m_pDevice == 0)
	{
		return 0;
	}

	int errnum;
	m_trackingThread.setTrackingStatus(false);
	m_trackingThread.wait();

	ndiCommand(m_pDevice,"TSTOP:");
	errnum = ndiGetError(m_pDevice);
	if (errnum) 
	{
		qDebug(ndiErrorString(errnum));
	}
	m_isDeviceTracking = 0;

	return 1;
}

void RS232Ndi::closeSystem()
{
	if(m_pDevice == 0)
	{
		return;
	}

	int errnum, tool;
	m_trackingThread.setTrackingStatus(false);
	m_trackingThread.wait();

	if(m_isDeviceTracking)
	{
		ndiCommand(m_pDevice,"TSTOP:");
		errnum = ndiGetError(m_pDevice);
		if (errnum) 
		{
			qDebug(ndiErrorString(errnum));
		}
		m_isDeviceTracking = 0;
	}

	for (tool = 0; tool < NDI_NTOOLS; tool++)
	{
		if (m_pVirtualSROM[tool])
		{
			clearVirtualSROM(tool);
		}
	}

	disableToolPorts();

	// return to default comm settings
	ndiCommand(m_pDevice,"COMM:00000");
	errnum = ndiGetError(m_pDevice);
	if (errnum) 
	{
		qDebug(ndiErrorString(errnum));
	}
	ndiClose(m_pDevice);
	m_pDevice = 0;

	return;
}
