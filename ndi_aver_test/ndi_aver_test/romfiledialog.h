#ifndef ROMFILEDIALOG_H
#define ROMFILEDIALOG_H

#include <QDialog>

class QTreeWidget;
class QLineEdit;
class QPushButton;
class QSettings;

class ROMFileDialog : public QDialog
{
	Q_OBJECT

public:
	ROMFileDialog(QWidget *parent = 0);
	ROMFileDialog(QSettings *ini, QWidget *parent = 0);

	void setNoActivePorts(int num) { m_nNoActivePorts = num;}
	void setNoPassivePorts(int num) { m_nNoPassivePorts = num; }
	void fillROMFileTable();
	
private slots:
	void openROMFile();
	void saveChanges();
	void selectROMFileListItem();

private:
	void createUI();

	// member variables
	QTreeWidget *romFileList;
	QLineEdit *portIDEdit;
	QLineEdit *romFileEdit;
	QPushButton *openROMFileButton;
	QPushButton *saveButton;
	
	QSettings *settingsINI;
    QString curROMFile;
	QString curPortID;

	int m_nNoActivePorts;
	int m_nNoPassivePorts;
	bool m_bChangesSaved;
	
};

#endif

