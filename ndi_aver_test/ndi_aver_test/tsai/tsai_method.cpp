#include "tsai_method.h"

#include <iostream>

//#include <stdio.h>
//#include <stdlib.h>

#include <cstdio>
#include <malloc.h>

#include "matrix.h"
#include "cminpack.h"

/*******************************************************************************\
*                                                                               *
* This file contains routines for calibrating Tsai's 11 parameter camera model. *
* The camera model is based on the pin hole model of 3D-2D perspective          *
* projection with 1st order radial lens distortion.  The model consists of      *
* 5 internal (also called intrinsic or interior) camera parameters:             *
*                                                                               *
*       f       - effective focal length of the pin hole camera                 *
*       kappa1  - 1st order radial lens distortion coefficient                  *
*       Cx, Cy  - coordinates of center of radial lens distortion               *
*		  (also used as the piercing point of the camera coordinate	*
*		   frame's Z axis with the camera's sensor plane)               *
*       sx      - uncertainty factor for scale of horizontal scanline           *
*                                                                               *
* and 6 external (also called extrinsic or exterior) camera parameters:         *
*                                                                               *
*       Rx, Ry, Rz, Tx, Ty, Tz  - rotational and translational components of    *
*                                 the transform between the world's coordinate  *
*                                 frame and the camera's coordinate frame.      *
*                                                                               *
* Data for model calibration consists of the (x,y,z) world coordinates of a     *
* feature point (in mm) and the corresponding coordinates (Xf,Yf) (in pixels)   *
* of the feature point in the image.  Two types of calibration are available:   *
*                                                                               *
*       coplanar     - all of the calibration points lie in a single plane      *
*       non-coplanar - the calibration points do not lie in a single plane      *
*                                                                               *
* This file contains routines for two levels of calibration.  The first level   *
* of calibration is a direct implementation of Tsai's algorithm in which only   *
* the f, Tz and kappa1 parameters are optimized for.  The routines are:         *
*                                                                               *
*       coplanar_calibration ()                                                 *
*       noncoplanar_calibration ()                                              *
*                                                                               *
* The second level of calibration optimizes for everything.  This level is      *
* very slow but provides the most accurate calibration.  The routines are:      *
*                                                                               *
*       coplanar_calibration_with_full_optimization ()                          *
*       noncoplanar_calibration_with_full_optimization ()                       *
*                                                                               *
* Routines are also provided for initializing camera parameter variables        *
* for five of our camera/frame grabber systems.  These routines are:            *
*                                                                               *
*       initialize_photometrics_parms ()                                        *
*       initialize_general_imaging_mos5300_matrox_parms ()                      *
*       initialize_panasonic_gp_mf702_matrox_parms ()                           *
*       initialize_sony_xc75_matrox_parms ()                                    *
*       initialize_sony_xc77_matrox_parms ()                                    *
*       initialize_sony_xc57_androx_parms ()                                    *
*                                                                               *
*                                                                               *
* External routines                                                             *
* -----------------                                                             *
*                                                                               *
* Nonlinear optimization for these camera calibration routines is performed     *
* by the MINPACK lmdif subroutine.  lmdif uses a modified Levenberg-Marquardt   *
* with a jacobian calculated by a forward-difference approximation.             *
* The MINPACK FORTRAN routines were translated into C generated using f2c.      *
*                                                                               *
* Matrix operations (inversions, multiplications, etc.) are also provided by    *
* external routines.                                                            *
*                                                                               *
*                                                                               *
* Extra notes                                                                   *
* -----------                                                                   *
*                                                                               *
* An explanation of the basic algorithms and description of the variables       *
* can be found in several publications, including:                              *
*                                                                               *
* "An Efficient and Accurate Camera Calibration Technique for 3D Machine        *
*  Vision", Roger Y. Tsai, Proceedings of IEEE Conference on Computer Vision    *
*  and Pattern Recognition, Miami Beach, FL, 1986, pages 364-374.               *
*                                                                               *
*  and                                                                          *
*                                                                               *
* "A versatile Camera Calibration Technique for High-Accuracy 3D Machine        *
*  Vision Metrology Using Off-the-Shelf TV Cameras and Lenses", Roger Y. Tsai,  *
*  IEEE Journal of Robotics and Automation, Vol. RA-3, No. 4, August 1987,      *
*  pages 323-344.                                                               *
*                                                                               *
*                                                                               *
* Notation                                                                      *
* --------                                                                      *
*                                                                               *
* The camera's X axis runs along increasing column coordinates in the           *
* image/frame.  The Y axis runs along increasing row coordinates.               *
*                                                                               *
* pix == image/frame grabber picture element                                    *
* sel == camera sensor element                                                  *
*                                                                               *
* Internal routines starting with "cc_" are for coplanar calibration.           *
* Internal routines starting with "ncc_" are for noncoplanar calibration.       *
*                                                                               *
*                                                                               *
* History                                                                       *
* -------                                                                       *
*                                                                               *
* 20-May-95  Reg Willson (rgwillson@mmm.com) at 3M St. Paul, MN                 *
*       Return the error to lmdif rather than the squared error.                *
*         lmdif calculates the squared error internally during optimization.    *
*         Before this change calibration was essentially optimizing error^4.    *
*       Put transform and evaluation routines into separate files.              *
*                                                                               *
* 02-Apr-95  Reg Willson (rgwillson@mmm.com) at 3M St. Paul, MN                 *
*       Rewrite memory allocation to avoid memory alignment problems            *
*       on some machines.                                                       *
*       Strip out IMSL code.  MINPACK seems to work fine.                       *
*       Filename changes for DOS port.                                          *
*                                                                               *
* 04-Jun-94  Reg Willson (rgwillson@mmm.com) at 3M St. Paul, MN                 *
*       Replaced ncc_compute_Xdp_and_Ydp with ncc_compute_Xd_Yd_and_r_squared.  *
*         (effectively propagates the 22-Mar-94 to the non-coplanar routines)   *
*       Added alternate macro definitions for less common math functions.       *
*                                                                               *
* 25-Mar-94  Torfi Thorhallsson (torfit@verk.hi.is) at the University of Iceland*
*       Added a new version of the routines:                                    *
*            cc_compute_exact_f_and_Tz ()                                       *
*            cc_five_parm_optimization_with_late_distortion_removal ()          *
*            cc_five_parm_optimization_with_early_distortion_removal ()         *
*            cc_nic_optimization ()                                             *
*            cc_full_optimization ()                                            *
*            ncc_compute_exact_f_and_Tz ()                                      *
*            ncc_nic_optimization ()                                            *
*            ncc_full_optimization ()                                           *
*                                                                               *
*       The new routines use the *public domain* MINPACK library for            *
*       optimization instead of the commercial IMSL library.                    *
*       To select the new routines, compile this file with the flag -DMINPACK   *
*                                                                               *
* 22-Mar-94  Torfi Thorhallsson (torfit@verk.hi.is) at the University of Iceland*
*       Fixed a bug in cc_nic_optimization_error and cc_full_optimization_error.*
*       A division by cp.sx was missing.                                        *
*                                                                               *
* 15-Feb-94  Reg Willson (rgw@cs.cmu.edu) at Carnegie-Mellon University         *
*       Included Frederic Devernay's (<Frederic.Devernay@sophia.inria.fr>)	*
*	significantly improved routine for converting from undistorted to	*
*	distorted sensor coordinates.  Rather than iteratively solving a	*
*	system of two non-linear equations to perform the conversion, the 	*
*	new routine algebraically solves a cubic polynomial in Rd (using	*
*	the Cardan method).							*
*                                                                               *
* 14-Feb-94  Reg Willson (rgw@cs.cmu.edu) at Carnegie-Mellon University         *
*	Fixed a coding bug in ncc_compute_R and ncc_compute_better_R.           *
*       The r4, r5, and r6 terms should not be divided by cp.sx.                *
*       Bug reported by: Volker Rodehorst <vr@cs.tu-berlin.de>                  *
*                                                                               *
* 04-Jul-93  Reg Willson (rgw@cs.cmu.edu) at Carnegie-Mellon University         *
*	Added new routines to evaluate the accuracy of camera calibration.      *
*                                                                               *
*       Added check for coordinate handedness problem in calibration data.      *
*                                                                               *
* 01-May-93  Reg Willson (rgw@cs.cmu.edu) at Carnegie-Mellon University         *
*	For efficiency the non-linear optimizations are now all based on	*
*	the minimization of squared error in undistorted image coordinates      *
*	instead of the squared error in distorted image coordinates.		*
*										*
*	New routine for inverse perspective projection.				*
*										*
* 14-Feb-93  Reg Willson (rgw@cs.cmu.edu) at Carnegie-Mellon University         *
*       Bug fixes and speed ups.                                                *
*                                                                               *
* 07-Feb-93  Reg Willson (rgw@cs.cmu.edu) at Carnegie-Mellon University         *
*       Original implementation.                                                *
*                                                                               *
\*******************************************************************************/
#define DEBUG 0

/* Variables used by the subroutines for I/O (perhaps not the best way of doing this) */
struct calibration_data *cd_temp;

struct camera_parameters *cp_temp;
struct calibration_constants *cc_temp;

struct camera_parameters *cp_h_temp;
struct calibration_constants_heik *cc_h_temp;

///* Local working storage */
//double *Xd, *Yd, *r_squared;
//double U[7];
double *Xd_temp, *Yd_temp, *r_squared_temp;
double *U_temp;

/***********************************************************************\
* This routine solves for the roll, pitch and yaw angles (in radians)	*
* for a given orthonormal rotation matrix (from Richard P. Paul,        *
* Robot Manipulators: Mathematics, Programming and Control, p70).       *
* Note 1, should the rotation matrix not be orthonormal these will not  *
* be the "best fit" roll, pitch and yaw angles.                         *
* Note 2, there are actually two possible solutions for the matrix.     *
* The second solution can be found by adding 180 degrees to Rz before   *
* Ry and Rx are calculated.                                             *
\***********************************************************************/
void solve_RPY_transform(calibration_constants *cc)
{
    double sg, cg;

    cc->Rz = atan2 (cc->r4, cc->r1);

    SINCOS (cc->Rz, sg, cg);

    cc->Ry = atan2 (-cc->r7, cc->r1 * cg + cc->r4 * sg);

    cc->Rx = atan2 (cc->r3 * sg - cc->r6 * cg, cc->r5 * cg - cc->r2 * sg);
}

/***********************************************************************\
* This routine simply takes the roll, pitch and yaw angles and fills in	*
* the rotation matrix elements r1-r9.					*
\***********************************************************************/
void apply_RPY_transform(calibration_constants *cc)
{
    double sa, ca, sb, cb, sg, cg;

    SINCOS (cc->Rx, sa, ca);
    SINCOS (cc->Ry, sb, cb);
    SINCOS (cc->Rz, sg, cg);

    cc->r1 = cb * cg;
    cc->r2 = cg * sa * sb - ca * sg;
    cc->r3 = sa * sg + ca * cg * sb;
    cc->r4 = cb * sg;
    cc->r5 = sa * sb * sg + ca * cg;
    cc->r6 = ca * sb * sg - cg * sa;
    cc->r7 = -sb;
    cc->r8 = cb * sa;
    cc->r9 = ca * cb;
}

/************************************************************************/
const char* camera_type_string(int camera_model)
{
	switch(camera_model) {
	// - cameras
	case PHOTOMETRICS_STAR_I:
		return "Photometrics Star I";	
	case GENERAL_IMAGING_MOS5300:
		return "General Imaging MOS5300";
	case PANASONIC_GP_MF702:
		return "Panasonic GP-MF702";
	case SONY_XC75:
		return "Sony XC75";
	case SONY_XC77:
		return "Sony XC77";
	case SONY_XC57: 
		return "Sony XC57";
	
	// - webcams
	case LOGITECH_QUICKCAM_PRO_4000: 
		return "Logitech Quickcam Pro4000";
	case CREATIVE_WEBCAM_NX_PRO: 
		return "Creative Webcam NX pro";
	// - Default camera 
	case PINHOLE_CAMERA: 
		return "pinhole camera";
	case NEW_CAMERA: 
		return "new camera";

	default:
		return "Invalid camera selection";
	}
}

void initialize_camera_parameters(int CameraModel, 
								  double Ncx, double Nfx, double dx, double dy, 
								  double Cx, double Cy, double sx, struct camera_parameters *cp)
{
	switch(CameraModel) {
	// - cameras
	case PHOTOMETRICS_STAR_I:
		cp->Ncx = 576;		/* [sel]        */
		cp->Nfx = 576;		/* [pix]        */
		cp->dx = 0.023;		/* [mm/sel]     */
		cp->dy = 0.023;		/* [mm/sel]     */
		cp->dpx = cp->dx * cp->Ncx / cp->Nfx;	/* [mm/pix]     */
		cp->dpy = cp->dy;		/* [mm/pix]     */
		cp->Cx = 576 / 2;		/* [pix]        */
		cp->Cy = 384 / 2;		/* [pix]        */
		cp->sx = 1.0;		/* []		 */
		/* something a bit more realistic */
		cp->Cx = 258.0;
		cp->Cy = 204.0;
		break;	
	case GENERAL_IMAGING_MOS5300:
		cp->Ncx = 649;		/* [sel]        */
		cp->Nfx = 512;		/* [pix]        */
		cp->dx = 0.015;		/* [mm/sel]     */
		cp->dy = 0.015;		/* [mm/sel]     */
		cp->dpx = cp->dx * cp->Ncx / cp->Nfx;	/* [mm/pix]     */
		cp->dpy = cp->dy;		/* [mm/pix]     */
		cp->Cx = 512 / 2;		/* [pix]        */
		cp->Cy = 480 / 2;		/* [pix]        */
		cp->sx = 1.0;		/* []		 */
		break;
	case PANASONIC_GP_MF702:
		cp->Ncx = 649;		/* [sel]        */
		cp->Nfx = 512;		/* [pix]        */
		cp->dx = 0.015;		/* [mm/sel]     */
		cp->dy = 0.015;		/* [mm/sel]     */
		cp->dpx = cp->dx * cp->Ncx / cp->Nfx;	/* [mm/pix]     */
		cp->dpy = cp->dy;		/* [mm/pix]     */
		cp->Cx = 268;		/* [pix]        */
		cp->Cy = 248;		/* [pix]        */
		cp->sx = 1.078647;		/* []           */
		break;
	case SONY_XC75:
		cp->Ncx = 768;		/* [sel]        */
		cp->Nfx = 512;		/* [pix]        */
		cp->dx = 0.0084;		/* [mm/sel]     */
		cp->dy = 0.0098;		/* [mm/sel]     */
		cp->dpx = cp->dx * cp->Ncx / cp->Nfx;	/* [mm/pix]     */
		cp->dpy = cp->dy;		/* [mm/pix]     */
		cp->Cx = 512 / 2;		/* [pix]        */
		cp->Cy = 480 / 2;		/* [pix]        */
		cp->sx = 1.0;		/* []           */
		break;
	case SONY_XC77:
		cp->Ncx = 768;		/* [sel]        */
		cp->Nfx = 512;		/* [pix]        */
		cp->dx = 0.011;		/* [mm/sel]     */
		cp->dy = 0.013;		/* [mm/sel]     */
		cp->dpx = cp->dx * cp->Ncx / cp->Nfx;	/* [mm/pix]     */
		cp->dpy = cp->dy;		/* [mm/pix]     */
		cp->Cx = 512 / 2;		/* [pix]        */
		cp->Cy = 480 / 2;		/* [pix]        */
		cp->sx = 1.0;		/* []           */
		break;
	case SONY_XC57: 
		cp->Ncx = 510;               /* [sel]        */
		cp->Nfx = 512;               /* [pix]        */
		cp->dx = 0.017;              /* [mm/sel]     */
		cp->dy = 0.013;              /* [mm/sel]     */
		cp->dpx = cp->dx * cp->Ncx / cp->Nfx;   /* [mm/pix]     */
		cp->dpy = cp->dy;             /* [mm/pix]     */
		cp->Cx = 512 / 2;            /* [pix]        */
		cp->Cy = 480 / 2;            /* [pix]        */
		cp->sx = 1.107914;           /* []           */
		break;
	
	// - webcams
	case LOGITECH_QUICKCAM_PRO_4000: 
		cp->Ncx = 659;               /* [sel]        */
		cp->Nfx = 640;               /* [pix]        */
		cp->dx = 0.0056;              /* [mm/sel]     */
		cp->dy = 0.0056;              /* [mm/sel]     */
		cp->dpx = cp->dx * cp->Ncx / cp->Nfx;   /* [mm/pix]     */
		cp->dpy = cp->dy;             /* [mm/pix]     */
		cp->Cx = 640 / 2;            /* [pix]        */
		cp->Cy = 480 / 2;            /* [pix]        */
		cp->sx = 1.0;           /* []           */
		break;	
	case CREATIVE_WEBCAM_NX_PRO: 
		cp->Ncx = 642;               /* [sel]        */
		cp->Nfx = 640;               /* [pix]        */
		cp->dx = 0.008;              /* [mm/sel]     */
		cp->dy = 0.008;              /* [mm/sel]     */
		cp->dpx = cp->dx * cp->Ncx / cp->Nfx;   /* [mm/pix]     */
		cp->dpy = cp->dy;             /* [mm/pix]     */
		cp->Cx = 640 / 2;            /* [pix]        */
		cp->Cy = 480 / 2;            /* [pix]        */
		cp->sx = 1.0;           /* []           */
		break;
	// - Default camera 
	case PINHOLE_CAMERA: 
		cp->Ncx = Cx*2.0;               /* [sel]        */
		cp->Nfx = Cx*2.0;               /* [pix]        */
		cp->dx = 1.0;              /* [mm/sel]     */
		cp->dy = 1.0;              /* [mm/sel]     */
		cp->dpx = cp->dx * cp->Ncx / cp->Nfx;   /* [mm/pix]     */
		cp->dpy = cp->dy;             /* [mm/pix]     */
		cp->Cx = Cx;            /* [pix]        */
		cp->Cy = Cy;            /* [pix]        */
		cp->sx = 1.0;           /* []           */
		break;
	case NEW_CAMERA: 
		cp->Ncx = Ncx;               /* [sel]        */
		cp->Nfx = Nfx;               /* [pix]        */
		cp->dx = dx;              /* [mm/sel]     */
		cp->dy = dy;              /* [mm/sel]     */
		cp->dpx = cp->dx * cp->Ncx / cp->Nfx;   /* [mm/pix]     */
		cp->dpy = cp->dy;             /* [mm/pix]     */
		cp->Cx = Cx;            /* [pix]        */
		cp->Cy = Cy;            /* [pix]        */
		cp->sx = 1.0;           /* []           */
		break;

	default:
		printf("\nInvalid camera selection");
	}
}

//////////////////////////////////////////////////////////////////////////
void init_cd(calibration_data* cd)
{
	cd->point_count = 0;
	cd->xw = NULL;
	cd->yw = NULL;
	cd->zw = NULL;
	cd->Xf = NULL;
	cd->Yf = NULL;
}

int allocate_memory_cd(int point_count, calibration_data* cd)
{
	if(cd->point_count !=0 )
	{
		delete[] cd->xw;
		delete[] cd->yw;
		delete[] cd->zw;
		delete[] cd->Xf;
		delete[] cd->Yf;
	}
	cd->xw = new double[point_count];
	cd->yw = new double[point_count];
	cd->zw = new double[point_count];
	cd->Xf = new double[point_count];
	cd->Yf = new double[point_count];
	
	if(cd->xw && cd->yw && cd->zw && cd->Xf && cd->Yf)
	{
		cd->point_count = point_count;
		return 1;
	}
	else
	{
		return -1;
	}
}

void free_memory_cd(calibration_data* cd)
{
	if(cd->point_count != 0)
	{
		delete[] cd->xw;
		delete[] cd->yw;
		delete[] cd->zw;
		delete[] cd->Xf;
		delete[] cd->Yf;
		cd->point_count = 0;
	}
}

/////////////////////////////////////////////////////////////////////////
//void noncoplanar_calibration_with_full_optimization()
//{    /* start with a 3 parameter (Tz, f, kappa1) optimization */
//    ncc_three_parm_optimization();
//
//    /* do a full optimization minus the image center */
//    ncc_nic_optimization();
//
//    /* do a full optimization including the image center */
//    ncc_full_optimization();
//}
//void ncc_three_parm_optimization()
//{
//    ncc_compute_Xd_Yd_and_r_squared ();
//
//    ncc_compute_U ();
//
//    ncc_compute_Tx_and_Ty ();
//
//    ncc_compute_sx ();
//
//    ncc_compute_Xd_Yd_and_r_squared ();
//
//    ncc_compute_better_R ();
//
//    ncc_compute_approximate_f_and_Tz ();
//
//    if (cc.f < 0) {
//	/* try the other solution for the orthonormal matrix */
//	cc.r3 = -cc.r3;
//	cc.r6 = -cc.r6;
//	cc.r7 = -cc.r7;
//	cc.r8 = -cc.r8;
//	solve_RPY_transform ();
//
//	ncc_compute_approximate_f_and_Tz ();
//
//        if (cc.f < 0) {
//            fprintf (stderr, "error - possible handedness problem with data\n");
//            exit (-1);
//	}
//    }
//
//    ncc_compute_exact_f_and_Tz ();
//}
/***********************************************************************\
* Routines for noncoplanar camera calibration	 			*
\***********************************************************************/
void ncc_compute_Xd_Yd_and_r_squared(camera_parameters *cp, calibration_data *cd, double *Xd, double *Yd, double *r_squared)
{    
	double Xd_, Yd_;
	int point_count = cd->point_count;

	for (int i = 0; i < point_count; i++) 
	{
		Xd[i] = Xd_ = cp->dpx * (cd->Xf[i] - cp->Cx) / cp->sx;      /* [mm] */
		Yd[i] = Yd_ = cp->dpy * (cd->Yf[i] - cp->Cy);              /* [mm] */
		r_squared[i] = SQR(Xd_) + SQR (Yd_);                   /* [mm^2] */
	}
}

int ncc_compute_U(calibration_data *cd, double* Xd, double *Yd, double *U)
{
    int      i,line;
	double   *A,*x,*b;
	
	//A = (double*)malloc(sizeof(double)*(cd->point_count*7)); 
	//x =  (double*)malloc(sizeof(double)*7); 
	//b =  (double*)malloc(sizeof(double)*cd->point_count); 
	A = new double[cd->point_count*7];
	x = new double[7];
	b = new double[cd->point_count];
	
	for (i = 0; i < cd->point_count; i++) {
		line = i*7;
		A[line + 0] = Yd[i] * cd->xw[i];
		A[line + 1] = Yd[i] * cd->yw[i];
		A[line + 2] = Yd[i] * cd->zw[i];
		A[line + 3] = Yd[i];
		A[line + 4] = -Xd[i] * cd->xw[i];
		A[line + 5] = -Xd[i] * cd->yw[i];
		A[line + 6] = -Xd[i] * cd->zw[i];
		b[i] = Xd[i];
	}
	mtxAx_b(A,cd->point_count,7,b,x);
	
	U[0] = x[0];
	U[1] = x[1];
	U[2] = x[2];
	U[3] = x[3];
	U[4] = x[4];
	U[5] = x[5];
	U[6] = x[6];

	delete[] A;
	delete[] x;
	delete[] b;

	return 1;
}

void ncc_compute_Tx_and_Ty(calibration_data *cd, double *Xd, double *Yd, double *r_squared, double *U, calibration_constants *cc)
{
    int i, far_point;

    double Tx, Ty, Ty_squared,
           x, y,
           r1, r2, r3, r4, r5, r6,
           distance, far_distance;

    /* first find the square of the magnitude of Ty */
    Ty_squared = 1 / (SQR (U[4]) + SQR (U[5]) + SQR (U[6]));

    /* find a point that is far from the image center */
    far_distance = 0;
    far_point = 0;
	for (i = 0; i < cd->point_count; i++)
	{
		if ((distance = r_squared[i]) > far_distance)
		{
			far_point = i;
			far_distance = distance;
		}
	}

    /* now find the sign for Ty */
    /* start by assuming Ty > 0 */
    Ty = sqrt (Ty_squared);
    r1 = U[0] * Ty;
    r2 = U[1] * Ty;
    r3 = U[2] * Ty;
    Tx = U[3] * Ty;
    r4 = U[4] * Ty;
    r5 = U[5] * Ty;
    r6 = U[6] * Ty;
    x = r1 * cd->xw[far_point] + r2 * cd->yw[far_point] + r3 * cd->zw[far_point] + Tx;
    y = r4 * cd->xw[far_point] + r5 * cd->yw[far_point] + r6 * cd->zw[far_point] + Ty;

	/* flip Ty if we guessed wrong */
	if ((SIGNBIT (x) != SIGNBIT (Xd[far_point])) ||	(SIGNBIT (y) != SIGNBIT (Yd[far_point])))
		Ty = -Ty;

    /* update the calibration constants */
    cc->Tx = U[3] * Ty;
    cc->Ty = Ty;
}

void ncc_compute_sx(camera_parameters *cp, calibration_constants *cc, double *U)
{
    cp->sx = sqrt (SQR (U[0]) + SQR (U[1]) + SQR (U[2])) * fabs (cc->Ty);
}

void ncc_compute_better_R(camera_parameters *cp, calibration_constants *cc, double *U)
{
    double r1, r2, r3, r4, r5, r6, r7,
           sa, ca, sb, cb, sg, cg;

    r1 = U[0] * cc->Ty / cp->sx;
    r2 = U[1] * cc->Ty / cp->sx;
    r3 = U[2] * cc->Ty / cp->sx;

    r4 = U[4] * cc->Ty;
    r5 = U[5] * cc->Ty;
    r6 = U[6] * cc->Ty;

    /* use the outer product of the first two rows to get the last row */
    r7 = r2 * r6 - r3 * r5;

    /* now find the RPY angles corresponding to the estimated rotation matrix */
    cc->Rz = atan2 (r4, r1);

    SINCOS (cc->Rz, sg, cg);

    cc->Ry = atan2 (-r7, r1 * cg + r4 * sg);

    cc->Rx = atan2 (r3 * sg - r6 * cg, r5 * cg - r2 * sg);

    SINCOS (cc->Rx, sa, ca);

    SINCOS (cc->Ry, sb, cb);

    /* now generate a more orthonormal rotation matrix from the RPY angles */
    cc->r1 = cb * cg;
    cc->r2 = cg * sa * sb - ca * sg;
    cc->r3 = sa * sg + ca * cg * sb;
    cc->r4 = cb * sg;
    cc->r5 = sa * sb * sg + ca * cg;
    cc->r6 = ca * sb * sg - cg * sa;
    cc->r7 = -sb;
    cc->r8 = cb * sa;
    cc->r9 = ca * cb;
}

int ncc_compute_approximate_f_and_Tz(calibration_data *cd, calibration_constants *cc, double *Yd)
{
	int	i,line;
	double	*A,*x,*b;

	//A = (double*)malloc(sizeof(double)*(cd->point_count*2)); 
	//x = (double*)malloc(sizeof(double)*2); 
	//b = (double*)malloc(sizeof(double)*cd->point_count); 
	A = new double[cd->point_count*2];
	x = new double[2];
	b = new double[cd->point_count];

	for (i = 0; i < cd->point_count; i++) {
		line = i*2;
		A[line + 0] = cc->r4 * cd->xw[i] + cc->r5 * cd->yw[i] + cc->r6 * cd->zw[i] + cc->Ty;
		A[line + 1] = -Yd[i];
		b[i] = (cc->r7 * cd->xw[i] + cc->r8 * cd->yw[i] + cc->r9 * cd->zw[i]) * Yd[i];
	}
	mtxAx_b(A,cd->point_count,2,b,x);
	// update the calibration constants 
	cc->f = x[0];
	cc->Tz = x[1];
	cc->kappa1 = 0.0;		// this is the assumption that our calculation was made under 

	delete[] A;
    delete[] x;
	delete[] b;

	return 1;
}

/************************************************************************/
void ncc_compute_exact_f_and_Tz_error(int m_ptr, int n_ptr, double *params, double *err, int *erro)
{	
	// m_ptr:  pointer to number of points to fit 
	// n_ptr:  pointer to number of parameters 
	// params: vector of parameters 
	// err:    vector of error from data 
	int       i;
	double    xc,yc,zc, Xu_1,Yu_1, Xu_2,Yu_2, distortion_factor,f,Tz,kappa1;
	double errAVG;

	f = params[0];
	Tz = params[1];
	kappa1 = params[2];

	errAVG = 0.0;

	for (i = 0; i < cd_temp->point_count; i++) 
	{
		// convert from world coordinates to camera coordinates 
		xc = cc_temp->r1 * cd_temp->xw[i] + cc_temp->r2 * cd_temp->yw[i] + cc_temp->r3 * cd_temp->zw[i] + cc_temp->Tx;
		yc = cc_temp->r4 * cd_temp->xw[i] + cc_temp->r5 * cd_temp->yw[i] + cc_temp->r6 * cd_temp->zw[i] + cc_temp->Ty;
		zc = cc_temp->r7 * cd_temp->xw[i] + cc_temp->r8 * cd_temp->yw[i] + cc_temp->r9 * cd_temp->zw[i] + Tz;

		// convert from camera coordinates to undistorted sensor coordinates 
		Xu_1 = f * xc / zc;
		Yu_1 = f * yc / zc;

		// convert from distorted sensor coordinates to undistorted sensor coordinates 
		distortion_factor = 1 + kappa1 * r_squared_temp[i];
		Xu_2 = Xd_temp[i] * distortion_factor;
		Yu_2 = Yd_temp[i] * distortion_factor;

		// record the error in the undistorted sensor coordinates 
		err[i] = hypot (Xu_1 - Xu_2, Yu_1 - Yu_2);
		errAVG+=err[i];
	};
	if(DEBUG)	
		printf("\nErro medio= %lf",errAVG/cd_temp->point_count);
}

void ncc_compute_exact_f_and_Tz()
{
#define NPARAMS 3

	// Parameters needed by simplified "C" version of Levenberg-Marquardt method 

	int m = cd_temp->point_count;
	int     n = NPARAMS;
	double  *x;    //	x[NPARAMS];
	int     *msk;  // msk[NPARAMS];
	double  *fvec; // fvec[m];
	double  tol = 1.0e-10;
	int     info;
	int     nfev;

	// allocate some workspace 
	//if ( (x = (double*)malloc(NPARAMS * sizeof(double))) == NULL ) 
	//{
	//	printf("malloc: Cannot allocate workspace x\n");
	//	return;
	//}
	x = new double[NPARAMS];

	//if ( (msk = (int*)malloc(NPARAMS * sizeof(int))) == NULL ) 
	//{
	//	printf("malloc: Cannot allocate workspace msk\n");
	//	return;
	//}
	msk = new int[NPARAMS];

	//if ( (fvec = (double*)malloc(m * sizeof(double))) == NULL ) 
	//{
	//	printf("malloc: Cannot allocate workspace fvec\n");
	//	return;
	//}
	fvec = new double[m];

	// use the current calibration constants as an initial guess 
	x[0] = cc_temp->f;
	x[1] = cc_temp->Tz;
	x[2] = cc_temp->kappa1;

	// Here we call a function Levenberg-Marquardt Nonlinear optimization in "C" 
	// minpack simplified version
	if(lmdif0(ncc_compute_exact_f_and_Tz_error,m,n,x,msk,fvec,tol,&info,&nfev)){
			printf("\nParameters error lmdif process \n");
			
	}
	else{
		printf("\nParameters successful lmdif process \n");
		///* update the calibration constants */
		cc_temp->f = x[0];
		cc_temp->Tz = x[1];
		cc_temp->kappa1 = x[2];
	}
	
	/* print the number of function calls during iteration */
	if(DEBUG)
		printf("\ninfo: %d nfev: %d\n\n",info,nfev);
	
#undef NPARAMS

	delete[] x;
	delete[] msk;
	delete[] fvec;
	
	return;
}

//////////////////////////////////////////////////
void ncc_nic_optimization_error(int m_ptr, int n_ptr, double *params, double *err, int *erro)
{
	// m_ptr:  pointer to number of points to fit 
	// n_ptr:  pointer to number of parameters 
	// params: vector of parameters 
	// err:    vector of error from data 
	int       i;
	//double    xc,yc,zc, Xu_1,Yu_1, Xu_2,Yu_2, distortion_factor,f,Tz,kappa1;
	double    xc,yc,zc,Xd_,Yd_,Xu_1,Yu_1,Xu_2,Yu_2,distortion_factor,
              Rx,Ry,Rz,Tx,Ty,Tz,kappa1,sx,f,
              r1,r2,r3,r4,r5,r6,r7,r8,r9,
              sa,sb,sg,ca,cb,cg;
	
	double errAVG;

	Rx = params[0];
    Ry = params[1];
    Rz = params[2];
    Tx = params[3];
    Ty = params[4];
    Tz = params[5];
    kappa1 = params[6];
    f = params[7];
    sx = params[8];

	SINCOS (Rx, sa, ca);
    SINCOS (Ry, sb, cb);
    SINCOS (Rz, sg, cg);
    r1 = cb * cg;
    r2 = cg * sa * sb - ca * sg;
    r3 = sa * sg + ca * cg * sb;
    r4 = cb * sg;
    r5 = sa * sb * sg + ca * cg;
    r6 = ca * sb * sg - cg * sa;
    r7 = -sb;
    r8 = cb * sa;
    r9 = ca * cb;

	errAVG = 0.0;

	for(i = 0; i < cd_temp->point_count; i++) 
	{
		// convert from world coordinates to camera coordinates 
		xc = r1 * cd_temp->xw[i] + r2 * cd_temp->yw[i] + r3 * cd_temp->zw[i] + Tx;
		yc = r4 * cd_temp->xw[i] + r5 * cd_temp->yw[i] + r6 * cd_temp->zw[i] + Ty;
		zc = r7 * cd_temp->xw[i] + r8 * cd_temp->yw[i] + r9 * cd_temp->zw[i] + Tz;
		
		// convert from camera coordinates to undistorted sensor plane coordinates
		Xu_1 = f * xc / zc;
		Yu_1 = f * yc / zc;

		// convert from 2D image coordinates to distorted sensor coordinates
		Xd_ = cp_temp->dpx * (cd_temp->Xf[i] - cp_temp->Cx) / sx;
		Yd_ = cp_temp->dpy * (cd_temp->Yf[i] - cp_temp->Cy);

    	// convert from distorted sensor coordinates to undistorted sensor plane coordinates
		distortion_factor = 1 + kappa1 * (SQR (Xd_) + SQR (Yd_));
		Xu_2 = Xd_ * distortion_factor;
		Yu_2 = Yd_ * distortion_factor;

		// record the error in the undistorted sensor coordinates 
		err[i] = hypot (Xu_1 - Xu_2, Yu_1 - Yu_2);

		errAVG+=err[i];
	};
	if(DEBUG)	
		printf("\nErro medio= %lf",errAVG/cd_temp->point_count);
}

void ncc_nic_optimization()
{
#define NPARAMS 9

	// Parameters needed by simplified "C" version of Levenberg-Marquardt method 

	int     m = cd_temp->point_count;
	int     n = NPARAMS;
	double  *x;    //	x[NPARAMS];
	int     *msk;  // msk[NPARAMS];
	double  *fvec; // fvec[m];
	double  tol = 1.0e-10;
	int     info;
	int     nfev;

	// allocate some workspace 
	//if ( (x = (double*)malloc(NPARAMS * sizeof(double))) == NULL ) 
	//{
	//	printf("malloc: Cannot allocate workspace x\n");
	//	return;
	//}
	x = new double[NPARAMS];

	//if ( (msk = (int*)malloc(NPARAMS * sizeof(int))) == NULL ) 
	//{
	//	printf("malloc: Cannot allocate workspace msk\n");
	//	return;
	//}
	msk = new int[NPARAMS];

	//if ( (fvec = (double*)malloc(m * sizeof(double))) == NULL ) 
	//{
	//	printf("malloc: Cannot allocate workspace fvec\n");
	//	return;
	//}
	fvec = new double[m];

	// use the current calibration constants as an initial guess 
    /* use the current calibration and camera constants as a starting point */
    x[0] = cc_temp->Rx;
    x[1] = cc_temp->Ry;
    x[2] = cc_temp->Rz;
    x[3] = cc_temp->Tx;
    x[4] = cc_temp->Ty;
    x[5] = cc_temp->Tz;
    x[6] = cc_temp->kappa1;
    x[7] = cc_temp->f;
    x[8] = cp_temp->sx;

	// Here we call a function Levenberg-Marquardt Nonlinear optimization in "C" 
	// minpack simplified version
	if(lmdif0(ncc_nic_optimization_error,m,n,x,msk,fvec,tol,&info,&nfev)){
			printf("\nParameters error lmdif process \n");
			
	}
	else{
		printf("\nParameters successful lmdif process \n");
		/* update the calibration and camera constants */
		cc_temp->Rx = x[0];
		cc_temp->Ry = x[1];
		cc_temp->Rz = x[2];
		apply_RPY_transform(cc_temp);

		cc_temp->Tx = x[3];
		cc_temp->Ty = x[4];
		cc_temp->Tz = x[5];
		cc_temp->kappa1 = x[6];
		cc_temp->f = x[7];
		cp_temp->sx = x[8];		
	}
	
	/* print the number of function calls during iteration */
   if(DEBUG)
	   printf("\ninfo: %d nfev: %d\n\n",info,nfev);
	
#undef NPARAMS

   delete[] x;
   delete[] msk;
   delete[] fvec;
	
	return;
}

///////////////////////////////////////////////////////////////////
void ncc_full_optimization_error(int m_ptr, int n_ptr, double *params, double *err, int *erro)
{
	// m_ptr:  pointer to number of points to fit 
	// n_ptr:  pointer to number of parameters 
	// params: vector of parameters 
	// err:    vector of error from data 
	int       i;
	
	double    xc,yc,zc,Xd_,Yd_,Xu_1,Yu_1,Xu_2,Yu_2,distortion_factor,
              Rx,Ry,Rz,Tx,Ty,Tz,kappa1,sx,f,Cx,Cy,
              r1,r2,r3,r4,r5,r6,r7,r8,r9,
              sa,sb,sg,ca,cb,cg;

	double errAVG;

	Rx = params[0];
    Ry = params[1];
    Rz = params[2];
    Tx = params[3];
    Ty = params[4];
    Tz = params[5];
    kappa1 = params[6];
    f = params[7];
    sx = params[8];
    Cx = params[9];
    Cy = params[10];

	SINCOS (Rx, sa, ca);
    SINCOS (Ry, sb, cb);
    SINCOS (Rz, sg, cg);
    r1 = cb * cg;
    r2 = cg * sa * sb - ca * sg;
    r3 = sa * sg + ca * cg * sb;
    r4 = cb * sg;
    r5 = sa * sb * sg + ca * cg;
    r6 = ca * sb * sg - cg * sa;
    r7 = -sb;
    r8 = cb * sa;
    r9 = ca * cb;

	errAVG = 0.0;

	for(i = 0; i < cd_temp->point_count; i++) 
	{
		///* convert from world coordinates to camera coordinates */
		xc = r1 * cd_temp->xw[i] + r2 * cd_temp->yw[i] + r3 * cd_temp->zw[i] + Tx;
		yc = r4 * cd_temp->xw[i] + r5 * cd_temp->yw[i] + r6 * cd_temp->zw[i] + Ty;
		zc = r7 * cd_temp->xw[i] + r8 * cd_temp->yw[i] + r9 * cd_temp->zw[i] + Tz;

		///* convert from camera coordinates to undistorted sensor plane coordinates */
		Xu_1 = f * xc / zc;
		Yu_1 = f * yc / zc;

		///* convert from 2D image coordinates to distorted sensor coordinates */
		Xd_ = cp_temp->dpx * (cd_temp->Xf[i] - Cx) / sx;
		Yd_ = cp_temp->dpy * (cd_temp->Yf[i] - Cy);

		///* convert from distorted sensor coordinates to undistorted sensor plane coordinates */
		distortion_factor = 1 + kappa1 * (SQR (Xd_) + SQR (Yd_));
		Xu_2 = Xd_ * distortion_factor;
		Yu_2 = Yd_ * distortion_factor;

		///* record the error in the undistorted sensor coordinates */
		err[i] = hypot (Xu_1 - Xu_2, Yu_1 - Yu_2);

		errAVG+=err[i];
	};
	if(DEBUG)	
		printf("\nErro medio= %lf",errAVG/cd_temp->point_count);
}

void ncc_full_optimization()
{
#define NPARAMS 11

	// Parameters needed by simplified "C" version of Levenberg-Marquardt method 

	int     m = cd_temp->point_count;
	int     n = NPARAMS;
	double  *x;    //	x[NPARAMS];
	int     *msk;  // msk[NPARAMS];
	double  *fvec; // fvec[m];
	double  tol = 1.0e-10;
	int     info;
	int     nfev;

	// allocate some workspace 
	//if ( (x = (double*)malloc(NPARAMS * sizeof(double))) == NULL ) 
	//{
	//	printf("malloc: Cannot allocate workspace x\n");
	//	return;
	//}
	x = new double[NPARAMS];

	//if ( (msk = (int*)malloc(NPARAMS * sizeof(int))) == NULL ) 
	//{
	//	printf("malloc: Cannot allocate workspace msk\n");
	//	return;
	//}
	msk = new int[NPARAMS];

	//if ( (fvec = (double*)malloc(m * sizeof(double))) == NULL ) 
	//{
	//	printf("malloc: Cannot allocate workspace fvec\n");
	//	return;
	//}
	fvec = new double[m];

	// use the current calibration and camera constants as a starting point
    x[0] = cc_temp->Rx;
    x[1] = cc_temp->Ry;
    x[2] = cc_temp->Rz;
    x[3] = cc_temp->Tx;
    x[4] = cc_temp->Ty;
    x[5] = cc_temp->Tz;
    x[6] = cc_temp->kappa1;
    x[7] = cc_temp->f;

    x[8] = cp_temp->sx;
	x[9] = cp_temp->Cx;
    x[10] = cp_temp->Cy;

	// Here we call a function Levenberg-Marquardt Nonlinear optimization in "C" 
	// minpack simplified version
	if(lmdif0(ncc_full_optimization_error,m,n,x,msk,fvec,tol,&info,&nfev)){
			printf("\nParameters error lmdif process \n");
			
	}
	else{
		printf("\nParameters successful lmdif process \n");
		///* update the calibration and camera constants */
		cc_temp->Rx = x[0];
		cc_temp->Ry = x[1];
		cc_temp->Rz = x[2];
		apply_RPY_transform(cc_temp);

		cc_temp->Tx = x[3];
		cc_temp->Ty = x[4];
		cc_temp->Tz = x[5];
		cc_temp->kappa1 = x[6];
		cc_temp->f = x[7];
		cp_temp->sx = x[8];
		cp_temp->Cx = x[9];
		cp_temp->Cy = x[10];	
	}	
		
	/* print the number of function calls during iteration */
    if(DEBUG)
		printf("\ninfo: %d nfev: %d\n\n",info,nfev);

#undef NPARAMS

	delete[] x;
	delete[] msk;
	delete[] fvec;
	
	return;
}

//////////////////////////////////////////////////////////////
int nccal_fo(camera_parameters *cp, calibration_data *cd, double *A, double *K, double *distortion, calibration_constants *cc)
{
	int point_count = cd->point_count;

	if(point_count <= 0)
		return -1;

	double *Xd = new double[point_count];
	double *Yd = new double[point_count];
    double *r_squared = new double[point_count];
	double U[7];

	//////////////////////////////////////////////
	// related internal pointers to their global counterpart
	cp_temp = cp;
	cd_temp = cd;
	cc_temp = cc;

	Xd_temp = Xd;
	Yd_temp = Yd;
	r_squared_temp = r_squared;
	U_temp = U;
	///////////////////////////////////////////////

	// start with a 3 parameter (Tz, f, kappa1) optimization
	// 1. ncc_three_parm_optimization()
	// 1.1 ncc_compute_Xd_Yd_and_r_squared()
	ncc_compute_Xd_Yd_and_r_squared(cp, cd, Xd, Yd, r_squared);
	// 1.2 ncc_compute_U() 
	if(ncc_compute_U(cd, Xd, Yd, U) > 0)
	{
		// 1.3 ncc_compute_Tx_and_Ty()
		ncc_compute_Tx_and_Ty(cd, Xd, Yd, r_squared, U, cc);
		// 1.4 ncc_compute_sx()
		ncc_compute_sx(cp, cc, U);
		// 1.5 ncc_compute_Xd_Yd_and_r_squared()
		ncc_compute_Xd_Yd_and_r_squared(cp, cd, Xd, Yd, r_squared);
		// 1.6 ncc_compute_better_R()
		ncc_compute_better_R(cp, cc, U);
		// 1.7 ncc_compute_approximate_f_and_Tz()
		ncc_compute_approximate_f_and_Tz(cd, cc, Yd);
		// 1.8 /* try the other solution for the orthonormal matrix */
		if (cc->f < 0) 
		{			
			cc->r3 = -cc->r3;
			cc->r6 = -cc->r6;
			cc->r7 = -cc->r7;
			cc->r8 = -cc->r8;
			solve_RPY_transform(cc);

			ncc_compute_approximate_f_and_Tz(cd, cc, Yd);

			if (cc->f < 0) 
			{
				std::cout << "\nerror - possible handedness problem with data" << std::endl;

				delete[] Xd;
				delete[] Yd;
				delete[] r_squared;

				return (-1);
			}
		} 
		// 1.9 ncc_compute_exact_f_and_Tz()
		ncc_compute_exact_f_and_Tz();
	}	
	else
	{
		delete[] Xd;
		delete[] Yd;
		delete[] r_squared;

		return -1;
	}

	/* do a full optimization minus the image center */
    // 2. ncc_nic_optimization();
	ncc_nic_optimization();

	/* do a full optimization including the image center */
    // 3. ncc_full_optimization();
    ncc_full_optimization();

	printf ("\n-- Calibration Tsai Non-coplanar process successful --\n");

	A[0] = cc->f;	A[1] = 0.0;		A[2] = cp->Cx;
	A[3] = 0.0;		A[4] = cc->f;	A[5] = cp->Cy;
	A[6] = 0.0;		A[7] = 0.0;		A[8] = 1.0;

	K[0] = cc->r1;	K[1] = cc->r2;	K[2]  = cc->r3; K[3]  = cc->Tx; 
	K[4] = cc->r4;	K[5] = cc->r5;	K[6]  = cc->r6; K[7]  = cc->Ty; 
	K[8] = cc->r7;	K[9] = cc->r8;	K[10] = cc->r9; K[11] = cc->Tz; 

	*distortion = cc->kappa1;	
	
	delete[] Xd;
	delete[] Yd;
	delete[] r_squared;

	return 1;
}

// for heikkila model
void apply_RPY_transform_heik(calibration_constants_heik *cc_h)
{
    double sa, ca, sb, cb, sg, cg;

    SINCOS (cc_h->Rx, sa, ca);
    SINCOS (cc_h->Ry, sb, cb);
    SINCOS (cc_h->Rz, sg, cg);

    cc_h->r1 = cb * cg;
    cc_h->r2 = cg * sa * sb - ca * sg;
    cc_h->r3 = sa * sg + ca * cg * sb;
    cc_h->r4 = cb * sg;
    cc_h->r5 = sa * sb * sg + ca * cg;
    cc_h->r6 = ca * sb * sg - cg * sa;
    cc_h->r7 = -sb;
    cc_h->r8 = cb * sa;
    cc_h->r9 = ca * cb;
}

void ncc_compute_Xd_Yd_and_r_squared_heik(camera_parameters *cp, calibration_data *cd, double *Xd, double *Yd, double *r_squared)
{    
	double Xd_, Yd_;
	int point_count = cd->point_count;

	for (int i = 0; i < point_count; i++) 
	{
		Xd[i] = Xd_ = cp->dpx * (cd->Xf[i] - cp->Cx);      /* [mm] */
		//Xd[i] = Xd_ = cp->dpx * (cd->Xf[i] - cp->Cx) / cp->sx;      /* [mm] */
		Yd[i] = Yd_ = cp->dpy * (cd->Yf[i] - cp->Cy);              /* [mm] */
		r_squared[i] = SQR(Xd_) + SQR (Yd_);                   /* [mm^2] */
	}
}

void ncc_full_optimization_error_heik(int m_ptr, int n_ptr, double *params, double *err, int *erro)
{
	// m_ptr:  pointer to number of points to fit 
	// n_ptr:  pointer to number of parameters 
	// params: vector of parameters 
	// err:    vector of error from data 
	int       i;
	
	//double    xc,yc,zc,Xd_,Yd_,Xu_1,Yu_1,Xu_2,Yu_2,distortion_factor,
 //             Rx,Ry,Rz,Tx,Ty,Tz,kappa1,sx,f,Cx,Cy,
 //             r1,r2,r3,r4,r5,r6,r7,r8,r9,
 //             sa,sb,sg,ca,cb,cg;
	double xc,yc,zc,
		   Xu_,Yu_,Xd_1,Yd_1,Xd_2,Yd_2,xd,yd,
		   Rx,Ry,Rz,Tx,Ty,Tz,k1,k2,p1,p2,fx,fy,sx,Cx,Cy,
		   r1,r2,r3,r4,r5,r6,r7,r8,r9,
		   sa,sb,sg,ca,cb,cg;

	double errAVG;

	Rx = params[0];
    Ry = params[1];
    Rz = params[2];
    Tx = params[3];
    Ty = params[4];
    Tz = params[5];
    k1 = params[6];
	k2 = params[7];
	p1 = params[8];
	p2 = params[9];
    fx = params[10];
    fy = params[11];
	Cx = params[12];
    Cy = params[13];

	//sx = params[14];

	SINCOS (Rx, sa, ca);
    SINCOS (Ry, sb, cb);
    SINCOS (Rz, sg, cg);
    r1 = cb * cg;
    r2 = cg * sa * sb - ca * sg;
    r3 = sa * sg + ca * cg * sb;
    r4 = cb * sg;
    r5 = sa * sb * sg + ca * cg;
    r6 = ca * sb * sg - cg * sa;
    r7 = -sb;
    r8 = cb * sa;
    r9 = ca * cb;

	errAVG = 0.0;

	for(i = 0; i < cd_temp->point_count; i++) 
	{
		double rad2, rad4, rad6,
			   a1, a2, a3, cdist, icdist2;

		///* convert from world coordinates to camera coordinates */
		xc = r1 * cd_temp->xw[i] + r2 * cd_temp->yw[i] + r3 * cd_temp->zw[i] + Tx;
		yc = r4 * cd_temp->xw[i] + r5 * cd_temp->yw[i] + r6 * cd_temp->zw[i] + Ty;
		zc = r7 * cd_temp->xw[i] + r8 * cd_temp->yw[i] + r9 * cd_temp->zw[i] + Tz;

		///* convert from camera coordinates to undistorted sensor plane coordinates */
		zc = zc ? 1.0/zc : 1;
		Xu_ = xc * zc;
		Yu_ = yc * zc;

		rad2 = Xu_ * Xu_ + Yu_ * Yu_;
		rad4 = rad2 * rad2;
		rad6 = rad4 * rad2;
		a1 = 2.0 * Xu_ * Yu_;
		a2 = rad2 + 2.0 * Xu_ * Xu_;
		a3 = rad2 + 2.0 * Yu_ * Yu_;
		cdist = 1 + k1 * rad2 + k2 * rad4;
		icdist2 = 1.0;

		///* convert from distorted sensor coordinates to undistorted sensor plane coordinates */		
		xd = Xu_ * cdist * icdist2 + p1 * a1 + p2 * a2;
		yd = Yu_ * cdist * icdist2 + p1 * a3 + p2 * a1;
		
		Xd_1 = xd * fx + Cx;    
		//Xd_1 = xd * fx * sx + Cx;
		Yd_1 = yd * fy + Cy;		
		
		///* convert from 2D image coordinates to distorted sensor coordinates */
		Xd_2 = cd_temp->Xf[i];
		Yd_2 = cd_temp->Yf[i];		

		///* record the error in the undistorted sensor coordinates */
		err[i] = hypot (Xd_1 - Xd_2, Yd_1 - Yd_2);

		errAVG+=err[i];
	};
	if(DEBUG)	
		printf("\nErro medio= %lf",errAVG/cd_temp->point_count);
}

void ncc_full_optimization_heik()
{
#define NPARAMS 14
//#define NPARAMS 15

	// Parameters needed by simplified "C" version of Levenberg-Marquardt method 

	int     m = cd_temp->point_count;
	int     n = NPARAMS;
	double  *x;    //	x[NPARAMS];
	int     *msk;  // msk[NPARAMS];
	double  *fvec; // fvec[m];
	double  tol = 1.0e-10;
	int     info;
	int     nfev;

	// allocate some workspace 
	//if ( (x = (double*)malloc(NPARAMS * sizeof(double))) == NULL ) 
	//{
	//	printf("malloc: Cannot allocate workspace x\n");
	//	return;
	//}
	x = new double[NPARAMS];

	//if ( (msk = (int*)malloc(NPARAMS * sizeof(int))) == NULL ) 
	//{
	//	printf("malloc: Cannot allocate workspace msk\n");
	//	return;
	//}
	msk = new int[NPARAMS];

	//if ( (fvec = (double*)malloc(m * sizeof(double))) == NULL ) 
	//{
	//	printf("malloc: Cannot allocate workspace fvec\n");
	//	return;
	//}
	fvec = new double[m];

	// use the current calibration and camera constants as a starting point
    x[0] = cc_h_temp->Rx;
    x[1] = cc_h_temp->Ry;
    x[2] = cc_h_temp->Rz;
    x[3] = cc_h_temp->Tx;
    x[4] = cc_h_temp->Ty;
    x[5] = cc_h_temp->Tz;
    x[6] = cc_h_temp->k1;
	x[7] = cc_h_temp->k2;
	x[8] = cc_h_temp->p1;
	x[9] = cc_h_temp->p2;
    x[10] = cc_h_temp->fx;
    x[11] = cc_h_temp->fy;

	x[12] = cp_h_temp->Cx;
    x[13] = cp_h_temp->Cy;

	//x[14] = cp_h_temp->sx;

	// Here we call a function Levenberg-Marquardt Nonlinear optimization in "C" 
	// minpack simplified version
	if(lmdif0(ncc_full_optimization_error_heik,m,n,x,msk,fvec,tol,&info,&nfev)){
			printf("\nParameters error lmdif process \n");
			
	}
	else{
		printf("\nParameters successful lmdif process \n");
		///* update the calibration and camera constants */
		cc_h_temp->Rx = x[0];
		cc_h_temp->Ry = x[1];
		cc_h_temp->Rz = x[2];
		apply_RPY_transform_heik(cc_h_temp);

		cc_h_temp->Tx = x[3];
		cc_h_temp->Ty = x[4];
		cc_h_temp->Tz = x[5];
		cc_h_temp->k1 = x[6];
		cc_h_temp->k2 = x[7];
		cc_h_temp->p1 = x[8];
		cc_h_temp->p2 = x[9];
		cc_h_temp->fx = x[10];
		cc_h_temp->fy = x[11];
		
		cp_h_temp->Cx = x[12];
		cp_h_temp->Cy = x[13];	

		//cp_h_temp->sx = x[14];
	}	
		
	/* print the number of function calls during iteration */
    if(DEBUG)
		printf("\ninfo: %d nfev: %d\n\n",info,nfev);

#undef NPARAMS

	delete[] x;
	delete[] msk;
	delete[] fvec;
	
	return;
}

int nccal_fo_heik(camera_parameters *cp, calibration_constants_heik *cc_h, calibration_data *cd,
				  double *A, double *K, double *distortion)
{
	int point_count = cd->point_count;

	if(point_count <= 0)
		return -1;

	double *Xd = new double[point_count];
	double *Yd = new double[point_count];
    double *r_squared = new double[point_count];

	//////////////////////////////////////////////
	// related internal pointers to their global counterpart
	cp_h_temp = cp;
	cd_temp = cd;
	cc_h_temp = cc_h;

	Xd_temp = Xd;
	Yd_temp = Yd;
	r_squared_temp = r_squared;

	ncc_compute_Xd_Yd_and_r_squared_heik(cp, cd, Xd, Yd, r_squared);

	ncc_full_optimization_heik();

	printf ("\n-- Calibration Heikkila Non-coplanar process successful --\n");

	A[0] = cc_h->fx; 	A[1] = 0.0;		  A[2] = cp->Cx;
	A[3] = 0.0;		    A[4] = cc_h->fy;	  A[5] = cp->Cy;
	A[6] = 0.0;		    A[7] = 0.0;		  A[8] = 1.0;

	K[0] = cc_h->r1; 	K[1] = cc_h->r2;		K[2]  = cc_h->r3;	K[3]  = cc_h->Tx; 
	K[4] = cc_h->r4;	    K[5] = cc_h->r5;		K[6]  = cc_h->r6;	K[7]  = cc_h->Ty; 
	K[8] = cc_h->r7;  	K[9] = cc_h->r8;		K[10] = cc_h->r9;	K[11] = cc_h->Tz; 

	distortion[0] = cc_h->k1;	
	distortion[1] = cc_h->k2;
	distortion[2] = cc_h->p1;
	distortion[3] = cc_h->p2;
	
	delete[] Xd;
	delete[] Yd;
	delete[] r_squared;
	
	return 1;
}

