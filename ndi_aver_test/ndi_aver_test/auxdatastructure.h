#ifndef AUX_DATASTRUCTURE_H
#define AUX_DATASTRUCTURE_H

#define NUM_ENDO_TOOL 3

// auxilary data structure
typedef struct HandleDataStruct
{
	bool valid;
	unsigned long frame;
	double q[4];
	double m[16];
}HandleData;

#endif