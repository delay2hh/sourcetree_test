#ifndef RS232NDI_H
#define  RS232NDI_H

// #include "../ndi/include/ndicapi.h"
#include "ndicapi/ndicapi.h"
#include "NdiTrackingThread.h"

// #define  NDI_NTOOLS 8
#define NDI_NTOOLS 12   // polaris system: 3 active, 9 passive


class RS232Ndi
{
	friend class NdiTrackingThread;
public:
	RS232Ndi();
	~RS232Ndi();
	bool probe();
	bool startTracking();
	bool stopTracking();
	void loadVirtualSROM(int tool, char *filename);
	void readSROM(int tool, const unsigned char data[1024]);

	
	void enableToolPorts();
	void disableToolPorts();
	void clearVirtualSROM(int tool);
	int getPortFromHandle(int handle);
	void readOffsetFromSROM(double offset[3]);
	void setSerialPort(int s){m_serialPort = s;}
	int  numberOfSensors();
	bool getToolTransform(int tool, SensorData& s);

	// functions written by banbanfish
	bool resetHareware();
	bool setCOMParams(int baudRate, int dataBits, int parity, int stopBits, int hardware);
	bool initializeSystem();
	bool getSystemInfo();
	bool activeAllPorts();
	bool startTracking_banban();  
	bool stopTracking_banban();
	void closeSystem();

	int getActivePortNum() { return m_activePortNum; }
	int getPassivePortNum() { return m_passivePortNum; }
	int isPortEnabled(int i) { return (i >= 0 && i < NDI_NTOOLS) ? m_portEnabled[i] : 0; }
	int getPortHandle(int i) { return (i >= 0 && i < NDI_NTOOLS) ? m_portHandle[i] : 0;}

private:
	ndicapi *m_pDevice;
	int m_serialPort; 
	int m_baudRate;
	int m_portEnabled[NDI_NTOOLS];
	int m_portHandle[NDI_NTOOLS];
	int m_handleToPort[NDI_NTOOLS];
	unsigned char *m_pVirtualSROM[NDI_NTOOLS];
    bool m_isDeviceTracking;
	char m_commandReply[2048];
	NdiTrackingThread m_trackingThread;
	double offset[3];

	// declared by banbanfish
    int m_activePortNum;
	int m_passivePortNum;

};
#endif  //RS232NDI_H