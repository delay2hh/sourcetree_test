#include "NdiTrackingThread.h"
//#include "../ndi/include/ndicapi_math.h"
#include "ndicapi/ndicapi_math.h"

#include <float.h>
#include <iostream>

typedef double (*MatPtr)[4];

NdiTrackingThread::NdiTrackingThread()

{
	m_trackingThreadWorking = false;
	m_pSystem = 0;

}

NdiTrackingThread::~NdiTrackingThread()
{

}

void NdiTrackingThread::run()
{
	forever {
		m_mutex.lock();
		if (!m_trackingThreadWorking) {
			m_mutex.unlock();
			break;
		}
		refreshFrame();
		m_mutex.unlock();
	}
}



void NdiTrackingThread::setTrackingStatus(bool status)
{
	QMutexLocker locker(&m_mutex);
	m_trackingThreadWorking = status;
}

void NdiTrackingThread::setNdiSystem(ndicapi *s,int h[])
{
	QMutexLocker locker(&m_mutex);
	m_pSystem = s;
	for(int i=0; i<NDI_NTOOLS;i++)
		m_handle[i] = h[i];

}


void NdiTrackingThread::refreshFrame()
{
	int errnum, tool, ph;
	int status[NDI_NTOOLS];
	int absent[NDI_NTOOLS];
	double transform[NDI_NTOOLS][8];
	const unsigned long mflags = NDI_TOOL_IN_PORT | NDI_INITIALIZED | NDI_ENABLED;
	double zyx[3];
	double xyz[3];
	double interm[16];

	if (!m_trackingThreadWorking)
	{
		qDebug("Tracking Thread is not started");
		return;
	}
    
	clearLastReserve();   // clear the matrix in each sensor data

	// initialize transformations to identity
	for (tool = 0; tool < NDI_NTOOLS; tool++)
	{
		transform[tool][0] = 1.0;
		transform[tool][1] = transform[tool][2] = transform[tool][3] = 0.0;
		transform[tool][4] = transform[tool][5] = transform[tool][6] = 0.0;
		transform[tool][7] = 0.0;
	}

	// get the transforms for all tools from the NDI
	char *pszTransformInfo = NULL;

	if (m_pSystem)
	{
		pszTransformInfo = ndiCommand(m_pSystem,"TX:0801");  // points to <# of Handles>
	} 
	else
	{
		return;
	}

	errnum = ndiGetError(m_pSystem);

	if (errnum)
	{
		if (errnum == NDI_BAD_CRC || errnum == NDI_TIMEOUT) // common errors
		{
			qDebug(ndiErrorString(errnum));
		}
		else
		{
			qDebug(ndiErrorString(errnum));
		}
		return;
	}

	for (tool = 0; tool < NDI_NTOOLS; tool++)
	{
		ph = m_handle[tool];
		if (ph == 0)
		{
			record[tool].x = 0;
			record[tool].y = 0;
			record[tool].z = 0;
			record[tool].rx = 0;
			record[tool].ry = 0;
			record[tool].rz = 0;
			record[tool].flag = 0;
		    // dataClass.addPtToCollectionData(tool,record[tool]);

			// added by banbanfish
			record[tool].q0 = 1.0;
			record[tool].qx = 0;
			record[tool].qy = 0;
			record[tool].qz = 0;
			record[tool].frame = 0;
			record[tool].error = FLT_MIN;
			record[tool].transformStatus = -1;

			continue;
		}
        
		absent[tool] = ndiGetTXTransform(m_pSystem, ph, transform[tool]);
		ndiTransformToMatrixd(transform[tool],interm);
		ndiAnglesFromMatrixd(zyx,interm);
		ndiCoordsFromMatrixd(xyz,interm);
		transposeMatrix(interm,record[tool].m);
		record[tool].x = xyz[0];
		record[tool].y = xyz[1];
		record[tool].z = xyz[2];
		record[tool].rx = zyx[2]*57.295779513082320876798154814105;
		record[tool].ry = zyx[1]*57.295779513082320876798154814105;
		record[tool].rz = zyx[0]*57.295779513082320876798154814105;
		status[tool] = ndiGetTXPortStatus(m_pSystem, ph);
		record[tool].flag = status[tool] & mflags;
//		if(abs(xyz[1]) > 0)
//		qDebug("x =%6.3f y=%6.3f z=%6.3f,flag=%d\n",record[tool].x,record[tool].y,record[tool].z,record[tool].flag);
//		dataClass.addPtToCollectionData(tool,record[tool]);

		// added by banbanfish
		record[tool].q0 = transform[tool][0];
		record[tool].qx = transform[tool][1];
		record[tool].qy = transform[tool][2];
		record[tool].qz = transform[tool][3];
		record[tool].transformStatus = absent[tool];
		record[tool].frame = ndiGetTXFrame(m_pSystem, ph);
		record[tool].error = transform[tool][7];; // temporary value which would be set in following step
	}

	// set tool errors - added by banbanfish 
	// this also shows a direct way to parse the reply to the TX command
	int noHandles = 0, handle = 0;
	bool disabled = false;
	noHandles = (int)ndiHexToUnsignedLong(pszTransformInfo, 2);
	pszTransformInfo += 2;
	for(int i = 0; i < noHandles; i++)
	{
		handle = (int)ndiHexToUnsignedLong(pszTransformInfo, 2);
		pszTransformInfo += 2;
		disabled = false;
		
		if ( strlen( pszTransformInfo ) < 18 )
			return ;

		if( !strncmp( pszTransformInfo, "MISSING", 7 ) ||
			!strncmp( pszTransformInfo, "DISABLED", 8 ) || 
			!strncmp( pszTransformInfo, "UNOCCUPIED", 10 ))
		{
			if ( !strncmp( pszTransformInfo, "UNOCCUPIED", 10 ))
			{
				pszTransformInfo += 10;					
				disabled = true;
			}
			else if ( !strncmp( pszTransformInfo, "DISABLED", 8 ))
			{
				pszTransformInfo += 8;
				disabled = true;
			}
			else
			{
				pszTransformInfo += 7;
			}			
		}
		else
		{
			if(!bExtractValue(pszTransformInfo + 45, 6, 10000., &record[handle].error))
			{
				return;
			}			
			else
			{
				pszTransformInfo += 51;
			}
		}		
		//get handle status...
		if (!disabled)
		{
			unsigned int unHandleStatus = (unsigned int)ndiHexToUnsignedLong(pszTransformInfo, 8);
			if(unHandleStatus & 0x40)
			{
				record[handle].flag |= 0x40;
			}
			if(unHandleStatus & 0x80)
			{
				record[handle].flag |= 0x80;
			}
				
			pszTransformInfo += 8;
			// get frame number...
			// record[handle].frame = ndiHexToUnsignedLong(pszTransformInfo, 8);
			// std::cout << ndiHexToUnsignedLong(pszTransformInfo, 8) << std::endl;
			pszTransformInfo += 8;
		}
		pszTransformInfo++; // for the carriage return
	}

}

void NdiTrackingThread::transposeMatrix(double in[16], double out[16])
{
	MatPtr inElem = (MatPtr)in;
	MatPtr outElem = (MatPtr)out;
	int i, j;
	double temp;

	for (i=0; i<4; i++)
	{
		for(j=i; j<4; j++)
		{
			temp = inElem[i][j];
			outElem[i][j] = inElem[j][i];
			outElem[j][i] = temp;
		}
	}
}

void NdiTrackingThread::clearLastReserve()
{
	for(int tool = 0; tool < NDI_NTOOLS; ++tool)
	{
		memset(&record[tool].m, 0, 16*sizeof(double));
	}
}

SensorData
NdiTrackingThread::getToolTransform(int tool)
{
   QMutexLocker locker(&m_mutex);
   return record[tool];
  
}

/*****************************************************************
Routine:    bExtractValue

Inputs:	pszVal - value to be extracted, uLen - length of value
		fDivisor - how to break up information, pfValue - data

Returns:

*****************************************************************/
bool NdiTrackingThread::bExtractValue(char *pszVal, unsigned uLen, float fDivisor, float *pfValue)
{
	unsigned i;
    char szBuff[ 10 ];

    *pfValue = FLT_MIN;

    // Make sure that the first character is either a "+" or "-"     
    if( *pszVal != '-' && *pszVal != '+' )
	{
		//std::cout << "no sign!" << std::endl;
		return false;
	}

    // Copy the + or - character to the buffer
    szBuff[0] = *pszVal;

    // Copy the rest of the value.  Make sure that the remainder of the value string contains only digits 0 - 9.
    for( i = 1; i < uLen; i++ )
    {
        if( pszVal[i] < '0' || pszVal[i] > '9' )
		{
			//std::cout << "invalid numbers!" << std::endl;
			return false;
		} 
        szBuff[i] = pszVal[i];
    } 
    szBuff[i] = '\0';

    *pfValue = float(atof( szBuff ) / fDivisor);
    
    return true;
}