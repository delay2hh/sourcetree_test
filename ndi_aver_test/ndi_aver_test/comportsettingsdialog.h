#ifndef COMPORTSETTINGSDIALOG_H
#define COMPORTSETTINGSDIALOG_H

#include <QDialog>

class QComboBox;
class QCheckBox;
class QSettings;
class QGroupBox;

class COMPortSettingsDialog : public QDialog
{
	Q_OBJECT

public:
	COMPortSettingsDialog(QWidget *parent = 0);
	COMPortSettingsDialog(QSettings *ini, QWidget *parent = 0);
	
protected:
	void createUI();

	QComboBox *comPortsComboBox;
	QComboBox *baudRateComboBox;
	QComboBox *dataBitsComboBox;
	QComboBox *parityComboBox;
	QComboBox *stopBitsComboBox;

	QCheckBox *hardWareCheckBox;
	QCheckBox *wirelessCheckBox;
	QCheckBox *resetCheckBox;
	QGroupBox *wirelessTipGroup;

	QSettings *settingsINI;

	bool m_bHardware;
	bool m_bWireless;
	bool m_bReset;
	
	protected slots:
		void accept();
		void wirelessStateChanged();

};

#endif