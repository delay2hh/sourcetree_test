#include <QtUiTools/QtUiTools>

#include <iostream>

#include "mainwindow.h"

#include "comportsettingsdialog.h"
#include "romfiledialog.h"

#include "kneemotionobserver.h"

#include "aver/endogadget.h"

/////////////////////////////////////////////////////////////
// member functions
/////////////////////////////////////////////////////////////
MainWindow::MainWindow()
{
	refreshTimer = new QTimer;

	m_bIsTracking = false;
	m_bResetHardware = false;
	m_bWireless = false;
	m_bSystemInitialized = false;
	m_bPortsActivated = false;
	m_nCOMPort = 0;

	//kneeMotionObserverVisible = false;
	kneeMotionObserverVisible = true;

	//for(int i = 0; i < 4; i++)
	for(int i = 0; i < NUM_ENDO_TOOL; i++)	
	{
		handleDataForKnee[i].valid = false;
		for(int j = 0; j < 16; j++)
		{
			handleDataForKnee[i].m[j] = 0.0;
		}
		for(int j = 0; j < 4; j++)
		{
			handleDataForKnee[i].q[j] = 0.0;
		}
	}

	createINI();
	createActions();
	createMenus(); 
	createUserInterface();		
}

void MainWindow::selectAVerDevice(unsigned int dev)
{
	m_endoGadget->selectCaptureDevice(dev);  // 0 - SD; 1 - HD	
}

void MainWindow::createActions()
{
	// file menu item(s)
	exitAct = new QAction(tr("E&xit"), this);
	exitAct->setShortcut(tr("Alt+F4"));
	connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));

	// settings menu item(s)
	comPortSettingsAct = new QAction(tr("&COM Port Settings"),this);
	comPortSettingsAct->setShortcut(tr("F6"));
	connect(comPortSettingsAct, SIGNAL(triggered()), this, SLOT(comPortSettings()));

	sromImageFileSettingsAct = new QAction(tr("&SROM Image File Settings"), this);
	sromImageFileSettingsAct->setShortcut(tr("F7"));
	connect(sromImageFileSettingsAct, SIGNAL(triggered()), this, SLOT(sromImageFileSettings()));

	// system menu items(s)
	initializeSystemAct = new QAction(tr("&Initialize System"), this);
	initializeSystemAct->setShortcut(tr("F2"));

	activateHandlesAct = new QAction(tr("&Activate Handles"), this);
	activateHandlesAct->setShortcut(tr("F3"));

	startTrackingAct = new QAction(tr("&Start Tracking"), this);
	startTrackingAct->setShortcut(tr("F4"));

	stopTrackingAct = new QAction(tr("S&top Tracking"), this);
	stopTrackingAct->setShortcut(tr("F5"));

	// view menu item(s) 
	systemPropertiesAct = new QAction(tr("System &Properties"), this);
	systemPropertiesAct->setShortcut(tr("F8"));

	diagnosticAlertsAct = new QAction(tr("&Diagnostic Alerts"), this);
	diagnosticAlertsAct->setShortcut(tr("F9"));

	// option menu item(s)
	illuminatorActivationRateAct = new QAction(tr("&Illuminator Activation Rate"), this);

	programOptionsAct = new QAction(tr("&Program options"), this);

	// help menu item(s)
	aboutAct = new QAction(tr("&About SimpleTracking"), this);
	connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));     
}

void MainWindow::createMenus()
{
	// file menu
	fileMenu = menuBar()->addMenu(tr("&File"));
	fileMenu->addSeparator();
	fileMenu->addAction(exitAct);

	// settings menu
	settingsMenu = menuBar()->addMenu(tr("S&ettings"));
	settingsMenu->addAction(comPortSettingsAct);
	settingsMenu->addAction(sromImageFileSettingsAct);

	// system menu
	systemMenu = menuBar()->addMenu(tr("S&ystem"));
	systemMenu->addAction(initializeSystemAct);
	systemMenu->addAction(activateHandlesAct);	 
	systemMenu->addSeparator();
	systemMenu->addAction(startTrackingAct);	 
	systemMenu->addAction(stopTrackingAct);
	activateHandlesAct->setEnabled(false);
	startTrackingAct->setEnabled(false);
	stopTrackingAct->setEnabled(false);

	// view menu
	viewMenu = menuBar()->addMenu(tr("&View"));
	viewMenu->addAction(systemPropertiesAct);
	viewMenu->addAction(diagnosticAlertsAct);
	systemPropertiesAct->setEnabled(false);
	diagnosticAlertsAct->setEnabled(false);     

	// option menu
	optionMenu = menuBar()->addMenu(tr("O&ption"));
	optionMenu->addAction(illuminatorActivationRateAct);
	optionMenu->addSeparator();
	optionMenu->addAction(programOptionsAct);
	illuminatorActivationRateAct->setEnabled(false);

	// help menu
	helpMenu = menuBar()->addMenu(tr("&Help"));
	helpMenu->addAction(aboutAct); 

	// disable all menus 
	// all functions associated with items will be implemented in the future
	// fileMenu->setEnabled(false);
	// settingsMenu->setEnabled(false);
	systemMenu->setEnabled(false);
	viewMenu->setEnabled(false);
	optionMenu->setEnabled(false);
	helpMenu->setEnabled(false);

}

void MainWindow::createUserInterface()
{
	// connect timer signal-slot
	connect(refreshTimer, SIGNAL(timeout()), this, SLOT(on_timer_refreshFrame()));

	QGroupBox *ndiBasicGroupBox = new QGroupBox(tr("NDI Basic Information"), this);

	// system button group
	QGroupBox *systemGroupBox = new QGroupBox(tr("System"), ndiBasicGroupBox);

	resetSystemButton = new QPushButton(tr("Reset System"), this);
	resetSystemButton->setEnabled(false); // temporarily do nothing

	initializeSystemButton = new QPushButton(tr("Initialize System"), this);
	activateHandlesButton = new QPushButton(tr("Activate Handles"), this);
	trackingButton = new QPushButton(tr("Start Tracking"), this);
	closeSystemButton = new QPushButton(tr("Close System"), this);
	
	activateHandlesButton->setEnabled(false);
	trackingButton->setEnabled(false);
	
	connect(resetSystemButton, SIGNAL(clicked()), this, SLOT(resetSystem()));
	connect(initializeSystemButton, SIGNAL(clicked()), this, SLOT(initializeSystem()));
	connect(activateHandlesButton, SIGNAL(clicked()), this, SLOT(activateHandles()));
	connect(trackingButton, SIGNAL(clicked()), this, SLOT(tracking()));
	connect(closeSystemButton, SIGNAL(clicked()), this, SLOT(closeSystem()));
	
	QVBoxLayout *systemButtonsLayout = new QVBoxLayout;	
	systemButtonsLayout->addWidget(resetSystemButton);
	systemButtonsLayout->addWidget(initializeSystemButton);
	systemButtonsLayout->addWidget(activateHandlesButton);
	systemButtonsLayout->addWidget(trackingButton);
	systemButtonsLayout->addWidget(closeSystemButton);
	
	systemGroupBox->setLayout(systemButtonsLayout);
	systemGroupBox->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	// system information
	QGroupBox *systemInformationGroupBox = new QGroupBox(tr("System Information"), ndiBasicGroupBox);
	
	frameEdit = new QLineEdit(this);
	modeEdit = new QLineEdit(this);

	frameEdit->setReadOnly(true);
	modeEdit->setReadOnly(true);

	QGridLayout *systemInfoLayout = new QGridLayout;
	systemInfoLayout->addWidget(new QLabel(tr("Frame")), 0, 0, 1, 1);
	systemInfoLayout->addWidget(frameEdit, 0, 1, 1, 1);
	systemInfoLayout->addWidget(new QLabel(tr("Mode")), 1, 0, 1, 1);
	systemInfoLayout->addWidget(modeEdit, 1, 1, 1, 1);

	systemInformationGroupBox->setLayout(systemInfoLayout);
	systemInformationGroupBox->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    
	// tracking transform information
	QGroupBox *transformInfoGroupBox = new QGroupBox(tr("Transform Information"), ndiBasicGroupBox);
	
	transformInQuaternionList = new QTreeWidget(this);
	transformInQuaternionList->setMinimumSize(350,100);
	transformInQuaternionList->setRootIsDecorated(false);
	QStringList quaternionLabels;
	quaternionLabels << tr("Handle") << tr("Tx") << tr("Ty") << tr("Tz") 
		             << tr("Q0") << tr("Qx") << tr("Qy") << tr("Qz") 
					 << tr("Error") << tr("Status");
	transformInQuaternionList->setHeaderLabels(quaternionLabels);
	transformInQuaternionList->header()->resizeSection(0, 49);
	transformInQuaternionList->header()->resizeSection(1, 65);
	transformInQuaternionList->header()->resizeSection(2, 65);
	transformInQuaternionList->header()->resizeSection(3, 65);
	transformInQuaternionList->header()->resizeSection(4, 65);
	transformInQuaternionList->header()->resizeSection(5, 65);
	transformInQuaternionList->header()->resizeSection(6, 65);
	transformInQuaternionList->header()->resizeSection(7, 65);
	transformInQuaternionList->header()->resizeSection(8, 60);
	transformInQuaternionList->header()->resizeSection(9, 45);
    transformInQuaternionList->header()->setSectionResizeMode(QHeaderView::Fixed);
	
	transformInEulerAngleList = new QTreeWidget(this);
	transformInEulerAngleList->setRootIsDecorated(false);
	QStringList eulerAngleLabels;
	eulerAngleLabels << tr("Handle") << tr("Tx") << tr("Ty") << tr("Tz") 
		             << tr("Roll") << tr("Pitch") << tr("Yaw") 
					 << tr("Error") << tr("Status");
	transformInEulerAngleList->setHeaderLabels(eulerAngleLabels);
	transformInEulerAngleList->header()->resizeSection(0, 49);
	transformInEulerAngleList->header()->resizeSection(1, 65);
	transformInEulerAngleList->header()->resizeSection(2, 65);
	transformInEulerAngleList->header()->resizeSection(3, 65);
	transformInEulerAngleList->header()->resizeSection(4, 65);
	transformInEulerAngleList->header()->resizeSection(5, 65);
	transformInEulerAngleList->header()->resizeSection(6, 65);
	transformInEulerAngleList->header()->resizeSection(7, 60);
	transformInEulerAngleList->header()->resizeSection(8, 45);	
    transformInEulerAngleList->header()->setSectionResizeMode(QHeaderView::Fixed);
	
	QTabWidget *transformInfoTab = new QTabWidget(transformInfoGroupBox);
	
	QWidget *tabPage1 = new QWidget;
	QVBoxLayout *tabPage1Layout = new QVBoxLayout;
	tabPage1Layout->addWidget(transformInQuaternionList);
	tabPage1->setLayout(tabPage1Layout);
	
	QWidget *tabPage2 = new QWidget;	
	QVBoxLayout *tabPage2Layout = new QVBoxLayout;
	tabPage2Layout->addWidget(transformInEulerAngleList);
	tabPage2->setLayout(tabPage2Layout);
    
	transformInfoTab->addTab(tabPage1, tr("Transform In Quaternion"));
	transformInfoTab->addTab(tabPage2, tr("Transform In Euler Angle"));
	
	QGroupBox *statusLegendGroupBox = new QGroupBox(tr("Status Legend"), transformInfoGroupBox);
	QLabel *oovStatusLegendLabel = new QLabel(tr("OOV - Out Of Volume"), statusLegendGroupBox);
	QLabel *poovStatusLegendLabel = new QLabel(tr("POOV - Partially Out Of Volume"), statusLegendGroupBox);
	QLabel *okStatusLegendLabel = new QLabel(tr("OK - Tool is Tracking in Volume"), statusLegendGroupBox);
	QVBoxLayout *statusLegendLayout = new QVBoxLayout;
	statusLegendLayout->addWidget(oovStatusLegendLabel);
	statusLegendLayout->addWidget(poovStatusLegendLabel);
	statusLegendLayout->addWidget(okStatusLegendLabel);
	statusLegendGroupBox->setLayout(statusLegendLayout);
	
	QVBoxLayout *transformInfoLayout = new QVBoxLayout;
	transformInfoLayout->addWidget(transformInfoTab);
	transformInfoLayout->addWidget(statusLegendGroupBox);
	transformInfoGroupBox->setLayout(transformInfoLayout);
	transformInfoGroupBox->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	
	// ndi basic group
	QGridLayout *ndiBasicInfoLayout = new QGridLayout;
	ndiBasicInfoLayout->addWidget(systemGroupBox, 0, 0, 1, 1);
	ndiBasicInfoLayout->addWidget(systemInformationGroupBox, 0, 1, 1, 1);
    ndiBasicInfoLayout->addWidget(transformInfoGroupBox, 1, 0, 1, 3);
	ndiBasicGroupBox->setLayout(ndiBasicInfoLayout);
	ndiBasicGroupBox->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	// extended function(s) group
	//QGroupBox *extendedFunctionsGroup = new QGroupBox(tr("Extended Functions"), this);

	//observeKneeMotionButton = new QPushButton(tr("Knee Motion Observe >>"), this);
	//observeKneeMotionButton = new QPushButton(tr("Endoscopy Cali. && A.R. Observe >>"), this);
	//connect(observeKneeMotionButton, SIGNAL(clicked()), this, SLOT(observeKneeMotion()));
	kneeMotionObserver = new KneeMotionObserver(this);
	connect(kneeMotionObserver, SIGNAL(testPointListUpdated()), this, SLOT(updateTestPointList()));
	connect(kneeMotionObserver, SIGNAL(testPointListInvalid()), this, SLOT(clearTestPointList()));

	//QGridLayout *extendedFunctionsLayout = new QGridLayout;
	//extendedFunctionsLayout->addWidget(observeKneeMotionButton, 0, 0, 1, 1);	
	//extendedFunctionsGroup->setLayout(extendedFunctionsLayout);
	//extendedFunctionsGroup->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	m_endoGadget = new EndoGadget(true, this);	

	// get a pointer to the video capture device
	kneeMotionObserver->setEndoGadget(m_endoGadget);

	QWidget *widget = new QWidget;
	QVBoxLayout *basicVLayout = new QVBoxLayout;
	QHBoxLayout *basicHLayout = new QHBoxLayout;
	basicVLayout->addWidget(ndiBasicGroupBox);
	basicVLayout->addStretch(1);
	//basicLayout->addWidget(extendedFunctionsGroup);
	basicHLayout->addLayout(basicVLayout);
	basicHLayout->addWidget(m_endoGadget);
	basicHLayout->addStretch(1);
	widget->setLayout(basicHLayout);

	QWidget *kneeMotionWidget = new QWidget;
	QVBoxLayout *kneeMotionVLayout = new QVBoxLayout;
	QHBoxLayout *kneeMotionHLayout = new QHBoxLayout;
	kneeMotionObserver->setVisible(kneeMotionObserverVisible);
	kneeMotionHLayout->addWidget(kneeMotionObserver);
	kneeMotionHLayout->addStretch(1);
	kneeMotionVLayout->addLayout(kneeMotionHLayout);
	kneeMotionVLayout->addStretch(1);
	kneeMotionWidget->setLayout(kneeMotionVLayout);
	
	//QSplitter *mainSplitter = new QSplitter(Qt::Horizontal);
	QSplitter *mainSplitter = new QSplitter(Qt::Vertical);
	mainSplitter->addWidget(widget);
	mainSplitter->addWidget(kneeMotionWidget);
	mainSplitter->addWidget(new QWidget);
	mainSplitter->setStretchFactor(2, 1);  // this blank widget will contain the extended space
	setCentralWidget(mainSplitter);	
}

void MainWindow::createINI()
{
    QString iniFileName("simpletracking.ini");
	QFileInfo fileInfo(iniFileName);
	if(fileInfo.exists())
	{
		std::cout << "open the "<< iniFileName.toStdString() <<" file!" << std::endl;
	}
	else
	{
		std::cout << "create the "<< iniFileName.toStdString() <<" file!" << std::endl;
		QFile file(iniFileName);
		file.open(QIODevice::ReadWrite);
		file.close();
	}

	settingsINI = new QSettings(iniFileName, QSettings::IniFormat);	
}

void MainWindow::updateTrackingInfo()
{
    // update the tracking information list
	transformInQuaternionList->clear();
    transformInEulerAngleList->clear();
	
	// handle tx ty tz rx ry rz error status
	// handle tx ty tz q0 qx qy qz error status
	
	for(int i = 0; i < NDI_NTOOLS; i++)
	{
		if(m_ndiSystem.isPortEnabled(i))
		{
			SensorData s;
			m_ndiSystem.getToolTransform(i,s);

            QString strPortID = QString("%1").arg(m_ndiSystem.getPortHandle(i));
			QStringList itemContentq = QStringList() << strPortID;  // for quaternion
			QStringList itemContente = QStringList() << strPortID;  // for euler angle

			if(s.transformStatus != NDI_OKAY)
			{
				if(s.transformStatus == NDI_MISSING)
				{
					// show missing and update the frame number					
					QString strMissing = QString("Missing");
					itemContentq << strMissing; 
					itemContente << strMissing;	

					// update the frame number
					frameEdit->setText(QString("%1").arg(s.frame));
				}
				else
				{
					// show missing and update the frame number					
					QString strDisabled = QString("Disabled");
					itemContentq << strDisabled; 
					itemContente << strDisabled;						
				}
				itemContentq << QString("--") << QString("--")
						<< QString("--") << QString("--") << QString("--") << QString("--")
						<< QString("--");
				itemContente << QString("--") << QString("--")
						<< QString("--") << QString("--") << QString("--")
						<< QString("--");
				if(s.flag & 0x80)
				{
					itemContentq << QString("POOV");
					itemContente << QString("POOV");
				}
				else if(s.flag & 0x40)
				{
					itemContentq << QString("OOV");
					itemContente << QString("OOV");
				}
				else
				{
					itemContentq << QString("--");
					itemContente << QString("--");
				}		
			}
			else
			{
				QString strTx = QString("%1").arg(s.x, 0, 'f', 2);
				QString strTy = QString("%1").arg(s.y, 0, 'f', 2);
				QString strTz = QString("%1").arg(s.z, 0, 'f', 2);
				QString strRx = QString("%1").arg(s.rx, 0, 'f', 4);
				QString strRy = QString("%1").arg(s.ry, 0, 'f', 4);
				QString strRz = QString("%1").arg(s.rz, 0, 'f', 4);
				QString strQ0 = QString("%1").arg(s.q0, 0, 'f', 4);
				QString strQx = QString("%1").arg(s.qx, 0, 'f', 4);
				QString strQy = QString("%1").arg(s.qy, 0, 'f', 4);
				QString strQz = QString("%1").arg(s.qz, 0, 'f', 4);
				QString strError = QString("%1").arg(s.error, 0, 'f', 4);

				itemContentq << strTx << strTy << strTz
					<< strQ0 << strQx << strQy << strQz
					<< strError;
				itemContente << strTx << strTy << strTz
					<< strRx << strRy << strRz
					<< strError;

				if(s.flag & 0x80)
				{
					itemContentq << QString("POOV");
					itemContente << QString("POOV");
				}
				else if(s.flag & 0x40)
				{
					itemContentq << QString("OOV");
					itemContente << QString("OOV");
				}
				else
				{
					itemContentq << QString("OK");
					itemContente << QString("OK");
				}	

				// update the frame number
				frameEdit->setText(QString("%1").arg(s.frame));
			}
			
			QTreeWidgetItem *itemq = new QTreeWidgetItem(itemContentq);  // for quaternion
			QTreeWidgetItem *iteme = new QTreeWidgetItem(itemContente);  // for euler angle
			transformInQuaternionList->addTopLevelItem(itemq);
			transformInEulerAngleList->addTopLevelItem(iteme);

			// update handles for extented function
			if(kneeMotionObserverVisible)
			{
				//if(i < 4)
				if(i < NUM_ENDO_TOOL)
				{
					if(s.transformStatus == NDI_OKAY)
					{
						handleDataForKnee[i].valid = true;
						handleDataForKnee[i].frame = s.frame;
						for(int j = 0; j < 16; j++)
						{
							handleDataForKnee[i].m[j] = s.m[j];
						}
						handleDataForKnee[i].q[0] = s.q0;
						handleDataForKnee[i].q[1] = s.qx;
						handleDataForKnee[i].q[2] = s.qy;
						handleDataForKnee[i].q[3] = s.qz;
					}
					else
					{
						handleDataForKnee[i].valid = false;
					}
				}
			}
		}		
	}
}

void MainWindow::updateKneeMotionObserver()
{
	kneeMotionObserver->updateObserver(handleDataForKnee);
}

///////////////////////////////////////////////////////////
// slots
//////////////////////////////////////////////////////////
void MainWindow::comPortSettings()
{
	COMPortSettingsDialog comPortSettingsDialog(settingsINI);
	comPortSettingsDialog.exec();
}

void MainWindow::sromImageFileSettings()
{
	ROMFileDialog romFileDialog(settingsINI);
	
	initializeSystem();	
	romFileDialog.setNoActivePorts(m_ndiSystem.getActivePortNum());	
	romFileDialog.setNoPassivePorts(m_ndiSystem.getPassivePortNum());
	//romFileDialog.setNoActivePorts(3);
	//romFileDialog.setNoPassivePorts(9);
	romFileDialog.fillROMFileTable();

	romFileDialog.exec();
}

void MainWindow::about()
{
	QMessageBox::about(this, tr("About SimpleTracking"),
		tr("The <b>SimpleTracking</b> example shows\n"
		"how to programming with NDI API"));
} 

void MainWindow::resetSystem()
{
	// temporarily do nothing
}

void MainWindow::initializeSystem()
{
	int	nBaudRate = 0, 
		nStopBits = 0,
		nParity   = 0,
		nDataBits = 0,
		nHardware = 0;
		
	// read the COM port parameters from the ini file
	nBaudRate = settingsINI->value("Communication/Baud Rate").toInt();
	nStopBits = settingsINI->value("Communication/Stop Bits").toInt(); 
	nParity = settingsINI->value("Communication/Parity").toInt(); 
	nDataBits = settingsINI->value("Communication/Data Bits").toInt();
	nHardware = settingsINI->value("Communication/Hardware").toInt();
	m_nCOMPort = settingsINI->value("Communication/COM Port").toInt();
	m_bWireless = settingsINI->value("Communication/Wireless").toBool();
	// This feature is useful for debugging only, m_bResetHardware is set to TRUE to disable it. 
    m_bResetHardware = settingsINI->value("Communication/Reset Hardware").toBool();	

	m_ndiSystem.setSerialPort(m_nCOMPort + 1);
	/*if(!m_ndiSystem.probe())
	{
		QMessageBox::information(this, tr("COM Error"),
			tr("Check your COM Port settings and\nmake sure you system is turned on."));
		return;
	}*/

	if(m_bResetHardware)
	{
		// hardWareReset function to be implemented
		if(!m_ndiSystem.resetHareware())
			return;
	}

	if(m_ndiSystem.setCOMParams(nBaudRate, nDataBits, nParity, nStopBits, nHardware))
	{
		std::cout << "set COM parameters" << std::endl;
		
		if(m_ndiSystem.initializeSystem())
		{
			std::cout << "initialize the system" << std::endl;

			if(m_ndiSystem.getSystemInfo())
			{
				std::cout << "get system information" << std::endl;
			}

			m_bSystemInitialized = true;
			activateHandlesButton->setEnabled(true);
		}
	}
	
}

void MainWindow::activateHandles()
{
	if(!m_bSystemInitialized)
		return;

	// get the amount of tool ports
	int activePortsNum = m_ndiSystem.getActivePortNum();
	int passivePortsNum = m_ndiSystem.getPassivePortNum();

	// load SROM files
	int tool = 0;
    // active tools
	for(int i = 1; i <= activePortsNum; i++)
	{
		QString strPortID = QString("Port %1").arg(i);
		QString romFileName = settingsINI->value(QString("POLARIS SROM Image Files/%1").arg(strPortID)).toString();
		if(!romFileName.isEmpty())
		{
			m_ndiSystem.loadVirtualSROM(tool, const_cast<char*>(romFileName.toStdString().c_str()));
		}
		tool++;		
	}
    // passive tools
	for(int i = 1; i <= passivePortsNum; i++)
	{
		QString strPortID = QString("Wireless Tool %1").arg(i,2);
		QString romFileName = settingsINI->value(QString("POLARIS SROM Image Files/%1").arg(strPortID)).toString();
		if(!romFileName.isEmpty())
		{
			m_ndiSystem.loadVirtualSROM(tool, const_cast<char*>(romFileName.toStdString().c_str()));
		}
		tool++;
	}

	if(m_ndiSystem.activeAllPorts())
	{
		m_bPortsActivated = true;
		trackingButton->setEnabled(true);
		transformInQuaternionList->clear();
		transformInEulerAngleList->clear();

		for(int i = 0; i < NDI_NTOOLS; i++)
		{
			if(m_ndiSystem.isPortEnabled(i))
			{
				QString strPortID = QString("%1").arg(m_ndiSystem.getPortHandle(i));
				QStringList itemContent = QStringList() << strPortID;
				QTreeWidgetItem *itemq = new QTreeWidgetItem(itemContent);  // for quaternion
				QTreeWidgetItem *iteme = new QTreeWidgetItem(itemContent);  // for euler angle
				transformInQuaternionList->addTopLevelItem(itemq);
				transformInEulerAngleList->addTopLevelItem(iteme);
			}			
		}
	}
	
}

void MainWindow::tracking()
{
	if(!m_bPortsActivated)
		return;

	// if the text read Start, then we start, else we stop tracking
	if ( trackingButton->text() == "Start Tracking" )
		startTracking();
	else
		stopTracking();
}

void MainWindow::startTracking()
{
	if(m_ndiSystem.startTracking_banban())
	{
		m_bIsTracking = true;
		trackingButton->setText("Stop Tracking");
		refreshTimer->start(40);
	}
}

void MainWindow::stopTracking()
{
	if(m_ndiSystem.stopTracking_banban())
	{
		m_bIsTracking = false;
		trackingButton->setText("Start Tracking");
		refreshTimer->stop();
	}
}

void MainWindow::closeSystem()
{
	m_ndiSystem.closeSystem();
}

void MainWindow::on_timer_refreshFrame()
{
	/*SensorData s;
	if(!m_ndiSystem.getToolTransform(0,s))
	{
		std::cout << "no transform data!" << std::endl;
		return;
	}
	std::cout << s.x << ' ' << s.y << ' ' << s.z << std::endl;*/
	updateTrackingInfo();

	updateKneeMotionObserver();
}

//void MainWindow::observeKneeMotion()
//{
//	if(kneeMotionObserverVisible)
//	{
//		kneeMotionObserverVisible = false;
//		kneeMotionObserver->setVisible(false);
//		//observeKneeMotionButton->setText("Knee Motion Observe >>");
//		observeKneeMotionButton->setText("Endoscopy Cali. & A.R. Observe >>");
//	}
//	else
//	{
//		kneeMotionObserverVisible = true;
//		kneeMotionObserver->setVisible(true);
//		//observeKneeMotionButton->setText("Knee Motion Observe <<");
//		observeKneeMotionButton->setText("Endoscopy Cali. & A.R. Observe <<");
//	}
//}

void MainWindow::updateTestPointList()
{
	m_endoGadget->updateTestPoint(kneeMotionObserver->getTestPointList2d());
}

void MainWindow::clearTestPointList()
{
	m_endoGadget->clearTestPointList();
}



