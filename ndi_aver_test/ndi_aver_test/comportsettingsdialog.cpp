#include <QtUiTools/QtUiTools>

#include <iostream>

#include "comportsettingsdialog.h"

COMPortSettingsDialog::COMPortSettingsDialog(QWidget *parent)
    : QDialog(parent)
{
	
}

COMPortSettingsDialog::COMPortSettingsDialog(QSettings *ini, QWidget *parent)
    : QDialog(parent)
{
	settingsINI = ini;
	createUI();
}

void COMPortSettingsDialog::createUI()
{
	int	nCOMPort, nBaudRate, nStopBits,	nParity, nDataBits;

    nCOMPort = settingsINI->value("Communication/COM Port").toInt();
	nBaudRate = settingsINI->value("Communication/Baud Rate").toInt();
	nStopBits = settingsINI->value("Communication/Stop Bits").toInt(); 
	nParity = settingsINI->value("Communication/Parity").toInt(); 
	nDataBits = settingsINI->value("Communication/Data Bits").toInt();
	m_bHardware = settingsINI->value("Communication/Hardware").toBool();
	m_bReset = settingsINI->value("Communication/Reset Hardware").toBool();
	m_bWireless = settingsINI->value("Communication/Wireless").toBool();

	comPortsComboBox = new QComboBox(this);
	QStringList comPortsList;
	for(int i = 1; i <= 4; i++)
	{
		comPortsList << QString("COM%1").arg(i);
	}
	comPortsComboBox->addItems(comPortsList);
	comPortsComboBox->setCurrentIndex(nCOMPort);

	baudRateComboBox = new QComboBox(this);
	QStringList baudRateList;
	baudRateList << QString("9600") << QString("14400") << QString("19200") 
		         << QString("38400") << QString("57600") << QString("115200");
	baudRateComboBox->addItems(baudRateList);
	baudRateComboBox->setCurrentIndex(nBaudRate);

	dataBitsComboBox = new QComboBox(this);
	QStringList dataBitsList;
    dataBitsList << QString("8 bits") << QString("7 bits");
	dataBitsComboBox->addItems(dataBitsList);
	dataBitsComboBox->setCurrentIndex(nDataBits);

	parityComboBox = new QComboBox(this);
	QStringList parityList;
    parityList << QString("None") << QString("Odd") << QString("Even");
	parityComboBox->addItems(parityList);
	parityComboBox->setCurrentIndex(nParity);

	stopBitsComboBox = new QComboBox(this);
	QStringList stopBitsList;
	stopBitsList << QString("1 bit") << QString("2 bits");
	stopBitsComboBox->addItems(stopBitsList);
	stopBitsComboBox->setCurrentIndex(nStopBits);

	hardWareCheckBox = new QCheckBox(this);
	hardWareCheckBox->setText(tr("Hardware handshaking"));
	hardWareCheckBox->setChecked(m_bHardware);

	wirelessCheckBox = new QCheckBox(this);
	wirelessCheckBox->setText(tr("Wireless Communication Port"));
	wirelessCheckBox->setChecked(m_bWireless);

	resetCheckBox = new QCheckBox(this); 
	resetCheckBox->setText(tr("Reset hardware upon initialization"));
	resetCheckBox->setChecked(m_bReset);

	// layout
	QGridLayout* comboBoxLayout = new QGridLayout;
	comboBoxLayout->addWidget(new QLabel(tr("COM Port")), 0, 0, 1, 1);
	comboBoxLayout->addWidget(comPortsComboBox, 0, 1, 1, 1);
    comboBoxLayout->addWidget(new QLabel(tr("Baud Rate")), 1, 0, 1, 1);
	comboBoxLayout->addWidget(baudRateComboBox, 1, 1, 1, 1);
	comboBoxLayout->addWidget(new QLabel(tr("Data Bits")), 2, 0, 1, 1);
	comboBoxLayout->addWidget(dataBitsComboBox, 2, 1, 1, 1);
	comboBoxLayout->addWidget(new QLabel(tr("Parity")), 3, 0, 1, 1);
	comboBoxLayout->addWidget(parityComboBox, 3, 1, 1, 1);
	comboBoxLayout->addWidget(new QLabel(tr("Stop Bits")), 4, 0, 1, 1);
	comboBoxLayout->addWidget(stopBitsComboBox, 4, 1, 1, 1);

	QVBoxLayout* checkBoxLayout = new QVBoxLayout;
	QLabel *wirelessTip = new QLabel;
	wirelessTip->setText("When communicating wirelessly,\n"
		                 "make sure a SERIAL BREAK is not issued.\n"
						 "Instead use the RESET 0 command.");
	wirelessTipGroup = new QGroupBox("Wireless Tips");
    QVBoxLayout *wirelessTipGroupLayout = new QVBoxLayout;
	wirelessTipGroupLayout->addWidget(wirelessTip);
	wirelessTipGroup->setLayout(wirelessTipGroupLayout);
	checkBoxLayout->addWidget(wirelessTipGroup);
	checkBoxLayout->addWidget(hardWareCheckBox);
	checkBoxLayout->addWidget(wirelessCheckBox);
	checkBoxLayout->addWidget(resetCheckBox);
	connect(wirelessCheckBox, SIGNAL(stateChanged(int)), this, SLOT(wirelessStateChanged()));
	wirelessTipGroup->setVisible(m_bWireless);

	QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
	connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
	connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

	QVBoxLayout *mainLayout = new QVBoxLayout;
	mainLayout->addLayout(comboBoxLayout);
	mainLayout->addLayout(checkBoxLayout);
	mainLayout->addWidget(buttonBox);

	setLayout(mainLayout);	
}

///////////////////////////////////////////////////////////////////
// slots
//////////////////////////////////////////////////////////////////
void COMPortSettingsDialog::accept()
{
	m_bHardware = hardWareCheckBox->isChecked();
	m_bReset = resetCheckBox->isChecked();
	m_bWireless = wirelessCheckBox->isChecked();

	settingsINI->setValue("Communication/COM Port", comPortsComboBox->currentIndex());
	settingsINI->setValue("Communication/Baud Rate", baudRateComboBox->currentIndex());
	settingsINI->setValue("Communication/Stop Bits", stopBitsComboBox->currentIndex());
	settingsINI->setValue("Communication/Parity", parityComboBox->currentIndex());
	settingsINI->setValue("Communication/Data Bits", dataBitsComboBox->currentIndex());
	settingsINI->setValue("Communication/Hardware", m_bHardware ? 1 : 0);
	settingsINI->setValue("Communication/Reset Hardware", m_bReset ? 1 : 0);
	settingsINI->setValue("Communication/Wireless", m_bWireless ? 1 : 0);
	
	QDialog::accept();
}

void COMPortSettingsDialog::wirelessStateChanged()
{
	m_bWireless = wirelessCheckBox->isChecked();
	wirelessTipGroup->setVisible(m_bWireless);
	resetCheckBox->setVisible(!m_bWireless);
}
