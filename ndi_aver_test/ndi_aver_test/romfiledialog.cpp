#include <QtUiTools/QtUiTools>
#include <QHeaderView>
#include <QTreeView>

#include <iostream>

#include "romfiledialog.h"

ROMFileDialog::ROMFileDialog(QWidget *parent)
   : QDialog(parent)
{
	//
}

ROMFileDialog::ROMFileDialog(QSettings *ini, QWidget *parent)
   : QDialog(parent)
{
	settingsINI = ini;

	m_bChangesSaved = false;
	m_nNoActivePorts = 0;
	m_nNoPassivePorts = 0;
	
	curROMFile = "";
	curPortID = "";
	
	createUI();
}

void ROMFileDialog::createUI()
{
	// rom file list
	romFileList = new QTreeWidget(this);
	romFileList->setMinimumSize(400,100);
	QStringList romFileLabels;
	romFileLabels << tr("Port ID") << tr("SROM Image File");
	romFileList->setHeaderLabels(romFileLabels);
	romFileList->header()->resizeSection(0, 100);
	romFileList->header()->resizeSection(1, 300);
    romFileList->header()->setSectionResizeMode(QHeaderView::Fixed);
	connect(romFileList, SIGNAL(itemSelectionChanged()), this, SLOT(selectROMFileListItem()));	

	// rom file operation
	portIDEdit = new QLineEdit(this);
	portIDEdit->setReadOnly(true);	
	romFileEdit = new QLineEdit(this);  	
	romFileEdit->setReadOnly(true);
	openROMFileButton = new QPushButton(tr("..."), this);
	openROMFileButton->setEnabled(false);
	saveButton = new QPushButton(tr("Save"), this);
	saveButton->setEnabled(false);

	QGridLayout *romFileLayout = new QGridLayout;
	romFileLayout->addWidget(new QLabel(tr("Port ID")), 0, 0, 1, 2);
	romFileLayout->addWidget(portIDEdit, 0, 2, 1, 2);
	romFileLayout->addWidget(new QLabel(tr("SROM Image File")), 1, 0, 1, 2);
	romFileLayout->addWidget(romFileEdit, 1, 2, 1, 6);
	romFileLayout->addWidget(openROMFileButton, 1, 8, 1, 1);
	romFileLayout->addWidget(saveButton, 2, 3, 1, 3);

	connect(openROMFileButton, SIGNAL(clicked()), this, SLOT(openROMFile()));
	connect(saveButton, SIGNAL(clicked()), this, SLOT(saveChanges()));

	QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Close);
	connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

	QGroupBox *sromFileGroupBox = new QGroupBox(tr(""), this);
	QVBoxLayout *sromFileGroupLayout = new QVBoxLayout;
	sromFileGroupLayout->addWidget(romFileList);
	sromFileGroupLayout->addLayout(romFileLayout);
	sromFileGroupBox->setLayout(sromFileGroupLayout);

	QVBoxLayout *mainLayout = new QVBoxLayout;
	mainLayout->addWidget(sromFileGroupBox);
	mainLayout->addWidget(buttonBox);
	setLayout(mainLayout);

	setWindowTitle(tr("SROM Image Files"));
}

void ROMFileDialog::fillROMFileTable()
{
	// if the system supports 4 active ports show 12 for the case that the TDS is plugged in and active
	if ( m_nNoActivePorts == 4 )
		m_nNoActivePorts = 12;

	// for the number of active ports supported
	for (int i = 1; i <= m_nNoActivePorts; i++)
	{
		// TDS expansion port shall not be treated as a normal port.
		if( i == 4 )
		{
			QString strPortID = QString("Port %1").arg(i);
			QTreeWidgetItem *romFileListItem = 
				new QTreeWidgetItem(romFileList, QStringList()<< strPortID << QString("TDS Expansion Port"));
			romFileList->addTopLevelItem(romFileListItem);			
			continue;
		}

		QString strPortID = QString("Port %1").arg(i);
		QString romFileName = settingsINI->value(QString("POLARIS SROM Image Files/%1").arg(strPortID)).toString();
		QTreeWidgetItem *romFileListItem = 
				new QTreeWidgetItem(romFileList, QStringList()<< strPortID << romFileName);
		romFileList->addTopLevelItem(romFileListItem);		
	}

	// for the number of passive ports supported
	for ( int i = 1; i <= m_nNoPassivePorts; i++ )
	{
		QString strPortID = QString("Wireless Tool %1").arg(i,2);
		QString romFileName = settingsINI->value(QString("POLARIS SROM Image Files/%1").arg(strPortID)).toString();
		QTreeWidgetItem *romFileListItem = 
			new QTreeWidgetItem(romFileList, QStringList()<< strPortID << romFileName);
		romFileList->addTopLevelItem(romFileListItem);		
	} 
	 	
	romFileList->resizeColumnToContents(0);
	romFileList->resizeColumnToContents(1);
}

////////////////////////////////////////////////////////
// slots
////////////////////////////////////////////////////////
void ROMFileDialog::openROMFile()
{
	QString fileName = QFileDialog::getOpenFileName(this,
		tr("Open SROM Image File"),
		QDir::current().canonicalPath(),
		tr("ROM Files (*.rom *.ROM)"));
	if(!fileName.isEmpty())
	{
		romFileEdit->setText(fileName);
		curROMFile = fileName;
	}	
}

void ROMFileDialog::saveChanges()
{
	if(settingsINI)
	{
		bool bValidFile = true;

		QFileInfo romFile(curROMFile);
		if(!romFile.exists())
		{
			QMessageBox message(QMessageBox::Information, tr("Information"), tr("Invalid rom file!"));
			bValidFile = false;
		} 

		if (bValidFile)
		{	
			openROMFileButton->setEnabled(false);
			romFileList->currentItem()->setText(1,curROMFile);
			settingsINI->setValue(QString("POLARIS SROM Image Files/%1").arg(curPortID), curROMFile);			
            m_bChangesSaved = true;
			curROMFile = "";
			romFileEdit->setText(curROMFile);			
		} 
	}
	else
	{
		std::cout << "no ini file!" << std::endl;
		return;
	}
}

void ROMFileDialog::selectROMFileListItem()
{
	QTreeWidgetItem* item = romFileList->currentItem();
	curPortID = item->text(0);
	curROMFile = item->text(1);
	portIDEdit->setText(curPortID);
	romFileEdit->setText(curROMFile);

	openROMFileButton->setEnabled(true);
	saveButton->setEnabled(true);
	// the following section is to handle the special TDS expansion port case - Also make sure it's an aurora system!
	// ignoring... 
}
