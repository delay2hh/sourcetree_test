#ifndef KNEEMOTIONOBSERVER_H
#define KNEEMOTIONOBSERVER_H

#include <QWidget>
#include <QImage>
#include <QString>
#include <QTextStream>

#include <QMutex>

#include <array>
#include <vector>

class QPushButton;
class QLabel;
class QProgressBar;
class QTableWidget;
class QLineEdit;

class EndoGadget;

#include "auxdatastructure.h"
#include "tsai\tsai_method.h"
///////////////////////////////////////////////////////
// controls
////////////////////////////////////////////////////////

///////////////////////////////////////////////////////
// control for point setting
///////////////////////////////////////////////////////
class SetPointControl : public QWidget
{
	Q_OBJECT

public:
	SetPointControl(int i, QWidget *parent = 0);
	void setPointStatus(bool valid);
	bool pointStatus() { return point_status; }

signals:
	void getPoint(int index);
	void resetPoint(int index);
			
protected:
	void createUI();

protected slots:
	void getButtonSlot();
	void resetButtonSlot();

private:
	QPushButton *getPointButton;
	QPushButton *resetPointButton;
	QLabel *pointIndexLabel;
	QLabel *pointStatusLabel;

	bool point_status;
	int index;
	QImage greenPass, yellowWarning, redAlert;
};

//////////////////////////////////////////////////////////
// control for tool status display
//////////////////////////////////////////////////////////
class ToolStatusControl : public QWidget
{
public:
	ToolStatusControl(QString& name, QWidget *parent = 0);
	QString& toolName() { return tool_name; }
	void setToolStatus(bool valid);
	bool toolStatus() { return tool_status; }

protected:
	void createUI();

private:
	QLabel *toolStatusLabel;
	QLabel *toolNameLabel;

	QImage greenPass, yellowWarning, redAlert;

	QString tool_name;
	bool tool_status;
};


//////////////////////////////////////////////////////
// Knee Motion Observer
//////////////////////////////////////////////////////
struct imgPtd
{
	double x;
	double y;
};  // image point with double precision

struct objPtd
{
	double x;
	double y;
	double z;
};  // object point with double precision

class KneeMotionObserver : public QWidget
{
	Q_OBJECT

public:
	KneeMotionObserver(QWidget *parent = 0);
	~KneeMotionObserver();
	
	//void updateObserver(const HandleData handles[4]);
	void updateObserver(const HandleData handles[NUM_ENDO_TOOL]);

	void setEndoGadget(EndoGadget* device);
	
	const std::vector< std::array<double, 3> >& getTestPointList2d();

signals:
	void testPointListUpdated();
	void testPointListInvalid();

protected slots:
	void requirePointSetting(int index);
	void requirePointReset(int index);
	void setUseProbeOffset(bool used);
	void setUseEndoOffset(bool used);
	//void calculateRefCOS();
	void calculateLocalTransform();
	void loadLocalTransform();
	void requireCaptureEndoImageAndTransform();
	void loadCapturedResult();
	void resetCapturedResult();
	void endoCalibration();
	void endoCalibratedResultTest();

protected:
	void createUI();
	void getPoint();
	void updatePointData();
	//void updateRefTransInfo();
	//void recordRelativeTransform(int index);
	//void updateRelativeTransInfo(int index);
	//void updateKneeMotionInfo();
	//void updateKneeMotionInfoDisplay();

	//void createCOSInfoStream();
	//void createKneeMotionDataStream();

	void captureEndoImageAndTransform();

	// auxiliary math function
	double normalOfVector(double a[3]);
	double dotProductOfVectors(double a[3], double b[3]);
    void crossProductOfVectors(double a[3], double b[3], double r[3]);
	void inverseTransform(double a[16], double b[16]); // only for orthogonal rotation matrix inversion
	void multiplyTransform(double a[16], double b[16], double c[16]);
	void pointTransform(double a[3], double m[16], double b[3]);

	// image analysis
	void endoImageAnalysis();
	void endoCalibration_tsai();

	// for test
	void undistorted_to_distorted_sensor_coord(double Xu, double Yu, double *Xd, double *Yd);
	void world_coord_to_image_coord(double xw, double yw, double zw, double *Xf, double *Yf);
	void distorted_image_plane_error_stats(double* mean, double* stddev, double* max, double* sse, calibration_data* cd);

	void world_coord_to_image_coord_heik(double xw, double yw, double zw, double *Xf, double *Yf);
	void distorted_image_plane_error_stats_heik(double* mean, double* stddev, double* max, double* sse, calibration_data* cd);

	void world_coord_to_image_coord_opencv(double xw, double yw, double zw, double *Xf, double *Yf);
	void distorted_image_plane_error_stats_opencv(double* mean, double* stddev, double* max, double* sse, calibration_data* cd);
	
	void updateTestPointList();
	
private:
	
	// constants
	//enum { TOOLNUM = 4, POINTNUM = 5, POINTCOMPONENTNUM = 6, COLLECTIONNUM = 20 };
	enum { TOOLNUM = 3, POINTNUM = 4, POINTCOMPONENTNUM = 6, COLLECTIONNUM = 20 };

	//enum { CALIARRAY_ROWNUM = 8, CALIARRAY_COLNUM = 10 };
	enum { CALIARRAY_ROWNUM = 31, CALIARRAY_COLNUM = 41 };
	enum { CALIARRAY_ORIGIN_ROW = 13, CALIARRAY_ORIGIN_COL = 19 };

	ToolStatusControl* toolStatusControls[TOOLNUM];
	SetPointControl* setpointControls[POINTNUM];

	HandleData handleData[TOOLNUM];

	QProgressBar *getPointProgressBar;
	QTableWidget *pointsInfoTable;
	
	QLineEdit *probeOffsetEdit[3];
    double probeOffset[4];
	bool useProbeOffset;

	QLineEdit *endoOffsetEdit[3];
	double endoOffset[4];
	bool useEndoOffset;

	double m_safe_dist;  // safe distance for test

	// observer internal status
	bool isGettingPoint;
	bool isKneeMotionTracking;
	int collectedPointNum;
	int currentPointIndex;
	int currentToolIndex;
	int refIndex; // use which tool or reference frame as a local base
	bool pointsStatus[POINTNUM];
	bool toolsStatus[TOOLNUM];
	double pointsCoordinate[POINTNUM][3];
	double pointsRelativeCoordinate[POINTNUM][3];
	double collectedPoint[COLLECTIONNUM][3];
	double collectedPointRelative[COLLECTIONNUM][3];  // record the relative coordinates in the cali reference COS
    //double collectedQuaternion[COLLECTIONNUM][4];	

	double pointArrayLocalTransform[16];
	bool isPointArrayLocalTransformSet;
	//double caliPointArrayLocal[CALIARRAY_ROWNUM][CALIARRAY_COLNUM][3];
	double*** caliPointArrayLocal;
	int cali_row_num;
	int cali_col_num;
	double cali_row_interval;
	double cali_col_interval;

	int cali_origin_row_idx;
	int cali_origin_col_idx;

	EndoGadget* m_endoGadget;
		
	// for knee motion observe
	//QPushButton* calculateRefCOSButton;
	QPushButton* calculateLocalTransformButton;
	QPushButton* loadLocalTransformButton;
	QPushButton* captureButton;
	QPushButton* loadCapturedResultButton;
	QPushButton* resetCapturedResultButton;
	QPushButton* calibrationButton;
	QPushButton* calibrationTestButton;
	
	bool isCapturingEndoImageAndTransform;
		
	QLabel* captureCountLabel; 
	int m_captureCount; 	
	std::vector<double*> m_cap_t_cali_to_endo;
	std::vector< QImage > m_capturedImages;

	QString m_tmp_dir_name;
	QString m_tmp_capture_count_filename;

	// for endo calibration
	std::vector<imgPtd> m_imagePoints;  // double[2]
	std::vector<objPtd> m_objectPoints; // double[3]
	bool isCalibrationDataReady;

	// for test
	std::vector<int> m_fpCounts;  // feature point
	 
	camera_parameters m_cp;
	calibration_constants m_cc;
	bool isCalibrationSucceed;

	camera_parameters m_cp_h;
	calibration_constants_heik m_cc_h;

	camera_parameters m_cp_cv;
	calibration_constants_heik m_cc_cv;

	// for calibration test
	std::vector< std::array<double, 3> > m_testPointList3d; 
	// the first two coordinates are exact image coords,
	// the successive elements are other information
	std::vector< std::array<double, 3> > m_testPointList2d;  

	bool m_isTest;  // if test, update the test point list	
	bool m_isTestPointInitialized;
	int m_testPointNum;
	
	QMutex m_mutex;

    double a1, a2, a3, cdist, icdist2;

	//QTableWidget *refCOSTable;
	//QTableWidget *relTransTables[TOOLNUM-1];

	//QTableWidget *kneeMotionInfoTable;
	//QTableWidget *kneeRelativeMotionInfoTable;    

	//double referenceCOS[16];  // this coordinate system is defined by the five points we set
	//double referenceCOS_i[16];  // the inverse matrix of the reference transform
	//bool referenceCOSStatus;
	//double relativeTransform[TOOLNUM-1][16];
	//bool relativeTransformStatus[TOOLNUM-1];
	//double kneeMotions[TOOLNUM-1][16];
	//bool kneeMotionsStatus[TOOLNUM-1];

	//// for data output
	//QTextStream cosInfo;         // coordinate system information
	//bool cosInfoReady;
	//QTextStream kneeMotionData[TOOLNUM-1];  // knee motion data
	//bool kneeMotionDataReady[TOOLNUM-1];
	//QTextStream kneeRelativeMotionData;   // fermal motion relative to the tibial
	//bool kneeRelativeMotionDataReady;
	//QTextStream kneeRelativeMotionData2;  // patella motion relative to the fermal
	//bool kneeRelativeMotionDataReady2;
	
};

#endif
