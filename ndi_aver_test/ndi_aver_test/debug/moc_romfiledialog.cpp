/****************************************************************************
** Meta object code from reading C++ file 'romfiledialog.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../romfiledialog.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'romfiledialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ROMFileDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x08,
      29,   14,   14,   14, 0x08,
      43,   14,   14,   14, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ROMFileDialog[] = {
    "ROMFileDialog\0\0openROMFile()\0saveChanges()\0"
    "selectROMFileListItem()\0"
};

void ROMFileDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ROMFileDialog *_t = static_cast<ROMFileDialog *>(_o);
        switch (_id) {
        case 0: _t->openROMFile(); break;
        case 1: _t->saveChanges(); break;
        case 2: _t->selectROMFileListItem(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData ROMFileDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ROMFileDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ROMFileDialog,
      qt_meta_data_ROMFileDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ROMFileDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ROMFileDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ROMFileDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ROMFileDialog))
        return static_cast<void*>(const_cast< ROMFileDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int ROMFileDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
