/****************************************************************************
** Meta object code from reading C++ file 'kneemotionobserver.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../kneemotionobserver.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'kneemotionobserver.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_SetPointControl[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      23,   17,   16,   16, 0x05,
      37,   17,   16,   16, 0x05,

 // slots: signature, parameters, type, tag, flags
      53,   16,   16,   16, 0x09,
      69,   16,   16,   16, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_SetPointControl[] = {
    "SetPointControl\0\0index\0getPoint(int)\0"
    "resetPoint(int)\0getButtonSlot()\0"
    "resetButtonSlot()\0"
};

void SetPointControl::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SetPointControl *_t = static_cast<SetPointControl *>(_o);
        switch (_id) {
        case 0: _t->getPoint((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->resetPoint((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->getButtonSlot(); break;
        case 3: _t->resetButtonSlot(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData SetPointControl::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SetPointControl::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_SetPointControl,
      qt_meta_data_SetPointControl, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SetPointControl::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SetPointControl::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SetPointControl::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SetPointControl))
        return static_cast<void*>(const_cast< SetPointControl*>(this));
    return QWidget::qt_metacast(_clname);
}

int SetPointControl::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void SetPointControl::getPoint(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void SetPointControl::resetPoint(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
static const uint qt_meta_data_KneeMotionObserver[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      20,   19,   19,   19, 0x05,
      43,   19,   19,   19, 0x05,

 // slots: signature, parameters, type, tag, flags
      72,   66,   19,   19, 0x09,
      97,   66,   19,   19, 0x09,
     125,  120,   19,   19, 0x09,
     149,  120,   19,   19, 0x09,
     172,   19,   19,   19, 0x09,
     198,   19,   19,   19, 0x09,
     219,   19,   19,   19, 0x09,
     257,   19,   19,   19, 0x09,
     278,   19,   19,   19, 0x09,
     300,   19,   19,   19, 0x09,
     318,   19,   19,   19, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_KneeMotionObserver[] = {
    "KneeMotionObserver\0\0testPointListUpdated()\0"
    "testPointListInvalid()\0index\0"
    "requirePointSetting(int)\0"
    "requirePointReset(int)\0used\0"
    "setUseProbeOffset(bool)\0setUseEndoOffset(bool)\0"
    "calculateLocalTransform()\0"
    "loadLocalTransform()\0"
    "requireCaptureEndoImageAndTransform()\0"
    "loadCapturedResult()\0resetCapturedResult()\0"
    "endoCalibration()\0endoCalibratedResultTest()\0"
};

void KneeMotionObserver::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        KneeMotionObserver *_t = static_cast<KneeMotionObserver *>(_o);
        switch (_id) {
        case 0: _t->testPointListUpdated(); break;
        case 1: _t->testPointListInvalid(); break;
        case 2: _t->requirePointSetting((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->requirePointReset((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->setUseProbeOffset((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->setUseEndoOffset((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->calculateLocalTransform(); break;
        case 7: _t->loadLocalTransform(); break;
        case 8: _t->requireCaptureEndoImageAndTransform(); break;
        case 9: _t->loadCapturedResult(); break;
        case 10: _t->resetCapturedResult(); break;
        case 11: _t->endoCalibration(); break;
        case 12: _t->endoCalibratedResultTest(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KneeMotionObserver::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KneeMotionObserver::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_KneeMotionObserver,
      qt_meta_data_KneeMotionObserver, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KneeMotionObserver::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KneeMotionObserver::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KneeMotionObserver::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KneeMotionObserver))
        return static_cast<void*>(const_cast< KneeMotionObserver*>(this));
    return QWidget::qt_metacast(_clname);
}

int KneeMotionObserver::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    }
    return _id;
}

// SIGNAL 0
void KneeMotionObserver::testPointListUpdated()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void KneeMotionObserver::testPointListInvalid()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
QT_END_MOC_NAMESPACE
