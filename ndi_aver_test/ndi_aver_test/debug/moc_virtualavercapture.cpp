/****************************************************************************
** Meta object code from reading C++ file 'virtualavercapture.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../aver/virtualavercapture.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'virtualavercapture.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_AVerSettingDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      25,   19,   18,   18, 0x09,
      58,   19,   18,   18, 0x09,
     100,   95,   18,   18, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_AVerSettingDialog[] = {
    "AVerSettingDialog\0\0index\0"
    "onSourceCurrentIndexChanged(int)\0"
    "onResolutionCurrentIndexChanged(int)\0"
    "text\0onFrameRateCurrentIndexChanged(QString)\0"
};

void AVerSettingDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        AVerSettingDialog *_t = static_cast<AVerSettingDialog *>(_o);
        switch (_id) {
        case 0: _t->onSourceCurrentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->onResolutionCurrentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->onFrameRateCurrentIndexChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData AVerSettingDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject AVerSettingDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_AVerSettingDialog,
      qt_meta_data_AVerSettingDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AVerSettingDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AVerSettingDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AVerSettingDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AVerSettingDialog))
        return static_cast<void*>(const_cast< AVerSettingDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int AVerSettingDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
static const uint qt_meta_data_AVerClippingRectSettingDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      31,   30,   30,   30, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_AVerClippingRectSettingDialog[] = {
    "AVerClippingRectSettingDialog\0\0"
    "onRefreshClippingRect()\0"
};

void AVerClippingRectSettingDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        AVerClippingRectSettingDialog *_t = static_cast<AVerClippingRectSettingDialog *>(_o);
        switch (_id) {
        case 0: _t->onRefreshClippingRect(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData AVerClippingRectSettingDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject AVerClippingRectSettingDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_AVerClippingRectSettingDialog,
      qt_meta_data_AVerClippingRectSettingDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AVerClippingRectSettingDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AVerClippingRectSettingDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AVerClippingRectSettingDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AVerClippingRectSettingDialog))
        return static_cast<void*>(const_cast< AVerClippingRectSettingDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int AVerClippingRectSettingDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
static const uint qt_meta_data_AVerCapturingThread[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      29,   21,   20,   20, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_AVerCapturingThread[] = {
    "AVerCapturingThread\0\0present\0"
    "hasSignal(bool)\0"
};

void AVerCapturingThread::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        AVerCapturingThread *_t = static_cast<AVerCapturingThread *>(_o);
        switch (_id) {
        case 0: _t->hasSignal((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData AVerCapturingThread::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject AVerCapturingThread::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_AVerCapturingThread,
      qt_meta_data_AVerCapturingThread, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AVerCapturingThread::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AVerCapturingThread::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AVerCapturingThread::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AVerCapturingThread))
        return static_cast<void*>(const_cast< AVerCapturingThread*>(this));
    return QThread::qt_metacast(_clname);
}

int AVerCapturingThread::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void AVerCapturingThread::hasSignal(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
static const uint qt_meta_data_VirtualAVerCapture[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      30,   20,   19,   19, 0x08,
      51,   19,   19,   19, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_VirtualAVerCapture[] = {
    "VirtualAVerCapture\0\0hasSignal\0"
    "signalPresence(bool)\0onTimerRefreshFrame()\0"
};

void VirtualAVerCapture::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        VirtualAVerCapture *_t = static_cast<VirtualAVerCapture *>(_o);
        switch (_id) {
        case 0: _t->signalPresence((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->onTimerRefreshFrame(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData VirtualAVerCapture::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject VirtualAVerCapture::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_VirtualAVerCapture,
      qt_meta_data_VirtualAVerCapture, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &VirtualAVerCapture::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *VirtualAVerCapture::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *VirtualAVerCapture::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_VirtualAVerCapture))
        return static_cast<void*>(const_cast< VirtualAVerCapture*>(this));
    return QWidget::qt_metacast(_clname);
}

int VirtualAVerCapture::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
