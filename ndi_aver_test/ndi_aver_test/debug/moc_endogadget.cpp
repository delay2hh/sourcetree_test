/****************************************************************************
** Meta object code from reading C++ file 'endogadget.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../aver/endogadget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'endogadget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_EndoGadget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x09,
      44,   11,   11,   11, 0x09,
      75,   11,   11,   11, 0x09,
     100,   11,   11,   11, 0x09,
     131,   11,   11,   11, 0x09,
     155,  151,   11,   11, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_EndoGadget[] = {
    "EndoGadget\0\0onCaptureDeviceStartStreaming()\0"
    "onCaptureDeviceStopStreaming()\0"
    "onCaptureDeviceSetting()\0"
    "onCaptureClippingRectSetting()\0"
    "onCaptureToBuffer()\0pos\0showContextMenu(QPoint)\0"
};

void EndoGadget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        EndoGadget *_t = static_cast<EndoGadget *>(_o);
        switch (_id) {
        case 0: _t->onCaptureDeviceStartStreaming(); break;
        case 1: _t->onCaptureDeviceStopStreaming(); break;
        case 2: _t->onCaptureDeviceSetting(); break;
        case 3: _t->onCaptureClippingRectSetting(); break;
        case 4: _t->onCaptureToBuffer(); break;
        case 5: _t->showContextMenu((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData EndoGadget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject EndoGadget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_EndoGadget,
      qt_meta_data_EndoGadget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &EndoGadget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *EndoGadget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *EndoGadget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_EndoGadget))
        return static_cast<void*>(const_cast< EndoGadget*>(this));
    return QWidget::qt_metacast(_clname);
}

int EndoGadget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
