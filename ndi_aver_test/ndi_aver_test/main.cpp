#include <QApplication>

#include "mainwindow.h"

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);

	MainWindow mainWindow;
	mainWindow.showMaximized();
	mainWindow.selectAVerDevice(1); // 0 - SD; 1 - HD

	return app.exec();
}