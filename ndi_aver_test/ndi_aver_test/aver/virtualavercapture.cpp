#include <QImage>
#include <QPainter>
#include <QPaintEngine>

#include <QString>
#include <QKeyEvent>

#include <QtUiTools/QtUiTools>

#include <iostream>
#include <sstream>
#include <string>
// #include <vector> //included in the header file
#include <set>
#include <map>

#include "virtualavercapture.h"

#include <windows.h>
#include "AVerCapAPI.h"

enum
{
	DEVICETYPE_NULL = 0
};

// for device frame rate setting
#define DEVICE_NUM 11
#define SOURCE_NUM 6 // 0:Composite 1:SVideo 2:Component 3:HDMI 4:VGA
#define RESOLUTION_NUM 44
#define FRAMERATE_NUM 5

DWORD g_dwFrameRateArray[DEVICE_NUM][SOURCE_NUM][RESOLUTION_NUM][FRAMERATE_NUM+1] = {
	// C727
	{
		//Composite
		{
			{1, 0x2}, {1, 0x1}, {1, 0x2}, {0}, {1, 0x1}
		},

		//s-video
		{
			{1, 0x2}, {1, 0x1}, {1, 0x2}, {0}, {1, 0x1}
		},

		//Component
		{
			{1, 0x2}, {1, 0x1}, {1, 0x2}, {1, 0x8}, {1, 0x1}, {1, 0x4},
			{2, 0xC}, {2, 0x3}
		},

		//HDMI
		{
			{1, 0x2}, {1, 0x1}, {1, 0x2}, {1, 0x8}, {1, 0x1}, {1, 0x4},
			{2, 0xC}, {2, 0x3}, {4, 0x8, 7000, 7500, 8500}, {1, 0x8},
			{1, 0x4}, {1, 0x4}, {1, 0x2}, {0}, {0}, {0}, {4, 0x8, 7200, 7500, 8500},
			{5, 0x8, 5600, 7200, 7500, 8500}
		}
	},
	// C729
	{
		// Composite
		{
			{1, 0x2}, {1, 0x1}, {1, 0x2}, {0}, {1, 0x1}
		},
			
		// SVideo
		{
			{1, 0x2}, {1, 0x1}, {1, 0x2}, {0}, {1, 0x1}
		},
			
		// Component
		{
			{1, 0x2}, {1, 0x1}, {1, 0x2}, {1, 0x8}, {1, 0x1}, {1, 0x4},
			{2, 0xC}, {2, 0x3}
		},
			
		// HDMI
		{
			{1, 0x2}, {1, 0x1}, {1, 0x2}, {1, 0x8}, {1, 0x1}, {1, 0x4},
			{2, 0xC}, {2, 0x3}, {4, 0x8, 7000, 7500, 8500}, {1, 0x8}, {1, 0x4}, {1, 0x4},
			{1, 0x2}, {1, 0, 3500}, {2, 0x3}, {1, 0, 2400}, {4, 0x8, 7200, 7500, 8500},
			{5, 0x8, 5600, 7200, 7500, 8500}, {1, 0x8}, {1, 0x8}, {0},  {0}, {0}, {0}, {0}, 
			{0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {1, 0, 7500}, {1, 0x8}
		}
	},
	// C199&C199X
	{
		// Composite
		{0},
			
		// SVideo
		{0},
		
		// Component
		{0},
		
		// HDMI
		{
			{0}, {0}, {0}, {0}, {0}, {0},
			{2, 0xC}, {2, 0x3}, {1, 0x8}, {1, 0x8}, {1, 0x4}, {1, 0x4},
			{0}, {1, 0, 3500}, {2, 0x3}, {0}, {1, 0x8}, {1, 0x8},
			{1, 0x8}, {1, 0x8}
		},
			
		// VGA
		{      
			{0}, {0}, {0}, {0}, {0}, {0},
			{2, 0xC}, {2, 0x3}, {1, 0x8}, {1, 0x8}, {1, 0x4}, {1, 0x4},
			{0}, {1, 0, 3500}, {2, 0x3}, {0}, {1, 0x8}, {1, 0x8},
			{1, 0x8}, {1, 0x8}
		}
	},
	// C874
	{
		// Composite
		{
			{0}, {0}, {1, 0x2}, {0}, {1, 0x1}
		},
			
		// SVideo
		{
			{0}, {0}, {1, 0x2}, {0}, {1, 0x1}
		},
			
		// Component
		{
			{0}, {0}, {1, 0x2}, {0}, {1, 0x1}, {0},
			{2, 0xC}, {2, 0x3}
		}
	},
	// V1A8&C725
	{
		// Composite
		{
			{1, 0x2}, {1, 0x1}, {1, 0x2}, {0}, {1, 0x1}, {0},
			{0}, {0}, {0}, {0}, {0}, {0},
			{0}, {0}, {0}, {0}, {0}, {0},
			{0}, {0}, {1, 0x2}, {1, 0x1}, {1, 0x1}, {1, 0x2},
			{1, 0x2}, {1, 0x2}, {1, 0x1}, {1, 0x2}, {1, 0x1}, {1, 0x2},
			{1, 0x1}
		},
			
		// SVideo
		{
			{1, 0x2}, {1, 0x1}, {1, 0x2}, {0}, {1, 0x1}, {0},
			{0}, {0}, {0}, {0}, {0}, {0},
			{0}, {0}, {0}, {0}, {0}, {0},
			{0}, {0}, {1, 0x2}, {1, 0x1}, {1, 0x1}, {1, 0x2},
			{1, 0x2}, {1, 0x2}, {1, 0x1}, {1, 0x2}, {1, 0x1}, {1, 0x2},
			{1, 0x1}
		}
	},
	// C039
	{
		// Composite
		{
			{1, 0x2}, {1, 0x1}, {1, 0x2}, {0}, {1, 0x1}, {0},
			{0}, {0}, {0}, {0}, {0}, {0},
			{0}, {0}, {0}, {0}, {0}, {0},
			{0}, {0}, {1, 0x2}, {1, 0x1}, {1, 0x1}, {1, 0x2},
			{1, 0x2}, {1, 0x2}, {1, 0x1}, {1, 0x2}, {1, 0x1}, {1, 0x2},
			{1, 0x1}, {1, 0x2}, {1, 0x1}, {1, 0x1}, {1, 0x1}
		},
			
		// SVideo
		{
			{1, 0x2}, {1, 0x1}, {1, 0x2}, {0}, {1, 0x1}, {0},
			{0}, {0}, {0}, {0}, {0}, {0},
			{0}, {0}, {0}, {0}, {0}, {0},
			{0}, {0}, {1, 0x2}, {1, 0x1}, {1, 0x1}, {1, 0x2},
			{1, 0x2}, {1, 0x2}, {1, 0x1}, {1, 0x2}, {1, 0x1}, {1, 0x2},
			{1, 0x1}, {1, 0x2}, {1, 0x1}, {1, 0x1}, {1, 0x1}
		}

	},
	// C129
	{
		// Composite
		{
			{1, 0x2}, {1, 0x1}, {1, 0x2}, {1, 0x8}, {1, 0x1}
		},
			
		// SVideo
		{
			{1, 0x2}, {1, 0x1}, {1, 0x2}, {1, 0x8}, {1, 0x1}
		},
			
		// Component
		{
			{1, 0x2}, {1, 0x1}, {1, 0x2}, {1, 0x8}, {1, 0x1}, {1, 0x4},
			{2, 0xC}, {2, 0x3}
		},
			
		// HDMI
		{
			{1, 0x2}, {1, 0x1}, {1, 0x2}, {1, 0x8}, {1, 0x1}, {1, 0x4},
			{2, 0xC}, {2, 0x3}, {2, 0x8, 7000}, {1, 0x8}, {1, 0x4}, {1, 0x4},
			{1, 0x2}, {1, 0, 3500}, {2, 0x3}, {1, 0, 2400}, {1, 0x8}, {4, 0x8, 5600, 7200, 7500},
			{1, 0x8}, {1, 0x8}, {0}, {0}, {0}, {0},
			{0}, {0}, {0}, {0}, {0}, {0},
			{0}, {0}, {0}, {0}, {0}, {1, 0, 7500},
			{1, 0x8}
		}
	},
	//C968&C351
	{
		//Composite
		{
			{1, 0x2}, {0}, {1, 0x2}, {0}, {1, 0x1}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, 
			{0}, {0}, {0}, {1, 0x2}, {0}, {0}, {0}, {1, 0x2}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, 
			{0}, {0}, {1, 0x2}, {1, 0x1}, {1, 0x2}, {1, 0x1}
		}
	},
	//C353
	{
		// Composite
		{0},
			
		// SVideo
		{0},
		
		// Component
		{0},
		
		// HDMI
		{
			{0}, {0}, {1, 0x2}, {1, 0x8}, {1, 0x1}, {1, 0x4}, 
			{2, 0xC}, {2, 0x3}, {1, 0x8}, {1, 0x8}, {1, 0x8}, {1, 0x8},
			{0}, {1, 0x8}, {1, 0x8}, {0}, {1, 0x8}, {1, 0x8}, 
			{1, 0x8}, {1, 0x8}
		},
			
		// VGA
		{
			{0}, {0}, {0}, {0}, {0}, {0}, {2, 0xC}, {0}, {4, 0x8, 7000, 7500, 8500},
			{1, 0x8}, {1, 0x8}, {1, 0x8}, {0}, {1, 0x8}, {1, 0x8}, {0}, {4, 0x8, 7200, 7500, 8500},
			{5, 0x8, 5600, 7200, 7500, 8500}, {1, 0x8}, {1, 0x8}
		}
	},
	//C997
	{
		// Composite
		{0},
			
		// SVideo
		{0},
		
		// Component
		{0},
		
		//HDMI
		{0},
		
		//VGA
		{0},
		
		//SDI
		{
			{0}, {0}, {1, 0x2}, {0}, {1, 0x1}, {0},
			{8, 0xF, 2398, 2400, 3000, 6000}, {2, 0x3}, {0}, {0}, {0}, {0}, 
			{0}, {0}, {5, 0x3, 2398, 2400, 3000}, {1, 0, 2400}
		}
	},
	//C351_PAL_SQ
	{
		//Composite
		{
			{0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, 
			{0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, 
			{0}, {0}, {0}, {0}, {0}, {0},{1, 0x1},{1, 0x1},{1, 0x1}
		}
	}
};

// auxiliary function(s)
int getCaptureVideoResolution(std::string res_str)
{
	if(res_str == std::string("640X480")){
		return VIDEORESOLUTION_640X480;
	}else if(res_str == std::string("704X576")){
		return VIDEORESOLUTION_704X576;
	}else if(res_str == std::string("720X480")){
		return VIDEORESOLUTION_720X480;
    }else if(res_str == std::string("720X480P")){
		return VIDEORESOLUTION_720X480P;
    }else if(res_str == std::string("720X576")){
		return VIDEORESOLUTION_720X576;
    }else if(res_str == std::string("720X576P")){
		return VIDEORESOLUTION_720X576P;
    }else if(res_str == std::string("1280X720P")){
		return VIDEORESOLUTION_1280X720P;
    }else if(res_str == std::string("1920X1080")){
		return VIDEORESOLUTION_1920X1080;
    }else if(res_str == std::string("1024X768P")){
		return VIDEORESOLUTION_1024X768P;
    }else if(res_str == std::string("1280X800P")){
		return VIDEORESOLUTION_1280X800P;
    }else if(res_str == std::string("1280X1024P")){
		return VIDEORESOLUTION_1280X1024P;
    }else if(res_str == std::string("1440X900P")){
		return VIDEORESOLUTION_1440X900P;
    }else if(res_str == std::string("1600X1200P")){
		return VIDEORESOLUTION_1600X1200P;
    }else if(res_str == std::string("1680X1050P")){
		return VIDEORESOLUTION_1680X1050P;
    }else if(res_str == std::string("1920X1080P")){
		return VIDEORESOLUTION_1920X1080P;
    }else if(res_str == std::string("1920X1080P_24")){
		return VIDEORESOLUTION_1920X1080P_24FPS;
    }else if(res_str == std::string("640X480P")){
		return VIDEORESOLUTION_640X480P;
    }else if(res_str == std::string("800X600P")){
		return VIDEORESOLUTION_800X600P;
    }else if(res_str == std::string("1280X768P")){
		return VIDEORESOLUTION_1280X768P;
    }else if(res_str == std::string("1360X768P")){
		return VIDEORESOLUTION_1360X768P;
    }else if(res_str == std::string("160X120")){
		return VIDEORESOLUTION_160X120;
    }else if(res_str == std::string("176X144")){
		return VIDEORESOLUTION_176X144;
    }else if(res_str == std::string("240X176")){
		return VIDEORESOLUTION_240X176;
    }else if(res_str == std::string("240X180")){
		return VIDEORESOLUTION_240X180;
    }else if(res_str == std::string("320X240")){
		return VIDEORESOLUTION_320X240;
    }else if(res_str == std::string("352X240")){
		return VIDEORESOLUTION_352X240;
    }else if(res_str == std::string("352X288")){
		return VIDEORESOLUTION_352X288;
    }else if(res_str == std::string("640X240")){
		return VIDEORESOLUTION_640X240;
    }else if(res_str == std::string("640X288")){
		return VIDEORESOLUTION_640X288;
    }else if(res_str == std::string("720X240")){
		return VIDEORESOLUTION_720X240;
    }else if(res_str == std::string("720X288")){
		return VIDEORESOLUTION_720X288;
    }else if(res_str == std::string("80X60")){
		return VIDEORESOLUTION_80X60;
    }else if(res_str == std::string("88X72")){
		return VIDEORESOLUTION_88X72;
    }else if(res_str == std::string("128X96")){
		return VIDEORESOLUTION_128X96;
    }else if(res_str == std::string("640X576")){
		return VIDEORESOLUTION_640X576;
    }else if(res_str == std::string("1152X864P")){
		return VIDEORESOLUTION_1152X864P;
    }else if(res_str == std::string("1280X960P")){
		return VIDEORESOLUTION_1280X960P;
    }else if(res_str == std::string("180X120")){
		return VIDEORESOLUTION_180X120;
    }else if(res_str == std::string("180X144")){
		return VIDEORESOLUTION_180X144;
    }else if(res_str == std::string("360X240")){
		return VIDEORESOLUTION_360X240;
	}else if(res_str == std::string("360X288")){
		return VIDEORESOLUTION_360X288;
	}else if(res_str == std::string("768X576")){
		return VIDEORESOLUTION_768X576;
	}else if(res_str == std::string("384x288")){
		return VIDEORESOLUTION_384x288;
	}else if(res_str == std::string("192x144")){
		return VIDEORESOLUTION_192x144;
	}else
	{
		return -1; // fail to get a valid resolution
	}
}

///////////////////////////////////////////////////////////////////
// for AVerSettingDialog
AVerSettingDialog::AVerSettingDialog(HANDLE hCaptureDevice, DWORD dwDeviceType, DWORD dwCurrCardType, QWidget* parent)
	:QDialog(parent,  Qt::WindowTitleHint | Qt::WindowSystemMenuHint)  // without a "What's This" button in the title bar
{
	// variables
	m_bIsSourceSettingChanged = false;
	m_bIsResolutionSettingChanged = false;
	m_bIsFrameRateSettingChanged = false;
	
	m_hCaptureDevice = hCaptureDevice;
	m_dwDeviceType = dwDeviceType;
	m_dwCurrCardType = dwCurrCardType;

	if(m_hCaptureDevice)
	{		
		createUi(); // ui 
	}
}
	
AVerSettingDialog::~AVerSettingDialog()
{
	//
}

//////////////////////////////////////////////
// protected slots
void AVerSettingDialog::onSourceCurrentIndexChanged(int index)
{
	if (m_dwCurrCardType == CARDTYPE_C729 || m_dwCurrCardType == CARDTYPE_C129)
	{
		if(m_dwDeviceType == DEVICETYPE_HD)
		{
			//m_sourceComboBox->addItem("HDMI");      // ind - 0
			//m_sourceComboBox->addItem("Component"); // ind - 1

			if(index == 0)
			{
				if(m_dwVideoSource == VIDEOSOURCE_HDMI)
				{
					m_bIsSourceSettingChanged = false;					
				}
				if(m_dwVideoSource == VIDEOSOURCE_COMPONENT)
				{
					m_dwVideoSource = VIDEOSOURCE_HDMI;
					m_bIsSourceSettingChanged = true;
				}				
			}
			if(index == 1)
			{
				if(m_dwVideoSource == VIDEOSOURCE_COMPONENT)
				{
					m_bIsSourceSettingChanged = false;					
				}
				if(m_dwVideoSource == VIDEOSOURCE_HDMI)
				{
					m_dwVideoSource = VIDEOSOURCE_COMPONENT;
					m_bIsSourceSettingChanged = true;
				}
			}
		}
		else
		{
			//m_sourceComboBox->addItem("S-Video");    // ind - 0
			//m_sourceComboBox->addItem("Composite");  // ind - 1

			if(index == 0)
			{
				if(m_dwVideoSource == VIDEOSOURCE_SVIDEO)
				{
					m_bIsSourceSettingChanged = false;					
				}
				if(m_dwVideoSource == VIDEOSOURCE_COMPOSITE)
				{
					m_dwVideoSource = VIDEOSOURCE_SVIDEO;
					m_bIsSourceSettingChanged = true;
				}				
			}
			if(index == 1)
			{
				if(m_dwVideoSource == VIDEOSOURCE_COMPOSITE)
				{
					m_bIsSourceSettingChanged = false;					
				}
				if(m_dwVideoSource == VIDEOSOURCE_SVIDEO)
				{
					m_dwVideoSource = VIDEOSOURCE_COMPOSITE;					
					m_bIsSourceSettingChanged = true;
				}
			}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
		}
	}

	if(m_bIsSourceSettingChanged)
	{
		C729VideoSourceSet();
		// the resolution information should be updated first
		updateResolutionWidgets(); // implicitly change the selected item and set the resolution
	}
}
	
void AVerSettingDialog::onResolutionCurrentIndexChanged(int index)
{
	m_bIsResolutionSettingChanged = true;

	if (m_dwCurrCardType == CARDTYPE_C729)
	{
		//C729ClickOk();
		int nVideoResolution = index;
		if (m_dwDeviceType == DEVICETYPE_SD && nVideoResolution == 3)
		{
			nVideoResolution++;
		}
		if (m_dwVideoSource == VIDEOSOURCE_HDMI && nVideoResolution >= 20)
		{
			nVideoResolution = nVideoResolution + 15;
		}

		AVerSetVideoResolution(m_hCaptureDevice, nVideoResolution);	

		m_nVideoResolution = nVideoResolution;

		// the frame rate will be effected by the choice of the resolution
		updateFrameRateWidgets();
	}
}

void AVerSettingDialog::onFrameRateCurrentIndexChanged(const QString &text)
{
	m_bIsFrameRateSettingChanged = true;

	//((CComboBox*)GetDlgItem(IDC_COMBO_FRAMERATE))->GetLBText(((CComboBox*)GetDlgItem(IDC_COMBO_FRAMERATE))->GetCurSel(), strFrameRate);
	QString strFrameRate = text;

	m_dwVideoInputFrameRate = (DWORD)(atof(strFrameRate.toStdString().c_str())*100);
	AVerSetVideoInputFrameRate(m_hCaptureDevice, m_dwVideoInputFrameRate);
}

void AVerSettingDialog::createUi()
{
	// ui elements
	QLabel* sourceLabel = new QLabel(tr("Source:"), this);
    QLabel* resolutionLabel = new QLabel(tr("Resolution:"), this);
	QLabel* frameRateLabel = new QLabel(tr("Frame Rate:"), this);

	m_sourceComboBox = new QComboBox(this);
	m_resolutionComboBox = new QComboBox(this);
	m_frameRateComboBox = new QComboBox(this);

	QGridLayout *settingLayout = new QGridLayout;
	settingLayout->setSizeConstraint(QLayout::SetNoConstraint);
	settingLayout->addWidget(sourceLabel, 0, 0);
	settingLayout->addWidget(m_sourceComboBox, 0, 1, 1, 2);
	settingLayout->addWidget(resolutionLabel, 1, 0);
	settingLayout->addWidget(m_resolutionComboBox, 1, 1, 1, 2);
	settingLayout->addWidget(frameRateLabel, 2, 0);
	settingLayout->addWidget(m_frameRateComboBox, 2, 1, 1, 2);
	
	QGroupBox *settingGroup = new QGroupBox(tr("Settings"),this);
	settingGroup->setLayout(settingLayout);    

	// button box
	m_buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
	connect(m_buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
	connect(m_buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

	// the main layout
	QVBoxLayout *mainLayout = new QVBoxLayout;
	mainLayout->setSizeConstraint(QLayout::SetNoConstraint);
	mainLayout->addWidget(settingGroup);
	mainLayout->addWidget(m_buttonBox);
	setLayout(mainLayout);

	setWindowTitle(tr("Capture Device Setting"));
	
	//setAttribute(Qt::WA_DeleteOnClose); // Makes Qt delete this widget when the widget has accepted the close event

	// initialize the ui according to the device status
	// video source
	updateSourceWidgets();
	connect(m_sourceComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(onSourceCurrentIndexChanged(int)));	
	// video resolution
	updateResolutionWidgets();
	connect(m_resolutionComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(onResolutionCurrentIndexChanged(int)));
	// video frame rate
	updateFrameRateWidgets();
	connect(m_frameRateComboBox, SIGNAL(currentIndexChanged(const QString&)), this, SLOT(onFrameRateCurrentIndexChanged(const QString&)));	
}

void AVerSettingDialog::updateSourceWidgets()
{
	// video source
	AVerGetVideoSource(m_hCaptureDevice, &m_dwVideoSource);

	if (m_dwCurrCardType == CARDTYPE_C729 || m_dwCurrCardType == CARDTYPE_C129)
	{
		//C729Init();
		m_sourceComboBox->clear();
		if(m_dwDeviceType == DEVICETYPE_HD)
		{
			m_sourceComboBox->addItem("HDMI");      // ind - 0
			m_sourceComboBox->addItem("Component"); // ind - 1

			if(m_dwVideoSource == VIDEOSOURCE_HDMI)
				m_sourceComboBox->setCurrentIndex(0);
			else
				m_sourceComboBox->setCurrentIndex(1);
		}
		else
		{
			m_sourceComboBox->addItem("S-Video");    // ind - 0
			m_sourceComboBox->addItem("Composite");  // ind - 1

			if(m_dwVideoSource == VIDEOSOURCE_SVIDEO)
				m_sourceComboBox->setCurrentIndex(0);
			else
				m_sourceComboBox->setCurrentIndex(1);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
		}
	}
}

void AVerSettingDialog::updateResolutionWidgets()
{
	m_resolutionComboBox->clear();

	if (m_dwCurrCardType == CARDTYPE_C729)
	{
		// C729InitDialog();
		m_resolutionComboBox->addItem("640X480");
		m_resolutionComboBox->addItem("704X576");
		m_resolutionComboBox->addItem("720X480");	
		if (m_dwDeviceType != DEVICETYPE_SD)
		{
			m_resolutionComboBox->addItem("720X480P");
		}
		m_resolutionComboBox->addItem("720X576");
		if (m_dwDeviceType != DEVICETYPE_SD)
		{
			m_resolutionComboBox->addItem("720X576P");
			m_resolutionComboBox->addItem("1280X720P");
			m_resolutionComboBox->addItem("1920X1080");
			if (m_dwVideoSource == VIDEOSOURCE_HDMI)
			{
				m_resolutionComboBox->addItem("1024X768P");
				m_resolutionComboBox->addItem("1280X800P");
				m_resolutionComboBox->addItem("1280X1024P");
				m_resolutionComboBox->addItem("1440X900P");
				m_resolutionComboBox->addItem("1600X1200P");
				m_resolutionComboBox->addItem("1680X1050P");
				m_resolutionComboBox->addItem("1920X1080P");
				m_resolutionComboBox->addItem("1920X1080P_24");
				m_resolutionComboBox->addItem("640X480P");
				m_resolutionComboBox->addItem("800X600P");
				m_resolutionComboBox->addItem("1280X768P");
				m_resolutionComboBox->addItem("1360X768P");
				m_resolutionComboBox->addItem("1152X864P");
				m_resolutionComboBox->addItem("1280X960P");
			}
		}

		DWORD dwVideoResolution;
		AVerGetVideoResolution(m_hCaptureDevice, &dwVideoResolution);
		m_nVideoResolution = dwVideoResolution;

		if (m_dwDeviceType == DEVICETYPE_SD && dwVideoResolution == VIDEORESOLUTION_720X576)
		{
			dwVideoResolution--;
		}
		if (m_dwVideoSource == VIDEOSOURCE_HDMI && dwVideoResolution >= VIDEORESOLUTION_1152X864P)
		{
			dwVideoResolution -= 15;
		}
		m_resolutionComboBox->setCurrentIndex(dwVideoResolution);	
	}
}

void AVerSettingDialog::updateFrameRateWidgets()
{
	DWORD dwDeviceIndex, dwVideoInputFrameRate, dwVideoResolution, dwVideoSource, dwSelIndex = 0xFFFFFFFF;

	AVerGetVideoSource(m_hCaptureDevice, &dwVideoSource);
	AVerGetVideoResolution(m_hCaptureDevice, &dwVideoResolution);
	AVerGetVideoInputFrameRate(m_hCaptureDevice, &dwVideoInputFrameRate);

	switch (m_dwCurrCardType)
	{
	case CARDTYPE_C727:
		dwDeviceIndex = 0;
		break;
	case CARDTYPE_C729:
		dwDeviceIndex = 1;
		break;
	case CARDTYPE_C199:
	case CARDTYPE_C199X:
		dwDeviceIndex = 2;
		break;
	case CARDTYPE_C874:
		dwDeviceIndex = 3;
		break;
	case CARDTYPE_V1A8:
	case CARDTYPE_C725:
		dwDeviceIndex = 4;
		break;
	case CARDTYPE_C039:
		dwDeviceIndex = 5;
		break;
	case CARDTYPE_C129:
		dwDeviceIndex = 6;
		break;
	case CARDTYPE_C968:
	case CARDTYPE_C351:
		dwDeviceIndex = 7;
		break;
	case CARDTYPE_C353:
		dwDeviceIndex = 8;
		break;
	case CARDTYPE_C997:
		dwDeviceIndex = 9;
		break;
	case CARDTYPE_C351_PAL_SQ:
		dwDeviceIndex = 10;
		break;
	}

	QString strText;
	double dFrameRate;
	int i, nCount = 0;

	m_frameRateComboBox->clear();
	switch (g_dwFrameRateArray[dwDeviceIndex][dwVideoSource][dwVideoResolution][1])
	{
	case 0x3:
		m_frameRateComboBox->addItem("29.97");
		m_frameRateComboBox->addItem("25");
		if (dwVideoInputFrameRate == 2997)
		{
			dwSelIndex = 0;
		}
		else if (dwVideoInputFrameRate == 2500)
		{
			dwSelIndex = 1;
		}
		nCount = 2;
		break;
	case 0xC:
		m_frameRateComboBox->addItem("59.94");
		m_frameRateComboBox->addItem("50");
		if (dwVideoInputFrameRate == 5994)
		{
			dwSelIndex = 0;
		}
		else if (dwVideoInputFrameRate == 5000)
		{
			dwSelIndex = 1;
		}
		nCount = 2;
		break;
	case 0x1:
		m_frameRateComboBox->addItem("25");
		if (dwVideoInputFrameRate == 2500)
		{
			dwSelIndex = 0;
		}
		nCount = 1;
		break;
	case 0x2:
		m_frameRateComboBox->addItem("29.97");
		if (dwVideoInputFrameRate == 2997)
		{
			dwSelIndex = 0;
		}
		nCount = 1;
		break;
	case 0x4:
		m_frameRateComboBox->addItem("50");
		if (dwVideoInputFrameRate == 5000)
		{
			dwSelIndex = 0;
		}
		nCount = 1;
		break;
	case 0x8:
		m_frameRateComboBox->addItem("59.94");
		if (dwVideoInputFrameRate == 5994)
		{
			dwSelIndex = 0;
		}
		nCount = 1;
		break;
	case 0xF:
		m_frameRateComboBox->addItem("29.97");
		m_frameRateComboBox->addItem("25");
		m_frameRateComboBox->addItem("59.94");
		m_frameRateComboBox->addItem("50");
		if (dwVideoInputFrameRate == 2997)
		{
			dwSelIndex = 0;
		}
		else if (dwVideoInputFrameRate == 2500)
		{
			dwSelIndex = 1;
		}
		else if (dwVideoInputFrameRate == 5994)
		{
			dwSelIndex = 2;
		}
		else if (dwVideoInputFrameRate == 5000)
		{
			dwSelIndex = 3;
		}
		nCount = 4;
	}

	for (i = 0; i < g_dwFrameRateArray[dwDeviceIndex][dwVideoSource][dwVideoResolution][0]-nCount; i++)
	{
		dFrameRate = g_dwFrameRateArray[dwDeviceIndex][dwVideoSource][dwVideoResolution][2+i] / 100.0;		
		strText.sprintf("%-4.2f", dFrameRate); //strText.Format("%-4.2f", dFrameRate);
		m_frameRateComboBox->addItem(strText);
		if (g_dwFrameRateArray[dwDeviceIndex][dwVideoSource][dwVideoResolution][2+i] == dwVideoInputFrameRate)
		{
			dwSelIndex = i+nCount;
		}
	}
	if (dwSelIndex != 0xFFFFFFFF)
	{
		m_frameRateComboBox->setCurrentIndex(dwSelIndex);
	}
	else
	{
		std::cout << "\nThe default settings will be restored since the previous settings are wrong." << std::endl;
		//AVerSetVideoInputFrameRate(m_hCaptureDevice, dwVideoInputFrameRate);
		m_frameRateComboBox->clear();
	}

}

void AVerSettingDialog::C729VideoSourceSet()
{
	if (m_dwVideoSource == VIDEOSOURCE_COMPONENT)
	{
		DWORD  dwVideoResolution;
		AVerGetVideoResolution(m_hCaptureDevice, &dwVideoResolution);
		if (dwVideoResolution !=VIDEORESOLUTION_640X480 && dwVideoResolution != VIDEORESOLUTION_704X576 && 
			dwVideoResolution != VIDEORESOLUTION_720X480 && dwVideoResolution != VIDEORESOLUTION_720X480P && 
			dwVideoResolution != VIDEORESOLUTION_720X576 && dwVideoResolution != VIDEORESOLUTION_720X576P &&
			dwVideoResolution != VIDEORESOLUTION_1280X720P && dwVideoResolution != VIDEORESOLUTION_1920X1080)
		{
			dwVideoResolution = VIDEORESOLUTION_640X480;
			AVerSetVideoResolution(m_hCaptureDevice, dwVideoResolution);
		}			
	}

	AVerSetVideoSource(m_hCaptureDevice, m_dwVideoSource); 
}

///////////////////////////////////////////////////////////////////
// for AVerClippingRectSettingDialog
AVerClippingRectSettingDialog::AVerClippingRectSettingDialog(Qt::HANDLE hCaptureDevice, QWidget* parent)
	:QDialog(parent,  Qt::WindowTitleHint | Qt::WindowSystemMenuHint)  // without a "What's This" button in the title bar
{
	m_hCaptureDevice = hCaptureDevice;
	
	if(m_hCaptureDevice)
	{
		createUi();
	}
}
	
AVerClippingRectSettingDialog::~AVerClippingRectSettingDialog()
{
	//
}


void AVerClippingRectSettingDialog::updateClippingPreview()
{
	if(m_resultImage.isNull())
		return;

	m_canvasLabel->setPixmap(QPixmap::fromImage(m_resultImage).scaled(m_canvasWidth, m_canvasHeight, Qt::KeepAspectRatio));
}

/////////////////////////////////////////////
//protected slots:
void AVerClippingRectSettingDialog::onRefreshClippingRect()
{
	// get the setting values
	int l = m_rectLeftLineEdit->text().toInt();
	int r = m_rectRightLineEdit->text().toInt();
	int t = m_rectTopLineEdit->text().toInt();
	int b = m_rectBottomLineEdit->text().toInt();

	// width and height must be multiple of 16
	int w = r - l + 1;
	int h = b - t + 1;
	int w16 = w / 16 * 16;
	int h16 = h / 16 * 16;

	int l16 = l;
	int t16 = t;
	int r16 = l + w16;
	int b16 = t + h16;

	m_rectLeftLineEdit->setText(QString("%1").arg(l16));
	m_rectRightLineEdit->setText(QString("%1").arg(r16));
	m_rectTopLineEdit->setText(QString("%1").arg(t16));
	m_rectBottomLineEdit->setText(QString("%1").arg(b16));

	// draw
	QPainter painter(&m_resultImage);
	painter.setCompositionMode(QPainter::CompositionMode_Source);
	painter.fillRect(m_resultImage.rect(), Qt::gray);
	painter.fillRect(m_prevClippingRect, Qt::transparent);
	painter.setCompositionMode(QPainter::CompositionMode_Plus);			
	painter.drawImage(0, 0, m_snapshotImage);	
	painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.setPen(QPen(Qt::blue,3,Qt::DashLine)); //set the pen
	painter.drawRect(QRect(l16, t16, w16, h16));
	painter.end();	

	m_canvasLabel->setPixmap(QPixmap::fromImage(m_resultImage).scaled(m_canvasWidth, m_canvasHeight, Qt::KeepAspectRatio));

	m_currClippingRect = QRect(QPoint(l16, t16), QPoint(r16, b16));
}
	
void AVerClippingRectSettingDialog::createUi()
{
	RECT prevClippingRect = {0};
	RECT resolution = {0};
	RECT zeroRect = {0};

	// as this operation would only be effective when the streaming is stopped
	// so it seems no need to stop streaming
	//AVerStopStreaming(m_hCaptureDevice);

	// get original clipping rect
	AVerGetVideoClippingRect(m_hCaptureDevice, &prevClippingRect);
	m_prevClippingRect = QRect(QPoint(prevClippingRect.left, prevClippingRect.top), QPoint(prevClippingRect.right, prevClippingRect.bottom));

	// get the resolution of the input video without clipping
	AVerSetVideoClippingRect(m_hCaptureDevice, zeroRect);
	// get the original resolution
	AVerGetVideoClippingRect(m_hCaptureDevice, &resolution);
	m_originalResolution = QRect(QPoint(resolution.left, resolution.top), QPoint(resolution.right, resolution.bottom));
	
	AVerStartStreaming(m_hCaptureDevice);

	// get a snapshot of the original input video as the background
	{
		LONG lBufferSize = 0;
		AVerCaptureSingleImageToBuffer(m_hCaptureDevice, NULL, &lBufferSize, TRUE, 2000);  // to get the correct buffer size

		// capture image
		BYTE *pbBuffer = NULL;
		pbBuffer = (BYTE*)malloc(lBufferSize);
		if (!pbBuffer)
		{
			std::cout <<"\nCan't allocate buffer." << std::endl;
			return;
		}

		LONG lRetVal;
		bool isOverlayMix = false;
		lRetVal = AVerCaptureSingleImageToBuffer(m_hCaptureDevice, pbBuffer, &lBufferSize, isOverlayMix, 2000);
		if (lRetVal != CAP_EC_SUCCESS)
		{
			std::cout <<"\nCan't capture image." << std::endl;
			free(pbBuffer);
			return;
		}

		// process the data in the buffer
		m_snapshotImage = QImage::fromData(pbBuffer, (int)lBufferSize, "BMP");
		// release the allocated buffer
		free(pbBuffer);

		// When the paint device is a QImage, the image format must be set to Format_ARGB32Premultiplied or Format_ARGB32 
		// for the composition modes to have any effect. 
		// For performance the premultiplied version is the preferred format.
		m_resultImage =  QImage(resolution.right-resolution.left+1, resolution.bottom-resolution.top+1, QImage::Format_ARGB32_Premultiplied);
	}

	AVerStopStreaming(m_hCaptureDevice);
	// restore the clipping rect
	AVerSetVideoClippingRect(m_hCaptureDevice, prevClippingRect);

	//no need to start
	//AVerStartStreaming(m_hCaptureDevice);

	// interface construction
	double aspect_ratio;
	
	if((resolution.top == resolution.bottom)||(resolution.left == resolution.right))
		aspect_ratio = 1.25;
	else
		aspect_ratio = (double)(resolution.right - resolution.left + 1)/(double)(resolution.bottom - resolution.top + 1);
	
	int canvas_width = 600;
	int canvas_height = (int)(canvas_width / aspect_ratio);
	
	m_canvasWidth = canvas_width;
	m_canvasHeight = canvas_height;
	
	m_canvasLabel = new QLabel(this);
	m_canvasLabel->setFixedSize(canvas_width, canvas_height);
	m_canvasLabel->setAlignment(Qt::AlignCenter);  // keep its content(text or image) in the center
	m_canvasLabel->setAutoFillBackground(true);
	m_canvasLabel->setBackgroundRole(QPalette::Shadow);
	
	QLabel* rectLeftLabel = new QLabel(tr("left"), this);
	m_rectLeftLineEdit = new QLineEdit(this);
	m_rectLeftLineEdit->setText(QString("%1").arg(prevClippingRect.left));

	QLabel* rectRightLabel = new QLabel(tr("right"), this);
	m_rectRightLineEdit = new QLineEdit(this);
	m_rectRightLineEdit->setText(QString("%1").arg(prevClippingRect.right));

	QLabel* rectTopLabel = new QLabel(tr("top"), this);
	m_rectTopLineEdit = new QLineEdit(this);
	m_rectTopLineEdit->setText(QString("%1").arg(prevClippingRect.top));

	QLabel* rectBottomLabel = new QLabel(tr("bottom"), this);
	m_rectBottomLineEdit = new QLineEdit(this);
	m_rectBottomLineEdit->setText(QString("%1").arg(prevClippingRect.bottom));

	m_refreshClippingRectPushButton = new QPushButton(tr("refresh"), this);
	connect(m_refreshClippingRectPushButton, SIGNAL(clicked()), this, SLOT(onRefreshClippingRect()));

	QGridLayout* rectParamLayout = new QGridLayout;
	// parameter for left
	rectParamLayout->addWidget(rectLeftLabel, 0, 0, 1, 1, Qt::AlignLeft);
	rectParamLayout->addWidget(m_rectLeftLineEdit, 0, 1, 1, 1, Qt::AlignLeft);
	// parameter for right
	rectParamLayout->addWidget(rectRightLabel, 1, 0, 1, 1, Qt::AlignLeft);
	rectParamLayout->addWidget(m_rectRightLineEdit, 1, 1, 1, 1, Qt::AlignLeft);
	// parameter for top
	rectParamLayout->addWidget(rectTopLabel, 2, 0, 1, 1, Qt::AlignLeft);
	rectParamLayout->addWidget(m_rectTopLineEdit, 2, 1, 1, 1, Qt::AlignLeft);
	// parameter for bottom
	rectParamLayout->addWidget(rectBottomLabel, 3, 0, 1, 1, Qt::AlignLeft);
	rectParamLayout->addWidget(m_rectBottomLineEdit, 3, 1, 1, 1, Qt::AlignLeft);

	QVBoxLayout* rectGroupLayout = new QVBoxLayout;
	rectGroupLayout->addLayout(rectParamLayout);
	rectGroupLayout->addStretch();
	rectGroupLayout->addWidget(m_refreshClippingRectPushButton, Qt::AlignRight);
	
	QGroupBox* rectGroupBox = new QGroupBox(tr("clipping rect"), this);
	rectGroupBox->setLayout(rectGroupLayout);

	QHBoxLayout* panelLayout = new QHBoxLayout;
	panelLayout->addWidget(m_canvasLabel);
	panelLayout->addWidget(rectGroupBox);
	
	// button box
	m_buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
	connect(m_buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
	connect(m_buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

	QVBoxLayout* mainLayout = new QVBoxLayout;
	mainLayout->addLayout(panelLayout);
	mainLayout->addWidget(m_buttonBox, Qt::AlignRight);
    // if you want the widget to have a fixed size based on its contents, 
	// you can call QLayout::setSizeConstraint(QLayout::SetFixedSize);
	mainLayout->setSizeConstraint(QLayout::SetFixedSize);

	setLayout(mainLayout);

	setWindowTitle(tr("Clipping Rect Setting"));
}

///////////////////////////////////////////////////////////////////
// for AVerCapturingThread
AVerCapturingThread::AVerCapturingThread()
{
	m_bIsCapturingThreadWorking = false;
	m_bIsPreviewEnabled = false;

	m_bufferSizeInitialized = false;
	m_pbBuffer = NULL;
}

AVerCapturingThread::~AVerCapturingThread()
{
	if(m_pbBuffer)
		free(m_pbBuffer);
}

void AVerCapturingThread::setCaptureDevice(Qt::HANDLE captureDevice)
{
	QMutexLocker locker(&m_mutex);
	m_hCaptureDevice = captureDevice;
}

void AVerCapturingThread::setCapturingStatus(bool status)
{
	QMutexLocker locker(&m_mutex);
	m_bIsCapturingThreadWorking = status;
}

void AVerCapturingThread::setPreviewEnabled(bool enabled)
{
	QMutexLocker locker(&m_mutex);
	m_bIsPreviewEnabled = enabled;
}

void AVerCapturingThread::setBufferSizeInitialized(bool initialized)
{
	QMutexLocker locker(&m_mutex);
	m_bufferSizeInitialized = initialized;
}

QImage AVerCapturingThread::getCurrentFrame()
{
	QMutexLocker locker(&m_mutex);
	return m_currentFrame;
}

void AVerCapturingThread::refreshFrame()
{
	if(!m_bufferSizeInitialized)
	{
		// get the size of buffer
		m_lBufferSize = 0;
		AVerCaptureSingleImageToBuffer(m_hCaptureDevice, NULL, &m_lBufferSize, TRUE, 2000);  // to get the correct buffer size

		// allocate the memory only for the first time
		if(m_pbBuffer != NULL)
		{
			free(m_pbBuffer);
		}
		m_pbBuffer = (BYTE*)malloc(m_lBufferSize);

		m_bufferSizeInitialized = true;
	}

	LONG lRetVal;
	bool isOverlayMix = false;
	lRetVal = AVerCaptureSingleImageToBuffer(m_hCaptureDevice, m_pbBuffer, &m_lBufferSize, isOverlayMix, 2000);
	if (lRetVal != CAP_EC_SUCCESS)
	{
		std::cout <<"\nCan't capture image." << std::endl;
		if(m_pbBuffer != NULL)
		{
			free(m_pbBuffer);
			m_pbBuffer = NULL;
		}
		return;
	}

	m_currentFrame = QImage::fromData(m_pbBuffer, (int)m_lBufferSize, "BMP");
}

void AVerCapturingThread::run()
{
	forever {
		m_mutex.lock();

		if (!m_bIsCapturingThreadWorking) {
			m_mutex.unlock();
			break;
		}

		BOOL bSignalPresence = FALSE;
		if(!FAILED(AVerGetSignalPresence(m_hCaptureDevice, &bSignalPresence)))
		{
			bool nosignal = bSignalPresence ? true : false;
			if (bSignalPresence)
			{		
				if(m_bIsPreviewEnabled)
				{
					// do nothing
				}
				else
				{
					refreshFrame();
					emit hasSignal(nosignal);
				}				
			}
			else
			{
				emit hasSignal(nosignal);
			}
		}
		else
		{
			// std::cout << "\nCan't get signal presence." << std::endl;
			continue;
		}
				
		m_mutex.unlock();
	}
}

///////////////////////////////////////////////////////////////////
// for VirtualAVerCapture
VirtualAVerCapture::VirtualAVerCapture(bool isDefaultMode, QWidget *parent)
	:QWidget(parent)
{
	m_dwDeviceNum = 0;
	m_bIsStartStreaming = FALSE;
	m_dwDeviceType = DEVICETYPE_NULL;
	m_hCaptureDevice = NULL;
	
	m_nDeviceIndex = -1;
	
	m_dwCurrentCardType = CARDTYPE_NULL;
	m_bHadSetVideoRenderer = FALSE;

	m_bIsDefaultMode = isDefaultMode;

	m_bIsCaptureDeviceReady = false;

	m_displayMode = AVER_ORIGINAL;
	m_bIsPreviewEnabled = true;

	m_displayMode = AVER_OVERLAYED;
	m_bIsPreviewEnabled = false;

	m_bIsSignalPresent = false;

	m_refreshTimer = new QTimer;

	m_testPointList.clear();
	
	create();
}

VirtualAVerCapture::~VirtualAVerCapture()
{
	if (m_hCaptureDevice)
	{
		DeleteCaptureDevice();
	}
}

void VirtualAVerCapture::selectCaptureDevice(unsigned int nID)
{
	// necessary clear or reset operation(s)
	if (m_hCaptureDevice)
	{
		DeleteCaptureDevice();
	}	
	m_dwDeviceType = DEVICETYPE_NULL;
	m_nDeviceIndex = -1;

	// LONG WINAPI AVerCreateCaptureObjectEx(DWORD dwDeviceIndex, DWORD dwType, HWND hWnd, HANDLE *phCaptureObject)
    // - Create a capture object of a specific type, and return the handle of the capture object.
	LONG lRetVal;
	// if ((nID-ID_APP_DEVICE1_SD)%2+1 == DEVICETYPE_SD) 
	if ((nID+1) == DEVICETYPE_SD)
	{
		// lRetVal = AVerCreateCaptureObjectEx((nID-ID_APP_DEVICE1_SD)/2, DEVICETYPE_SD, m_hWnd, &m_hCaptureDevice);
		lRetVal = AVerCreateCaptureObjectEx(0, DEVICETYPE_SD, (HWND)winId(), &m_hCaptureDevice);
	}
	else
	{
		// lRetVal = AVerCreateCaptureObjectEx((nID-ID_APP_DEVICE1_SD)/2, DEVICETYPE_HD, m_hWnd, &m_hCaptureDevice);
		lRetVal = AVerCreateCaptureObjectEx(0, DEVICETYPE_HD, (HWND)winId(), &m_hCaptureDevice);
	}

	switch (lRetVal)
	{
	case CAP_EC_SUCCESS:
		{
			m_bIsCaptureDeviceReady = true;
			std::cout << "\nThe capture device has been initialized successfully." << std::endl;
		}
		break;
	case CAP_EC_DEVICE_IN_USE:
		std::cout << "\nThe capture device has already been used." << std::endl;
		return;
	default:
		m_bIsCaptureDeviceReady = false;
		std::cout << "\nCan't initialize the capture device." << std::endl;
		return;
	}
	// m_nDeviceIndex = (nID-ID_APP_DEVICE1_SD)/2;
	// Note: There may be more than one card in the system and the index indicates the corresponding device
	//       For card C729, it provides SD and HD interfaces and the two interfaces belong to the same device,
	//       so whether SD or HD has been selected, the device index are the same. 
	//       Now, there are only one card in the system, so the device index should be 0.
	m_nDeviceIndex = 0; // nID/2
	
	if (m_dwDeviceType == DEVICETYPE_NULL)
	{
		// if ((nID-ID_APP_DEVICE1_SD)%2+1 == DEVICETYPE_SD)
		if ((nID+1) == DEVICETYPE_SD)
		{
			m_dwDeviceType = DEVICETYPE_SD;
		}
		else
		{
			m_dwDeviceType = DEVICETYPE_HD;
		}
		m_dwCurrentCardType = GetCurrentCardType(m_nDeviceIndex);
	}

	// to simplify the capture procedure, a default mode is applied
	if(m_bIsDefaultMode)
	{
		if(m_dwDeviceType == DEVICETYPE_HD)
		{
			// default video source
			m_dwVideoSource = VIDEOSOURCE_HDMI;
			AVerSetVideoSource(m_hCaptureDevice, m_dwVideoSource); 
			
			// default video resolution
			unsigned long default_dwVideoResolution = VIDEORESOLUTION_1024X768P;
			AVerSetVideoResolution(m_hCaptureDevice, default_dwVideoResolution);
			m_dwVideoResolution = default_dwVideoResolution;
			
			// only apply the default video frame rate
			DefaultFrameRateSet();
		}
	}
}

bool VirtualAVerCapture::startStreaming() 
{
	// if the device is not ready, return false
	if(!m_bIsCaptureDeviceReady)
		return false;

	// if the device starts streaming, return false 
	if(m_bIsStartStreaming)
		return false;

	if(m_displayMode == AVER_ORIGINAL)
	{
		m_bIsPreviewEnabled = true;
		// LONG WINAPI AVerSetVideoPreviewEnabled(HANDLE hCaptureObject, BOOL bEnabled)
		// - Display the captured video in a preview window.
		AVerSetVideoPreviewEnabled(m_hCaptureDevice, m_bIsPreviewEnabled);		

		// LONG WINAPI AVerSetVideoRenderer(HANDLE hCaptureObject, DWORD dwVideoRenderer)
		// - Choose the video renderer for image preview
		// ?? whether failure to set the video renderer will affect the start streaming or not??
		if (!m_bHadSetVideoRenderer)
		{
			LONG lRetVal = AVerSetVideoRenderer(m_hCaptureDevice, VIDEORENDERER_EVR);
			if (FAILED(lRetVal))
			{
				std::cout << "\nThe current system does not support this video renderer." << std::endl;	
				m_bHadSetVideoRenderer = FALSE;
			}
			m_bHadSetVideoRenderer = TRUE;
		}
	}

	if(m_displayMode == AVER_OVERLAYED)
	{
		m_bIsPreviewEnabled = false;
		AVerSetVideoPreviewEnabled(m_hCaptureDevice, m_bIsPreviewEnabled);

		m_bHadSetVideoRenderer = FALSE;  // due to no preview
	}

	// LONG WINAPI AVerStartStreaming (HANDLE hCaptureObject) 
    // - Initiate the video capture card. Start capturing and displaying video (in the preview window).
	LONG status = AVerStartStreaming(m_hCaptureDevice);
	if (status != CAP_EC_SUCCESS)
	{
		std::cout << "\nCan't start streaming." << std::endl;

		if(status == CAP_EC_ERROR_STATE)
			std::cout << "\nThe video capture card is working." << std::endl;

		if(status == CAP_EC_INVALID_PARAM)
			std::cout << "\nInvalid input parameter." << std::endl;

		return false;
	}
	m_bIsStartStreaming = TRUE;

	m_captureThread.setCaptureDevice(m_hCaptureDevice);
	m_captureThread.setCapturingStatus(m_bIsStartStreaming);
	m_captureThread.setPreviewEnabled(m_bIsPreviewEnabled);
	m_captureThread.setBufferSizeInitialized(false);
	
	if(!m_captureThread.isRunning())
		m_captureThread.start();

	if(m_displayMode == AVER_ORIGINAL)
	{
		RECT rectClient;
		//GetClientRect(&rectClient);
		rectClient.left = geometry().left();
		rectClient.right = geometry().right();
		rectClient.top = geometry().top();
		rectClient.bottom = geometry().bottom();

		// LONG WINAPI AVerSetVideoWindowPosition(HANDLE hCaptureObject, RECT rectVideoWnd)
		// - Set the current video window position and size.
		AVerSetVideoWindowPosition(m_hCaptureDevice, rectClient);
	}

	m_refreshTimer->start(40);

	// for test
	//C729VideoInputInit();

	return true;
}

void VirtualAVerCapture::stopStreaming() 
{
	//bool tmp = m_pixmap.save("temp_test.jpg","jpg");

	// if the device is not ready, do nothing
	if(!m_bIsCaptureDeviceReady)
		return;

	// if the device starts streaming, do nothing  
	if(!m_bIsStartStreaming)
		return;

	m_captureThread.setCapturingStatus(false);
	m_captureThread.wait();

	m_refreshTimer->stop();
	
	// LONG WINAPI AVerStopStreaming (HANDLE hCaptureObject)
    // - Stop the video capture card. Stop capturing and displaying videos.
	AVerStopStreaming(m_hCaptureDevice);

	m_bIsStartStreaming = FALSE;

	update();  // update the displayed content 
}

void VirtualAVerCapture::captureDeviceSetting()
{
	if(m_bIsStartStreaming)
		return;

	DWORD dwVideoInputFrameRate, dwVideoResolution, dwVideoSource;
	AVerGetVideoSource(m_hCaptureDevice, &dwVideoSource);
	AVerGetVideoResolution(m_hCaptureDevice, &dwVideoResolution);
	AVerGetVideoInputFrameRate(m_hCaptureDevice, &dwVideoInputFrameRate);

	AVerSettingDialog dlg(m_hCaptureDevice, m_dwDeviceType, m_dwCurrentCardType);
	
	if(dlg.exec() == QDialog::Accepted)
	{
		if(dlg.isSourceSettingChanged())
			m_dwVideoSource = dlg.videoSource();
		
		if(dlg.isResolutionSettingChanged())
			m_dwVideoResolution = dlg.videoResolution();
		
		if(dlg.isFrameRateSettingChanged())
			m_dwVideoFrameRate = dlg.videoInputFrameRate();

		QMessageBox::information(this, tr("Capture Device Setting"),tr("Settings Accept!"));		
	}
	else
	{
		// if abort the settings, use the original settings
		AVerSetVideoSource(m_hCaptureDevice, dwVideoSource);
		AVerSetVideoResolution(m_hCaptureDevice, dwVideoResolution);
		AVerSetVideoInputFrameRate(m_hCaptureDevice, dwVideoInputFrameRate);

		QMessageBox::information(this, tr("Capture Device Setting"),tr("Settings Abort!"));
	}

	//this->startStreaming();
}

void VirtualAVerCapture::captureClippingRectSetting()
{
	if(m_bIsStartStreaming)
		return;

	AVerClippingRectSettingDialog dlg(m_hCaptureDevice);
	dlg.updateClippingPreview();
	
	if(dlg.exec() == QDialog::Accepted)
	{
		QRect currClippingRect = dlg.currentClippingRect();
		RECT tmp_rect = {0};
		tmp_rect.left = currClippingRect.left();
		tmp_rect.right = currClippingRect.right();
		tmp_rect.top = currClippingRect.top();
		tmp_rect.bottom = currClippingRect.bottom();

		AVerSetVideoClippingRect(m_hCaptureDevice, tmp_rect);

		QMessageBox::information(this, tr("Capture Clipping Rect Setting"),tr("Clipping Rect Settings Accept!"));
		
	}
	else
	{
		QMessageBox::information(this, tr("Capture Clipping Rect Setting"),tr("Clipping Rect Settings Abort!"));
	}
}

void VirtualAVerCapture::captureToBuffer()
{
	if(m_displayMode == AVER_ORIGINAL)
	{
		// LONG WINAPI AVerCaptureSingleImageToBuffer (HANDLE hCaptureObject, BYTE *lpBmpData, LONG *plBufferSize, BOOL bOverlayMix, DWORD dwTimeout)

		// - Capture one picture and save it in BMP format in user memory
		//   - hCaptureObject: Specify a handlefor the video capture object
		//   - lpBmpData:      Pointer to the buffer allocated by user
		//   - plBufferSize:   Pointer to the variable where to the buffer size is stored
		//   - bOverlayMix:    Specify whether the user-defined text, image or time information should be overlaid on the captured pictures.
		//   - dwTimeout:      Specify the duration in nanoseconds. If the picture cannot be captured in the duration, stops capturing and returns.

		// Screenshots will be saved in the buffer allocated by user. The format is BMP, which contains BITMAPFILEHEADER,BITMAPINFOHEADER and RGB24 raw data. 
		// The buffer should be big enough to save the picture, or an error will occur.
		// If the user has no idea how much space should be allocated, just call AVerCaptureSingleImageToBuffer() and set lpBmpData to NULL, 
		// the function will indicate required buffer size through variable pointed by plBufferSize, then call AVerCaptureSingleImageToBuffer() again with the right size.

		// get the size of buffer
		LONG lBufferSize = 0;
		AVerCaptureSingleImageToBuffer(m_hCaptureDevice, NULL, &lBufferSize, TRUE, 2000);  // to get the correct buffer size

		// capture image
		BYTE *pbBuffer = NULL;
		pbBuffer = (BYTE*)malloc(lBufferSize);
		if (!pbBuffer)
		{
			std::cout <<"\nCan't allocate buffer." << std::endl;
			return;
		}

		LONG lRetVal;
		bool isOverlayMix = false;
		lRetVal = AVerCaptureSingleImageToBuffer(m_hCaptureDevice, pbBuffer, &lBufferSize, isOverlayMix, 2000);
		if (lRetVal != CAP_EC_SUCCESS)
		{
			std::cout <<"\nCan't capture image." << std::endl;
			free(pbBuffer);
			return;
		}

		// process the data in the buffer
		{
			QImage tmp_img = QImage::fromData(pbBuffer, (int)lBufferSize, "BMP");
			//QLabel tmp_label;
			//tmp_label.setAutoFillBackground(true);
			//tmp_label.setBackgroundRole(QPalette::Shadow);
			//tmp_label.setPixmap(QPixmap::fromImage(tmp_img));
			//tmp_label.show();
			tmp_img.save("capture to buffer test.jpg", "JPG");
		}
		// release the allocated buffer
		free(pbBuffer);
	}

	if(m_displayMode == AVER_OVERLAYED)
	{
		m_pixmap.toImage().save("capture to buffer test.jpg", "JPG");
	}
}

bool  VirtualAVerCapture::isSignalPresent()
{
	//QMutexLocker locker(&m_mutex);
	return m_bIsSignalPresent;
}

QImage VirtualAVerCapture::getCurrentFrame()
{
	return m_captureThread.getCurrentFrame();
}

void VirtualAVerCapture::updateTestPoint(const std::vector< std::array<double, 3> >& testPoints)
{
	//QMutexLocker locker(&m_mutex);
	m_testPointList.clear();
	size_t p_num = testPoints.size();
	for(size_t i = 0; i < p_num; i++)
		m_testPointList.push_back(testPoints[i]);
}

void VirtualAVerCapture::clearTestPointList()
{
	if(!m_testPointList.empty())
		m_testPointList.clear();
}
///////////////////////////////////////////////////////////////////////////
// protected slots

///////////////////////////////////////////////////////////////////////////
// protected
void VirtualAVerCapture::paintEvent(QPaintEvent* event)
{
	if (m_hCaptureDevice)
	{
		if(m_bIsStartStreaming)
		{
			if(m_bIsSignalPresent)
			{
				if(m_bIsPreviewEnabled)
				{
					AVerRepaintVideo(m_hCaptureDevice);	
				}
				else
				{
					QPainter painter(this);
					painter.drawPixmap(0,0, m_pixmap.scaled(size()));
					//drawInfoText(tr("Signals!"));
				}
			}
			else
			{
				drawInfoText(tr("No signals!"));
			}
		}
		else
		{
			drawInfoText(tr("Waiting for start streaming"));
		}
	}
	else
	{
		drawInfoText(tr("capture device is not ready"));
	}

}

void VirtualAVerCapture::resizeEvent(QResizeEvent* event)
{
	if(!m_bIsPreviewEnabled)
		return;

	if(m_bIsCaptureDeviceReady)
	{
		if(m_bIsStartStreaming)
		{
			RECT rectClient;
			//GetClientRect(&rectClient);
			rectClient.left = geometry().left();
			rectClient.right = geometry().right();
			rectClient.top = geometry().top();
			rectClient.bottom = geometry().bottom();

			if (m_hCaptureDevice)
			{
				AVerSetVideoWindowPosition(m_hCaptureDevice, rectClient);
			}
		}
	}

	//QWidget::resizeEvent(event);
}

void VirtualAVerCapture::moveEvent(QMoveEvent* event)
{
	if(!m_bIsPreviewEnabled)
		return;

	if(m_bIsCaptureDeviceReady)
	{
		if(m_bIsStartStreaming)
		{
			RECT rectClient;
			//GetClientRect(&rectClient);
			rectClient.left = geometry().left();
			rectClient.right = geometry().right();
			rectClient.top = geometry().top();
			rectClient.bottom = geometry().bottom();

			if (m_hCaptureDevice)
			{
				AVerSetVideoWindowPosition(m_hCaptureDevice, rectClient);
			}
		}
	}

	//QWidget::moveEvent(event);
}

// auxiliary function(s)
void VirtualAVerCapture::drawInfoText(QString str)
{
	QPainter painter(this);
	QFont oldFont = painter.font();

	int pointSize = geometry().height() / 40;		
	int textWidth = geometry().width() / 2;
	QPoint center = geometry().center();
	QRect textRect(center.x() - textWidth/2, center.y() - pointSize, textWidth, pointSize*2);

	painter.setFont(QFont("Consolas", pointSize, QFont::Light, true));
	painter.drawText(textRect, Qt::AlignCenter, str);
	painter.setFont(oldFont);
}

///////////////////////////////////////////////////////////////////////////
// private
unsigned long VirtualAVerCapture::GetCurrentCardType(int nCurrDeviceIndex)
{
	if (nCurrDeviceIndex < 0)
	{
		return CARDTYPE_NULL;
	}

	WCHAR wszDeviceName[MAX_PATH] = {0};

	// LONG WINAPI AVerGetDeviceName (DWORD dwDeviceIndex, LPWSTR szDeviceName)   
    // - Get the device name (friendly name) of one of video capture cards in current system.
	// - If the system has multiple videocapture cards, 
	//   their indexes will be defined from 0 to (N-1) and their friendly names will contain the corresponding index. 
	//   They can be assigned from their index. 
	//   Besides, users can differentiate the type of the card according to the name returned.
	AVerGetDeviceName(nCurrDeviceIndex, wszDeviceName);
	
	int iNum = 0;
	while(nCurrDeviceIndex >= 10)
	{
		nCurrDeviceIndex /= 10;
		iNum++;
	}

	const PWCHAR pszTemp = wszDeviceName + iNum + 2;
	
	if (wcscmp(pszTemp, L"AVerMedia C727 PCIe HD Capture Device") == 0)
	{
		return CARDTYPE_C727;
	}
	else if (wcscmp(pszTemp, L"AVerMedia C729 PCIe HD Capture Device") == 0)
	{
		return CARDTYPE_C729;
	}
	else if (wcscmp(pszTemp, L"AVerMedia C199 PCIe VGA Capture Device") == 0)
	{
		return CARDTYPE_C199;
	}
	else if (wcscmp(pszTemp, L"AVerMedia C199 Extreme PCIe HD Capture Device") == 0)
	{
		return CARDTYPE_C199X;
	}
	else if (wcscmp(pszTemp, L"AVerMedia C874 USB HD Capture") == 0)
	{
		return CARDTYPE_C874;
	}
	else if (wcscmp(pszTemp, L"AVerMedia EZCapture Gold WDM Video Capture") == 0)
	{
		return CARDTYPE_V1A8;
	}
	else if (wcscmp(pszTemp, L"AVerMedia C039 USB Capture Card") == 0)
	{
		return CARDTYPE_C039;
	}
	else if (wcscmp(pszTemp, L"AVerMedia C725 PCIe SD Capture Device") == 0)
	{
		return CARDTYPE_C725;
	}
	else if (wcscmp(pszTemp, L"AVerMedia C129 PCIe HD Capture Device") == 0)
	{
		return CARDTYPE_C129;
	}
	else if (wcscmp(pszTemp, L"AVerMedia C968 PCIe SD Capture Device") == 0)
	{
		return CARDTYPE_C968;
	}
	else if (wcscmp(pszTemp, L"AVerMedia C353 PCIe HD Capture Device") == 0)
	{
		return CARDTYPE_C353;
	}
	else if (wcscmp(pszTemp, L"AVerMedia C351 PCIe SD Capture Device") == 0)
	{
		return CARDTYPE_C351;
	}
	else if (wcscmp(pszTemp, L"AVerMedia C997 PCIe SDI Capture Device") == 0)
	{
		return CARDTYPE_C997;
	}
	else if (wcscmp(pszTemp, L"AVerMedia C351 PAL SQ PCIe SD Capture Device") == 0)
	{
		return CARDTYPE_C351_PAL_SQ;
	}
	else
	{
		return CARDTYPE_NULL;
	}
}

void VirtualAVerCapture::create()
{
	// widget setting
	setBackgroundRole(QPalette::Dark);
	setAutoFillBackground(true);
	
	// get signal from the thread
	connect(&m_captureThread, SIGNAL(hasSignal(bool)), this, SLOT(signalPresence(bool)));

	// connect timer signal-slot
	connect(m_refreshTimer, SIGNAL(timeout()), this, SLOT(onTimerRefreshFrame()));

	// aver capture initialization
	WCHAR wszDeviceName[MAX_PATH] = {0};

	// LONG WINAPI AVerGetDeviceNum (DWORD*pdwDeviceNum) 
    // - Get amount of video capture card in current system. 
	if (AVerGetDeviceNum(&m_dwDeviceNum) != CAP_EC_SUCCESS)
	{
		std::cout << "\nCan't get the number of devices." << std::endl;
		return;
	}
	else
	{
		std::cout << "There is(are) " << m_dwDeviceNum << " device(s) in current system." << std::endl; 
	}

	if (m_dwDeviceNum == 0) {
		return;
	}

	// traversal all the possible devices
	for (DWORD dwDeviceIndex = 0; dwDeviceIndex < m_dwDeviceNum; dwDeviceIndex++)
	{
		// LONG WINAPI AVerGetDeviceName (DWORD dwDeviceIndex, LPWSTR szDeviceName) 
		// - Get the device name (friendly name) of one of video capture cards in current system. 
		if(AVerGetDeviceName(dwDeviceIndex, wszDeviceName) != CAP_EC_SUCCESS)
		{
			std::cout << "\nCan't get the name of device."<< std::endl;
			return;
		}
		else
		{
			QString tmp_str = QString::fromWCharArray(wszDeviceName+2); // wchar_t to QString
			std::cout << "\nThe No." << dwDeviceIndex << " device is " << tmp_str.toStdString() << std::endl;
		}

		m_dwCurrentCardType = GetCurrentCardType(dwDeviceIndex);
		
		{	
			// ID_APP_DEVICE1_SD+dwDeviceIndex*2, "SD Device"; 
			// ID_APP_DEVICE1_SD+dwDeviceIndex*2+1, "HD Device"; 
		}
	}

	return;
}

void VirtualAVerCapture::C729VideoInputInit()
{
	// LONG WINAPI AVerGetVideoSource (HANDLE hCaptureObject, DWORD *pdwVideoSource)
    // - Get the current input video terminal. 
    AVerGetVideoSource(m_hCaptureDevice, &m_dwVideoSource);
	switch (m_dwVideoSource)
	{
	case VIDEOSOURCE_SVIDEO:
		std::cout << "\nS-Video source" << std::endl;  // SD
		break;
	case VIDEOSOURCE_COMPOSITE:
		std::cout << "\nComposite source" << std::endl; // SD
		break;
	case VIDEOSOURCE_COMPONENT:
		std::cout << "\nComponent source" << std::endl; // HD
		break;
	case VIDEOSOURCE_HDMI:
		std::cout << "\nHDMI source" << std::endl;      // HD 
		break;
	case VIDEOSOURCE_VGA:
		std::cout << "\nVGA source" << std::endl;
		break;
	case VIDEOSOURCE_SDI:
		std::cout << "\nSDI source" << std::endl;
		break;
	case VIDEOSOURCE_ASI:
		std::cout << "\nASI source" << std::endl;
	}

	INPUT_VIDEO_INFO inputVideoInfo = {0};
	inputVideoInfo.dwVersion = 1;
	AVerGetVideoInfo(m_hCaptureDevice, &inputVideoInfo);
	
	// parse the content of the INPUT_VIDEO_INFO	
	std::cout << "\nthe video width is: " << inputVideoInfo.dwWidth << "." << std::endl;
	std::cout << "\nthe video height is: " << inputVideoInfo.dwHeight << "." << std::endl;

	if(inputVideoInfo.bProgressive)
		std::cout << "\nthe video is progressive." << std::endl;
	else
		std::cout << "\nthe video is interlaced." << std::endl;

	if(inputVideoInfo.dwFormat == VIDEOFORMAT_NTSC)
		std::cout << "\nthe video format is NTSC." << std::endl;
	if(inputVideoInfo.dwFormat == VIDEOFORMAT_PAL)
		std::cout << "\nthe video format is PAL." << std::endl;

	std::cout << "\nthe frame rate of the video input is: " << inputVideoInfo.dwFramerate << "." << std::endl;

	// for test
	DWORD dwVideoResolution;
	AVerGetVideoResolution(m_hCaptureDevice, &dwVideoResolution);
	std::cout << "\ninitial resolution is: " << dwVideoResolution << "." << std::endl;

	DWORD dwVideoInputFrameRate;
	AVerGetVideoInputFrameRate(m_hCaptureDevice, &dwVideoInputFrameRate);	
	std::cout << "\ninitial frame rate is: " << dwVideoInputFrameRate << "." << std::endl;
}

void VirtualAVerCapture::C729VideoSourceSet()
{
	if (m_dwVideoSource == VIDEOSOURCE_COMPONENT)
	{
		DWORD  dwVideoResolution;
		AVerGetVideoResolution(m_hCaptureDevice, &dwVideoResolution);
		if (dwVideoResolution !=VIDEORESOLUTION_640X480 && dwVideoResolution != VIDEORESOLUTION_704X576 && 
			dwVideoResolution != VIDEORESOLUTION_720X480 && dwVideoResolution != VIDEORESOLUTION_720X480P && 
			dwVideoResolution != VIDEORESOLUTION_720X576 && dwVideoResolution != VIDEORESOLUTION_720X576P &&
			dwVideoResolution != VIDEORESOLUTION_1280X720P && dwVideoResolution != VIDEORESOLUTION_1920X1080)
		{
			dwVideoResolution = VIDEORESOLUTION_640X480;
			AVerSetVideoResolution(m_hCaptureDevice, dwVideoResolution);
		}			
	}

	AVerSetVideoSource(m_hCaptureDevice, m_dwVideoSource); 
}

void VirtualAVerCapture::DeleteCaptureDevice()
{
	if (m_bIsStartStreaming)
	{
		// LONG WINAPI AVerStopStreaming (HANDLE hCaptureObject)
		// - Stop the video capture card. Stop capturing and displaying videos.
		AVerStopStreaming(m_hCaptureDevice);

		m_bIsStartStreaming = FALSE;
	}
	// LONG WINAPI AVerDeleteCaptureObject (HANDLE hCaptureObject)
	// - Delete the capture object and release the devices occupied by the selected video capture card.
	AVerDeleteCaptureObject(m_hCaptureDevice);

	m_hCaptureDevice = NULL;
}

void VirtualAVerCapture::DefaultFrameRateSet()
{
	DWORD dwDeviceIndex, dwVideoInputFrameRate, dwVideoResolution, dwVideoSource, dwSelIndex = 0xFFFFFFFF;

	AVerGetVideoSource(m_hCaptureDevice, &dwVideoSource);
	AVerGetVideoResolution(m_hCaptureDevice, &dwVideoResolution);
	AVerGetVideoInputFrameRate(m_hCaptureDevice, &dwVideoInputFrameRate);

	switch (m_dwCurrentCardType)
	{
	case CARDTYPE_C727:
		dwDeviceIndex = 0;
		break;
	case CARDTYPE_C729:
		dwDeviceIndex = 1;
		break;
	case CARDTYPE_C199:
	case CARDTYPE_C199X:
		dwDeviceIndex = 2;
		break;
	case CARDTYPE_C874:
		dwDeviceIndex = 3;
		break;
	case CARDTYPE_V1A8:
	case CARDTYPE_C725:
		dwDeviceIndex = 4;
		break;
	case CARDTYPE_C039:
		dwDeviceIndex = 5;
		break;
	case CARDTYPE_C129:
		dwDeviceIndex = 6;
		break;
	case CARDTYPE_C968:
	case CARDTYPE_C351:
		dwDeviceIndex = 7;
		break;
	case CARDTYPE_C353:
		dwDeviceIndex = 8;
		break;
	case CARDTYPE_C997:
		dwDeviceIndex = 9;
		break;
	case CARDTYPE_C351_PAL_SQ:
		dwDeviceIndex = 10;
		break;
	}

	QString strText;
	double dFrameRate;
	int i, nCount = 0;

	std::vector<QString> frameRateStrings;

	switch (g_dwFrameRateArray[dwDeviceIndex][dwVideoSource][dwVideoResolution][1])
	{
	case 0x3:
		frameRateStrings.push_back("29.97"); //m_frameRateComboBox->addItem("29.97");
		frameRateStrings.push_back("25");    //m_frameRateComboBox->addItem("25");
		if (dwVideoInputFrameRate == 2997)
		{
			dwSelIndex = 0;
		}
		else if (dwVideoInputFrameRate == 2500)
		{
			dwSelIndex = 1;
		}
		nCount = 2;
		break;
	case 0xC:
		frameRateStrings.push_back("59.94"); //m_frameRateComboBox->addItem("59.94");
		frameRateStrings.push_back("50");    //m_frameRateComboBox->addItem("50");
		if (dwVideoInputFrameRate == 5994)
		{
			dwSelIndex = 0;
		}
		else if (dwVideoInputFrameRate == 5000)
		{
			dwSelIndex = 1;
		}
		nCount = 2;
		break;
	case 0x1:
		frameRateStrings.push_back("25");  //m_frameRateComboBox->addItem("25");
		if (dwVideoInputFrameRate == 2500)
		{
			dwSelIndex = 0;
		}
		nCount = 1;
		break;
	case 0x2:
		frameRateStrings.push_back("29.97");  //m_frameRateComboBox->addItem("29.97");
		if (dwVideoInputFrameRate == 2997)
		{
			dwSelIndex = 0;
		}
		nCount = 1;
		break;
	case 0x4:
		frameRateStrings.push_back("50"); //m_frameRateComboBox->addItem("50");
		if (dwVideoInputFrameRate == 5000)
		{
			dwSelIndex = 0;
		}
		nCount = 1;
		break;
	case 0x8:
		frameRateStrings.push_back("59.94");//m_frameRateComboBox->addItem("59.94");
		if (dwVideoInputFrameRate == 5994)
		{
			dwSelIndex = 0;
		}
		nCount = 1;
		break;
	case 0xF:
		frameRateStrings.push_back("29.97"); //m_frameRateComboBox->addItem("29.97");
		frameRateStrings.push_back("25");    //m_frameRateComboBox->addItem("25");
		frameRateStrings.push_back("59.94"); //m_frameRateComboBox->addItem("59.94");
		frameRateStrings.push_back("50");    //m_frameRateComboBox->addItem("50");
		if (dwVideoInputFrameRate == 2997)
		{
			dwSelIndex = 0;
		}
		else if (dwVideoInputFrameRate == 2500)
		{
			dwSelIndex = 1;
		}
		else if (dwVideoInputFrameRate == 5994)
		{
			dwSelIndex = 2;
		}
		else if (dwVideoInputFrameRate == 5000)
		{
			dwSelIndex = 3;
		}
		nCount = 4;
	}

	for (i = 0; i < g_dwFrameRateArray[dwDeviceIndex][dwVideoSource][dwVideoResolution][0]-nCount; i++)
	{
		dFrameRate = g_dwFrameRateArray[dwDeviceIndex][dwVideoSource][dwVideoResolution][2+i] / 100.0;		
		strText.sprintf("%-4.2f", dFrameRate); //strText.Format("%-4.2f", dFrameRate);
		frameRateStrings.push_back(strText);//m_frameRateComboBox->addItem(strText);
		if (g_dwFrameRateArray[dwDeviceIndex][dwVideoSource][dwVideoResolution][2+i] == dwVideoInputFrameRate)
		{
			dwSelIndex = i+nCount;
		}
	}
	if (dwSelIndex != 0xFFFFFFFF)
	{
		//m_frameRateComboBox->setCurrentIndex(dwSelIndex);
		DWORD default_dwVideoInputFrameRate = (DWORD)(atof(frameRateStrings[dwSelIndex].toStdString().c_str())*100);
	    AVerSetVideoInputFrameRate(m_hCaptureDevice, default_dwVideoInputFrameRate);
		m_dwVideoFrameRate = default_dwVideoInputFrameRate;
	}
	else
	{
		std::cout << "\nThe default settings will be restored since the previous settings are wrong." << std::endl;
		AVerSetVideoInputFrameRate(m_hCaptureDevice, dwVideoInputFrameRate);
		m_dwVideoFrameRate = dwVideoInputFrameRate;
		//m_frameRateComboBox->clear();
	}
}

void VirtualAVerCapture::signalPresence(bool hasSignal)
{
	QMutexLocker locker(&m_mutex);

	m_bIsSignalPresent = hasSignal;

	if(m_bIsSignalPresent)
	{
		//std::cout << "\nsignal!" << std::endl;
	}
	else
	{
		std::cout << "\nno signal!" << std::endl;
	}
}

void VirtualAVerCapture::onTimerRefreshFrame()
{
	if (m_hCaptureDevice)
	{
		if(m_bIsStartStreaming)
		{
			if(m_bIsSignalPresent)
			{
				if(!m_bIsPreviewEnabled)
				{
					m_pixmap = QPixmap::fromImage(m_captureThread.getCurrentFrame());

					// if test point list is not empty, overlay it
					if(!m_testPointList.empty())
					{
						QPainter painter(&m_pixmap);
						drawTestPointList(&painter);
					}
				}
			}
			update();
		}
	}	
}

void VirtualAVerCapture::drawTestPointList(QPainter *painter)
{
	QPen oldPen = painter->pen();

	int pen_width = 5;
	QPen newPen;
	newPen.setWidth(pen_width);
	newPen.setColor(Qt::red);
	painter->setPen(newPen);
	QRect pixRect = m_pixmap.rect();
	
	//m_mutex.lock();
	int p_num = m_testPointList.size();
	for(int i = 0; i < p_num; i++)
	{
		QPointF ptf(m_testPointList[i][0], m_testPointList[i][1]);
		if(pixRect.contains(ptf.toPoint()))
		{			
			painter->drawPoint(ptf);
		}
	}
	//m_mutex.unlock();

	painter->setPen(oldPen);
}

/////////////////////////////////////////////////////////////
// auxiliary function
bool VirtualAVerCapture::getCurrentResolution(int *w, int *h)
{
	switch(m_dwVideoResolution)
	{
	case VIDEORESOLUTION_640X480:
		*w = 640;	
		*h = 480;	
		return true;	
		break;
	case VIDEORESOLUTION_704X576:
		*w = 704;	
		*h = 576;	
		return true;
		break;
	case VIDEORESOLUTION_720X480:
		*w = 720;	
		*h = 480;	
		return true;
		break;
	case VIDEORESOLUTION_720X480P:
		*w = 720;	
		*h = 480;	
		return true;
		break;
	case VIDEORESOLUTION_720X576:
		*w = 720;	
		*h = 576;	
		return true;
		break;
	case VIDEORESOLUTION_720X576P:
		*w = 720;	
		*h = 576;	
		return true;
		break;
	case VIDEORESOLUTION_1280X720P:
		*w = 1280;	
		*h = 720;	
		return true;
		break;
	case VIDEORESOLUTION_1920X1080:
		*w = 1920;	
		*h = 1080;	
		return true;
		break;
	case VIDEORESOLUTION_1024X768P:
		*w = 1024;	
		*h = 768;	
		return true;
		break;
	case VIDEORESOLUTION_1280X800P:
		*w = 1280;	
		*h = 800;	
		return true;
		break;
	case VIDEORESOLUTION_1280X1024P:
		*w = 1280;	
		*h = 1024;	
		return true;
		break;
	case VIDEORESOLUTION_1440X900P:
		*w = 1440;	
		*h = 900;	
		return true;
		break;
	case VIDEORESOLUTION_1600X1200P:
		*w = 1600;	
		*h = 1200;
		return true;
		break;
	case VIDEORESOLUTION_1680X1050P:
		*w = 1680;	
		*h = 1050;	
		return true;
		break;
	case VIDEORESOLUTION_1920X1080P:
		*w = 1920;	
		*h = 1080;	
		return true;
		break;
	case VIDEORESOLUTION_1920X1080P_24FPS:
		*w = 1920;	
		*h = 1080;	
		return true;
		break;
	case VIDEORESOLUTION_640X480P:
		*w = 640;	
		*h = 480;	
		return true;
		break;
	case VIDEORESOLUTION_800X600P:
		*w = 800;	
		*h = 600;	
		return true;
		break;
	case VIDEORESOLUTION_1280X768P:
		*w = 1280;	
		*h = 768;	
		return true;
		break;
	case VIDEORESOLUTION_1360X768P:
		*w = 1360;	
		*h = 768;	
		return true;
		break;
	case VIDEORESOLUTION_160X120:
		*w = 160;	
		*h = 120;	
		return true;
		break;
	case VIDEORESOLUTION_176X144:
		*w = 176;	
		*h = 144;	
		return true;
		break;
	case VIDEORESOLUTION_240X176:
		*w = 240;	
		*h = 176;	
		return true;
		break;
	case VIDEORESOLUTION_240X180:
		*w = 240;	
		*h = 180;	
		return true;
		break;
	case VIDEORESOLUTION_320X240:
		*w = 320;	
		*h = 240;	
		return true;
		break;
	case VIDEORESOLUTION_352X240:
		*w = 352;	
		*h = 240;	
		return true;
		break;
	case VIDEORESOLUTION_352X288:
		*w = 352;	
		*h = 288;	
		return true;
		break;
	case VIDEORESOLUTION_640X240:
		*w = 640;	
		*h = 240;	
		return true;
		break;
	case VIDEORESOLUTION_640X288:
		*w = 640;	
		*h = 288;	
		return true;
		break;
	case VIDEORESOLUTION_720X240:
		*w = 720;	
		*h = 240;	
		return true;
		break;
	case VIDEORESOLUTION_720X288:
		*w = 720;	
		*h = 288;	
		return true;
		break;
	case VIDEORESOLUTION_80X60:
		*w = 80;	
		*h = 60;	
		return true;
		break;
	case VIDEORESOLUTION_88X72:
		*w = 88;	
		*h = 72;	
		return true;
		break;
	case VIDEORESOLUTION_128X96:
		*w = 128;	
		*h = 96;	
		return true;
		break;
	case VIDEORESOLUTION_640X576:
		*w = 640;	
		*h = 576;	
		return true;
		break;
	case VIDEORESOLUTION_1152X864P:
		*w = 1152;	
		*h = 864;	
		return true;
		break;
	case VIDEORESOLUTION_1280X960P:
		*w = 1280;	
		*h = 960;	
		return true;
		break;
	case VIDEORESOLUTION_180X120:
		*w = 180;	
		*h = 120;	
		return true;
		break;
	case VIDEORESOLUTION_180X144:
		*w = 180;	
		*h = 144;	
		return true;
		break;
	case VIDEORESOLUTION_360X240:
		*w = 360;	
		*h = 240;	
		return true;
		break;
	case VIDEORESOLUTION_360X288:
		*w = 360;	
		*h = 288;	
		return true;
		break;
	case VIDEORESOLUTION_768X576:
		*w = 768;	
		*h = 576;	
		return true;
		break;
	case VIDEORESOLUTION_384x288:
		*w = 384;	
		*h = 288;	
		return true;
		break;
	case VIDEORESOLUTION_192x144:
		*w = 192;	
		*h = 144;	
		return true;
		break;
	default:
		return false;
		break;
	};
}

	
