#include "endogadget.h"

#include <QLayout>

#include "virtualavercapture.h"

// for aver resolution
#define AVER_RESOLUTION_W 1024
#define AVER_RESOLUTION_H 768

EndoGadget::EndoGadget(bool isTest, QWidget *parent)
	:QWidget(parent)
{
	m_bIsTest = isTest;
	
	// construct a capture device object
	m_virtualAVerCapture = new VirtualAVerCapture(m_bIsTest, this);
	// Some methods must be called before capturing or you will get a CAP_EC_ERROR_STATE error, for instance
    // AVerSetVideoSource()
    // AVerSetVideoFormat()
    // AVerSetVideoResolution()
    // AVerSetVideoInputFrameRate()
    // If you want to reset the video configurations by the methods listed above, call AVerStopStreaming() first to stop video capture card. 
	// After the new configuration is done, call AVerStartStreaming() to restart the video capture card.

	// create UI element(s)
	createUi();
}

EndoGadget::~EndoGadget()
{
	if(m_virtualAVerCapture)
	{
		delete m_virtualAVerCapture; 
	}
}

void EndoGadget::selectCaptureDevice(unsigned int nID)
{
	if(m_virtualAVerCapture)
	{
		m_virtualAVerCapture->selectCaptureDevice(nID);
	}

	updateCaptureDeviceWidgets();
}

bool EndoGadget::isDeviceReady()
{
	return m_virtualAVerCapture->isCaptureDeviceReady();
}

bool EndoGadget::isDeviceStartStreaming()
{
	return m_virtualAVerCapture->isCaptureDeviceStartStreaming();
}

bool EndoGadget::isSignalPresent()
{
	return m_virtualAVerCapture->isSignalPresent();
}

QImage EndoGadget::getCurrentFrame()
{
	return m_virtualAVerCapture->getCurrentFrame();
}

bool EndoGadget::getCurrentResolution(int *w, int *h)
{
	if(m_virtualAVerCapture->getCurrentResolution(w, h))
		return true;
	else
		return false;
}

void EndoGadget::updateTestPoint(const std::vector< std::array<double, 3> > &testPoints)
{
	m_virtualAVerCapture->updateTestPoint(testPoints);
}

void EndoGadget::clearTestPointList()
{
	m_virtualAVerCapture->clearTestPointList();
}
////////////////////////////////////////////
// protected slot(s)
void EndoGadget::onCaptureDeviceStartStreaming()
{
	if(m_virtualAVerCapture->isCaptureDeviceReady())
	{
		if(!m_virtualAVerCapture->startStreaming())
			return;
		
		m_captureDeviceStartStreamingAction->setEnabled(false);
		m_captureDeviceStopStreamingAction->setEnabled(true);

		m_captureDeviceSettingAction->setEnabled(false);
		m_captureClippingRectSettingAction->setEnabled(false);

		m_captureToBufferAction->setEnabled(true);
	}	
}

void EndoGadget::onCaptureDeviceStopStreaming()
{
	if(m_virtualAVerCapture->isCaptureDeviceReady())
	{
		if(!m_virtualAVerCapture->isCaptureDeviceStartStreaming())
			return;

		m_virtualAVerCapture->stopStreaming();

		m_captureDeviceStartStreamingAction->setEnabled(true);
		m_captureDeviceStopStreamingAction->setEnabled(false);

		m_captureDeviceSettingAction->setEnabled(true);
		m_captureClippingRectSettingAction->setEnabled(true);

		m_captureToBufferAction->setEnabled(false);
	}	
}

void EndoGadget::onCaptureDeviceSetting()
{
	if(m_virtualAVerCapture->isCaptureDeviceReady())
	{
		if(m_virtualAVerCapture->isCaptureDeviceStartStreaming())
			return;
		
		m_virtualAVerCapture->captureDeviceSetting();
	}
}

void EndoGadget::onCaptureClippingRectSetting()
{
	if(m_virtualAVerCapture->isCaptureDeviceReady())
	{
		if(m_virtualAVerCapture->isCaptureDeviceStartStreaming())
			return;
		
		m_virtualAVerCapture->captureClippingRectSetting();
	}
}

void EndoGadget::onCaptureToBuffer()
{
	if(m_virtualAVerCapture->isCaptureDeviceReady())
	{
		if(!m_virtualAVerCapture->isCaptureDeviceStartStreaming())
			return;
		
		m_virtualAVerCapture->captureToBuffer();
	}
}

void EndoGadget::showContextMenu(const QPoint & pos)
{
	// only in test mode, the context menu will be effective
	if(!m_bIsTest)
		return;

	if(m_contextMenu)
	{
		m_contextMenu->exec(QCursor::pos());
	}
}

////////////////////////////////////////////
// private function(s)
void EndoGadget::createUi()
{
	// layout the widget(s)
	m_virtualAVerCapture->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	m_virtualAVerCapture->setFixedSize(AVER_RESOLUTION_W * 0.85, AVER_RESOLUTION_H * 0.85);
	QHBoxLayout* mainLayout = new QHBoxLayout;
	mainLayout->addWidget(m_virtualAVerCapture);
	setLayout(mainLayout);

	// for context menu
	m_contextMenu = new QMenu;	
	m_contextMenu->setFont(QFont("Sans Serif", 10));  // note: the font of the menu controls the font used by menu items
	// the widget emits the QWidget::customContextMenuRequested() signal.
	setContextMenuPolicy(Qt::CustomContextMenu); 
	// only above policy has been set, the connection will be valid
	connect(this, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(showContextMenu(const QPoint&))); 
		
	m_captureDeviceStartStreamingAction = new QAction(tr("Start Streaming"), this);
	connect(m_captureDeviceStartStreamingAction, SIGNAL(triggered()), this, SLOT(onCaptureDeviceStartStreaming()));
	m_captureDeviceStartStreamingAction->setEnabled(false);
	
	m_captureDeviceStopStreamingAction = new QAction(tr("Stop Streaming"), this);
	connect(m_captureDeviceStopStreamingAction, SIGNAL(triggered()), this, SLOT(onCaptureDeviceStopStreaming()));
	m_captureDeviceStopStreamingAction->setEnabled(false);
	
	m_captureDeviceSettingAction = new QAction(tr("Parameter Setting"), this);
	connect(m_captureDeviceSettingAction, SIGNAL(triggered()), this, SLOT(onCaptureDeviceSetting()));
	m_captureDeviceSettingAction->setEnabled(false);

	m_captureClippingRectSettingAction = new QAction(tr("Clipping Rect"), this);
	connect(m_captureClippingRectSettingAction, SIGNAL(triggered()), this, SLOT(onCaptureClippingRectSetting()));
	m_captureClippingRectSettingAction->setEnabled(false);

	m_captureToBufferAction = new QAction(tr("Capture To Buffer"), this);
	connect(m_captureToBufferAction, SIGNAL(triggered()), this, SLOT(onCaptureToBuffer()));
	m_captureToBufferAction->setEnabled(false);

	m_captureDeviceMenu = new QMenu(tr("Capture Device"), this);
	m_captureDeviceMenu->addAction(m_captureDeviceStartStreamingAction);
	m_captureDeviceMenu->addAction(m_captureDeviceStopStreamingAction);
	m_captureDeviceMenu->addSeparator();
	m_captureDeviceMenu->addAction(m_captureDeviceSettingAction);
	m_captureDeviceMenu->addAction(m_captureClippingRectSettingAction);
	m_captureDeviceMenu->addSeparator();
	m_captureDeviceMenu->addAction(m_captureToBufferAction);
	m_captureDeviceMenu->setEnabled(false);
	
	m_contextMenu->addMenu(m_captureDeviceMenu);	
}

void EndoGadget::updateCaptureDeviceWidgets()
{
	if(!m_virtualAVerCapture->isCaptureDeviceReady())
	{
		m_captureDeviceMenu->setEnabled(false); // the device is not ready, so the menu item should be disabled
	}
	else
	{
		m_captureDeviceMenu->setEnabled(true);
		if(m_virtualAVerCapture->isCaptureDeviceStartStreaming())
		{
			m_captureDeviceStartStreamingAction->setEnabled(false);
			m_captureDeviceStopStreamingAction->setEnabled(true);

			m_captureDeviceSettingAction->setEnabled(false);
			m_captureClippingRectSettingAction->setEnabled(false);

			m_captureToBufferAction->setEnabled(true);
		}
		else
		{
			m_captureDeviceStartStreamingAction->setEnabled(true);
			m_captureDeviceStopStreamingAction->setEnabled(false);

			m_captureDeviceSettingAction->setEnabled(true);
			m_captureClippingRectSettingAction->setEnabled(true);

			m_captureToBufferAction->setEnabled(false);
		}
	}
}
