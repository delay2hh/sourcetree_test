#ifndef VIRTUAL_AVER_CAPTURE_H
#define VIRTUAL_AVER_CAPTURE_H

#include <QWidget>
#include <QDialog>
#include <QMenu>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>

#include <QTimer>
#include <QThread>
#include <QMutex>

#include <array>
#include <vector>

enum
{
	CARDTYPE_NULL = 0,
	CARDTYPE_C727 = 1,
	CARDTYPE_C729 = 2,
	CARDTYPE_C199 = 3,
	CARDTYPE_C199X = 4,
	CARDTYPE_C874 = 5,
	CARDTYPE_V1A8 = 6,
	CARDTYPE_C039 = 7,
	CARDTYPE_C725 = 8,
	CARDTYPE_C129 = 9,
	CARDTYPE_C968 = 10,
	CARDTYPE_C353 = 11,
	CARDTYPE_C351 = 12,
	CARDTYPE_C997 = 13,
	CARDTYPE_C351_PAL_SQ = 14
};

enum DisplayMode
{
	AVER_ORIGINAL = 0,
	AVER_OVERLAYED = 1
};

class QDialogButtonBox;
class QComboBox;

class QPainter;

// auxiliary class(es)
class AVerSettingDialog : public QDialog
{
	Q_OBJECT

public:
	AVerSettingDialog(Qt::HANDLE hCaptureDevice, unsigned long dwDeviceType, unsigned long dwCurrCardType, QWidget* parent = 0);
	~AVerSettingDialog();
	
	unsigned long videoSource() { return m_dwVideoSource;}
	int videoResolution() { return m_nVideoResolution;}
	unsigned long videoInputFrameRate() { return m_dwVideoInputFrameRate;}

	bool isSourceSettingChanged() { return m_bIsSourceSettingChanged; }
	bool isResolutionSettingChanged()  { return m_bIsResolutionSettingChanged; }
	bool isFrameRateSettingChanged()  { return m_bIsFrameRateSettingChanged; }

protected slots:
	void onSourceCurrentIndexChanged(int index);
	void onResolutionCurrentIndexChanged(int index);
	void onFrameRateCurrentIndexChanged(const QString &text);

private:
	Qt::HANDLE m_hCaptureDevice;
	unsigned long m_dwDeviceType;
	unsigned long m_dwCurrCardType;
	
	unsigned long m_dwVideoSource;          // for video source
	int m_nVideoResolution;                 // for video resolution
	unsigned long m_dwVideoInputFrameRate;  // for video frame rate
	
	bool m_bIsSourceSettingChanged;
	bool m_bIsResolutionSettingChanged;
	bool m_bIsFrameRateSettingChanged;

	QComboBox* m_sourceComboBox;
	QComboBox* m_resolutionComboBox;
	QComboBox* m_frameRateComboBox;
	QDialogButtonBox *m_buttonBox;

	void createUi();

	void updateSourceWidgets();
	void updateResolutionWidgets();
	void updateFrameRateWidgets();

	void C729VideoSourceSet();
};

class AVerClippingRectSettingDialog : public QDialog
{
	Q_OBJECT

public:
	AVerClippingRectSettingDialog(Qt::HANDLE hCaptureDevice, QWidget* parent = 0);
	~AVerClippingRectSettingDialog();

	QRect currentClippingRect() { return m_currClippingRect; }
	void updateClippingPreview();
	
protected slots:
	void onRefreshClippingRect();

private:
	Qt::HANDLE m_hCaptureDevice;

	QLineEdit* m_rectLeftLineEdit;
	QLineEdit* m_rectTopLineEdit;
	QLineEdit* m_rectRightLineEdit;
	QLineEdit* m_rectBottomLineEdit;
	QPushButton* m_refreshClippingRectPushButton;
	QLabel* m_canvasLabel;
	QDialogButtonBox *m_buttonBox;

	QImage m_snapshotImage;
	QImage m_resultImage;

	int m_canvasWidth;
	int m_canvasHeight;

	QRect m_prevClippingRect;
	QRect m_originalResolution;
	QRect m_currClippingRect;

	void createUi();
};

class AVerCapturingThread : public QThread
{
	Q_OBJECT

public:
	AVerCapturingThread();
	~AVerCapturingThread();
	void setCaptureDevice(Qt::HANDLE captureDevice);
	void setCapturingStatus(bool status);
	void setPreviewEnabled(bool enabled);
	void setBufferSizeInitialized(bool initialized);
	QImage getCurrentFrame();

signals:
	void hasSignal(bool present);

protected:
	void refreshFrame();
	void run();

private:
	Qt::HANDLE m_hCaptureDevice;     //HANDLE m_hCaptureDevice;
	QImage m_currentFrame;
	QMutex m_mutex;
	bool m_bIsCapturingThreadWorking;
	bool m_bIsPreviewEnabled;
	bool m_bIsFrameValid;

	bool m_bufferSizeInitialized;
	long m_lBufferSize;
	unsigned char *m_pbBuffer;
};

/////////////////////////////////////////////////
// the main class for AVer Capture
class VirtualAVerCapture : public QWidget
{
	Q_OBJECT

public:
	VirtualAVerCapture(bool isDefaultMode = true, QWidget *parent = 0);
	~VirtualAVerCapture();

	// interface function(s)
	void selectCaptureDevice(unsigned int nID); // 0 - SD; 1 - HD

	bool startStreaming(); 
	void stopStreaming();

	void captureDeviceSetting();
	void captureClippingRectSetting();
	
	void captureToBuffer();

	bool isCaptureDeviceReady() { return m_bIsCaptureDeviceReady; }
	bool isCaptureDeviceStartStreaming() { return m_bIsStartStreaming; }
	bool isSignalPresent(); 

	QImage getCurrentFrame();

	bool getCurrentResolution(int *w, int *h);
	// for test
	void updateTestPoint(const std::vector< std::array<double, 3> >& testPoints);
	void clearTestPointList();

protected:
	void paintEvent(QPaintEvent* event);
	void resizeEvent(QResizeEvent* event);
	void moveEvent(QMoveEvent* event);
	// auxiliary function(s)
	void drawInfoText(QString str);

private:
	// variables
	unsigned long m_dwDeviceNum;       //DWORD  m_dwDeviceNum;
	unsigned long m_dwDeviceType;      //DWORD  m_dwDeviceType;
	unsigned long m_dwCurrentCardType; //DWORD  m_dwCurrentCardType;
		
	unsigned long  m_dwVideoSource;       // for video source setting	
	unsigned long  m_dwVideoResolution;   // for video resolution setting
	unsigned long  m_dwVideoFrameRate;    // for video frame rate
		
	int    m_nDeviceIndex;
	
	bool m_bHadSetVideoRenderer; //BOOL   m_bHadSetVideoRenderer;
	bool m_bIsStartStreaming;    //BOOL   m_bIsStartStreaming;

	bool m_bIsCaptureDeviceReady;

	bool m_bIsDefaultMode;

	Qt::HANDLE m_hCaptureDevice;     //HANDLE m_hCaptureDevice;

	AVerCapturingThread m_captureThread;

	QTimer *m_refreshTimer;
	QPixmap m_pixmap;

	// for test
	std::vector< std::array<double, 3> > m_testPointList;
	QMutex m_mutex;

	bool m_bIsSignalPresent;
	DisplayMode m_displayMode;
	bool m_bIsPreviewEnabled;

	// widgets
	QMenu* m_contextMenu;  // for the customized context menu

	// functions
	void create();

	unsigned long GetCurrentCardType(int nCurrDeviceIndex);	
	void C729VideoInputInit();	
	void C729VideoSourceSet();	
	void DeleteCaptureDevice();
	void DefaultFrameRateSet();

	void drawTestPointList(QPainter *painter);
	
private slots:
	void signalPresence(bool hasSignal);
	void onTimerRefreshFrame();
};

#endif