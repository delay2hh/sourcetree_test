#ifndef ENDOSCOPY_GADGET_H
#define ENDOSCOPY_GADGET_H

#include <QWidget>

#include <array>
#include <vector>

class QAction;
class QMenu;

class VirtualAVerCapture;

class EndoGadget: public QWidget
{
	Q_OBJECT

public:
	EndoGadget(bool isTest = true, QWidget *parent = 0);
	~EndoGadget();	

	void selectCaptureDevice(unsigned int nID = 1); // 0 - SD; 1 - HD

	bool isDeviceReady();
	bool isDeviceStartStreaming();
	bool isSignalPresent();

	QImage getCurrentFrame();

	bool getCurrentResolution(int *w, int *h);
	
	// for test
	void updateTestPoint(const std::vector< std::array<double, 3> > &testPoints);
	void clearTestPointList();
	
protected slots:
	// for capture device
	void onCaptureDeviceStartStreaming();
	void onCaptureDeviceStopStreaming();
	void onCaptureDeviceSetting();
	void onCaptureClippingRectSetting();
	void onCaptureToBuffer();
	// for ui elements
	void showContextMenu(const QPoint & pos);

private:
	////////////////////////////////////
	// variables & members
	bool m_bIsTest;

	// widgets
	QMenu* m_contextMenu;  // for the customized context menu
	
	QMenu* m_captureDeviceMenu;
	QAction* m_captureDeviceStartStreamingAction;
	QAction* m_captureDeviceStopStreamingAction;
	
	QAction* m_captureDeviceSettingAction;
	QAction* m_captureClippingRectSettingAction;
	
	QAction* m_captureToBufferAction;

	// AVer capture device related parameters
	VirtualAVerCapture* m_virtualAVerCapture;

	/////////////////////////////////////
	// functions
	void createUi();
	void updateCaptureDeviceWidgets();
};

#endif