#include <QtUiTools/QtUiTools>

#include <QTextStream>
#include <QDateTime>
#include <QFile>

#include <QMutexLocker>

#include <opencv2/core.hpp>
#include <opencv2/core/core_c.h>
#include <opencv2/core/utility.hpp>

#include <opencv2/calib3d.hpp>
#include <opencv2/features2d.hpp>

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <math.h>

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <set>
#include <map>
#include <queue>

#include <sys/timeb.h>
#include <limits.h>

#include "aver\endogadget.h"

#include "kneemotionobserver.h"

///////////////////////////////////////////////////
// auxiliary data structure(s) & function(s)

/* Maximum number of data points allowed */
#define MAX_POINTS	10000

#define PI 3.14159265358979323846264338327950288419716939937511

#define SQRT3   1.732050807568877293527446341505872366943

/////////////////////////////////////////////////////////////////////////////////////////
// data structure(s)
struct Center
{
	cv::Point2d location;
	double radius;
	double confidence;
};

struct BlobDetectorParams
{
	bool filterByColor;
	uchar blobColor;

	bool filterByArea;
	float minArea, maxArea;

	bool filterByCircularity;
	float minCircularity, maxCircularity;

	bool filterByInertia;
	float minInertiaRatio, maxInertiaRatio;

	bool filterByConvexity;
	float minConvexity, maxConvexity;
};

struct MSTEdge
{
	int v1;  // indicate point1
	int v2;  // indicate point2
	cv::Point2f dir;  // point2 to point1
	cv::Point2f abs_dir; 
};

struct GridNode
{
	int ind;   // index of the node in the blob center array

	// four search directions
	// 0 -> +x; 1 -> -y; 2 -> -x; 3 -> +y 
	bool searchable[4];  
	cv::Point2f search_dir[4];
	double search_step[4];

	// the label is used to locate the node in a topological way
	bool labeled;  // whether the node has been assigned a valid label
	int label[2];  // 0 -> x_dir; 1 -> y_dir
};

/////////////////////////////////////////////////////////////////////////////////////////
// function(s)

// for world coord to image coord
#define SQRT(x) sqrt(fabs(x))
/************************************************************************/
/* This cube root routine handles negative arguments (unlike cbrt).     */
double CBRT(double x)
{
	if (x == 0)
		return (0);
	else if (x > 0)
		return (pow(x, (double) 1.0 / 3.0));
	else
		return (-pow(-x, (double) 1.0 / 3.0));
} 

// for calibration test
int loadTestData(char* filename, double *objectPoints, double *imagePoints)
{
	int n;
	FILE* fp = fopen(filename, "r");

	if(fp == NULL)
	{
		std::cout << "\nFail to open the data file." << std::endl;
		return -1;
	}

	n = 0;
	while (fscanf (fp, "%lf %lf %lf %lf %lf",
		&(objectPoints[3*n]),
		&(objectPoints[3*n+1]),
		&(objectPoints[3*n+2]),
		&(imagePoints[2*n]),
		&(imagePoints[2*n+1])) != EOF)
	if (n++ >= MAX_POINTS) {
	    std::cout << "\ntoo many points, the limit is " << MAX_POINTS << std::endl;
	    return -1;
	}

	fclose(fp);

	return n;
}

void print_cp_cc_data (camera_parameters *cp, calibration_constants *cc)
{
	printf("\n");
    printf("       f = %.6lf  [mm]\n\n", cc->f);

    printf("       kappa1 = %.6le  [1/mm^2]\n\n", cc->kappa1);

    printf("       Tx = %.6lf,  Ty = %.6lf,  Tz = %.6lf  [mm]\n\n", cc->Tx, cc->Ty, cc->Tz);

    printf("       Rx = %.6lf,  Ry = %.6lf,  Rz = %.6lf  [deg]\n\n",
	     cc->Rx * 180 / PI, cc->Ry * 180 / PI, cc->Rz * 180 / PI);

    printf("       R\n");
    printf("       %9.6lf  %9.6lf  %9.6lf\n", cc->r1, cc->r2, cc->r3);
    printf("       %9.6lf  %9.6lf  %9.6lf\n", cc->r4, cc->r5, cc->r6);
    printf("       %9.6lf  %9.6lf  %9.6lf\n\n", cc->r7, cc->r8, cc->r9);

    printf("       sx = %.6lf\n", cp->sx);
    printf("       Cx = %.6lf,  Cy = %.6lf  [pixels]\n\n", cp->Cx, cp->Cy);

    printf("       Tz / f = %.6lf\n\n", cc->Tz / cc->f);
}

void print_cp_cc_data_heik(camera_parameters *cp, calibration_constants_heik *cc)
{
	printf("\n");
    printf("       fx = %.6lf  [mm];  fy = %.6lf  [mm]\n\n", cc->fx, cc->fy);

    printf("       k1 = %.6le  k2 = %.6le\n\n", cc->k1, cc->k2);

	printf("       p1 = %.6le  p2 = %.6le\n\n", cc->p1, cc->p2);

    printf("       Tx = %.6lf,  Ty = %.6lf,  Tz = %.6lf  [mm]\n\n", cc->Tx, cc->Ty, cc->Tz);

    printf("       Rx = %.6lf,  Ry = %.6lf,  Rz = %.6lf  [deg]\n\n",
	     cc->Rx * 180 / PI, cc->Ry * 180 / PI, cc->Rz * 180 / PI);

    printf("       R\n");
    printf("       %9.6lf  %9.6lf  %9.6lf\n", cc->r1, cc->r2, cc->r3);
    printf("       %9.6lf  %9.6lf  %9.6lf\n", cc->r4, cc->r5, cc->r6);
    printf("       %9.6lf  %9.6lf  %9.6lf\n\n", cc->r7, cc->r8, cc->r9);

    printf("       sx = %.6lf\n", cp->sx);
    printf("       Cx = %.6lf,  Cy = %.6lf  [pixels]\n\n", cp->Cx, cp->Cy);

    printf("       Tz / f = %.6lf\n\n", cc->Tz / cc->fx);
}

// A utility function to find the vertex with minimum key value, from the set of vertices not yet included in MST
int minKey(const std::vector<double> &key, const std::vector<bool> &mstSet)
{
   // Initialize min value
   double min = DBL_MAX;
   int min_index;

   int V = key.size();

   for (int v = 0; v < V; v++)
     if (mstSet[v] == false && key[v] <= min)
         min = key[v], min_index = v;

   return min_index;
}

// A utility function to print the constructed MST stored in parent[]
void printMST(const std::vector<int> &parent, int n, const std::vector< std::vector<double> > &graph)
{
	int V = n;
	printf("Edge Weight\n");
	for (int i = 1; i < V; i++)
		printf("%d - %d    %f \n", parent[i], i, graph[i][parent[i]]);
}

// Function to construct and print MST for a graph represented using adjacency matrix representation
void primMST(const std::vector< std::vector<double> > &graph, int n, std::vector<int> &parent)
{
	int V = n;
	parent.clear();
	parent.resize(V);
   
	std::vector<double> key(V);   // Key values used to pick minimum weight edge in cut
    std::vector<bool> mstSet(V);  // To represent set of vertices not yet included in MST

     // Initialize all keys as INFINITE
     for (int i = 0; i < V; i++)
        key[i] = DBL_MAX, mstSet[i] = false;

     // Always include first 1st vertex in MST.
     key[0] = 0;     // Make key 0 so that this vertex is picked as first vertex
     parent[0] = -1; // First node is always root of MST 

     // The MST will have V vertices
     for (int count = 0; count < V-1; count++)
     {
        // Pick thd minimum key vertex from the set of vertices
        // not yet included in MST
        int u = minKey(key, mstSet);

        // Add the picked vertex to the MST Set
        mstSet[u] = true;

        // Update key value and parent index of the adjacent vertices of
        // the picked vertex. Consider only those vertices which are not yet
        // included in MST
        for (int v = 0; v < V; v++)
           // graph[u][v] is non zero only for adjacent vertices of m
           // mstSet[v] is false for vertices not yet included in MST
           // Update the key only if graph[u][v] is smaller than key[v]
          if (graph[u][v] && mstSet[v] == false && graph[u][v] < key[v])
             parent[v]  = u, key[v] = graph[u][v];
     }

     //// print the constructed MST
     //printMST(parent, V, graph);
}

int Otsu(IplImage* src)    
{    
    int height=src->height;    
    int width=src->width;        
  
    //histogram    
    float histogram[256] = {0};    
    for(int i=0; i < height; i++)  
    {    
        unsigned char* p=(unsigned char*)src->imageData + src->widthStep * i;    
        for(int j = 0; j < width; j++)   
        {    
            histogram[*p++]++;    
        }    
    }    
    //normalize histogram    
    int size = height * width;    
    for(int i = 0; i < 256; i++)  
    {    
        histogram[i] = histogram[i] / size;    
    }    
  
    //average pixel value    
    float avgValue=0;    
    for(int i=0; i < 256; i++)  
    {    
        avgValue += i * histogram[i];  //����ͼ����ƽ���Ҷ�  
    }     
  
    int threshold;      
    float maxVariance=0;    
    float w = 0, u = 0;    
    for(int i = 0; i < 256; i++)   
    {    
        w += histogram[i];  //���赱ǰ�Ҷ�iΪ��ֵ, 0~i �Ҷȵ�����(��������ֵ�ڴ˷�Χ�����ؽ���ǰ������) ��ռ����ͼ���ı���  
        u += i * histogram[i];  // �Ҷ�i ֮ǰ������(0~i)��ƽ���Ҷ�ֵ�� ǰ�����ص�ƽ���Ҷ�ֵ  
  
        float t = avgValue * w - u;    
        float variance = t * t / (w * (1 - w) );    
        if(variance > maxVariance)   
        {    
            maxVariance = variance;    
            threshold = i;    
        }    
    }    
  
    return threshold;    
}

// convert from QImage to cv::Mat (QImageToCvMat) is pretty straightforward. 
// It supports three QImage formats that are analogous to the OpenCV ones discussed above: 
// QImage::Format_RGB32 (8-bit unsigned, 4 channels), 
// QImage::Format_RGB888 (8-bit unsigned, 3 channels), and
// QImage::Format_Indexed8 (8-bit unsigned, 1 channel �C grayscale).
cv::Mat QImageToCvMat( const QImage &inImage, bool inCloneImageData = true )
{
	switch ( inImage.format() )
	{
		// 8-bit, 4 channel
	case QImage::Format_RGB32:
		{
			cv::Mat  mat( inImage.height(), inImage.width(), CV_8UC4, const_cast<uchar*>(inImage.bits()), inImage.bytesPerLine() );
			
			return (inCloneImageData ? mat.clone() : mat);
		}

		// 8-bit, 3 channel
	case QImage::Format_RGB888:
		{
			if ( !inCloneImageData )
				//qWarning() << "ASM::QImageToCvMat() - Conversion requires cloning since we use a temporary QImage";
				std::cout << "\nASM::QImageToCvMat() - Conversion requires cloning since we use a temporary QImage" << std::endl;

			QImage   swapped = inImage.rgbSwapped();

			return cv::Mat( swapped.height(), swapped.width(), CV_8UC3, const_cast<uchar*>(swapped.bits()), swapped.bytesPerLine() ).clone();
		}

		// 8-bit, 1 channel
	case QImage::Format_Indexed8:
		{
			cv::Mat  mat( inImage.height(), inImage.width(), CV_8UC1, const_cast<uchar*>(inImage.bits()), inImage.bytesPerLine() );

			return (inCloneImageData ? mat.clone() : mat);
		}

	default:
		//qWarning() << "ASM::QImageToCvMat() - QImage format not handled in switch:" << inImage.format();
		std::cout << "\nASM::QImageToCvMat() - QImage format not handled in switch:" << inImage.format() << std::endl;
		break;
	}

	return cv::Mat();
}

///////////////////////////////////////////////////
// controls implementation
///////////////////////////////////////////////////
#define POINTCONTROL_H  30
#define POINTCONTROL_W  250

//////////////////////////////////////////////////////////////
// SetPointControl implementation
//////////////////////////////////////////////////////////////

SetPointControl::SetPointControl(int i, QWidget *parent)
    : index(i), QWidget(parent)
{
	// prepare images
	greenPass = QImage(":/images/Green32.png");
	greenPass = greenPass.scaled(greenPass.width()/2, greenPass.height()/2);
	yellowWarning = QImage(":/images/Yellow32.png");
	yellowWarning = yellowWarning.scaled(yellowWarning.width()/2, yellowWarning.height()/2);
	redAlert = QImage(":/images/Red32.png");
	redAlert = redAlert.scaled(redAlert.width()/2, redAlert.height()/2);

	point_status = false;

	createUI();
}

void SetPointControl::createUI()
{
	getPointButton = new QPushButton(tr("Get"), this); 
	resetPointButton = new QPushButton(tr("Reset"), this);
	connect(getPointButton, SIGNAL(clicked()), this, SLOT(getButtonSlot()));
	connect(resetPointButton, SIGNAL(clicked()), this, SLOT(resetButtonSlot()));

	pointIndexLabel = new QLabel(this);
	pointIndexLabel->setText(QString("Point %1").arg(index));
	
	pointStatusLabel = new QLabel(this);
	pointStatusLabel->resize(redAlert.width(), redAlert.height());
	pointStatusLabel->setAlignment(Qt::AlignCenter);	
	pointStatusLabel->setPixmap(QPixmap::fromImage(redAlert));

	getPointButton->setFixedHeight(POINTCONTROL_H * 0.8);
	resetPointButton->setFixedHeight(POINTCONTROL_H * 0.8);
	pointIndexLabel->setFixedHeight(POINTCONTROL_H * 0.8);
	pointStatusLabel->setFixedHeight(POINTCONTROL_H * 0.8);

	QHBoxLayout* layout = new QHBoxLayout;
	layout->setContentsMargins(0, 0, 0, 0);
	layout->addWidget(pointStatusLabel);
	layout->addWidget(pointIndexLabel);
	layout->addWidget(getPointButton);
	layout->addWidget(resetPointButton);	

	setLayout(layout);	
	setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
	setFixedSize(POINTCONTROL_W, POINTCONTROL_H);
}

void SetPointControl::setPointStatus(bool valid)
{
	point_status = valid;
	
	if(point_status)
	{
		pointStatusLabel->setPixmap(QPixmap::fromImage(greenPass));
	}
	else
	{
		pointStatusLabel->setPixmap(QPixmap::fromImage(redAlert));
	}
}

void SetPointControl::getButtonSlot()
{
	emit getPoint(index);
}

void SetPointControl::resetButtonSlot()
{
	emit resetPoint(index);
}


//////////////////////////////////////////////////////////////
// ToolStatusControl implementation
//////////////////////////////////////////////////////////////
ToolStatusControl::ToolStatusControl(QString& name, QWidget *parent)
   : QWidget(parent), tool_name(name)
{
	// prepare images
	greenPass = QImage(":/images/Green32.png");
	greenPass = greenPass.scaled(greenPass.width()/2, greenPass.height()/2);
	yellowWarning = QImage(":/images/Yellow32.png");
	yellowWarning = yellowWarning.scaled(yellowWarning.width()/2, yellowWarning.height()/2);
	redAlert = QImage(":/images/Red32.png");
	redAlert = redAlert.scaled(redAlert.width()/2, redAlert.height()/2);

	tool_status = false;

	createUI();
}

void ToolStatusControl::createUI()
{
	toolStatusLabel = new QLabel(this);
	toolStatusLabel->resize(redAlert.width(), redAlert.height());
	toolStatusLabel->setAlignment(Qt::AlignCenter);	
	toolStatusLabel->setPixmap(QPixmap::fromImage(redAlert));

	toolNameLabel = new QLabel(tool_name, this);

	toolStatusLabel->setFixedHeight(POINTCONTROL_H);
	toolNameLabel->setFixedHeight(POINTCONTROL_H);

	QHBoxLayout* layout = new QHBoxLayout;
	layout->setContentsMargins(0, 0, 0, 0);
	layout->addWidget(toolStatusLabel);
	layout->addWidget(toolNameLabel);

	setLayout(layout);	
	setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
}

void ToolStatusControl::setToolStatus(bool valid)
{
	tool_status = valid;
	
	if(tool_status)
	{
		toolStatusLabel->setPixmap(QPixmap::fromImage(greenPass));
	}
	else
	{
		toolStatusLabel->setPixmap(QPixmap::fromImage(redAlert));
	}
}


/////////////////////////////////////////////////////////////////
// KneeMotionObserver implementation
/////////////////////////////////////////////////////////////////
KneeMotionObserver::KneeMotionObserver(QWidget *parent)
    : QWidget(parent)
{
	isGettingPoint = false;
	isKneeMotionTracking = false;

	refIndex = 1; // use the reference 1 which corresponds to the 2nd handle

	for(int i = 0; i < POINTNUM; i++)
	{
		pointsStatus[i] = false;
	}
	for(int i = 0; i < TOOLNUM; i++)
	{
		toolsStatus[i] = false;
	}
    for(int i = 0; i < 3; i++)
	{
		probeOffset[i] = 0.0;
	}
	probeOffset[3] = 1.0;	
	useProbeOffset = false;

	for(int i = 0; i < 3; i++)
	{
		endoOffset[i] = 0.0;
	}
	endoOffset[3] = 1.0;
	useEndoOffset = false;
	
	m_safe_dist = 5.0;  //[mm]

	isPointArrayLocalTransformSet = false;

	// generate point coordinate for each calibration point in the predefined or customized array
	cali_row_interval = 3.0f;
	cali_col_interval = 3.0f;

	// allocate memory for caliPointArrayLocal
	cali_row_num = CALIARRAY_ROWNUM;
	cali_col_num = CALIARRAY_COLNUM;

	cali_origin_row_idx = CALIARRAY_ORIGIN_ROW;
	cali_origin_col_idx = CALIARRAY_ORIGIN_COL;

	caliPointArrayLocal = new double** [cali_row_num];
	for(int i = 0; i < cali_row_num; i++)
	{
		caliPointArrayLocal[i] = new double* [cali_col_num];
		for(int j = 0; j < cali_col_num; j++)
		{
			caliPointArrayLocal[i][j] = new double[3];			
		}
	}

	for(int i = 0; i < cali_row_num; i++)
	{
		// the ith row has the same y coordinates
		for(int j = 0; j < cali_col_num; j++)
		{
			caliPointArrayLocal[i][j][0] = j * cali_col_interval;  // x-coord
			caliPointArrayLocal[i][j][1] = i * cali_row_interval;  // y-coord
			caliPointArrayLocal[i][j][2] = 0.0;
		}
	}

	isCapturingEndoImageAndTransform = false;
	m_capturedImages.clear();
	m_cap_t_cali_to_endo.clear();

	m_tmp_dir_name = QString("/tmp_files");
	m_tmp_capture_count_filename = QString("/capture_count.dat");

	isCalibrationDataReady = false;

	isCalibrationSucceed = false;

	m_endoGadget = NULL;

	m_isTest = false;
	m_isTestPointInitialized = false;

	//referenceCOSStatus = false;
	
	//for(int i = 0; i < TOOLNUM-1; i++)
	//{
	//	relativeTransformStatus[i] = false;
	//}
	//for(int i = 0; i < TOOLNUM-1; i++)
	//{
	//	kneeMotionsStatus[i] = false;
	//}

 //   cosInfoReady = false;
	//for(int i = 0; i < TOOLNUM-1; i++)
	//{
	//	kneeMotionDataReady[i] = false;
	//}

	//kneeRelativeMotionDataReady = false;
	//kneeRelativeMotionDataReady2 = false;

	createUI();	
}

KneeMotionObserver::~KneeMotionObserver()
{
	// clear the allocated memory
	for(int i = 0; i < cali_row_num; i++)
	{
		for(int j = 0; j < cali_col_num; j++)
		{
			delete[] caliPointArrayLocal[i][j];
		}
		delete[] caliPointArrayLocal[i];
	}
	delete[] caliPointArrayLocal;

	if(!m_capturedImages.empty())
		m_capturedImages.clear();
	
	if(!m_cap_t_cali_to_endo.empty())
	{
		for(size_t i = 0; i < m_cap_t_cali_to_endo.size(); i++)
			delete[] m_cap_t_cali_to_endo[i];
		m_cap_t_cali_to_endo.clear();
	}
}

void KneeMotionObserver::createUI()
{
	// tool status
	QGroupBox *toolStatusGroup = new QGroupBox("Tool Status", this);
	QVBoxLayout *toolStatusGroupLayout = new QVBoxLayout;
	toolStatusGroupLayout->setSpacing(0);
	toolStatusGroupLayout->setContentsMargins(11, 5, 11, 5);
	
	//toolStatusControls[0] = new ToolStatusControl(QString("Probe"), this);
	//toolStatusControls[1] = new ToolStatusControl(QString("reference 1"), this);
	//toolStatusControls[2] = new ToolStatusControl(QString("reference 2"), this);
	//toolStatusControls[3] = new ToolStatusControl(QString("reference 3"), this);

	toolStatusControls[0] = new ToolStatusControl(QString("Probe"), this);
	toolStatusControls[1] = new ToolStatusControl(QString("Endo. Ref."), this);
	toolStatusControls[2] = new ToolStatusControl(QString("Cali. Ref."), this);
	
	for(int i = 0; i < TOOLNUM; i++)
	{ 
		toolStatusGroupLayout->addWidget(toolStatusControls[i]);
	}
	toolStatusGroup->setLayout(toolStatusGroupLayout);
	toolStatusGroup->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	// probe offset option
	QGroupBox* probeOffsetGroup = new QGroupBox(tr("Probe Offset"), this);
	probeOffsetGroup->setCheckable(true);
	probeOffsetGroup->setChecked(false);
	QGridLayout *probeOffsetGroupLayout = new QGridLayout;
    probeOffsetGroupLayout->addWidget(new QLabel(tr("offset x")), 0, 0, 1, 1, Qt::AlignLeft);
	probeOffsetGroupLayout->addWidget(new QLabel(tr("offset y")), 1, 0, 1, 1, Qt::AlignLeft);
	probeOffsetGroupLayout->addWidget(new QLabel(tr("offset z")), 2, 0, 1, 1, Qt::AlignLeft);
	for(int i = 0; i < 3; i++)
	{
		probeOffsetEdit[i] = new QLineEdit(this);
		probeOffsetEdit[i]->setText(QString("0.0"));
		probeOffsetEdit[i]->setFixedWidth(50);
        probeOffsetGroupLayout->addWidget(probeOffsetEdit[i], i, 1, 1, 1, Qt::AlignLeft);
	}
	probeOffsetGroup->setLayout(probeOffsetGroupLayout);
	probeOffsetGroup->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	connect(probeOffsetGroup, SIGNAL(clicked(bool)), this, SLOT(setUseProbeOffset(bool)));

	// endo offset option
	QGroupBox* endoOffsetGroup = new QGroupBox(tr("Endo Offset"), this);
	endoOffsetGroup->setCheckable(true);
	endoOffsetGroup->setChecked(false);
	QGridLayout *endoOffsetGroupLayout = new QGridLayout;
	endoOffsetGroupLayout->addWidget(new QLabel(tr("offset x")), 0, 0, 1, 1, Qt::AlignLeft);
	endoOffsetGroupLayout->addWidget(new QLabel(tr("offset y")), 1, 0, 1, 1, Qt::AlignLeft);
	endoOffsetGroupLayout->addWidget(new QLabel(tr("offset z")), 2, 0, 1, 1, Qt::AlignLeft);
	for(int i = 0; i < 3; i++)
	{
		endoOffsetEdit[i] = new QLineEdit(this);
		endoOffsetEdit[i]->setText(QString("0.0"));
		endoOffsetEdit[i]->setFixedWidth(50);
		endoOffsetGroupLayout->addWidget(endoOffsetEdit[i], i, 1, 1, 1, Qt::AlignLeft);
	}
	endoOffsetGroup->setLayout(endoOffsetGroupLayout);
	endoOffsetGroup->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	connect(endoOffsetGroup, SIGNAL(clicked(bool)), this, SLOT(setUseEndoOffset(bool)));

	// points information
	QGroupBox *pointsInfoGroup = new QGroupBox(tr("Points Information"), this);
	QGridLayout *pointsInfoGroupLayout = new QGridLayout;
	
	QGroupBox *setPointControlsGroup = new QGroupBox(tr("Set Points"), this);
	QVBoxLayout *setPointControlsGroupLayout = new QVBoxLayout;
	setPointControlsGroupLayout->setSpacing(0);
	setPointControlsGroupLayout->setContentsMargins(11, 5, 11, 5);

	for(int i = 0; i < POINTNUM ; i++)
	{
		setpointControls[i] = new SetPointControl(i, this);
		connect(setpointControls[i], SIGNAL(getPoint(int)), this, SLOT(requirePointSetting(int)));
		connect(setpointControls[i], SIGNAL(resetPoint(int)), this, SLOT(requirePointReset(int)));
		setPointControlsGroupLayout->addWidget(setpointControls[i]);
	}
	setPointControlsGroup->setLayout(setPointControlsGroupLayout);
	setPointControlsGroup->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	getPointProgressBar = new QProgressBar(this);
	getPointProgressBar->setFixedWidth(POINTCONTROL_W + 60);
	getPointProgressBar->setFixedHeight(POINTCONTROL_H / 3); 
	getPointProgressBar->setRange(0,COLLECTIONNUM);

	pointsInfoTable = new QTableWidget(this);
	pointsInfoTable->setColumnCount(POINTCOMPONENTNUM);
	pointsInfoTable->setRowCount(POINTNUM);
	QStringList pointsInfoVHeaderLabels;
	for(int i = 0; i < POINTNUM; i++)
	{
		pointsInfoVHeaderLabels << QString(tr("Point %1")).arg(i);
	}
	QStringList pointsInfoHHeaderLabels;
    pointsInfoHHeaderLabels << QString(tr("x")) << QString(tr("y")) << QString(tr("z"))
		                    << QString(tr("x\'")) << QString(tr("y\'")) << QString(tr("z\'"));

	pointsInfoTable->setVerticalHeaderLabels(pointsInfoVHeaderLabels);
	pointsInfoTable->setHorizontalHeaderLabels(pointsInfoHHeaderLabels);
    pointsInfoTable->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
	for(int i = 0; i < POINTCOMPONENTNUM; i++)
	{
		pointsInfoTable->setColumnWidth(i, 50); 		
	}
	for(int i = 0; i < POINTNUM; i++)
	{
		for(int j = 0; j < POINTCOMPONENTNUM; j++)
		{
			QTableWidgetItem *newItem = new QTableWidgetItem("-");
			pointsInfoTable->setItem(i, j, newItem);
		}
	}
	  
	pointsInfoGroupLayout->setContentsMargins(11, 0, 11, 0);
	pointsInfoGroupLayout->addWidget(setPointControlsGroup, 0, 0, 4, 1, Qt::AlignLeft);
	pointsInfoGroupLayout->addWidget(getPointProgressBar, 4, 0, 1, 1, Qt::AlignLeft|Qt::AlignTop);
    pointsInfoGroupLayout->addWidget(pointsInfoTable, 0, 1, 5, 1, Qt::AlignLeft);
	
	pointsInfoGroup->setLayout(pointsInfoGroupLayout);
	pointsInfoGroup->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	// observe functions
	//QGroupBox *observeFunctionsGroup = new QGroupBox(tr("Knee Motion Observe Functions"), this);
	QGroupBox *observeFunctionsGroup = new QGroupBox(tr("Endo. Cali. && A.R. Functions"), this);
    // - buttons group
	QGroupBox *observerFunctionButtonGroup = new QGroupBox(tr(""), observeFunctionsGroup);
	//calculateRefCOSButton = new QPushButton(tr("Calculate Ref COS"), this);
	calculateLocalTransformButton = new QPushButton(tr("Calculate"), this);
	loadLocalTransformButton = new QPushButton(tr("Load"), this);
	captureButton = new QPushButton(tr("Capture"), this);
	loadCapturedResultButton = new QPushButton(tr("Load"), this);
	resetCapturedResultButton = new QPushButton(tr("Reset"), this);
	calibrationButton = new QPushButton(tr("Calibration"), this);	
	calibrationTestButton = new QPushButton(tr("Test Start"), this);
	
	captureCountLabel = new QLabel("0");
	m_captureCount = 0;
	// - - connections
	//connect(calculateRefCOSButton, SIGNAL(clicked()), this, SLOT(calculateRefCOS()));
	connect(calculateLocalTransformButton, SIGNAL(clicked()), this, SLOT(calculateLocalTransform()));
	connect(loadLocalTransformButton, SIGNAL(clicked()), this, SLOT(loadLocalTransform()));
	connect(captureButton, SIGNAL(clicked()), this, SLOT(requireCaptureEndoImageAndTransform()));
	connect(loadCapturedResultButton,  SIGNAL(clicked()), this, SLOT(loadCapturedResult()));
	connect(resetCapturedResultButton,  SIGNAL(clicked()), this, SLOT(resetCapturedResult()));
	connect(calibrationButton, SIGNAL(clicked()), this, SLOT(endoCalibration()));
	connect(calibrationTestButton, SIGNAL(clicked()), this, SLOT(endoCalibratedResultTest()));
	// - - layout buttons
	QVBoxLayout* observerFunctionButtonGroupLayout = new QVBoxLayout;
	{
		//observerFunctionButtonGroupLayout->addWidget(calculateRefCOSButton);
		QGroupBox *localTransformGroupBox = new QGroupBox(tr("Local Transform"));
		QHBoxLayout* localTransformGroupLayout = new QHBoxLayout;
		localTransformGroupLayout->setContentsMargins(0, 5, 0, 0);
		localTransformGroupLayout->addWidget(calculateLocalTransformButton);
		localTransformGroupLayout->addWidget(loadLocalTransformButton);
		localTransformGroupBox->setLayout(localTransformGroupLayout);

		observerFunctionButtonGroupLayout->addWidget(localTransformGroupBox);
	}
	{
		QGroupBox *captureGroupBox = new QGroupBox(tr("Capture"));
		QGridLayout* captureGroupLayout = new QGridLayout;
		captureGroupLayout->setContentsMargins(0, 5, 0, 0);
		captureGroupLayout->addWidget(captureButton, 0, 0, 1, 1);
		captureGroupLayout->addWidget(captureCountLabel, 0, 1, 1, 1, Qt::AlignCenter);
		captureGroupLayout->addWidget(loadCapturedResultButton, 1, 0, 1, 1);
		captureGroupLayout->addWidget(resetCapturedResultButton, 1, 1, 1, 1);
		captureGroupBox->setLayout(captureGroupLayout);	

		observerFunctionButtonGroupLayout->addWidget(captureGroupBox);
	}	
	observerFunctionButtonGroupLayout->addWidget(calibrationButton);
	observerFunctionButtonGroupLayout->addWidget(calibrationTestButton);
    observerFunctionButtonGroupLayout->addStretch(1);
	observerFunctionButtonGroup->setLayout(observerFunctionButtonGroupLayout);
 //   // - transform information tab
	//QGroupBox *transformInfoGroup = new QGroupBox(tr("Transforms Information"), this);
	//QGridLayout *transformInfoGroupLayout = new QGridLayout;
 //   QTabWidget *transformInfoTab = new QTabWidget(this);
	//QWidget *transformPages[4];
	//QVBoxLayout *tansformPageLayouts[4];
	//QString transformPageTitles[4];	
	//transformPageTitles[0] = QString(tr("ref. cos"));
	//for(int i = 1; i < 4; i++)
	//{
	//	transformPageTitles[i] = QString("rel. cos %1").arg(i);
	//}	
	//for(int i = 0; i < 4; i++)
	//{
	//	transformPages[i] = new QWidget;
	//	tansformPageLayouts[i] = new QVBoxLayout;
	//}
	//refCOSTable = new QTableWidget(4, 4, this);
	//for(int i = 0; i < 4; i++)
	//{
	//	for(int j = 0; j < 4; j++)
	//	{
	//		QTableWidgetItem *item = new QTableWidgetItem("-");
	//		refCOSTable->setItem(i, j, item);
	//		refCOSTable->setColumnWidth(j, 50);
	//	}		
	//}
	//tansformPageLayouts[0]->addWidget(refCOSTable);	
	//for(int k = 0; k < TOOLNUM-1; k++)
	//{
	//	relTransTables[k] = new QTableWidget(4, 4, this);
	//	for(int i = 0; i < 4; i++)
	//	{
	//		for(int j = 0; j < 4; j++)
	//		{
	//			QTableWidgetItem *item = new QTableWidgetItem("-");
	//			relTransTables[k]->setItem(i, j, item);
	//			relTransTables[k]->setColumnWidth(j, 50);
	//		}
	//	}
	//	tansformPageLayouts[k+1]->addWidget(relTransTables[k]);
	//}
	//for(int i = 0; i < 4; i++)
	//{
	//	transformPages[i]->setLayout(tansformPageLayouts[i]);
	//	transformInfoTab->addTab(transformPages[i], transformPageTitles[i]);
	//}
 //   transformInfoGroupLayout->addWidget(transformInfoTab, 0, 0, 1, 1);
	//transformInfoGroup->setLayout(transformInfoGroupLayout);
	//// - knee motion information
	//QGroupBox *kneeMotionInfoGroup = new QGroupBox(tr("Knee Motion Info."), this);
	//// - - knee motion information table
	//kneeMotionInfoTable = new QTableWidget(TOOLNUM-1, 6, this);
	//QStringList kneeMotionInfoVHeaderLabels;
	//for(int i = 1; i < TOOLNUM; i++)
	//{
	//	kneeMotionInfoVHeaderLabels << QString(tr("ref. %1")).arg(i);
	//}
	//QStringList kneeMotionInfoHHeaderLabels;
 //   kneeMotionInfoHHeaderLabels << QString(tr("roll")) << QString(tr("pitch")) << QString(tr("yaw"))
	//	<< QString(tr("tx")) << QString(tr("ty")) << QString(tr("tz"));
	//kneeMotionInfoTable->setVerticalHeaderLabels(kneeMotionInfoVHeaderLabels);
	//kneeMotionInfoTable->setHorizontalHeaderLabels(kneeMotionInfoHHeaderLabels);
    //kneeMotionInfoTable->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
	//for(int i = 0; i < TOOLNUM-1; i++)
	//{
	//	for(int j = 0; j < 6; j++)
	//	{
	//		QTableWidgetItem *item = new QTableWidgetItem("-");
	//		kneeMotionInfoTable->setItem(i, j, item);
	//		kneeMotionInfoTable->setColumnWidth(j, 40);
	//	}
	//}
	//// - - knee motion information table(relative motion)
	//kneeRelativeMotionInfoTable = new QTableWidget(2, 6, this);
 //   QStringList kneeRelativeMotionInfoVHeaderLabels;
	//for(int i = 0; i < 2; i++)
	//{
	//	kneeRelativeMotionInfoVHeaderLabels << QString(tr("relative %1")).arg(i);
	//}
	//QStringList kneeRelativeMotionInfoHHeaderLabels;
 //   kneeRelativeMotionInfoHHeaderLabels << QString(tr("roll")) << QString(tr("pitch")) << QString(tr("yaw"))
	//	<< QString(tr("tx")) << QString(tr("ty")) << QString(tr("tz"));
	//kneeRelativeMotionInfoTable->setVerticalHeaderLabels(kneeRelativeMotionInfoVHeaderLabels);
	//kneeRelativeMotionInfoTable->setHorizontalHeaderLabels(kneeRelativeMotionInfoHHeaderLabels);
    //kneeRelativeMotionInfoTable->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
	//for(int i = 0; i < 2; i++)
	//{
	//	for(int j = 0; j < 6; j++)
	//	{
	//		QTableWidgetItem *item = new QTableWidgetItem("-");
	//		kneeRelativeMotionInfoTable->setItem(i, j, item);
	//		kneeRelativeMotionInfoTable->setColumnWidth(j, 40);
	//	}
	//}
	//// - - layout the knee motion group
	//QVBoxLayout *kneeMotionInfoGroupLayout = new QVBoxLayout;
	//kneeMotionInfoGroupLayout->addWidget(kneeMotionInfoTable);
	//kneeMotionInfoGroupLayout->addWidget(kneeRelativeMotionInfoTable);
	//kneeMotionInfoGroupLayout->addStretch(1);
	//kneeMotionInfoGroup->setLayout(kneeMotionInfoGroupLayout);
    // - layout the observe function group
	QGridLayout *observeFunctionsGroupLayout = new QGridLayout;
	observeFunctionsGroupLayout->addWidget(observerFunctionButtonGroup, 0, 0, 1, 1, Qt::AlignLeft);
	//observeFunctionsGroupLayout->addWidget(transformInfoGroup, 0, 1, 1, 1, Qt::AlignLeft);
	//observeFunctionsGroupLayout->addWidget(kneeMotionInfoGroup, 0, 2, 1, 1, Qt::AlignLeft);
	observeFunctionsGroup->setLayout(observeFunctionsGroupLayout);
	observeFunctionsGroup->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);	

	// main layout
	QGridLayout* mainLayout = new QGridLayout;
	mainLayout->addWidget(toolStatusGroup, 0, 0, 1, 1, Qt::AlignLeft|Qt::AlignTop);
	mainLayout->addWidget(probeOffsetGroup, 0, 1, 1, 1, Qt::AlignLeft|Qt::AlignTop);
	mainLayout->addWidget(endoOffsetGroup, 0, 2, 1, 1, Qt::AlignLeft|Qt::AlignTop);
	mainLayout->addWidget(pointsInfoGroup, 0, 3, 2, 2, Qt::AlignLeft|Qt::AlignTop);
	mainLayout->addWidget(observeFunctionsGroup, 0, 5, 2, 1, Qt::AlignLeft|Qt::AlignTop);
	setLayout(mainLayout);
	setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

//void KneeMotionObserver::updateObserver(const HandleData handles[4])
void KneeMotionObserver::updateObserver(const HandleData handles[NUM_ENDO_TOOL])
{
	// update data 
	//for(int i = 0; i < 4; i++)
	for(int i = 0; i < NUM_ENDO_TOOL; i++)
	{
		handleData[i].valid = handles[i].valid;
		handleData[i].frame = handles[i].frame;
		for(int j = 0; j < 16; j++)
		{
			handleData[i].m[j] = handles[i].m[j];
		}
		for(int j = 0; j < 4; j++)
		{
			handleData[i].q[j] = handles[i].q[j];
		}
		
		toolsStatus[i] = handles[i].valid;
	}

	if(isGettingPoint)
	{
		getPoint();
	}

	//if(isKneeMotionTracking)
	//{
	//	//updateKneeMotionInfo();
	//	//updateKneeMotionInfoDisplay();
	//}

	if(isCapturingEndoImageAndTransform)
	{
		captureEndoImageAndTransform();
	}

	if(m_isTest)
	{
		updateTestPointList();		
	}

	// update the controls
	for(int i = 0; i < TOOLNUM; i++)
	{
		toolStatusControls[i]->setToolStatus(toolsStatus[i]);
	}
}

void KneeMotionObserver::setEndoGadget(EndoGadget* device)
{
	m_endoGadget = device;
}

const std::vector< std::array<double, 3> >& KneeMotionObserver::getTestPointList2d()
{
	//QMutexLocker locker(&m_mutex);
	return m_testPointList2d;
}


void KneeMotionObserver::requirePointSetting(int index)
{
	std::cout << QString("set the point %1").arg(index).toStdString() << std::endl;
	isGettingPoint = true;
	collectedPointNum = 0;
	currentPointIndex = index;
}

void KneeMotionObserver::requirePointReset(int index)
{
	std::cout << QString("reset the point %1").arg(index).toStdString() << std::endl;
    
	pointsStatus[index] = false;	
	
	// update related controls
	setpointControls[index]->setPointStatus(pointsStatus[index]);
	pointsInfoTable->item(index, 0)->setText(QString("-"));
	pointsInfoTable->item(index, 1)->setText(QString("-"));
	pointsInfoTable->item(index, 2)->setText(QString("-"));

}

void KneeMotionObserver::getPoint()
{
	if(!isGettingPoint)
		return;

	if(!(handleData[0].valid && handleData[2].valid))
		return;

	// handle 0 - probel tool
	double probeTip[3];

	// handle 2 - calibration reference set
	double cali_t[16];
	double cali_t_inv[16];
	
	if(useProbeOffset)
	{
		for(int i = 0; i < 3; i++)
		{
			probeTip[i] = 0.0;
			for(int j = 0; j < 4; j++)
			{
				probeTip[i] += handleData[0].m[i * 4 + j] * probeOffset[j]; 
			}
		}		
	}
	else
	{
		probeTip[0] = handleData[0].m[3];
		probeTip[1] = handleData[0].m[7];
		probeTip[2] = handleData[0].m[11];
	}

	for(int i = 0; i < 16; i++)
		cali_t[i] = handleData[2].m[i];

	inverseTransform(cali_t, cali_t_inv);

	if(collectedPointNum < COLLECTIONNUM)
	{
		if(handleData[0].valid)
		{
			collectedPoint[collectedPointNum][0] = probeTip[0];
			collectedPoint[collectedPointNum][1] = probeTip[1];
			collectedPoint[collectedPointNum][2] = probeTip[2];

			// calculate the relative coordinates
            pointTransform(collectedPoint[collectedPointNum], cali_t_inv, collectedPointRelative[collectedPointNum]);

			collectedPointNum++;

			getPointProgressBar->setValue(collectedPointNum);
		}
	}
	else
	{
		collectedPointNum = 0;
		isGettingPoint = false;
		getPointProgressBar->setValue(0);
		updatePointData();
	}
}

void KneeMotionObserver::updatePointData()
{
	double x_average = 0 , y_average = 0, z_average = 0;
	double xr_average = 0 , yr_average = 0, zr_average = 0;  // for relative coordinates
	
	for(int i = 0; i < COLLECTIONNUM; i++)
	{
		x_average += collectedPoint[i][0] / COLLECTIONNUM;
		y_average += collectedPoint[i][1] / COLLECTIONNUM;
		z_average += collectedPoint[i][2] / COLLECTIONNUM;

		xr_average += collectedPointRelative[i][0] / COLLECTIONNUM;
		yr_average += collectedPointRelative[i][1] / COLLECTIONNUM;
		zr_average += collectedPointRelative[i][2] / COLLECTIONNUM;
	}

	pointsCoordinate[currentPointIndex][0] = x_average;
	pointsCoordinate[currentPointIndex][1] = y_average;
	pointsCoordinate[currentPointIndex][2] = z_average;

	pointsRelativeCoordinate[currentPointIndex][0] = xr_average;
	pointsRelativeCoordinate[currentPointIndex][1] = yr_average;
	pointsRelativeCoordinate[currentPointIndex][2] = zr_average;

	pointsStatus[currentPointIndex] = true;	
	
	// update related controls
	setpointControls[currentPointIndex]->setPointStatus(pointsStatus[currentPointIndex]);
	
	pointsInfoTable->item(currentPointIndex, 0)
		->setText(QString("%1").arg(pointsCoordinate[currentPointIndex][0], 0, 'f', 2));
	pointsInfoTable->item(currentPointIndex, 1)
		->setText(QString("%1").arg(pointsCoordinate[currentPointIndex][1], 0, 'f', 2));
	pointsInfoTable->item(currentPointIndex, 2)
		->setText(QString("%1").arg(pointsCoordinate[currentPointIndex][2], 0, 'f', 2));

	pointsInfoTable->item(currentPointIndex, 3)
		->setText(QString("%1").arg(pointsRelativeCoordinate[currentPointIndex][0], 0, 'f', 2));
	pointsInfoTable->item(currentPointIndex, 4)
		->setText(QString("%1").arg(pointsRelativeCoordinate[currentPointIndex][1], 0, 'f', 2));
	pointsInfoTable->item(currentPointIndex, 5)
		->setText(QString("%1").arg(pointsRelativeCoordinate[currentPointIndex][2], 0, 'f', 2));
}

void KneeMotionObserver::setUseProbeOffset(bool used)
{
	useProbeOffset = used;

	if(useProbeOffset)
	{
		for(int i = 0; i < 3; i++)
		{
			probeOffset[i] = probeOffsetEdit[i]->text().toDouble();
		}
		probeOffset[3] = 1.0;
	}	
}

void KneeMotionObserver::setUseEndoOffset(bool used)
{
	useEndoOffset = used;

	if(useEndoOffset)
	{
		for(int i = 0; i < 3; i++)
		{
			endoOffset[i] = endoOffsetEdit[i]->text().toDouble();
		}
		endoOffset[3] = 1.0;
	}	
}

//void KneeMotionObserver::calculateRefCOS()
//{
//	bool pointsReady = true;
//
//	for(int i = 0; i < POINTNUM; i++)
//	{
//		if(!pointsStatus[i])
//		{
//			pointsReady = false;
//			break;
//		}
//	}
//
//	if(!pointsReady)
//	{
//		std::cout << "not enough points! fail to calculate the reference cos!" << std::endl;
//		return;
//	}
//
//	// calculate the reference coordinate system
//	double origin[3], x_axis[3], y_axis[3], z_axis[3];
//	double temp;
//	// - calculate the origin
//	for(int i = 0; i < 3; i++)
//	{
//		origin[i] = (pointsCoordinate[0][i] + pointsCoordinate[1][i]) / 2.0;
//	}
//
//	std::cout << "origin: " <<origin[0] << ' ' << origin[1] << ' ' << origin[2] << std::endl;
//
//	// - calculate the z axis
//	// - - calculate the z direction vector
//	for(int i = 0; i < 3; i++)
//	{
//		z_axis[i] = pointsCoordinate[2][i] - origin[i];
//	}
//	// - - normalize the z vector
//	{
//		double normal = normalOfVector(z_axis);
//		if(normal > 0)
//		{
//			for(int i = 0; i < 3; i++)
//			{
//				z_axis[i] /= normal;
//			}
//		}
//		else
//		{
//			std::cout << "zero z-axis!" << std::endl;
//			return;
//		}
//	}
//
//	std::cout << "z axis: " << z_axis[0] << ' ' << z_axis[1] << ' ' << z_axis[2] << std::endl;
//
//	// - calculate the x axis
//	// - - calculate the vector originating from p3(inside) to p4(outside)
//	double x_vector[3];
//	for(int i = 0; i < 3; i++)
//	{
//		x_vector[i] = pointsCoordinate[4][i] - pointsCoordinate[3][i];
//	}
//	// - - calculate the project of the x vector on the plane whose normal is z axis
//	temp = dotProductOfVectors(x_vector, z_axis);
//	for(int i = 0; i < 3; i++)
//	{
//		x_axis[i] = x_vector[i] - temp * z_axis[i];
//	}
//	// - - normalize the x axis
//	{
//		double normal = normalOfVector(x_axis);
//		if(normal > 0)
//		{
//			for(int i = 0; i < 3; i++)
//			{
//				x_axis[i] /= normal;
//			}
//		}
//		else
//		{
//			std::cout << "zero x-axis!" << std::endl;
//			return;
//		}
//	}
//
//	std::cout << "x axis: " << x_axis[0] << ' ' << x_axis[1] << ' ' << x_axis[2] << std::endl;
//
//	// - calculate the y axis
//	// - - calculate the y direction vector
//	crossProductOfVectors(z_axis, x_axis, y_axis);
//	// - - normalize the y axis
//	{
//		double normal = normalOfVector(y_axis);
//		if(normal > 0)
//		{
//			for(int i = 0; i < 3; i++)
//			{
//				y_axis[i] /= normal;
//			}
//		}
//		else
//		{
//			std::cout << "zero y-axis!" << std::endl;
//			return;
//		}
//	}
//	std::cout << "y axis: " << y_axis[0] << ' ' << y_axis[1] << ' ' << y_axis[2] << std::endl;
//
//	//// store the result
//	//for(int i = 0; i < 3; i++)
//	//{
//	//	referenceCOS[i * 4] = x_axis[i]; 
//	//	referenceCOS[i * 4 + 1] = y_axis[i];
//	//	referenceCOS[i * 4 + 2] = z_axis[i];
//	//	referenceCOS[i * 4 + 3] = origin[i];
//	//}
//	//referenceCOS[12] = referenceCOS[13] = referenceCOS[14] = 0.0;
//	//referenceCOS[15] = 1.0;
//
//	//inverseTransform(referenceCOS, referenceCOS_i);
//	//referenceCOSStatus = true;
//
//	//createCOSInfoStream();
//	//updateRefTransInfo();
//}

void KneeMotionObserver::calculateLocalTransform()
{
	// first, identify whether all of four points have been set correctly
	int pointsReady = 0;
	for(int i = 0; i < POINTNUM; i++)
	{
		if(pointsStatus[i] == true)
		{
			pointsReady++;
		}
	}
	if(pointsReady < POINTNUM)
	{
		qDebug() << "\nThere are not enough points for point array calculation!";
		return;
	}	

	// second, construct the COS representing in the calibration reference cos
	// in which, the point array is on the xy-plane of the local COS
	// origin - 4
	// x-axis - from 4 to 3
	// y-axis - from 4 to 1
	// z-axis - x cross product y
	double origin[3], x_axis[3], y_axis[3], z_axis[3];

	for(int i = 0; i < 3; i++)
	{
		origin[i] = pointsRelativeCoordinate[3][i];
		x_axis[i] = pointsRelativeCoordinate[2][i] - pointsRelativeCoordinate[3][i];
		y_axis[i] = pointsRelativeCoordinate[0][i] - pointsRelativeCoordinate[3][i];
	}
	// calculate the z-axis
	crossProductOfVectors(x_axis, y_axis, z_axis);
	
	// - - normalize the x axis
	{
		double normal = normalOfVector(x_axis);
		if(normal > 0)
		{
			for(int i = 0; i < 3; i++)
			{
				x_axis[i] /= normal;
			}
		}
		else
		{
			std::cout << "zero x-axis!" << std::endl;
			return;
		}
	}
	// - - normalize the y axis
	{
		double normal = normalOfVector(y_axis);
		if(normal > 0)
		{
			for(int i = 0; i < 3; i++)
			{
				y_axis[i] /= normal;
			}
		}
		else
		{
			std::cout << "zero y-axis!" << std::endl;
			return;
		}
	}
	// - - normalize the z axis
	{
		double normal = normalOfVector(z_axis);
		if(normal > 0)
		{
			for(int i = 0; i < 3; i++)
			{
				z_axis[i] /= normal;
			}
		}
		else
		{
			std::cout << "zero z-axis!" << std::endl;
			return;
		}
	}
    // get the complete transformation matrix
	for(int i = 0; i < 3; i++)
	{
		pointArrayLocalTransform[i*4 + 0] = x_axis[i];
		pointArrayLocalTransform[i*4 + 1] = y_axis[i];
		pointArrayLocalTransform[i*4 + 2] = z_axis[i];
		pointArrayLocalTransform[i*4 + 3] = origin[i];
	}
	pointArrayLocalTransform[12] = pointArrayLocalTransform[13] = pointArrayLocalTransform[14] = 0.0;
	pointArrayLocalTransform[15] = 1.0;

	isPointArrayLocalTransformSet = true;

	// output to a binary data file
	std::ofstream outStream;		
	outStream.open("temp_local_transform.dat", std::ios::binary);
	if(outStream.fail())
	{
		std::cout << "\nfail to open the local transformation file for output." << std::endl;
	}
	else
	{
		outStream.write(reinterpret_cast<const char*>(pointArrayLocalTransform), sizeof(double)*16);
		outStream.close();
	}
}

void KneeMotionObserver::loadLocalTransform()
{
	std::ifstream inStream;
	inStream.open("temp_local_transform.dat", std::ios::binary);
	if(inStream.fail())
	{
		std::cout << "\nfail to open the local transformation file for input." << std::endl;
	}
	else
	{
		std::cout << "\nopen the input file" << std::endl;
		double b[16];
		inStream.read(reinterpret_cast<char*>(b), sizeof(double)*16);
		
		for(int i = 0; i < 16; i++)
			pointArrayLocalTransform[i] = b[i];

		printf("\nlocal transformation:\n");
		for(int i = 0; i < 4; i++)
			printf("%f %f %f %f\n", b[4*i+0], b[4*i+1], b[4*i+2], b[4*i+3]);
	}
	inStream.close();	
	
	isPointArrayLocalTransformSet = true;
}

//void KneeMotionObserver::updateRefTransInfo()
//{
//	if(referenceCOSStatus)
//	{
//		if(cosInfoReady)
//		{
//			cosInfo << "local cos:" << endl;
//			for(int i = 0; i < 16; i++)
//			{
//				cosInfo << referenceCOS[i] << ' ';
//			}
//			cosInfo << endl;
//		}
//
//		for(int i = 0; i < 4; i++)
//		{
//			for(int j = 0; j < 4; j++)
//			{
//				refCOSTable->item(i,j)
//					->setText(QString("%1").arg(referenceCOS[i * 4 + j], 0, 'f', 3));
//			}
//		}
//	}
//}

//void KneeMotionObserver::recordRelativeTransform(int index)
//{
//	double mr[16];
//	for(int i = 0; i < 4; i++)
//	{
//		for(int j = 0; j < 4; j++)
//		{
//			mr[i * 4 + j] = referenceCOS[i * 4 + j];
//		}
//	}
//	mr[3] = handleData[index].m[3];
//	mr[7] = handleData[index].m[7];
//	mr[11] = handleData[index].m[11];
//
//	double mi[16];
//	for(int i = 0; i < 4; i++)
//	{
//		for(int j = 0; j < 4; j++)
//		{
//			mi[i * 4 + j] = handleData[index].m[i * 4 + j];
//		}
//	}
//
//	double mi_inv[16];
//	inverseTransform(mi, mi_inv);
//
//	multiplyTransform(mi_inv, mr, relativeTransform[index - 1]);
//	relativeTransformStatus[index - 1] = true;
//
//	updateRelativeTransInfo(index - 1);
//}

//void KneeMotionObserver::updateRelativeTransInfo(int index)
//{
//	if(relativeTransformStatus[index])
//	{
//		if(cosInfoReady)
//		{
//			cosInfo << "relative cos " << index << ":" << endl;
//			for(int i = 0; i < 16; i++)
//			{
//				cosInfo << relativeTransform[index][i] << ' ';
//			}
//			cosInfo << endl;
//		}
//
//		for(int i = 0; i < 4; i++)
//		{
//			for(int j = 0; j < 4; j++)
//			{
//				relTransTables[index]->item(i,j)
//					->setText(QString("%1").arg(relativeTransform[index][i * 4 + j], 0, 'f', 3));
//			}
//		}		
//	}
//	else
//	{
//		for(int i = 0; i < 4; i++)
//		{
//			for(int j = 0; j < 4; j++)
//			{
//				relTransTables[index]->item(i,j)->setText(QString("-"));
//			}
//		}
//	}
//}

void KneeMotionObserver::requireCaptureEndoImageAndTransform()
{
	isCapturingEndoImageAndTransform = true;
}

void KneeMotionObserver::captureEndoImageAndTransform()
{
	// check whether the capture device is valid
	bool error = false;
	QString msg;
	if(!m_endoGadget)
	{
		msg = QString(tr("no valid capture device."));
		error = true;
	}
	if(!m_endoGadget->isDeviceReady())
	{
		msg = QString(tr("device is not ready."));
		error = true;
	}
	if(!m_endoGadget->isDeviceStartStreaming())
	{
		msg = QString(tr("device does not start streaming."));
		error = true;
	}
	if(!m_endoGadget->isSignalPresent())
	{
		msg = QString(tr("there are no signals."));
		error = true;
	}
	if(error)
	{
		QMessageBox::information(this, tr("endo image capture"), msg);
		return;
	}
	
	// check whether transformations are valid
	if(!(handleData[1].valid && handleData[2].valid))
	{
		QMessageBox::information(this, tr("endo image capture"), tr("invalid transformation"));
		return;
	}

	// capture the image 
	m_capturedImages.push_back(m_endoGadget->getCurrentFrame());

	// get the transform 
	double t_endo[16], t_endo_inv[16];
	double t_cali[16];
	//double t_cali_to_endo[16];
	double *t_cali_to_endo = new double[16];

	for(int i = 0; i < 16; i++)
	{
		t_endo[i] = handleData[1].m[i];
		t_cali[i] = handleData[2].m[i];
	}
	inverseTransform(t_endo, t_endo_inv);
	multiplyTransform(t_endo_inv, t_cali, t_cali_to_endo);
	m_cap_t_cali_to_endo.push_back(t_cali_to_endo);

	// the last step
	m_captureCount++;
	captureCountLabel->setText(QString("%1").arg(m_captureCount));

	isCapturingEndoImageAndTransform = false;

	// create tmp files 
	{
		QString tmp_dir_path = QDir::currentPath().append(m_tmp_dir_name);
		QDir tmp_dir(tmp_dir_path);
		if(!tmp_dir.exists())
		{
			std::cout << "\ncreate the directory " << tmp_dir_path.toStdString() << std::endl;
			tmp_dir.mkdir(tmp_dir_path);
		}
		else
		{
			std::cout << "\nthe directory " << tmp_dir_path.toStdString() << " exists." << std::endl;
		}
		// record the capture count
		std::ofstream outStream;
		QString capture_count_filename = tmp_dir.path().append(m_tmp_capture_count_filename);
		outStream.open(capture_count_filename.toStdString().c_str(), std::ios::binary);
		if(outStream.fail()) {
			std::cout << "\nfail to open the output file." << std::endl;
		}
		else {
			outStream.write(reinterpret_cast<const char*>(&m_captureCount), sizeof(int)*1);
		}
		outStream.close();
		// record the captured image
		QString tmp_img_filename = tmp_dir.path().append("/img%1.bmp").arg(m_captureCount-1);
		m_capturedImages[m_captureCount-1].save(tmp_img_filename);
		// record the captured transformation
		QString tmp_transform_filename = tmp_dir.path().append("/transform%1.dat").arg(m_captureCount-1);
		outStream.open(tmp_transform_filename.toStdString().c_str(), std::ios::binary);
		if(outStream.fail()) {
			std::cout << "\nfail to open the output file." << std::endl;
		}
		else {
			outStream.write(reinterpret_cast<const char*>(t_cali_to_endo), sizeof(double)*16);
		}
		outStream.close();
	}
}

void KneeMotionObserver::loadCapturedResult()
{
	QString tmp_dir_path = QDir::currentPath().append(m_tmp_dir_name);
	QDir tmp_dir(tmp_dir_path);
	
	// read the capture count
	int num_count = 0;
	std::ifstream inStream;
	QString capture_count_filename = tmp_dir.path().append(m_tmp_capture_count_filename);
	inStream.open(capture_count_filename.toStdString().c_str(), std::ios::binary);
	if(inStream.fail())	{
		std::cout << "\nfail to open the input file." << std::endl;
	}
	else	{
		std::cout << "\nopen the input file" << std::endl;
		inStream.read(reinterpret_cast<char*>(&num_count), sizeof(int)*1);
		std::cout << "\nthere are " << num_count << " files in the temp directory." << std::endl;
	}
	inStream.close();
	if(num_count < 1)
	{
		QMessageBox::information(this, tr("endo image capture"), "there is no captured result.");
		return;
	}

	// read the .dat file for captured transformation
	m_cap_t_cali_to_endo.clear();
	for(int i = 0; i < m_cap_t_cali_to_endo.size(); i++)
		delete[] m_cap_t_cali_to_endo[i];
	
	for(int i = 0; i < num_count; i++)
	{
		QString tmp_filename = tmp_dir.path().append("/transform%1.dat").arg(i);
		inStream.open(tmp_filename.toStdString().c_str(), std::ios::binary);
		if(inStream.fail())
		{
			std::cout << "\nfail to open the input file." << std::endl;
			break;
		}
		else
		{
			double *a = new double[16];
			std::cout << "\nopen the input file." << std::endl;
			inStream.read(reinterpret_cast<char*>(a), sizeof(double)*16);
			m_cap_t_cali_to_endo.push_back(a);
		}
		inStream.close();
	}
	if(m_cap_t_cali_to_endo.size() != num_count)
	{
		QMessageBox::information(this, tr("endo image capture"), "there is not enough\n captured transformation.");
		return;
	}

	// read the .bmp file for the captured image
	for(int i = 0; i < num_count; i++)
	{
		QString tmp_img_filename = tmp_dir.path().append("/img%1.bmp").arg(i);
		QImage tmp_img(tmp_img_filename);
		if(tmp_img.isNull())
			continue;
		m_capturedImages.push_back(tmp_img);
	}
	if(m_capturedImages.size() != num_count)
	{
		QMessageBox::information(this, tr("endo image capture"), "there is not enough\n captured images.");
		return;
	}

	m_captureCount = num_count;
	captureCountLabel->setText(QString("%1").arg(m_captureCount));
}

void KneeMotionObserver::resetCapturedResult()
{
	m_capturedImages.clear();
	for(int i = 0; i < m_cap_t_cali_to_endo.size(); i++)
		delete[] m_cap_t_cali_to_endo[i];
	m_cap_t_cali_to_endo.clear();

	// remove all the temporary files in the tmp_files
	if(QMessageBox::question(this, "endo image capture", "delete all of the files", 
		QMessageBox::Ok|QMessageBox::Cancel, QMessageBox::Cancel) == QMessageBox::Ok)
	{	
		// delete all file in a directory
		QString tmp_dir_path = QDir::currentPath().append(m_tmp_dir_name);
		QDir tmp_dir(tmp_dir_path);	
		tmp_dir.setFilter(QDir::AllEntries | QDir::NoDotAndDotDot);
		QFileInfoList fileList = tmp_dir.entryInfoList(); 
		foreach (QFileInfo fi, fileList) { 
			if (fi.isFile()) {
				fi.dir().remove(fi.fileName()); 
			}
		} 		
	}
	else
	{
		std::cout << "\nall temporary files remain." << std::endl;
	}

	m_captureCount = 0;
	captureCountLabel->setText(QString("%1").arg(m_captureCount));
}

void KneeMotionObserver::endoCalibration()
{
	if(!isPointArrayLocalTransformSet)
	{
		QMessageBox::information(this, tr("endo calibration"), "no valid point array\nlocal transformation");
		return;
	}

	if(m_captureCount < 2)
	{
		QMessageBox::information(this, tr("endo calibration"), "not enough captured image\n(at least 2 images)");
		return;
	}
	// image analysis
	endoImageAnalysis();

	if(!isCalibrationDataReady)
	{
		QMessageBox::information(this, tr("endo calibration"), "data for calibration is not ready");
		return;
	}
    // calibration
	endoCalibration_tsai();  // using Tsai Method

	//when this calibration procedure ends, the capture count should be reset to 0
	m_captureCount = 0;
	captureCountLabel->setText(QString("%1").arg(m_captureCount));

	m_capturedImages.clear();
	
	for(size_t i = 0; i < m_cap_t_cali_to_endo.size(); i++)
		delete[] m_cap_t_cali_to_endo[i];
	m_cap_t_cali_to_endo.clear();
}

void KneeMotionObserver::endoCalibratedResultTest()
{
	// load the test point list
	if(!m_isTestPointInitialized)
	{
		// use big 6 as the test point
		m_testPointNum = 6;

		int index[6][2]; // [i][0] - row index; [i][1] - col index;
		index[0][0] = cali_origin_row_idx + 3;	index[0][1] = cali_origin_col_idx + 0;
		index[1][0] = cali_origin_row_idx + 2;	index[1][1] = cali_origin_col_idx + 0;
		index[2][0] = cali_origin_row_idx + 1;	index[2][1] = cali_origin_col_idx + 0;
		index[3][0] = cali_origin_row_idx + 0;	index[3][1] = cali_origin_col_idx + 0;
		index[4][0] = cali_origin_row_idx + 0;	index[4][1] = cali_origin_col_idx + 1;
		index[5][0] = cali_origin_row_idx + 0;	index[5][1] = cali_origin_col_idx + 2;

		std::array<double, 3> localCoords[6];
		for(int i = 0; i < 6; i++){
			for(int j = 0; j < 3; j++){
				localCoords[i][j] = caliPointArrayLocal[index[i][0]][index[i][1]][j]; 
			}
		}

		for(int i = 0; i < 6; i++){
			m_testPointList3d.push_back(localCoords[i]);
		}

		m_isTestPointInitialized = true;
	}

	if(!m_isTest)
	{
		calibrationTestButton->setText(tr("Test Stop"));
		m_isTest = true;
	}
	else
	{
		calibrationTestButton->setText(tr("Test Start"));
		m_isTest = false;
		emit testPointListInvalid();		
	}
}

// for image analysis
void KneeMotionObserver::endoImageAnalysis()
{
	int trans_num = m_cap_t_cali_to_endo.size();
	int endo_img_num = m_capturedImages.size();
	if(trans_num != endo_img_num)
	{
		QMessageBox::information(this, tr("endo calibration"), "the num of transformation\nisnot equal to\nthe num of captured image.");
		return;
	}
	
	m_imagePoints.clear();
	m_objectPoints.clear();
	isCalibrationDataReady = false;  // data is reset

	m_fpCounts.clear();

	// display intermedia result or not
	bool isDrawResult = false;

	// blob detector parameter
	BlobDetectorParams blobDetectorParams = {0};
	// parameter initialization
	{
		blobDetectorParams.filterByColor = true;
		blobDetectorParams.blobColor = 0;

		blobDetectorParams.filterByArea = true;
		blobDetectorParams.minArea = 50;
		blobDetectorParams.maxArea = 5000;

		blobDetectorParams.filterByInertia = false;
		blobDetectorParams.minInertiaRatio = 0.1f;
		blobDetectorParams.maxInertiaRatio = std::numeric_limits<float>::max();

		blobDetectorParams.filterByCircularity = false;
		blobDetectorParams.minCircularity = 0.6f; 
		blobDetectorParams.maxCircularity = std::numeric_limits<float>::max();

		blobDetectorParams.filterByConvexity = false;
		blobDetectorParams.minConvexity = 0.8f;
		blobDetectorParams.maxConvexity = std::numeric_limits<float>::max();
	}

	for(int img_num = 0; img_num < endo_img_num; img_num++)
	{
		std::cout << "\ntest image #" << img_num  <<" :"<< std::endl;
		// conversion from a QImage format to cv::Mat format
		cv::Mat test_img_mat = QImageToCvMat(m_capturedImages[img_num]);
		//cv::imshow("test image",test_img_mat);
		// information of the test image(s)
		std::cout << "\nimage size: "<< test_img_mat.size().width << " ," << test_img_mat.size().height << std::endl;
		//std::cout << "\ntest image channel(s): " << test_img_mat.channels() << '.' << std::endl;		
		// process or analysize the image 
		{
			// convert a color image to a gray one
			cv::Mat gray_test_img_mat;
			cv::cvtColor(test_img_mat, gray_test_img_mat, cv::COLOR_RGB2GRAY);
			//cv::imshow("gray test image", gray_test_img_mat);

			// binarize the gray image
			cv::Mat binImageByOtsu;
			IplImage iplimage = gray_test_img_mat;
			int threshold = Otsu(&iplimage);
			cv::threshold(gray_test_img_mat, binImageByOtsu, threshold, 255, cv::THRESH_BINARY);
			//cv::imshow("otsu image", binImageByOtsu);

			// find contours
			std::vector< std::vector<cv::Point> > contours; 
			cv::Mat tmpBinaryImage = binImageByOtsu.clone();
			{
				// for time counting
				double curr_time01 = (double)QDateTime::currentDateTime().toMSecsSinceEpoch();

				cv::findContours(tmpBinaryImage, contours, cv::RETR_LIST, cv::CHAIN_APPROX_NONE);

				double curr_time02 = (double)QDateTime::currentDateTime().toMSecsSinceEpoch();
				double diff = fabs(curr_time02 - curr_time01);
				std::cout << "\nthe elapsed time for cv::findContours is " << diff << " (ms)." << std::endl;
			}

			// find blobs
			std::vector< Center > centers;
			{
				double curr_time01 = (double)QDateTime::currentDateTime().toMSecsSinceEpoch();

				// travesal all the contours 
				for (size_t contourIdx = 0; contourIdx < contours.size(); contourIdx++)
				{
					Center center;
					center.confidence = 1;
					cv::Moments moms = cv::moments(cv::Mat(contours[contourIdx]));
					if(blobDetectorParams.filterByArea)
					{
						double area = moms.m00;
						if (area < blobDetectorParams.minArea || area >= blobDetectorParams.maxArea)
							continue;
					}

					if (blobDetectorParams.filterByCircularity)
					{
						double area = moms.m00;
						double perimeter = cv::arcLength(cv::Mat(contours[contourIdx]), true);
						double ratio = 4 * CV_PI * area / (perimeter * perimeter);
						if (ratio < blobDetectorParams.minCircularity || ratio >= blobDetectorParams.maxCircularity)
							continue;
					}

					if (blobDetectorParams.filterByInertia)
					{
						double denominator = std::sqrt(std::pow(2 * moms.mu11, 2) + std::pow(moms.mu20 - moms.mu02, 2));
						const double eps = 1e-2;
						double ratio;
						if (denominator > eps)
						{
							double cosmin = (moms.mu20 - moms.mu02) / denominator;
							double sinmin = 2 * moms.mu11 / denominator;
							double cosmax = -cosmin;
							double sinmax = -sinmin;

							double imin = 0.5 * (moms.mu20 + moms.mu02) - 0.5 * (moms.mu20 - moms.mu02) * cosmin - moms.mu11 * sinmin;
							double imax = 0.5 * (moms.mu20 + moms.mu02) - 0.5 * (moms.mu20 - moms.mu02) * cosmax - moms.mu11 * sinmax;
							ratio = imin / imax;
						}
						else
						{
							ratio = 1;
						}

						if (ratio < blobDetectorParams.minInertiaRatio || ratio >= blobDetectorParams.maxInertiaRatio)
							continue;

						center.confidence = ratio * ratio;
					}

					if (blobDetectorParams.filterByConvexity)
					{
						std::vector < cv::Point > hull;
						cv::convexHull(cv::Mat(contours[contourIdx]), hull);
						double area = cv::contourArea(cv::Mat(contours[contourIdx]));
						double hullArea = cv::contourArea(cv::Mat(hull));
						double ratio = area / hullArea;
						if (ratio < blobDetectorParams.minConvexity || ratio >= blobDetectorParams.maxConvexity)
							continue;
					}

					center.location = cv::Point2d(moms.m10 / moms.m00, moms.m01 / moms.m00);

					if (blobDetectorParams.filterByColor)
					{
						if (binImageByOtsu.at<uchar> (cvRound(center.location.y), cvRound(center.location.x)) != blobDetectorParams.blobColor)
							continue;
					}

					//compute blob radius
					{
						std::vector<double> dists;
						for (size_t pointIdx = 0; pointIdx < contours[contourIdx].size(); pointIdx++)
						{
							cv::Point2d pt = contours[contourIdx][pointIdx];
							dists.push_back(cv::norm(center.location - pt));
						}
						std::sort(dists.begin(), dists.end());
						center.radius = (dists[(dists.size() - 1) / 2] + dists[dists.size() / 2]) / 2.;
					}

					if(moms.m00 == 0.0)
						continue;
					centers.push_back(center);
				}

				double curr_time02 = (double)QDateTime::currentDateTime().toMSecsSinceEpoch();
				double diff = fabs(curr_time02 - curr_time01);
				std::cout << "\nthe elapsed time for findBlobs is " << diff << " (ms)." << std::endl;
				std::cout << "\nthere are " << centers.size() << " blobs in the image." << std::endl;
			}
			if(isDrawResult)
			{
				cv::Mat tmp_img_mat;
				test_img_mat.copyTo(tmp_img_mat);

				for(size_t i = 0; i < centers.size(); i++)
				{
					// the center of the blob
					// -1 -> thickness, negative thickness means that a filled circle is to be drawn.
					cv::circle(tmp_img_mat, centers[i].location, 1, cv::Scalar(0, 255, 255),-1);  
					// the approximate contour of the blob
					cv::circle(tmp_img_mat, centers[i].location, centers[i].radius, cv::Scalar(0, 255, 255)); 
				}
				cv::imshow("key points", tmp_img_mat);	
			}

			// the results are stored in a vector(centers),
		    // according to the vector, successive operation(s) will be performed
			int blob_num = centers.size();
			std::vector< GridNode > grid_nodes(blob_num);				
			// analyze those extracted centers
			{
				// constant definition
				const int cluster_count = 2;  // only 2 types of marker
				const int big_marker_count = 6; // 6 bigger markers in the point array
				const int x_axis_count = 3;
				const int y_axis_count = 4;

				// for centers labeling
				const double closest_factor = 0.3;  // to determine whether the feature point is the one closet to the candidate point

				// 1. sort all of the centers according to their size
				cv::Mat sortSizesIdx;
				// 1.1 initialize a vector of marker size
				std::vector<float> kpsizes(blob_num);			
				for(int i = 0; i < blob_num; i++){	
					kpsizes[i] = centers[i].radius * 2.0;					
				}
				// 1.2 sort the vector and the sorted result is stored in the form of index
				cv::sortIdx(cv::Mat(kpsizes).reshape(1,0), sortSizesIdx, cv::SORT_EVERY_COLUMN + cv::SORT_DESCENDING);

				// 2. find bigger markers(the amount is 6, a constant determined by pattern design)
				bool isFeatureFound = false;
				int candidate_kp_num = 2 * big_marker_count;
				std::vector<float> candidate_kpsizes(candidate_kp_num);
				std::vector<int> candidate_marker_ind(candidate_kp_num);
				cv::Mat markerLabels, markerMeans;
				// 2.1 initialize a vector for candidates to be seperated
				for(int i = 0; i < candidate_kp_num; i++) {
					int tmp_idx = sortSizesIdx.at<int>(i);
					candidate_kpsizes[i] = centers[tmp_idx].radius * 2.0;
					candidate_marker_ind[i] = tmp_idx;
					//std::cout << candidate_kpsizes[i] << std::endl;
				}
				// 2.2 clustering (the candidate set should be spilt into two groups)			
				cv::kmeans(cv::Mat(candidate_kpsizes).reshape(1,0), cluster_count, markerLabels, 
					cv::TermCriteria(cv::TermCriteria::EPS+cv::TermCriteria::COUNT, 10, 1.0),
					3, cv::KMEANS_PP_CENTERS, markerMeans); // means �C Output matrix of the cluster centers, one row per each cluster center.
				//std::cout << std::endl;
				//for(int i = 0; i < candidate_kp_num; i++){	
				//	std::cout << "#" << i << " - " << markerLabels.at<int>(i) <<" - " << candidate_kpsizes[i] << std::endl;
				//}
				// 2.3 find the bigger 6 according to cluster label and its mean
				// 2.3.1 find the label with the largest mean value
				int largestLabel = 0;
				{
					double lmean = DBL_MIN;  // lmean - largest mean value
					for(int i = 0; i < cluster_count; i++){
						double tmp_mean = markerMeans.at<float>(i);
						if(tmp_mean > lmean){
							lmean = tmp_mean;
							largestLabel = i;
						}
					}
					//std::cout << "\nthe label with the largest mean is " << largestLabel << " and corresponding is " << markerMeans.at<float>(largestLabel) << "." << std::endl;
				}
				// 2.3.2 find those markers with the largestLabel
				std::vector< Center > spCenters;
				std::vector< int > spCenterIdx;
				for(int i = 0; i < candidate_kp_num; i++){
					if(markerLabels.at<int>(i) == largestLabel){
						spCenters.push_back(centers[candidate_marker_ind[i]]);
						spCenterIdx.push_back(candidate_marker_ind[i]);
					}
				}
				if(spCenters.size() == big_marker_count){
					isFeatureFound = true; std::cout << "\nthe big 6 found." << std::endl;
				}
				else{
					isFeatureFound = false; std::cout << "\nthe big 6 not found." << std::endl;
					return;
				}

				// 3. construct a basis based on the big 6
				std::vector< std::vector<double> > graph;
				std::vector<int> parent(big_marker_count); // Array to store constructed MST				
				// 3.1 generate a MST of these big 6
				{
					//  construct the graph in an adjacent matrix form				
					double xi, yi, xj, yj;
					for(int i = 0; i < big_marker_count; i++)
					{
						std::vector<double> dists(big_marker_count);
						xi = spCenters[i].location.x;	yi = spCenters[i].location.y;

						for(int j = 0; j < big_marker_count; j++)
						{
							xj = spCenters[j].location.x;	yj = spCenters[j].location.y;
							dists[j] =  sqrt((xi-xj)*(xi-xj)+(yi-yj)*(yi-yj)); 
						}
						graph.push_back(dists);
					}
					// construct the MST using Prim algorithm
					primMST(graph, big_marker_count, parent);

					if(isDrawResult){
						cv::Mat tmp_img;
						test_img_mat.copyTo(tmp_img);

						for(size_t i = 1; i < spCenters.size(); i++)						
							cv::line(tmp_img, spCenters[parent[i]].location, spCenters[i].location, cv::Scalar(0, 255, 255),2);

						cv::imshow("MST graph", tmp_img);							
					}				
				}
				// 3.2 construct the basis from the MST for big 6
				cv::Point2d origin2d;  int origin_ind;
				cv::Point2d xaxis2d;   double init_xstep;
				cv::Point2d yaxis2d;   double init_ystep;
				// 3.2.1 find all of the edge in the generated MST
				std::vector<MSTEdge> edges;
				for(size_t i = 1; i < spCenters.size(); i++)
				{
					MSTEdge edge; 
					edge.v1 = i;
					edge.v2 = parent[i];
					edge.dir = spCenters[parent[i]].location - spCenters[i].location;
					edge.abs_dir.x = fabs(edge.dir.x);
					edge.abs_dir.y = fabs(edge.dir.y);
					edges.push_back(edge);
				}
				//std::cout << std::endl;
				//for(size_t i = 0; i < edges.size(); i++)
				//	std::cout << edges[i].abs_dir.x << ", " << edges[i].abs_dir.y << std::endl;
				// 3.2.2 choose the corner of the L shape as the origin
				const int dir_num = 2;				
				int edge_num = edges.size();
				cv::Mat dirLabels;
				cv::Mat dirMeans;
				std::vector < std::set<int> > vert_sets(dir_num);	// index of the point in the spCenters
				// set[0] -> label[0] -> means[0]
				// set[1] -> label[1] -> means[1]
				{
					// 3.2.2.1 clustering the absolute direction of each edge
					std::vector<cv::Point2f> tmp_edges(edge_num);
					// initialize the edge vector
					for(int i = 0; i < edge_num; i++)
						tmp_edges[i] = edges[i].abs_dir;
					// edge direction clustering 
					cv::kmeans(cv::Mat(tmp_edges).reshape(1,0), dir_num, dirLabels, 
						cv::TermCriteria(cv::TermCriteria::EPS+cv::TermCriteria::COUNT, edge_num, 1.0),
						3, cv::KMEANS_PP_CENTERS, dirMeans);
					//std::cout << std::endl;
					//for(int i = 0; i < edge_num; i++)
					//	std::cout << "#" << i << " - " << edges[i].abs_dir.x << ", " << edges[i].abs_dir.y << " - " << dirLabels.at<int>(i) << std::endl;
					// 3.2.2.2 divide vertice into two sets each of which corresponds to one edge clustering			    
					for(int i = 0; i < edge_num; i++)
					{
						int tmp_label = dirLabels.at<int>(i);
						vert_sets[tmp_label].insert(edges[i].v1);
						vert_sets[tmp_label].insert(edges[i].v2);
					}
					// 3.2.2.3 find the common point of these two sets
					int vert_ind = -1;
					for(int i = 1; i < dir_num; i++){
						for (std::set<int>::iterator it = vert_sets[0].begin(); it != vert_sets[0].end(); it++){
							if(vert_sets[i].count(*it) != 0){
								vert_ind = *it;							
								break;
							}
						}
					}
					origin_ind = spCenterIdx[vert_ind];
					origin2d = centers[origin_ind].location;
					{   // result output
						bool isDrawResult = false;
						if(isDrawResult){
							cv::Mat tmp_img_mat;
							test_img_mat.copyTo(tmp_img_mat);
							cv::circle(tmp_img_mat, centers[spCenterIdx[vert_ind]].location, 2, cv::Scalar(0, 255, 255),-1); 	
							cv::imshow("origin point", tmp_img_mat);	
						}
					}
				}
				// 3.2.3 build the x-axis and y-axis
				{
					// the set owning 3 markers -> x-axis				
					// the set owning 4 markers -> y-axis
					//std::cout << std::endl;
					//for(int i = 0; i < dir_num; i++){
					//	std::cout << "set # " << i << ": " << vert_sets[i].size() << " points.\n"
					//		<< "- the mean direction: " << dirMeans.row(i) << std::endl;
					//}
					for(int i = 0; i < dir_num; i++)
					{
						double tmp_dist;
						double max_dist;
						double min_dist; 
						if(vert_sets[i].size() == x_axis_count)
						{
							// find the farest point to the origin
							int tmp_xi;  // the index of the farest vertex
							max_dist = DBL_MIN;
							min_dist = DBL_MAX;
							for (std::set<int>::iterator it = vert_sets[i].begin(); it != vert_sets[i].end(); it++)
							{
								tmp_dist = norm(spCenters[*it].location - origin2d);
								if(tmp_dist > max_dist)
								{
									max_dist = tmp_dist;
									tmp_xi = *it;
								}
								if(spCenterIdx[*it] != origin_ind)
								{
									if(tmp_dist < min_dist)
										min_dist = tmp_dist;
								}
							}
							cv::Point2d tmp_xaxis = spCenters[tmp_xi].location - origin2d;
							xaxis2d = cv::Point2d(tmp_xaxis.x / norm(tmp_xaxis), tmp_xaxis.y / norm(tmp_xaxis));
							init_xstep = min_dist;
						}
						if(vert_sets[i].size() == y_axis_count)
						{
							// find the farest point to the origin
							int tmp_yi;  // the index of the farest vertex
							max_dist = DBL_MIN;
							min_dist = DBL_MAX;
							for (std::set<int>::iterator it = vert_sets[i].begin(); it != vert_sets[i].end(); it++)
							{
								tmp_dist = norm(spCenters[*it].location - origin2d);
								if(tmp_dist > max_dist)
								{
									max_dist = tmp_dist;
									tmp_yi = *it;
								}
								if(spCenterIdx[*it] != origin_ind)
								{
									if(tmp_dist < min_dist)
										min_dist = tmp_dist;
								}
							}
							cv::Point2d tmp_yaxis = spCenters[tmp_yi].location - origin2d;
							yaxis2d = cv::Point2d(tmp_yaxis.x / norm(tmp_yaxis), tmp_yaxis.y / norm(tmp_yaxis));
							init_ystep = min_dist;
						}
					}	
					std::cout << "\nx axis is " << xaxis2d << std::endl;
					std::cout << "\ny axis is " << yaxis2d << std::endl;
				}

				// 4. get the arrange of points
				std::vector<cv::Point2f> center_locations(blob_num);
				for(int i = 0; i < blob_num; i++){
					center_locations[i] = centers[i].location;
				}
				{
					// labelling all of the points
					int label_delta_x[4];
					int label_delta_y[4];
					label_delta_x[0] = 1;  label_delta_y[0] = 0;
					label_delta_x[1] = 0;  label_delta_y[1] = -1;
					label_delta_x[2] = -1; label_delta_y[2] = 0;
					label_delta_x[3] = 0;  label_delta_y[3] = 1;

					std::queue< int > start_queue;

					// initializing the grid
					for(int i = 0; i < blob_num; i++)
					{
						grid_nodes[i].ind = i;

						grid_nodes[i].labeled = false;
						grid_nodes[i].label[0] = 0;	grid_nodes[i].label[1] = 0;

						for(int j = 0;  j < 4; j++)
						{
							grid_nodes[i].searchable[j] = true; 
							grid_nodes[i].search_dir[j] = cv::Point2f(0.0,0.0); 
							grid_nodes[i].search_step[j] = -1;
						}
					}

					// origin added into the start_queue
					grid_nodes[origin_ind].label[0] = 0; grid_nodes[origin_ind].label[1] = 0;
					grid_nodes[origin_ind].labeled = true;

					grid_nodes[origin_ind].search_dir[0] = xaxis2d;  grid_nodes[origin_ind].search_step[0] = init_xstep;
					grid_nodes[origin_ind].search_dir[1] = -yaxis2d; grid_nodes[origin_ind].search_step[1] = init_ystep;
					grid_nodes[origin_ind].search_dir[2] = -xaxis2d; grid_nodes[origin_ind].search_step[2] = init_xstep;
					grid_nodes[origin_ind].search_dir[3] = yaxis2d;  grid_nodes[origin_ind].search_step[3] = init_ystep;

					start_queue.push(origin_ind);

					int label_num = 1;

					std::cout << "\ngrid searching begin..." << std::endl;
					while(!start_queue.empty())
					{
						// as parent node
						int cur_ind = start_queue.front();
						GridNode cur_node = grid_nodes[cur_ind];

						bool child_found[4];
						child_found[0] = child_found[1] = child_found[2] = child_found[3] = false;

						int child_ind[4];

						double child_dist[4];

						// its child node
						for(int i = 0; i < 4; i++)
						{
							if(cur_node.searchable[i])
							{
								cv::Point2f candidate = center_locations[cur_ind] + cur_node.search_step[i] * cur_node.search_dir[i];
								// find the closest point in the point set
								double tmp_dist;
								double min_dist = DBL_MAX; 
								int min_ind;
								for(int j = 0; j < blob_num; j++)
								{
									tmp_dist = cv::norm(center_locations[j] - candidate);
									if(tmp_dist < min_dist)
									{
										min_dist = tmp_dist;
										min_ind = j;
									}
								}							
								//determine wether the candidate should be added into the search list
								if(min_dist < closest_factor * cur_node.search_step[i])
								{
									child_found[i] = true;
									child_ind[i] = min_ind;
									child_dist[i] = cv::norm(center_locations[cur_ind] - center_locations[min_ind]);								
								}
								else
									child_found[i] = false;
							}
						}

						// process found child(ren)
						for(int i = 0; i < 4; i++)
						{
							if(!child_found[i])
								continue;

							int tmp_ind = child_ind[i];

							if(grid_nodes[tmp_ind].labeled)
								continue;

							grid_nodes[tmp_ind].ind = tmp_ind;

							grid_nodes[tmp_ind].label[0] = grid_nodes[cur_ind].label[0] + label_delta_x[i];
							grid_nodes[tmp_ind].label[1] = grid_nodes[cur_ind].label[1] + label_delta_y[i];
							grid_nodes[tmp_ind].labeled = true;

							label_num++;

							grid_nodes[tmp_ind].searchable[(i+2)%4] = false;

							for(int j = 0; j < 4; j++)
							{
								if(child_found[j])
								{
									cv::Point2f tmp_dir = center_locations[child_ind[j]] - center_locations[cur_node.ind];
									double tmp_norm = norm(tmp_dir);
									grid_nodes[tmp_ind].search_dir[j] = cv::Point2f(tmp_dir.x / tmp_norm, tmp_dir.y / tmp_norm);
									grid_nodes[tmp_ind].search_step[j] = child_dist[j];
								}
								else
								{
									grid_nodes[tmp_ind].search_dir[j] = cur_node.search_dir[j];
									grid_nodes[tmp_ind].search_step[j] = cur_node.search_step[j];
								}
							}
							start_queue.push(tmp_ind);						
						}

						start_queue.pop();
					}
					std::cout << "\n...grid searching end." << std::endl;
					std::cout << "\n" << label_num << " nodes have been labeled." << std::endl;
				} // -- end -- labelling all of the points
			} // -- end -- // analyze those extracted centers
            
			int tmp_count = 0;

			for(int i = 0; i < blob_num; i++)
			{
				if(!grid_nodes[i].labeled)
					continue;

				// image coordinate 
				imgPtd img_p;
				img_p.x = centers[i].location.x;
				img_p.y = centers[i].location.y;
				m_imagePoints.push_back(img_p);

				// object coordinate
				int tmp_row = grid_nodes[i].label[1] + cali_origin_row_idx;
				int tmp_col = grid_nodes[i].label[0] + cali_origin_col_idx;
				
				double p_l[3];  // point coordinates in local cos
				p_l[0] = caliPointArrayLocal[tmp_row][tmp_col][0];
				p_l[1] = caliPointArrayLocal[tmp_row][tmp_col][1];
				p_l[2] = caliPointArrayLocal[tmp_row][tmp_col][2];

				double p_c[3];  // point coordinates in cali cos
				pointTransform(p_l, pointArrayLocalTransform, p_c);
				
				double p_e[3];  // point coordinates in endo cos
				pointTransform(p_c, m_cap_t_cali_to_endo[img_num], p_e);
				
				objPtd obj_p;
				obj_p.x = p_e[0];	
				obj_p.y = p_e[1];	
				obj_p.z = p_e[2]; 
				m_objectPoints.push_back(obj_p);

				tmp_count++;
			}
			// the feature point number in each image 
			m_fpCounts.push_back(tmp_count);

		} // -- end -- process or analysize the image 
	} // -- end -- captured image(s)'for loop'

	if(m_objectPoints.empty() || m_imagePoints.empty())
		isCalibrationDataReady = false;
	else
		isCalibrationDataReady = true;

}

void KneeMotionObserver::endoCalibration_tsai()
{
	isCalibrationSucceed = false;
	
	std::cout << "tsai method!" << std::endl;

	bool isTest = false;

	if(isTest)
	{
		camera_parameters cp;
		int camera_model = PHOTOMETRICS_STAR_I;  // for test
		//int camera_model = PINHOLE_CAMERA;

		initialize_camera_parameters(camera_model, 0, 0, 0, 0, 0, 0, 0, &cp);
		// note: if PINHOLE_CAMERA is used as the parameter, the Cx and Cy should be set 
		std::cout << "\ncamera type: " << camera_type_string(camera_model) << std::endl;

		// non-coplanar, full optimization
		std::cout << "\nNon-coplanar calibration (full optimization)" << std::endl;

		// load a data file for test
		double objectPoints[3*MAX_POINTS];
		double imagePoints[2*MAX_POINTS];

        //为解决 error: C2664:  无法将参数 1 从“const char [11]”转换为“char *”
        QString str = "ncc_cd.dat";//创建字符串
        char* ch;//创建指针
        QByteArray ba = str.toLatin1();//创建类QByteArray的对象
        ch = ba.data();//赋值

        int point_num = loadTestData(ch, objectPoints, imagePoints);  // ncc_cd.dat -> non-coplanar calibration
		if(point_num < 0)
		{
			std::cout << "\nfail to load the test data." << std::endl;
			return;
		}
		else
		{
			std::cout << "\nthere are " << point_num << " points in the data file." << std::endl; 
			//for(int i = 0; i < point_num; i++)
			//	std::cout << objectPoints[3*i] << ", "<< objectPoints[3*i+1] << ", " << objectPoints[3*i+2] << " - "
			//	<< imagePoints[2*i] << ", " << imagePoints[2*i+1] << std::endl;
		}

		calibration_data cd;
		init_cd(&cd);
		// allocate memories for cd
		if(allocate_memory_cd(point_num, &cd) < 0)
		{
			std::cout << "\nfail to allocate necessary memory." << std::endl;
			return;
		}
		// copy data into the cd data structure
		for(int i = 0; i < point_num; i++)
		{
			cd.xw[i] = objectPoints[3*i+0];
			cd.yw[i] = objectPoints[3*i+1];
			cd.zw[i] = objectPoints[3*i+2];
			cd.Xf[i] = imagePoints[2*i+0];
			cd.Yf[i] = imagePoints[2*i+1];
		}

		// process calibration data
		calibration_constants cc = {0};
		double A[3*3], K[3*4], distortion;

		// non-coplanar calibration with full optimization
		nccal_fo(&cp, &cd, A, K, &distortion, &cc);
		print_cp_cc_data(&cp, &cc);

		// free memory for cd
		free_memory_cd(&cd);
	}
	else
	{
		int camera_model = PINHOLE_CAMERA;

		int w, h;
/*		bool res = m_endoGadget->getCurrentResolution(&w, &h);
		if(!res)
		{
			isCalibrationSucceed = false;
			std::cout << "\ninvalid resolution." << std::endl;
			return;
		}	*/	
		
		w = 1024;
		h = 768;
		//void initialize_camera_parameters(int CameraModel, 
		//						  double Ncx, double Nfx, double dx, double dy, 
		//						  double Cx, double Cy, double sx, struct camera_parameters *cp)
		initialize_camera_parameters(camera_model, 0, 0, 0, 0, double(w)/2.0, double(h)/2.0, 0, &m_cp);
		// note: if PINHOLE_CAMERA is used as the parameter, the Cx and Cy should be set 
		std::cout << "\ncamera type: " << camera_type_string(camera_model) << std::endl;

		// non-coplanar, full optimization
		std::cout << "\nNon-coplanar calibration (full optimization)" << std::endl;

		int point_num = m_imagePoints.size();

		calibration_data cd;
		init_cd(&cd);
		// allocate memories for cd
		if(allocate_memory_cd(point_num, &cd) < 0)
		{
			isCalibrationSucceed = false;
			std::cout << "\nfail to allocate necessary memory." << std::endl;
			return;
		}
		// copy data into the cd data structure
		for(int i = 0; i < point_num; i++)
		{
			cd.xw[i] = m_objectPoints[i].x;
			cd.yw[i] = m_objectPoints[i].y;
			cd.zw[i] = m_objectPoints[i].z;
			cd.Xf[i] = m_imagePoints[i].x;
			cd.Yf[i] = m_imagePoints[i].y;
		}

		// process calibration data
		double A[3*3], K[3*4], distortion;

		// non-coplanar calibration with full optimization
		nccal_fo(&m_cp, &cd, A, K, &distortion, &m_cc);
		print_cp_cc_data(&m_cp, &m_cc);

		double mean, stddev, max, sse;
		distorted_image_plane_error_stats(&mean, &stddev, &max, &sse, &cd);
		printf("\nthe statistics for tsai method is:\n");
		printf("mean - %f, stddev - %f, max - %f, sse - %f\n", mean, stddev, max, sse);

		// use the calibration result as initial estimation of the Heikkilae calibration model 
		m_cp_h = m_cp;

		m_cc_h.fx = m_cc.f;	  m_cc_h.fy = m_cc.f;
		m_cc_h.Tx = m_cc.Tx;  m_cc_h.Ty = m_cc.Ty;	m_cc_h.Tz = m_cc.Tz;
		m_cc_h.Rx = m_cc.Rx;  m_cc_h.Ry = m_cc.Ry;  m_cc_h.Rz = m_cc.Rz;
		m_cc_h.k1 = 0;
		m_cc_h.k2 = 0;
		m_cc_h.p1 = 0;
		m_cc_h.p2 = 0;

		double A_h[3*3], K_h[3*4], distortion_h[4];

		nccal_fo_heik(&m_cp_h, &m_cc_h, &cd, A_h, K_h, distortion_h);
		print_cp_cc_data_heik(&m_cp_h, &m_cc_h);

		double mean_h, stddev_h, max_h, sse_h;
		distorted_image_plane_error_stats_heik(&mean_h, &stddev_h, &max_h, &sse_h, &cd);
		printf("\nthe statistics for heikkila model is:\n");
		printf("mean - %f, stddev - %f, max - %f, sse - %f\n", mean_h, stddev_h, max_h, sse_h);
		
		//// OpenCV implementation for optimization refining
		//{
		//	int nimages = m_fpCounts.size();
		//	std::vector< std::vector<cv::Point2f> > imagePoints;
		//	std::vector< std::vector<cv::Point3f> > objectPoints;

		//	int offset = 0;
		//	for(int i = 0; i < nimages; i++)
		//	{
		//		std::vector<cv::Point2f> img_point_buf;
		//		std::vector<cv::Point3f> obj_point_buf;

		//		for(int j = 0; j < m_fpCounts[i]; j++)
		//		{
		//			int tmp_ind = j+offset;
		//			img_point_buf.push_back(cv::Point2f(cd.Xf[tmp_ind], cd.Yf[tmp_ind]));
		//			obj_point_buf.push_back(cv::Point3f(cd.xw[tmp_ind], cd.yw[tmp_ind], cd.zw[tmp_ind]));
		//		}
		//		offset += m_fpCounts[i];

		//		imagePoints.push_back(img_point_buf);
		//		objectPoints.push_back(obj_point_buf);
		//    }

		//	int flags= 0;
		//	flags |= cv::CALIB_USE_INTRINSIC_GUESS;

		//	cv::Size imageSize(w, h);

		//	cv::Mat cameraMatrix = cv::Mat::eye(3, 3, CV_64F);
		//	cameraMatrix.at<double>(0,0) = m_cc.f; 
		//	cameraMatrix.at<double>(1,1) = m_cc.f;
		//	cameraMatrix.at<double>(0,2) = m_cp.Cx;
		//	cameraMatrix.at<double>(1,2) = m_cp.Cy;

		//	cv::Mat distCoeffs = cv::Mat::zeros(4, 1, CV_64F);

		//	std::vector<cv::Mat> rvecs, tvecs;
		//	std::vector<float> reprojErrs;
		//	double totalAvgErr = 0;

		//	double rms = cv::calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix, distCoeffs, rvecs, tvecs, flags);
		//	printf("\nRMS error reported by calibrateCamera: %g\n", rms);

		//	bool ok = cv::checkRange(cameraMatrix) && cv::checkRange(distCoeffs);
		//	double r_avg[3];
		//	double t_avg[3];
		//	double r_mat[9]; 
		//	cv::Mat matR = cv::Mat(3,3,CV_64F,r_mat);
		//	// compute reprojection errors for each view
		//	{
		//		std::vector<cv::Point2f> imagePoints2;
		//		int i, totalPoints = 0;
		//		double totalErr = 0, err;
		//			
		//		r_avg[0] = 0; r_avg[1] = 0; r_avg[2] = 0; 
		//		t_avg[0] = 0; t_avg[1] = 0; t_avg[2] = 0;
		//		for(i = 0; i < nimages; i++)
		//		{
		//			cv::projectPoints(cv::Mat(objectPoints[i]), rvecs[i], tvecs[i], cameraMatrix, distCoeffs, imagePoints2);
		//			err = norm(cv::Mat(imagePoints[i]), cv::Mat(imagePoints2), cv::NORM_L2);
		//			int n = (int)objectPoints[i].size();
		//			totalErr += err * err;
		//			totalPoints += n;

		//			// image output
		//			cv::Mat tmp_img_mat = QImageToCvMat(m_capturedImages[i]);
		//			for(int j = 0; j < objectPoints[i].size(); j++)
		//			{
		//				cv::circle(tmp_img_mat, imagePoints[i][j], 1, cv::Scalar(0, 255, 255),-1);  
		//				cv::circle(tmp_img_mat, imagePoints2[j], 1, cv::Scalar(0, 0, 255), -1); 
		//			}
		//			QString filename = QString("reproj%1_opencv.jpg").arg(i);
		//			cv::imwrite(filename.toStdString().c_str(),tmp_img_mat);
		//			// translation and rotation
		//			printf("\nimage %i:\n", i);
		//			printf("translation: %f, %f, %f\n", tvecs[i].at<double>(0), tvecs[i].at<double>(1), tvecs[i].at<double>(2));
		//			printf("rotation: %f, %f, %f\n", rvecs[i].at<double>(0), rvecs[i].at<double>(1), rvecs[i].at<double>(2));
		//			printf("\n");

		//			r_avg[0] += rvecs[i].at<double>(0);
		//			r_avg[1] += rvecs[i].at<double>(1);
		//			r_avg[2] += rvecs[i].at<double>(2);

		//			t_avg[0] += tvecs[i].at<double>(0);
		//			t_avg[1] += tvecs[i].at<double>(1);
		//			t_avg[2] += tvecs[i].at<double>(2);
		//		}

		//		for(int j = 0; j < 3; j++)
		//		{
		//			r_avg[j] /= (double)nimages;
		//			t_avg[j] /= (double)nimages;
		//		}
		//		printf("\naverage:\n", i);
		//		printf("translation: %f, %f, %f\n", t_avg[0], t_avg[1], t_avg[2]);
		//		printf("rotation: %f, %f, %f\n", r_avg[0], r_avg[1], r_avg[2]);
		//		printf("\n");

		//		totalAvgErr = std::sqrt(totalErr / totalPoints);
		//	}
		//	printf("%s. avg reprojection error = %.2f\n", ok ? "Calibration succeed" : "Calibration failed", totalAvgErr);

		//	// compute reprojection errors using the average tvec and rvec
		//	{
		//		std::vector<cv::Point2f> imagePoints2;
		//		int i, totalPoints = 0;
		//		double totalErr = 0, err;

		//		cv::Mat _r_avg = cv::Mat(3, 1, CV_64F, r_avg);
		//		cv::Mat _t_avg = cv::Mat(3, 1, CV_64F, t_avg);
		//		
		//		for(i = 0; i < nimages; i++)
		//		{
		//			cv::projectPoints(cv::Mat(objectPoints[i]), _r_avg, _t_avg, cameraMatrix, distCoeffs, imagePoints2);
		//			err = norm(cv::Mat(imagePoints[i]), cv::Mat(imagePoints2), cv::NORM_L2);
		//			int n = (int)objectPoints[i].size();
		//			totalErr += err * err;
		//			totalPoints += n;					
		//		}

		//		totalAvgErr = std::sqrt(totalErr / totalPoints);
		//	}
		//	printf("\nusing the average tvec and rvec\n");
		//	printf("%s. avg reprojection error = %.2f\n", ok ? "Calibration succeed" : "Calibration failed", totalAvgErr);

		//	cv::Mat _r_avg = cv::Mat(3, 1, CV_64F, r_avg);
		//	cv::Rodrigues(_r_avg, matR);

		//	m_cc_cv.fx = cameraMatrix.at<double>(0,0);
		//	m_cc_cv.fy = cameraMatrix.at<double>(1,1);
		//	m_cc_cv.k1 = distCoeffs.at<double>(0);
		//	m_cc_cv.k2 = distCoeffs.at<double>(1);
		//	m_cc_cv.p1 = distCoeffs.at<double>(2);
		//	m_cc_cv.p2 = distCoeffs.at<double>(3);
		//	m_cc_cv.Rx = r_avg[0];
		//	m_cc_cv.Ry = r_avg[1];
		//	m_cc_cv.Rz = r_avg[2];
		//	m_cc_cv.Tx = t_avg[0];
		//	m_cc_cv.Ty = t_avg[1];
		//	m_cc_cv.Tz = t_avg[2];

		//	m_cc_cv.r1 = r_mat[0];  m_cc_cv.r2 = r_mat[1];  m_cc_cv.r3 = r_mat[2]; 
		//	m_cc_cv.r4 = r_mat[3];  m_cc_cv.r5 = r_mat[4];  m_cc_cv.r6 = r_mat[5]; 
		//	m_cc_cv.r7 = r_mat[6];  m_cc_cv.r8 = r_mat[7];  m_cc_cv.r9 = r_mat[8]; 

		//	m_cp_cv.Cx = cameraMatrix.at<double>(0,2);
		//	m_cp_cv.Cy = cameraMatrix.at<double>(1,2);

		//	double mean_cv, stddev_cv, max_cv, sse_cv;
		//	distorted_image_plane_error_stats_opencv(&mean_cv, &stddev_cv, &max_cv, &sse_cv, &cd);
		//	printf("\nthe statistics for opencv implementation is:\n");
		//	printf("mean - %f, stddev - %f, max - %f, sse - %f\n", mean_cv, stddev_cv, max_cv, sse_cv);
		//}
		//// end of OpenCV implementation for optimization refining

		// free memory for cd
		free_memory_cd(&cd);
	}

	// here is a simplified version to determine whether calibration is successful or not
	isCalibrationSucceed = true;
}

void KneeMotionObserver::undistorted_to_distorted_sensor_coord(double Xu, double Yu, double *Xd, double *Yd)
{
	double Ru, Rd, lambda, c, d, Q, R, D, S, T, sinT, cosT;

	if (((Xu == 0) && (Yu == 0)) || (m_cc.kappa1 == 0)) {
		*Xd = Xu;
		*Yd = Yu;
		return;
	}

	Ru = hypot(Xu, Yu);	/* SQRT(Xu*Xu+Yu*Yu) */

	c = 1 / m_cc.kappa1;
	d = -c * Ru;

	Q = c / 3;
	R = -d / 2;
	D = CUB (Q) + SQR (R);

	if (D >= 0) 
	{	
		/* one real root */
		D = SQRT (D);
		S = CBRT (R + D);
		T = CBRT (R - D);
		Rd = S + T;

		if (Rd < 0) 
		{
			Rd = SQRT (-1 / (3 * m_cc.kappa1));
			fprintf (stderr, "\nWarning: undistorted image point to distorted image point mapping limited by\n");
			fprintf (stderr, "         maximum barrel distortion radius of %lf\n", Rd);
			fprintf (stderr, "         (Xu = %lf, Yu = %lf) -> (Xd = %lf, Yd = %lf)\n\n", Xu, Yu, Xu * Rd / Ru, Yu * Rd / Ru);
		}
	} 
	else 
	{			
		/* three real roots */
		D = SQRT (-D);
		S = CBRT (hypot (R, D));
		T = atan2 (D, R) / 3;
		SINCOS (T, sinT, cosT);

		/* the larger positive root is    2*S*cos(T)                   */
		/* the smaller positive root is   -S*cos(T) + SQRT(3)*S*sin(T) */
		/* the negative root is           -S*cos(T) - SQRT(3)*S*sin(T) */

		Rd = -S * cosT + SQRT3 * S * sinT;	/* use the smaller positive root */
	}

	lambda = Rd / Ru;

	*Xd = Xu * lambda;
	*Yd = Yu * lambda;
}

void KneeMotionObserver::world_coord_to_image_coord(double xw, double yw, double zw, double *Xf, double *Yf)
{
	double xc, yc, zc, Xu, Yu, Xd, Yd;

    /* convert from world coordinates to camera coordinates */
    xc = m_cc.r1 * xw + m_cc.r2 * yw + m_cc.r3 * zw + m_cc.Tx;
    yc = m_cc.r4 * xw + m_cc.r5 * yw + m_cc.r6 * zw + m_cc.Ty;
    zc = m_cc.r7 * xw + m_cc.r8 * yw + m_cc.r9 * zw + m_cc.Tz;

    /* convert from camera coordinates to undistorted sensor plane coordinates */
    Xu = m_cc.f * xc / zc;
    Yu = m_cc.f * yc / zc;

    /* convert from undistorted to distorted sensor plane coordinates */
    undistorted_to_distorted_sensor_coord(Xu, Yu, &Xd, &Yd);

    /* convert from distorted sensor plane coordinates to image coordinates */
    *Xf = Xd * m_cp.sx / m_cp.dpx + m_cp.Cx;
    *Yf = Yd / m_cp.dpy + m_cp.Cy;
}

void KneeMotionObserver::distorted_image_plane_error_stats(double* mean, double* stddev, double* max, double* sse,
														   calibration_data* cd)
{
	int i;

	double Xf, Yf,
		error, squared_error,
		max_error = 0,
		sum_error = 0,
		sum_squared_error = 0;

	if (cd->point_count < 1) {
		*mean = *stddev = *max = *sse = 0;
		return;
	}

	//// for accuracy test
	//std::vector<cv::Point2f> original_points;
	//std::vector<cv::Point2f> projected_points;
	
	for (i = 0; i < cd->point_count; i++) {
		/* calculate the ideal location of the image of the data point */
		world_coord_to_image_coord(cd->xw[i], cd->yw[i], cd->zw[i], &Xf, &Yf);

		/* determine the error between the ideal and actual location of the data point	 */
		/* (in distorted image coordinates)						 */
		squared_error = SQR (Xf - cd->Xf[i]) + SQR (Yf - cd->Yf[i]);
		error = sqrt (squared_error);
		sum_error += error;
		sum_squared_error += squared_error;
		max_error = MAX (max_error, error);

		//// for accuracy test
		//original_points.push_back(cv::Point2f(cd->Xf[i], cd->Yf[i]));
		//projected_points.push_back(cv::Point2f(Xf, Yf));
	}

	*mean = sum_error / cd->point_count;
	*max = max_error;
	*sse = sum_squared_error;

	if (cd->point_count == 1)
		*stddev = 0;
	else
		*stddev = sqrt ((sum_squared_error - SQR (sum_error) / cd->point_count) / (cd->point_count - 1));

	//// show the results
	//int offset = 0;
	//for(size_t i = 0; i < m_capturedImages.size(); i++)
	//{
	//	cv::Mat tmp_img_mat = QImageToCvMat(m_capturedImages[i]);
	//	for(int j = 0; j < m_fpCounts[i]; j++)
	//	{
	//		cv::circle(tmp_img_mat, original_points[j+offset], 1, cv::Scalar(0, 255, 255),-1);  
	//		cv::circle(tmp_img_mat, projected_points[j+offset], 1, cv::Scalar(0, 0, 255), -1); 
	//	}
	//	offset += m_fpCounts[i];
	//	//QString filename = QString("img%1.jpg").arg(i);
	//	//cv::imwrite(filename.toStdString().c_str(),tmp_img_mat);
	//}
}
	
void KneeMotionObserver::world_coord_to_image_coord_heik(double xw, double yw, double zw, double *Xf, double *Yf)
{
	double xc, yc, zc,
           Xu, Yu,
           xd, yd;

    /* convert from world coordinates to camera coordinates */
    xc = m_cc_h.r1 * xw + m_cc_h.r2 * yw + m_cc_h.r3 * zw + m_cc_h.Tx;
    yc = m_cc_h.r4 * xw + m_cc_h.r5 * yw + m_cc_h.r6 * zw + m_cc_h.Ty;
    zc = m_cc_h.r7 * xw + m_cc_h.r8 * yw + m_cc_h.r9 * zw + m_cc_h.Tz;

    /* convert from camera coordinates to undistorted sensor plane coordinates */
    zc = zc ? 1.0/zc : 1;
	Xu = xc * zc;
    Yu = yc * zc;

    /* convert from undistorted to distorted sensor plane coordinates */
    double rad_2, rad_4, rad_6, a1, a2, a3, cdist, icdist2;

    rad_2 = Xu * Xu + Yu * Yu;
    rad_4 = rad_2 * rad_2;
    rad_6 = rad_4 * rad_2;
	a1 = 2.0 * Xu * Yu;
    a2 = rad_2 + 2.0 * Xu * Xu;
    a3 = rad_2 + 2.0 * Yu * Yu;
    cdist = 1 + m_cc_h.k1 * rad_2 + m_cc_h.k2 * rad_4;
	icdist2 = 1.0;

	///* convert from distorted sensor coordinates to undistorted sensor plane coordinates */		
	xd = Xu * cdist * icdist2 + m_cc_h.p1 * a1 + m_cc_h.p2 * a2;
	yd = Yu * cdist * icdist2 + m_cc_h.p1 * a3 + m_cc_h.p2 * a1;

    /* convert from distorted sensor plane coordinates to image coordinates */
	*Xf = xd * m_cc_h.fx + m_cp_h.Cx;
	//*Xf = xd * m_cc_h.fx * m_cp_h.sx + m_cp_h.Cx;
    *Yf = yd * m_cc_h.fy + m_cp_h.Cy;
}
	
void KneeMotionObserver::distorted_image_plane_error_stats_heik(double* mean, double* stddev, double* max, double* sse, calibration_data* cd)
{
	int i;

	double Xf, Yf,
		error, squared_error,
		max_error = 0,
		sum_error = 0,
		sum_squared_error = 0;

	if (cd->point_count < 1) {
		*mean = *stddev = *max = *sse = 0;
		return;
	}

	//// for accuracy test
	//std::vector<cv::Point2f> original_points;
	//std::vector<cv::Point2f> projected_points;

	for (i = 0; i < cd->point_count; i++) {
		/* calculate the ideal location of the image of the data point */
		world_coord_to_image_coord_heik(cd->xw[i], cd->yw[i], cd->zw[i], &Xf, &Yf);

		/* determine the error between the ideal and actual location of the data point	 */
		/* (in distorted image coordinates)						 */
		squared_error = SQR (Xf - cd->Xf[i]) + SQR (Yf - cd->Yf[i]);
		error = sqrt (squared_error);
		sum_error += error;
		sum_squared_error += squared_error;
		max_error = MAX (max_error, error);

		//// for accuracy test
		//original_points.push_back(cv::Point2f(cd->Xf[i], cd->Yf[i]));
		//projected_points.push_back(cv::Point2f(Xf, Yf));
	}

	*mean = sum_error / cd->point_count;
	*max = max_error;
	*sse = sum_squared_error;

	if (cd->point_count == 1)
		*stddev = 0;
	else
		*stddev = sqrt ((sum_squared_error - SQR (sum_error) / cd->point_count) / (cd->point_count - 1));

	//// show the results
	//int offset = 0;
	//for(size_t i = 0; i < m_capturedImages.size(); i++)
	//{
	//	cv::Mat tmp_img_mat = QImageToCvMat(m_capturedImages[i]);
	//	for(int j = 0; j < m_fpCounts[i]; j++)
	//	{
	//		cv::circle(tmp_img_mat, original_points[j+offset], 1, cv::Scalar(0, 255, 255),-1);  
	//		cv::circle(tmp_img_mat, projected_points[j+offset], 1, cv::Scalar(0, 0, 255), -1); 
	//	}
	//	offset += m_fpCounts[i];
	//	QString filename = QString("reproj%1_heik.jpg").arg(i);
	//	cv::imwrite(filename.toStdString().c_str(),tmp_img_mat);
	//}
}

void KneeMotionObserver::world_coord_to_image_coord_opencv(double xw, double yw, double zw, double *Xf, double *Yf)
{
	double xc, yc, zc,
           Xu, Yu,
           xd, yd;

    /* convert from world coordinates to camera coordinates */
    xc = m_cc_cv.r1 * xw + m_cc_cv.r2 * yw + m_cc_cv.r3 * zw + m_cc_cv.Tx;
    yc = m_cc_cv.r4 * xw + m_cc_cv.r5 * yw + m_cc_cv.r6 * zw + m_cc_cv.Ty;
    zc = m_cc_cv.r7 * xw + m_cc_cv.r8 * yw + m_cc_cv.r9 * zw + m_cc_cv.Tz;

    /* convert from camera coordinates to undistorted sensor plane coordinates */
    zc = zc ? 1.0/zc : 1;
	Xu = xc * zc;
    Yu = yc * zc;

    /* convert from undistorted to distorted sensor plane coordinates */
    double rad_2, rad_4, rad_6, a1, a2, a3, cdist, icdist2;

    rad_2 = Xu * Xu + Yu * Yu;
    rad_4 = rad_2 * rad_2;
    rad_6 = rad_4 * rad_2;
	a1 = 2.0 * Xu * Yu;
    a2 = rad_2 + 2.0 * Xu * Xu;
    a3 = rad_2 + 2.0 * Yu * Yu;
    cdist = 1 + m_cc_cv.k1 * rad_2 + m_cc_cv.k2 * rad_4;
	icdist2 = 1.0;

	///* convert from distorted sensor coordinates to undistorted sensor plane coordinates */		
	xd = Xu * cdist * icdist2 + m_cc_cv.p1 * a1 + m_cc_cv.p2 * a2;
	yd = Yu * cdist * icdist2 + m_cc_cv.p1 * a3 + m_cc_cv.p2 * a1;

    /* convert from distorted sensor plane coordinates to image coordinates */
	*Xf = xd * m_cc_cv.fx + m_cp_cv.Cx;
    *Yf = yd * m_cc_cv.fy + m_cp_cv.Cy;
}
	
void KneeMotionObserver::distorted_image_plane_error_stats_opencv(double* mean, double* stddev, double* max, double* sse, calibration_data* cd)
{
	int i;

	double Xf, Yf,
		error, squared_error,
		max_error = 0,
		sum_error = 0,
		sum_squared_error = 0;

	if (cd->point_count < 1) {
		*mean = *stddev = *max = *sse = 0;
		return;
	}

	for (i = 0; i < cd->point_count; i++) {
		/* calculate the ideal location of the image of the data point */
		world_coord_to_image_coord_opencv(cd->xw[i], cd->yw[i], cd->zw[i], &Xf, &Yf);

		/* determine the error between the ideal and actual location of the data point	 */
		/* (in distorted image coordinates)						 */
		squared_error = SQR (Xf - cd->Xf[i]) + SQR (Yf - cd->Yf[i]);
		error = sqrt (squared_error);
		sum_error += error;
		sum_squared_error += squared_error;
		max_error = MAX (max_error, error);
	}

	*mean = sum_error / cd->point_count;
	*max = max_error;
	*sse = sum_squared_error;

	if (cd->point_count == 1)
		*stddev = 0;
	else
		*stddev = sqrt ((sum_squared_error - SQR (sum_error) / cd->point_count) / (cd->point_count - 1));
}

void KneeMotionObserver::updateTestPointList()
{
	if(!isCalibrationSucceed)
		return;

	if(!m_isTestPointInitialized)
		return;
	
	if(!(handleData[1].valid && handleData[2].valid))
	{
		emit testPointListInvalid();
		return;
	}

	// handle 1 - endo tool
	double endoTip[3];
	if(useEndoOffset)
	{
		for(int i = 0; i < 3; i++)
		{
			endoTip[i] = 0.0;
			for(int j = 0; j < 4; j++)
			{
				endoTip[i] += handleData[1].m[i * 4 + j] * endoOffset[j]; 
			}
		}		
	}
	else
	{
		endoTip[0] = handleData[1].m[3];
		endoTip[1] = handleData[1].m[7];
		endoTip[2] = handleData[1].m[11];
	}

	// get the transform 
	double t_endo[16], t_endo_inv[16];
	double t_cali[16];
	double t_cali_to_endo[16];		

	for(int i = 0; i < 16; i++)
	{
		t_endo[i] = handleData[1].m[i];
		t_cali[i] = handleData[2].m[i];
	}

	inverseTransform(t_endo, t_endo_inv);
	multiplyTransform(t_endo_inv, t_cali, t_cali_to_endo);

	double p_cali[3], p_endo[3];

	//m_mutex.lock();
	{
		m_testPointList2d.clear();
		for(int i = 0; i < m_testPointNum; i++)
		{
			double tmp_p_local[3];
			tmp_p_local[0] =  m_testPointList3d[i][0];
			tmp_p_local[1] =  m_testPointList3d[i][1];
			tmp_p_local[2] =  m_testPointList3d[i][2];

			pointTransform(tmp_p_local,pointArrayLocalTransform, p_cali);
			pointTransform(p_cali, t_cali_to_endo, p_endo);

			std::array<double,3> tmp_2d;
			//world_coord_to_image_coord(p_endo[0], p_endo[1], p_endo[2], &tmp_2d[0], &tmp_2d[1]);
			world_coord_to_image_coord_heik(p_endo[0], p_endo[1], p_endo[2], &tmp_2d[0], &tmp_2d[1]);

			// calculate other information
			double tmp_dist = sqrt((p_endo[0]-endoOffset[0])*(p_endo[0]-endoOffset[0])
				                 + (p_endo[1]-endoOffset[1])*(p_endo[1]-endoOffset[1])
								 + (p_endo[2]-endoOffset[2])*(p_endo[2]-endoOffset[2]));
			tmp_2d[2] = (tmp_dist > m_safe_dist) ? tmp_dist : -tmp_dist; 

			m_testPointList2d.push_back(tmp_2d);
		}
	}
	//m_mutex.unlock();

	emit testPointListUpdated();
}

//void KneeMotionObserver::updateKneeMotionInfo()
//{
//	if(referenceCOSStatus)
//	{
//		for(int i = 1; i < TOOLNUM; i++)
//		{
//			if(toolsStatus[i])
//			{
//				if(relativeTransformStatus[i-1])
//				{
//					double temp_m[16];
//					multiplyTransform(handleData[i].m, relativeTransform[i-1], temp_m);
//					multiplyTransform(referenceCOS_i, temp_m, kneeMotions[i-1]);
//					kneeMotionsStatus[i-1] = true;					
//				}
//				else
//				{
//					std::cout << QString("offset transform %1 is invalid ").arg(i).toStdString() << std::endl;
//					kneeMotionsStatus[i-1] = false;
//				}
//			}
//			else
//			{
//				std::cout << QString("tool %1 is invalid").arg(i).toStdString() << std::endl;
//				kneeMotionsStatus[i-1] = false;
//			}
//		}
//	}
//	else
//	{
//		std::cout << "no reference COS" << std::endl;
//	}
//}

//void KneeMotionObserver::updateKneeMotionInfoDisplay()
//{
//	for(int i = 0; i < TOOLNUM-1; i++)
//	{
//		if(kneeMotionsStatus[i])
//		{
//			double roll, pitch, yaw;
//			double cosRoll, sinRoll;
//			roll = atan2(kneeMotions[i][4], kneeMotions[i][0]);
//			cosRoll = cos(roll);
//			sinRoll = sin(roll);
//			pitch = atan2(-kneeMotions[i][8],
//				cosRoll * kneeMotions[i][0] + sinRoll * kneeMotions[i][4]);
//			yaw = atan2(sinRoll * kneeMotions[i][2] - cosRoll * kneeMotions[i][6],
//				cosRoll * kneeMotions[i][5] - sinRoll * kneeMotions[i][1]);
//
//			kneeMotionInfoTable->item(i,0)->setText(QString("%1").arg(roll));
//			kneeMotionInfoTable->item(i,1)->setText(QString("%1").arg(pitch));
//			kneeMotionInfoTable->item(i,2)->setText(QString("%1").arg(yaw));
//
//			double tx, ty, tz;
//			tx = kneeMotions[i][3];
//			ty = kneeMotions[i][7];
//			tz = kneeMotions[i][11];
//
//			kneeMotionInfoTable->item(i,3)->setText(QString("%1").arg(tx));
//			kneeMotionInfoTable->item(i,4)->setText(QString("%1").arg(ty));
//			kneeMotionInfoTable->item(i,5)->setText(QString("%1").arg(tz));
//
//			// data output
//			if(kneeMotionDataReady[i])
//			{
//				kneeMotionData[i] << "frame: " << handleData[i+1].frame << endl;
//				kneeMotionData[i] << "transform: " << endl;
//				for(int j = 0; j < 16; j++)
//				{
//					kneeMotionData[i] << kneeMotions[i][j] << ' '; 
//				}
//				kneeMotionData[i] << endl;
//				kneeMotionData[i] << "euler angle: " << roll << ' ' << pitch << ' ' << yaw << endl;
//				kneeMotionData[i] << "translation: " << tx << ' ' << ty << ' ' << tz << endl;
//			}
//		}
//		else
//		{
//			for(int j = 0; j < 6; j++)
//			{
//				kneeMotionInfoTable->item(i,j)->setText(QString("--"));			
//			}
//		}
//	}
//    // relative motion 1
//	if(kneeMotionsStatus[0] && kneeMotionsStatus[2])
//	{
//		double m0_inv[16];
//		double relative_motion[16];
//		inverseTransform(kneeMotions[0], m0_inv);
//		multiplyTransform(m0_inv, kneeMotions[2], relative_motion);
//
//		double roll, pitch, yaw;
//		double cosRoll, sinRoll;
//		roll = atan2(relative_motion[4], relative_motion[0]);
//		cosRoll = cos(roll);
//		sinRoll = sin(roll);
//		pitch = atan2(-relative_motion[8],
//			cosRoll * relative_motion[0] + sinRoll * relative_motion[4]);
//		yaw = atan2(sinRoll * relative_motion[2] - cosRoll * relative_motion[6],
//			cosRoll * relative_motion[5] - sinRoll * relative_motion[1]);
//
//		kneeRelativeMotionInfoTable->item(0,0)->setText(QString("%1").arg(roll));
//		kneeRelativeMotionInfoTable->item(0,1)->setText(QString("%1").arg(pitch));
//		kneeRelativeMotionInfoTable->item(0,2)->setText(QString("%1").arg(yaw));
//
//		double tx, ty, tz;
//		tx = relative_motion[3];
//		ty = relative_motion[7];
//		tz = relative_motion[11];
//
//		kneeRelativeMotionInfoTable->item(0,3)->setText(QString("%1").arg(tx));
//		kneeRelativeMotionInfoTable->item(0,4)->setText(QString("%1").arg(ty));
//		kneeRelativeMotionInfoTable->item(0,5)->setText(QString("%1").arg(tz));
//
//		// data output
//		if(kneeRelativeMotionDataReady)
//		{
//			kneeRelativeMotionData << "frame: " << handleData[3].frame << endl;
//			kneeRelativeMotionData << "transform: " << endl;
//			for(int j = 0; j < 16; j++)
//			{
//				kneeRelativeMotionData << relative_motion[j] << ' '; 
//			}
//			kneeRelativeMotionData << endl;
//			kneeRelativeMotionData << "euler angle: " << roll << ' ' << pitch << ' ' << yaw << endl;
//			kneeRelativeMotionData << "translation: " << tx << ' ' << ty << ' ' << tz << endl;
//		}
//	}
//	else
//	{
//		for(int j = 0; j < 6; j++)
//		{
//			kneeRelativeMotionInfoTable->item(0,j)->setText(QString("--"));			
//		}
//	}
//    // relative motion 2
//	if(kneeMotionsStatus[1] && kneeMotionsStatus[2])
//	{
//		double m2_inv[16];
//		double relative_motion[16];
//		inverseTransform(kneeMotions[2], m2_inv);
//		multiplyTransform(m2_inv, kneeMotions[1], relative_motion);
//
//		double roll, pitch, yaw;
//		double cosRoll, sinRoll;
//		roll = atan2(relative_motion[4], relative_motion[0]);
//		cosRoll = cos(roll);
//		sinRoll = sin(roll);
//		pitch = atan2(-relative_motion[8],
//			cosRoll * relative_motion[0] + sinRoll * relative_motion[4]);
//		yaw = atan2(sinRoll * relative_motion[2] - cosRoll * relative_motion[6],
//			cosRoll * relative_motion[5] - sinRoll * relative_motion[1]);
//
//		kneeRelativeMotionInfoTable->item(1,0)->setText(QString("%1").arg(roll));
//		kneeRelativeMotionInfoTable->item(1,1)->setText(QString("%1").arg(pitch));
//		kneeRelativeMotionInfoTable->item(1,2)->setText(QString("%1").arg(yaw));
//
//		double tx, ty, tz;
//		tx = relative_motion[3];
//		ty = relative_motion[7];
//		tz = relative_motion[11];
//
//		kneeRelativeMotionInfoTable->item(1,3)->setText(QString("%1").arg(tx));
//		kneeRelativeMotionInfoTable->item(1,4)->setText(QString("%1").arg(ty));
//		kneeRelativeMotionInfoTable->item(1,5)->setText(QString("%1").arg(tz));
//
//		// data output
//		if(kneeRelativeMotionDataReady2)
//		{
//			kneeRelativeMotionData2 << "frame: " << handleData[2].frame << endl;
//			kneeRelativeMotionData2 << "transform: " << endl;
//			for(int j = 0; j < 16; j++)
//			{
//				kneeRelativeMotionData2 << relative_motion[j] << ' '; 
//			}
//			kneeRelativeMotionData2 << endl;
//			kneeRelativeMotionData2 << "euler angle: " << roll << ' ' << pitch << ' ' << yaw << endl;
//			kneeRelativeMotionData2 << "translation: " << tx << ' ' << ty << ' ' << tz << endl;
//		}
//	}
//	else
//	{
//		for(int j = 0; j < 6; j++)
//		{
//			kneeRelativeMotionInfoTable->item(1,j)->setText(QString("--"));			
//		}
//	}
//}

//// for data output
//void KneeMotionObserver::createCOSInfoStream()
//{
//	cosInfoReady = false;
//
//	QDateTime t = QDateTime::currentDateTime();
//	QString infoFileName = QString("cos_info_%1%2%3%4%5%6%7.txt").
//		arg(t.date().year()).arg(t.date().month()).arg(t.date().day()).
//		arg(t.time().hour()).arg(t.time().minute()).arg(t.time().second()).arg(t.time().msec());
//
//	QFile *cosInfoFile = new QFile(infoFileName);
//	if(!cosInfoFile->open(QIODevice::WriteOnly))
//	{
//		std::cout << "cannot open file" << std::endl;
//		return;
//	}
//	cosInfo.setDevice(cosInfoFile);
//	cosInfoReady = true;
//}
//
//void KneeMotionObserver::createKneeMotionDataStream()
//{
//	for(int i = 0; i < TOOLNUM-1; i++)
//	{
//		kneeMotionDataReady[i] = false;
//	}
//	kneeRelativeMotionDataReady = false;
//
//	QDateTime t = QDateTime::currentDateTime();
//	QString tString = QString("%1%2%3%4%5%6%7").
//		arg(t.date().year()).arg(t.date().month()).arg(t.date().day()).
//		arg(t.time().hour()).arg(t.time().minute()).arg(t.time().second()).arg(t.time().msec());
//	for(int i = 0; i < TOOLNUM-1; i++)
//	{
//		QString kneeMotionDataFileName = QString("knee_motion_data_%1_%2.txt").arg(i).arg(tString);			
//		QFile *kneeMotionDataFile = new QFile(kneeMotionDataFileName);
//		if(!kneeMotionDataFile->open(QIODevice::WriteOnly))
//		{
//			std::cout << "cannot open file" << std::endl;
//			return;
//		}
//		kneeMotionData[i].setDevice(kneeMotionDataFile);
//		kneeMotionDataReady[i] = true;
//	}
//	// relative motion 1 - femur to the tibia
//	QString kneeRelativeMotionDataFileName = QString("knee_relative_motion_data_%1.txt").arg(tString);
//	QFile *kneeRelativeMotionDataFile = new QFile(kneeRelativeMotionDataFileName);
//	if(!kneeRelativeMotionDataFile->open(QIODevice::WriteOnly))
//	{
//		std::cout << "cannot open file" << std::endl;
//		return;
//	}
//	kneeRelativeMotionData.setDevice(kneeRelativeMotionDataFile);
//	kneeRelativeMotionDataReady = true;
//	// relative motion 2 - patella to the femur
//	QString kneeRelativeMotionDataFileName2 = QString("knee_relative_motion_data_2_%1.txt").arg(tString);
//	QFile *kneeRelativeMotionDataFile2 = new QFile(kneeRelativeMotionDataFileName2);
//	if(!kneeRelativeMotionDataFile2->open(QIODevice::WriteOnly))
//	{
//		std::cout << "cannot open file" << std::endl;
//		return;
//	}
//	kneeRelativeMotionData2.setDevice(kneeRelativeMotionDataFile2);
//	kneeRelativeMotionDataReady2 = true;
//
//}

// auxiliary math function
double KneeMotionObserver::normalOfVector(double a[3])
{
	return sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
}

double KneeMotionObserver::dotProductOfVectors(double a[3], double b[3])
{
	return (a[0]*b[0] + a[1]*b[1] + a[2]*b[2]);
}

void KneeMotionObserver::crossProductOfVectors(double a[3], double b[3], double r[3])
{
	// r - the result of the cross product
	r[0] = a[1]*b[2] - a[2]*b[1];
	r[1] = a[2]*b[0] - a[0]*b[2];
	r[2] = a[0]*b[1] - a[1]*b[0];	
}

void KneeMotionObserver::inverseTransform(double a[16], double b[16])
{
	double p[3]; p[0] = a[3]; p[1] = a[7]; p[2] = a[11];
    double nv[3]; nv[0] = a[0]; nv[1] = a[4]; nv[2] = a[8];
	double ov[3]; ov[0] = a[1]; ov[1] = a[5]; ov[2] = a[9];
	double av[3]; av[0] = a[2]; av[1] = a[6]; av[2] = a[10];
	
	for(int i = 0; i < 3; i++)
	{
		for(int j = 0; j < 3; j++)
		{
			b[i * 4 + j] = a[j * 4 + i];
		}
	}
	b[12] = 0.0; b[13] = 0.0; b[14] = 0.0; b[15] = 1.0;
	b[3] = -dotProductOfVectors(p, nv);
	b[7] = -dotProductOfVectors(p, ov);
	b[11] = -dotProductOfVectors(p, av);	
}

void KneeMotionObserver::multiplyTransform(double a[16], double b[16], double c[16])
{
	for(int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 4; j++)
		{
			c[i * 4 + j] = a[i * 4] * b[j]
			               + a[i * 4 + 1] * b[4 + j]    // 4 = 1 * 4
						   + a[i * 4 + 2] * b[8 + j]    // 8 = 2 * 4
						   + a[i * 4 + 3] * b[12 + j];  // 12 = 3 * 4
		}
	}
}

// b = m * a;  m - matrix; a - src vector; b - des vector
void KneeMotionObserver::pointTransform(double a[3], double m[16], double b[3])
{
	for(int i = 0; i < 3; i++)
	{
		b[i] = m[i * 4 + 0] * a[0] + m[i * 4 + 1] * a[1] + m[i * 4 + 2] * a[2] + m[i * 4 + 3];
	}

}
