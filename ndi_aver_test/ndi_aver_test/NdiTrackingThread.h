#ifndef NDITRACKINGTHREAD_H
#define NDITRACKINGTHREAD_H

#include <QThread>
//#include "../ndi/include/ndicapi.h"
#include "ndicapi/ndicapi.h"
#include <QMutex>

// #define NDI_NTOOLS 8  
#define NDI_NTOOLS 12 // for polaris - 3 active; 9 passive

typedef struct SensorDataStruct
{
	double m[16];
	double x,y,z;
	double rx,ry,rz;
	int flag;

	// added by banbanfish
	double q0,qx,qy,qz;
	unsigned long frame;
	float error;
	int transformStatus;


}SensorData;

class NdiTrackingThread : public QThread
{
	Q_OBJECT

public:
	NdiTrackingThread();
	~NdiTrackingThread();
	void setTrackingStatus(bool status);
	void refreshFrame();
	void setNdiSystem(ndicapi *s,int* h);
	void transposeMatrix(double in[16],double out[16]);
	SensorData getToolTransform(int tool);

protected:
	void run();
	void clearLastReserve();

	// added by banbanfish
	bool bExtractValue(char *pszVal, unsigned uLen, float fDivisor, float *pfValue);

private:
	ndicapi *m_pSystem;
	int m_handle[NDI_NTOOLS];
	QMutex m_mutex;
	bool m_trackingThreadWorking;
	SensorData record[NDI_NTOOLS];

		
};

#endif // NDITRACKINGTHREAD_H
